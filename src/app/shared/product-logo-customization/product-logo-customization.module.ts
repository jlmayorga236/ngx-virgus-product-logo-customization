import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DndModule } from 'ngx-drag-drop';
import { NgxMoveableModule} from 'ngx-moveable';
import { ClickOutsideModule } from 'ng-click-outside';

import { ProductLogoCustomizationComponent } from './product-logo-customization.component';
import { PlcPreviewComponent } from './components/plc-preview/plc-preview.component';
import { PlcPictureCarouselComponent } from './components/plc-picture-carousel/plc-picture-carousel.component';
import { PlcPictureAddComponent } from './components/plc-picture-add/plc-picture-add.component';
import { PlcPictureThumbComponent } from './components/plc-picture-thumb/plc-picture-thumb.component';
import { PlcPositionsListComponent } from './components/plc-positions-list/plc-positions-list.component';
import { PlcPositionItemComponent } from './components/plc-position-item/plc-position-item.component';

import { SettingsService } from './services/settings/settings.service';
import { PicturesService } from './services/common/pictures/pictures.service';
import { PositionsService } from './services/common/positions/positions.service';
import { LayoutsService } from './services/common/layouts/layouts.service';
import { LoadingService } from './services/common/loading/loading.service';
import { SnapshotService } from './services/common/snapshot/snapshot.service';
import { FabricHandlerService } from './services/libs-handlers-services/fabric-js/fabric-handler.service';
import { MoveableHandlerService } from './services/libs-handlers-services/moveable-js/moveable-handler.service';
import { PlcPreviewLoaderComponent } from './components/plc-preview-loader/plc-preview-loader.component';

const COMPONENTS = [ProductLogoCustomizationComponent, PlcPreviewComponent, PlcPictureCarouselComponent,
                    PlcPictureAddComponent, PlcPictureThumbComponent,
                    PlcPositionsListComponent, PlcPositionItemComponent];

const SERVICES = [
  SettingsService,
  PositionsService,
  LayoutsService,
  FabricHandlerService,
  MoveableHandlerService,
  LoadingService,
  SnapshotService,
  PicturesService,
];

@NgModule({
  imports: [
    CommonModule,
    DndModule,
    NgxMoveableModule,
    ClickOutsideModule
  ],
  providers: [ ... SERVICES ],
  declarations: [ ... COMPONENTS, PlcPreviewLoaderComponent ],
  exports: [ ... COMPONENTS ]
})
export class ProductLogoCustomizationModule { }
