export default [{

    name: 'T-Shirt Front Logo',

    center: {
      x: 50,
      y: 25,
    },

    size: {
      w: 15,
      h: 15,
    },

    colors: [
      {
        id: 'TSHIRTF33',
        hex: '#6f79a0',
        price: 12.34,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-gray.jpg',
        description : 'COLOR 1'
      },
      {
        id: 'TSHIRTF4F',
        hex: '#7f7f7f',
        price: 10.23,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-gray.jpg',
        description : 'COLOR 2'
      },
      {
        id: 'TSHIRTF22',
        hex: '#689144',
        price: 15.99,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-gray.jpg',
        description : 'COLOR 3'
      },
      {
        id: 'TSHIRTFR4D',
        hex: '#b46767',
        price: 13.33,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-gray.jpg',
        description : 'COLOR 4'
      },
    ],

    logos: [

      {
        id: '96YC5Z9G44VL4KS2P2SVD43YB9P4LPMUX',
        name: 'Logo Coca Cola',
        timestamp: '2019-12-30T04:30:32.799Z',
        src: 'https://blipoint.com/blog/wp-content/uploads/2018/04/logo-coca-cola-actual.png'
      },
      {
        id: '96YC5Z9G44VL4s3EE2P2SVD43YB9PMUP4',
        name: 'Logo PayPal ',
        timestamp: '2019-12-30T04:30:33.799Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/a/a4/Paypal_2014_logo.png'
      },
      {
        id: 'SVD43YB9G44VL49PMUP4EE2P2KKLI',
        name: 'Logo Apple',
        timestamp: '2019-11-30T04:30:32.799Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Apple_logo_black.svg/1200px-Apple_logo_black.svg.png'
      }
    ]

  }, {

    name: 'T-Shirt Back Logo',


    center: {
      x: 50,
      y: 20,
    },

    size: {
      w: 25,
      h: 15,
    },

    colors: [
      {
        id: 'TSHIRTB33',
        hex: '#6f79a0',
        price: 12.34,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-back-gray.jpg',
        description : 'COLOR 1'
      },
      {
        id: 'TSHIRTB4F',
        hex: '#7f7f7f',
        price: 10.23,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-back-gray.jpg',
        description : 'COLOR 2'
      },
      {
        id: 'TSHIRTB22',
        hex: '#689144',
        price: 15.99,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-back-gray.jpg',
        description : 'COLOR 3'
      },
      {
        id: 'TSHIRTB4D',
        hex: '#b46767',
        price: 13.33,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-back-gray.jpg',
        description : 'COLOR 4'
      },
    ],

    logos: [

      {
        id: '96YSsg4VLG4KS2P2SFCXX43YLKJMUX',
        name: 'Logo Microsoft',
        timestamp: '2019-11-20T04:30:32.799Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Microsoft_logo_%282012%29.svg/1280px-Microsoft_logo_%282012%29.svg.png'
      },
      {
        id: 'X43G44YLKJMUXZ9G44VL4TTGHVD43YB9UP3',
        name: 'Logo IBM ',
        timestamp: '2019-11-22T04:32:23.569Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/IBM_logo.svg/1200px-IBM_logo.svg.png'
      },
      {
        id: 'P2LSVD4MUXZ9GCXX43Y44VKMUP4EE2IK',
        name: 'Logo Intel',
        timestamp: '2019-09-30T04:30:32.799Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Intel-logo.svg/1280px-Intel-logo.svg.png'
      }

    ]

  }

]
