import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlcPositionItemComponent } from './plc-position-item.component';

describe('PlcPositionItemComponent', () => {
  let component: PlcPositionItemComponent;
  let fixture: ComponentFixture<PlcPositionItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlcPositionItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlcPositionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
