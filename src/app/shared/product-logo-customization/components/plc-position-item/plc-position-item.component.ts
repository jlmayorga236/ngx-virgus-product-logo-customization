import { Component, OnInit, Input, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { IPositionInputPLC, IColorInputPLC, ILogoChangeEventPLC } from '../../models/index';

@Component({
  selector: 'app-plc-position-item',
  templateUrl: './plc-position-item.component.html',
  styleUrls: ['./plc-position-item.component.css']
})
export class PlcPositionItemComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('name')
  name: string;

  @Input('isActive')
  isActive: boolean;

  // tslint:disable-next-line: no-input-rename
  @Input('colors')
  colors: any[];

  @Output()
  active: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  enable: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  colorChange: EventEmitter<any> = new EventEmitter<any>();

  private isChecked: boolean = false;
  private color: any = {};


  constructor() {

  }

  ngOnInit(): void {
    const colorId = this.colors[0].id;
    this.color = this.colors.filter((color: any) => color.id === colorId)[0];
  }

  ngOnChanges(changes: SimpleChanges): void {
    const colorId = this.colors[0].id;
    this.color = this.colors.filter((color: any) => color.id === colorId)[0];
  }

  setActive($event: any): void {
    if ($event.target.localName !== 'input'  && $event.target.localName !== 'select' ) {
      this.active.emit();
      this.colorChange.emit(this.color)
    }
  }

  setEnable($event: any): void {
    this.isChecked = !this.isChecked;
    this.enable.emit();
  }

  setColor($event: any): void {
    const colorId = $event.target.value;
    this.color = this.colors.filter((color: any) => color.id === colorId)[0];
    this.colorChange.emit(this.color)
  }

}
