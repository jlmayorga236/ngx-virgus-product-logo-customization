import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlcPictureThumbComponent } from './plc-picture-thumb.component';

describe('PlcPictureThumbComponent', () => {
  let component: PlcPictureThumbComponent;
  let fixture: ComponentFixture<PlcPictureThumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlcPictureThumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlcPictureThumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
