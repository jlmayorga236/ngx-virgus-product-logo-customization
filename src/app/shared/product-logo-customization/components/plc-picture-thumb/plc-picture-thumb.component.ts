import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-plc-picture-thumb',
  templateUrl: './plc-picture-thumb.component.html',
  styleUrls: ['./plc-picture-thumb.component.css']
})
export class PlcPictureThumbComponent implements OnInit {


  @Input("id")
  id: string;

  @Input("src")
  src: string;


  @Output()
  remove: EventEmitter<any> = new EventEmitter<any>();

  public UX: any = {
    isRemoving: false
  };


  constructor() { }

  ngOnInit() {
  }

  onClickRemove(): void{
    this.UX.isRemoving = true;
    setTimeout(() => {
      this.UX.isRemoving = false;
      this.remove.emit()
    }, 600);
  }


}
