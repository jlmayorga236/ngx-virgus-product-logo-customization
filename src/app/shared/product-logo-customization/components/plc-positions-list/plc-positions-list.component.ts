import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-plc-positions-list',
  templateUrl: './plc-positions-list.component.html',
  styleUrls: ['./plc-positions-list.component.css']
})
export class PlcPositionsListComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('hide')
  hide: boolean;

  constructor() { }

  ngOnInit() {
  }

}
