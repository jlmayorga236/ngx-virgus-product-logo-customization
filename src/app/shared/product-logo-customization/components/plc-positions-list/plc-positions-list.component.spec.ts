import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlcPositionsListComponent } from './plc-positions-list.component';

describe('PlcPositionsListComponent', () => {
  let component: PlcPositionsListComponent;
  let fixture: ComponentFixture<PlcPositionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlcPositionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlcPositionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
