import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlcPictureCarouselComponent } from './plc-picture-carousel.component';

describe('PlcPictureCarouselComponent', () => {
  let component: PlcPictureCarouselComponent;
  let fixture: ComponentFixture<PlcPictureCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlcPictureCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlcPictureCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
