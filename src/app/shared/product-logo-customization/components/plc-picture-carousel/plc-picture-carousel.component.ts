import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { IProductPositionLogo } from '../../models/IProductPositionLogo.model';
import {md5} from '../../helpers/md5';


@Component({
  selector: 'app-plc-picture-carousel',
  templateUrl: './plc-picture-carousel.component.html',
  styleUrls: ['./plc-picture-carousel.component.css']
})
export class PlcPictureCarouselComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('pictures')
  pictures: IProductPositionLogo[];

   // tslint:disable-next-line: no-input-rename
   @Input('hide')
   hide: boolean;

  @Output()
  addPicture: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  removePicture: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  errorUploadingPicture: EventEmitter<any> = new EventEmitter<any>();



  constructor() { }

  ngOnInit() {
  }



  setDragingStart($event: any){

  }
  setDragingStop($event: any){
    console.log('event')
    console.log($event)
  }

  onAddLogoPicture($event: { name: string; base64: string }): void {

    const name: any = $event.name;
    const base64:any = $event.base64;
    const logo: IProductPositionLogo = {
      id: md5(name + (new Date()).toISOString() + Math.random() + 'UPLOAD_IMAGE::'),
      name: name,
      timestamp: new Date().toISOString(),
      src: base64
    };
    this.addPicture.emit(logo);

  }

}
