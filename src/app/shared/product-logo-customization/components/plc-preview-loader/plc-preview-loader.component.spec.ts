import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlcPreviewLoaderComponent } from './plc-preview-loader.component';

describe('PlcPreviewLoaderComponent', () => {
  let component: PlcPreviewLoaderComponent;
  let fixture: ComponentFixture<PlcPreviewLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlcPreviewLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlcPreviewLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
