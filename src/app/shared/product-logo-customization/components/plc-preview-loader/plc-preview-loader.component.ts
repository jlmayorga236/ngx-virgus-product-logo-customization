import { Component, OnInit, Input } from '@angular/core';
import { LoadingService } from '../../services/common/loading/loading.service';

@Component({
  selector: 'app-plc-preview-loader',
  templateUrl: './plc-preview-loader.component.html',
  styleUrls: ['./plc-preview-loader.component.css']
})
export class PlcPreviewLoaderComponent implements OnInit {

  public loader: {
    isShow: true,
    transition: 'none',
  };

  private subs;

  constructor(private loaderService: LoadingService) {

   }

  ngOnInit() {

    this.loader = {
      isShow: true,
      transition: 'none',
    };



    this.subs = this.loaderService.dataObs
      .subscribe((loader: any) =>  this.loader = loader );

  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
