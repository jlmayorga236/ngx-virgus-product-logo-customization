import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-plc-picture-add",
  templateUrl: "./plc-picture-add.component.html",
  styleUrls: ["./plc-picture-add.component.css"]
})
export class PlcPictureAddComponent implements OnInit {
  @Output()
  addLogoPicture: EventEmitter<{ name: string; base64: string }> = new EventEmitter<{ name: ""; base64: "" }>();

  constructor() {}

  ngOnInit() {}

  public fileChangeEvent($event: any): void {
    const selectedFile = $event.target.files[0];
    const reader = new FileReader();

    reader.onload = (event: any) => {
      this.addLogoPicture.emit({
        name: selectedFile.name,
        base64: event.target.result
      });
    };

    reader.readAsDataURL(selectedFile);
  }
}
