import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlcPictureAddComponent } from './plc-picture-add.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

fdescribe('PlcPictureAddComponent', () => {

  let component: PlcPictureAddComponent;
  let fixture: ComponentFixture<PlcPictureAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlcPictureAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlcPictureAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  fit('should create', () => {
    expect(component).toBeTruthy();
  });

  fit('should have single button to upload files', () => {
    const id = 'add-picture-btn';
    const buttonDebugElems: DebugElement[] = fixture.debugElement.queryAll(By.css('#'+id));
    expect(buttonDebugElems.length).toEqual(1);
    expect(buttonDebugElems[0].nativeElement.innerText).toEqual('+');
  });

  fit('should have single input to upload files', () => {
    const id = 'add-picture-input';
    const inputDebugElems: DebugElement[] = fixture.debugElement.queryAll(By.css('#' + id));
    expect(inputDebugElems.length).toEqual(1);
  });

  fit('should uploads only a single image', () => {

    const id = 'add-picture-input';
    const fileList = { 0: { name: 'foo', size: 500001 } };

    const inputDebugElems: DebugElement[] = fixture.debugElement.queryAll(By.css('#' + id));
    const inputDebugElement: DebugElement = inputDebugElems[0];
    const inputNativeElement: HTMLInputElement = inputDebugElement.nativeElement;
    //inputNativeElement.files = fileList;
    inputNativeElement.dispatchEvent(new Event('change'));
  });


});
