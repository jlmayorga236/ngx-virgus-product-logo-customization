import { Component, OnInit, HostListener } from '@angular/core';
import { LayoutsService } from '../../services/common/layouts/layouts.service';
import { FabricHandlerService } from '../../services/libs-handlers-services/fabric-js/fabric-handler.service';

@Component({
  selector: 'app-plc-preview',
  templateUrl: './plc-preview.component.html',
  styleUrls: ['./plc-preview.component.css']
})
export class PlcPreviewComponent implements OnInit {

  @HostListener("window:keydown", ["$event"])
  onKeyPress(event) {
    if(event.key === 'Delete'){
      this.fabricService.remoteSelectedFromBoard();
    }
  }

  constructor(private fabricService: FabricHandlerService) { }

  ngOnInit() {
  }

  doZoomIn(){
    this.fabricService.zoomIn();
  }

  doZoomOut(){
    this.fabricService.zoomOut();
  }
}
