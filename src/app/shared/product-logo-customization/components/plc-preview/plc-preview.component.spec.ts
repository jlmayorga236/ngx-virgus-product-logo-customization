import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlcPreviewComponent } from './plc-preview.component';

describe('PlcPreviewComponent', () => {
  let component: PlcPreviewComponent;
  let fixture: ComponentFixture<PlcPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlcPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlcPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
