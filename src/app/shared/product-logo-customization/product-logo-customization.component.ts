import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, HostListener } from "@angular/core";

import { ILayout } from "./models/ILayout.model";
import { IProductPosition } from "./models/IProductPosition.model";
import { IProductPositionLogo } from "./models/IProductPositionLogo.model";
import { IPositionInputPLC, IColorInputPLC, ILogoChangeEventPLC } from "./models/index";


import { LoadingService } from "./services/common/loading/loading.service";
import { SnapshotService } from "./services/common/snapshot/snapshot.service";
import { LayoutsService } from "./services/common/layouts/layouts.service";
import { MoveableHandlerService } from "./services/libs-handlers-services/moveable-js/moveable-handler.service";
import { PicturesService, IPicture, toIPicture, ILogo } from './services/common/pictures/pictures.service';
import { PositionsService, IPosition, toIPosition } from './services/common/positions/positions.service';

import { Observable } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
import { FabricHandlerService } from './services/libs-handlers-services/fabric-js/fabric-handler.service';


@Component({
  selector: "app-product-logo-customization",
  templateUrl: "./product-logo-customization.component.html",
  styleUrls: ["./product-logo-customization.component.css"]
})
export class ProductLogoCustomizationComponent implements OnInit {

  public picturesList: IProductPositionLogo[] = [];
  public positionsList: IProductPosition[] = [];

  public positionActive: IProductPosition | null = null;
  public colorActive: any | null = null;

  public $LOGOS: Observable<ILogo[]>;
  public $POSITION: Observable<IPosition>;
  public $POSITIONS: Observable<IPosition[]>;
  public $PICTURES: Observable<IPicture[]>;
  public $IS_CLIPPING: Observable<boolean>;

  public PICTURES = {
    toAdd: [],
    toRemove: [],
  };

  public UX = {
    isResizingTimmer: null
  }

  @Input("isPreview")
  isPreview: boolean;

  @Input("positions")
  positions: IProductPosition[];

  @Output()
  change: EventEmitter<ILogoChangeEventPLC[]> = new EventEmitter<ILogoChangeEventPLC[]>();

  @Output()
  imageRemove: EventEmitter<Array<{ id: string }>> = new EventEmitter<Array<{ id: string }>>();

  @Output()
  imageAdd: EventEmitter<Array<{ id: string; file: any }>> = new EventEmitter<Array<{ id: string; file: any }>>();

  @HostListener("window:resize", ["$event"])
  onResize(event) {
clearTimeout(this.UX.isResizingTimmer);
    this.UX.isResizingTimmer = setTimeout(() =>  {

      const w = event.target.innerWidth;
      const h = event.target.innerHeight;
      const view = document.getElementById("view-id");
      view.style.setProperty("--plc-view-width", w + "px");
      view.style.setProperty("--plc-view-height", h + "px");

      this.layoutService.initLayout(this.positionActive).then(initsLayouts => {

        const container = this.layoutService.getLayoutContainer();
        const position = this.positionsService.getActive();
        const product = position.colors[0].imageSrc;
        const logos = this.picturesService.getLogos().filter(logo => logo.position.name === position.name)

        console.log({
          container,
          position,
          product,
          logos
        })

      });
    }, 750);


  }

  constructor(
    private loaderService: LoadingService,
    private snaphotService: SnapshotService,
    private moveableService: MoveableHandlerService,
    private fabricService: FabricHandlerService,
    private layoutService: LayoutsService,
    private picturesService: PicturesService,
    private positionsService: PositionsService
  ) {}


  // ------------------------------------------------------ //
  // -- Angular Hooks Methods ----------------------------- //
  // ------------------------------------------------------ //
  ngOnInit() {
    this.$POSITION = this.positionsService.syncActive();
    this.$POSITIONS = this.positionsService.syncList();
    this.$IS_CLIPPING = this.moveableService.syncClipping();
    this.$PICTURES = this.picturesService.syncPictures().pipe(
      mergeMap((pictures: any) =>
        this.$POSITION.pipe(
          map((position: any) =>
            pictures.filter(picture =>
              picture.position.name === position.name)
          ),
        )
      )
    );
    this.$POSITION.subscribe($position => {
      this.layoutService.initLayout(this.positionActive).then(initsLayouts => {

        const container = this.layoutService.getLayoutContainer();
        const position = this.positionsService.getActive();
        const product = position.colors[0].imageSrc;
        const logos = this.picturesService.getLogos().filter(logo => logo.position.name === position.name)

        this.fabricService.init(container, position, product, logos)
        .then(response => {

        }).catch(error => console.error(error))

      });
    });

    this.positionsList = this.positions;
    const initPictures: IPicture[] = [].concat( ...
      this.positionsList.map(position => position.logos.map(logo => toIPicture(logo, position.name, position.name)
    )));

    this.picturesService.setList(initPictures);
    this.positionsService.setList(this.positions);
    this.positionsService.setActive(this.positions[0]);

    //this.onPositionActive(this.positions[0]);
    //this.onPositionColorChange(this.positions[0], this.positions[0].colors[0]);
  }
  // ------------------------------------------------------ //





  // ------------------------------------------------------ //
  // -- Eevents Handlers Methods  ------------------------- //
  // ------------------------------------------------------ //

    public onPositionEnable(position: any): void {
      //this.positionsService.setEnablePosition(position);
    }
    public onPositionActive(positionActive: any): void {
      this.positionsService.setActive(positionActive);
    }
    public onPositionColorChange(position: any, color: any): void {
      //this.positionsService.setColorActive(position, color);
    }

  // ------------------------------------------------------ //






  // ------------------------------------------------------ //
  // -- UI/UX Methods ------------------------------------- //
  // ------------------------------------------------------ //

  public trackByIds(index: number, item: any) {
    return item.id;
  }

  public initLayout(): void {
    this.layoutService
      .initLayout(this.positionActive)
      .then((response: any) => {
        console.log(" initLayout :: " + JSON.stringify(response));
      })
      .catch((error: any) => {
        console.error(error);
      });
  }

  // ------------------------------------------------------ //






  // ------------------------------------------------------ //
  // -- Add/Remove Pictures-------------------------------- //
  // ------------------------------------------------------ //
  public addPicture($picture): void {

    const currentLocation = this.positionsService.getActive();
    this.picturesService.add($picture, currentLocation.name)
      .then(response => console.log('PictureSaved'))
      .catch(error => console.error(error));

    this.PICTURES.toAdd.push($picture);
    this.PICTURES.toAdd = Array.from(new Set(this.PICTURES.toAdd));
  }
  public removePicture($picture): void {

    this.picturesService.remove($picture)
      .then(response => console.log('PictureRemoved'))
      .catch(error => console.error(error));

    this.PICTURES.toRemove.push($picture);
    this.PICTURES.toRemove = Array.from(new Set(this.PICTURES.toRemove));


  }
  // ------------------------------------------------------ //

  // ------------------------------------------------------ //
  // -- Drag,Drop,Resize,Rotate Logos on Preview ---------- //
  // ------------------------------------------------------ //
  public onDrop($event: any): void {
    const picture = $event.data;
    this.fabricService.addToBoard(picture);
  }

  public onDropRemove($event: any): void {
    const logo = $event.data;
    this.onDeleteLogo(logo);
  }


  public onDeleteLogo(logo: any) {
    this.picturesService.removeFromBoard(logo)
    .then(response => console.log('LOGO REMOVED FROM BOARD'))
    .catch(error => console.error(error))
  }

  public onChangeLogo($event: any, $logo: any): void {

    const stateRelative = $event;
    const layoutProduct = this.layoutService.getLayoutProduct();

    const stateTop = (stateRelative.position.top - 0.5 * stateRelative.w0 * (stateRelative.size.h - 1))
                      - layoutProduct.position.top;
    const stateLeft = (stateRelative.position.left - 0.5 * stateRelative.w0 * (stateRelative.size.w - 1))
                      - layoutProduct.position.left;
    const stateAbsolute = {
      position : {
        top: stateTop,
        left: stateLeft,
      },
      size: {
        w: stateRelative.w0 * stateRelative.size.w,
        h: stateRelative.w0 * stateRelative.size.h,
      },
      rotation: stateRelative.rotation
    };

    const event: any = {
      positionId: this.positionActive.name,
      imagesToUpload: this.PICTURES.toAdd ,
      imagesIdsToDelete: this.PICTURES.toRemove,
      imageId: this.positionActive.logos[0] , // ONLY ONE LOGO???!!!!!
      offsetX: stateAbsolute.position.left,
      offsetY: stateAbsolute.position.top,
      scaleX: stateAbsolute.size.w,
      scaleY: stateAbsolute.size.h,
      rotation: stateAbsolute.rotation
    };


    this.change.emit(event);


  }
  // ------------------------------------------------------ //


















  // ------------------------------------------------------ //
  // -- Canvas Methods ------------------------------------- //
  // ------------------------------------------------------ //

  previewClip(): void {
    this.fabricService.setClipAreaLayout(true).then(response => console.log(""));
    setTimeout(() => {
      this.fabricService.setClipAreaLayout(false).then(response => console.log(""));
    }, 3500);
  }

  saveClip(): void {
    this.snaphotService
      .takeSnapshot()
      .then(response => {
          //
          var img = new Image();
          img.setAttribute('crossOrigin', 'anonymous');
          img.src = response;
          document.body.appendChild(img);

      })
      .catch(error => {
        console.error("");
        console.error("ProductLogoCustomization @saveClip() ");
        console.error(error);
        console.error("");
      });
  }

  // ------------------------------------------------------ //
}
