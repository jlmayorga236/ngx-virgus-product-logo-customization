import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductLogoCustomizationComponent } from './product-logo-customization.component';

describe('ProductLogoCustomizationComponent', () => {
  let component: ProductLogoCustomizationComponent;
  let fixture: ComponentFixture<ProductLogoCustomizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductLogoCustomizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductLogoCustomizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
