export interface IProductPositionColor {
  id: string;
  hex: string;
  price: number;
  imageSrc: string;
  description: string;
}
