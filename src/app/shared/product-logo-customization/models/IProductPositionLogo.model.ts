export interface IProductPositionLogo {
  id: string;
  name: string;
  timestamp: string;
  src: string;
}
