export interface IPositionInputPLC {

  id: string;
  backgroundImage: string;

  xCenter: number;
  yCenter: number;
  xSize: number;
  ySize: number;

  images: Array<{
    id: string;
    url: string;
  }>;

}
