import {IProductPositionColor} from './IProductPositionColor.model';
import {IProductPositionLogo} from './IProductPositionLogo.model';

export interface IProductPosition{
  name: string;

  center: {
    x: number;
    y: number;
  };

  size: {
    w: number;
    h: number;
  };

  colors: IProductPositionColor[];

  logos: IProductPositionLogo[];

}
