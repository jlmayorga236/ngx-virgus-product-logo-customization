export interface ILayout {

  center: {
    x: number;
    y: number;
  };

  position: {
    top: number,
    left: number
  };

  size: {
    w: number;
    h: number;
  };

}

export function toILayout(layout: any): ILayout {
  return {

    center: {
      x: layout.center ? layout.center.x : 0,
      y: layout.center ? layout.center.y : 0,
    },

    position: {
      top: layout.position ? layout.position.top : 0,
      left: layout.position ? layout.position.left : 0,
    },

    size: {
      w:  layout.size ? layout.size.w : 0,
      h:  layout.size ? layout.size.h : 0,
    },
  }
}
