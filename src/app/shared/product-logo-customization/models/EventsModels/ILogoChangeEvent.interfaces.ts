export interface ILogoChangeEventPLC {
  positionId: string;
  imagesToUpload: Array<{
    id: string;
    file: any;
  }>;
  imagesIdsToDelete: Array<string>;
  imageId: string;
  offsetX: number;
  offsetY: number;
  scaleX: number;
  scaleY: number;
  rotation: number;
}
