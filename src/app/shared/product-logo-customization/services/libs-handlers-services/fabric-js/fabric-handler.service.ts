import { Injectable } from "@angular/core";
import { fabric } from "fabric";
import { ILayout } from "../../../models/ILayout.model";
import { IPicture } from "../../common/pictures/pictures.service";
import { IPosition } from "../../common/positions/positions.service";
import { BehaviorSubject } from "rxjs";
import { scaleToRatio } from "../../../helpers/scaleToRatio";

@Injectable()
export class FabricHandlerService {
  private canvas;
  private zoom: any = {
    level: 1,
    x: 0,
    y: 0
  };
  private container: any = {};
  private position: any = {};
  private product: any = {};
  private logosBS = new BehaviorSubject<any[]>([]);
  private logosObs = this.logosBS.asObservable();

  constructor() {}

  init(containerLayout: ILayout, position: IPosition, productSrc: string, logos?: any[]): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const isCanvasLayoutReady = await this.setCanvasLayout(containerLayout);
        const isFabricJSReady = await this.setFabricCanvas();
        const isSetProductReady = await this.setProductLayout(containerLayout, productSrc);
        const isSetAreaReady = await this.setAreaLayout(position);
        const isSetZoomHandlerReady = await this.setZoomHandler();
        const isClipAreaReady = await this.setClipAreaLayout(false);
        const initLogos = this.logosBS.getValue().filter(logo => logo.picture.position.name === position.name);
        initLogos.forEach(logo => {
          this.addToBoard(logo.picture, false);
        });
        resolve();
      } catch (error) {
        console.error({
          name: "ERROR INIT FABRIC",
          error
        });
      }
    });
  }

  setFabricCanvas(): Promise<any> {
    return new Promise((resolve, reject) => {

      const self = this;
      if (self.canvas) {

        self.canvas.clear();
        this.resetZoom();

      } else {

        self.canvas = new fabric.Canvas("cnv-board");
        self.canvas.on("object:moved", function($event) {
          const target = $event.target;
          const logoId = target.logoId;
          const logoTop = target.top;
          const logoLeft = target.left;
          const logoWith = target.width * target.scaleX;
          const logoHeight = target.height * target.scaleY;
          self.updateLogo(logoId, {
            top: logoTop,
            left: logoLeft,
            width: logoWith,
            height: logoHeight
          });
        });
        self.canvas.on("object:scaled", function($event) {
          const target = $event.target;
          const logoId = target.logoId;
          const logoTop = target.top;
          const logoLeft = target.left;
          const logoWith = target.width * target.scaleX;
          const logoHeight = target.height * target.scaleY;
          self.updateLogo(logoId, {
            top: logoTop,
            left: logoLeft,
            width: logoWith,
            height: logoHeight
          });
        });
        self.canvas.on("object:rotated", function($event) {
          const target = $event.target;
          const logoId = target.logoId;
          const logoTop = target.top;
          const logoLeft = target.left;
          const logoWith = target.width * target.scaleX;
          const logoHeight = target.height * target.scaleY;
          self.updateLogo(logoId, {
            top: logoTop,
            left: logoLeft,
            width: logoWith,
            height: logoHeight
          });
        });
      }

      //this.logosBS.next([])

      resolve();
    });
  }

  setCanvasLayout($containerLayout: ILayout): Promise<any> {
    return new Promise((resolve, reject) => {
      const ctx = (document.getElementById("cnv-board") as any).getContext("2d");
      const w = $containerLayout.size.w;
      const h = $containerLayout.size.h;
      ctx.canvas.width = w;
      ctx.canvas.height = h;
      this.container.position = {
        top: 0,
        left: 0
      };
      this.container.size = {
        w,
        h
      };
      this.zoom.x = 0.5 * this.container.size.w;
      this.zoom.y = 0.5 * this.container.size.h;

      resolve();
    });
  }

  setZoomHandler(): Promise<any> {
    return new Promise((resolve, reject) => {
      const self = this;
      //self.canvas.selection = false;
      //self.canvas.perPixelTargetFind = true;

      self.canvas.on("mouse:drag", function(opt) {
        console.warn("touch:drag");
        console.warn(opt);
      });

      self.canvas.on("mouse:wheel", function(opt) {
        var delta = -opt.e.deltaY / 10;
        var zoom = self.canvas.getZoom();
        zoom = zoom + delta / 100;
        if (zoom > 10) zoom = 10;
        if (zoom < 0.25) zoom = 0.25;
        //self.canvas.setZoom(zoom);
        self.zoom.x = opt.pointer.x;
        self.zoom.y = opt.pointer.y;
        self.zoom.level = zoom;
        self.canvas.zoomToPoint(new fabric.Point(opt.pointer.x, opt.pointer.y), zoom);
        opt.e.preventDefault();
        opt.e.stopPropagation();
      });

      self.canvas.on("mouse:down", function(opt) {
        var evt = opt.e;
        var isThereAnActiveObject = self.canvas.getActiveObject();

        if (evt.altKey === true) {
          this.isDragging = true;
          this.selection = false;
          this.lastPosX = evt.clientX;
          this.lastPosY = evt.clientY;
        }

        if (this.isAllowCanvasDragging) {
          this.isDragging = true;
          this.selection = false;
          this.lastPosX = evt.clientX;
          this.lastPosY = evt.clientY;
        }
      });
      self.canvas.on("mouse:move", function(opt) {
        if (this.isDragging) {
          var e = opt.e;
          this.viewportTransform[4] += e.clientX - this.lastPosX;
          this.viewportTransform[5] += e.clientY - this.lastPosY;
          this.requestRenderAll();
          this.lastPosX = e.clientX;
          this.lastPosY = e.clientY;
        }

        if (this.isZooming) {
          var delta =
            (Math.sign(opt.pointer.y - this.lastPosY) *
              Math.sqrt(Math.pow(opt.pointer.x - this.lastPosX, 2) + Math.pow(opt.pointer.y - this.lastPosY, 2))) /
            500;

          console.log(delta);
          var zoom = self.canvas.getZoom();
          zoom = zoom + delta / 100;
          if (zoom > 10) zoom = 5;
          if (zoom < 0.25) zoom = 0.5;
          //self.canvas.setZoom(zoom);
          self.zoom.x = opt.pointer.x;
          self.zoom.y = opt.pointer.y;
          self.zoom.level = zoom;
          self.canvas.zoomToPoint(new fabric.Point(opt.pointer.x, opt.pointer.y), zoom);
          opt.e.preventDefault();
          opt.e.stopPropagation();
        }

        if (!(opt.target && opt.target.logoId)) {
          this.isAllowCanvasDragging = true;
        } else {
          this.isAllowCanvasDragging = false;
        }
      });
      self.canvas.on("mouse:up", function(opt) {
        this.isDragging = false;
        this.isZooming = false;
        this.selection = false;
        this.getObjects().map(function(o) {
          //  console.log(o)
          return o.set("active", true);
        });
      });
      resolve();
    });
  }

  getZoom() {
    const self = this;
    return self.zoom;
  }

  setZoom(zoom) {
    const self = this;
    return self.canvas.setZoom(zoom);
  }

  resetZoom(viewport?: any, level?: any) {
    const self = this;
    const x = 0 + 0.5 * self.container.size.w;
    const y = 0 + 0.5 * self.container.size.h;
    this.canvas.viewportTransform[4] = 0;
    this.canvas.viewportTransform[5] = 0;
    this.canvas.setViewportTransform(viewport || [1, 0, 0, 1, 0, 0]);
    this.canvas.requestRenderAll();
    this.canvas.setZoom(level || 1);
    //self.canvas.zoomToPoint(new fabric.Point(x, y), 1);
  }

  setProductLayout($containerLayout: ILayout, $productPicture: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const self = this;
      const productPictureSrc = $productPicture;

      fabric.Image.fromURL(productPictureSrc, function(oImg) {
        const productLayout = {
          w: 0,
          h: 0,
          top: 0,
          left: 0
        };

        const w0 = oImg.width;
        const h0 = oImg.height;

        if (h0 > w0) {
          productLayout.h = 0.75 * $containerLayout.size.h;
          productLayout.w = (w0 / h0) * productLayout.h;
          productLayout.left = 0.5 * $containerLayout.size.w - 0.5 * productLayout.w;
          productLayout.top = 0.5 * $containerLayout.size.h - 0.5 * productLayout.h;
          oImg.scaleToHeight(productLayout.h);
        }
        if (h0 == w0) {
          productLayout.h = 0.75 * $containerLayout.size.h;
          productLayout.w = (w0 / h0) * productLayout.h;
          productLayout.left = 0.5 * $containerLayout.size.w - 0.5 * productLayout.w;
          productLayout.top = 0.5 * $containerLayout.size.h - 0.5 * productLayout.h;
          oImg.scaleToHeight(productLayout.h);
        }

        if (h0 < w0) {
          productLayout.w = 0.75 * $containerLayout.size.w;
          productLayout.h = (h0 / w0) * productLayout.w;
          productLayout.left = 0.5 * $containerLayout.size.w - 0.5 * productLayout.w;
          productLayout.top = 0.5 * $containerLayout.size.h - 0.5 * productLayout.h;
          oImg.scaleToWidth(productLayout.w);
        }

        oImg.set({
          top: productLayout.top,
          left: productLayout.left
        });

        self.product.position = {
          top: oImg.top,
          left: oImg.left
        };

        self.product.center = {
          x: oImg.left + 0.5 * oImg.width,
          y: oImg.top + 0.5 * oImg.height
        };

        self.product.size = {
          w: productLayout.w,
          h: productLayout.h
        };

        oImg.hoverCursor = "default";
        (oImg.selectable = false), (oImg.hasBorders = false);
        oImg.lockMovementY = true;
        oImg.lockMovementX = true;
        oImg.hasControls = false;

        self.canvas.add(oImg);

        resolve();
      });
    });
  }

  setAreaLayout($positionLayout: IPosition): Promise<any> {
    return new Promise((resolve, reject) => {
      const self = this;
      const product = this.product;
      const location = {
        position: {
          top: $positionLayout.center.y - 0.5 * $positionLayout.size.h,
          left: $positionLayout.center.x - 0.5 * $positionLayout.size.w
        },
        center: {
          x: $positionLayout.center.x,
          y: $positionLayout.center.y
        },
        size: {
          w: $positionLayout.size.w,
          h: $positionLayout.size.h
        }
      };

      const base_width = 100;
      const base_height = product.size.h * (base_width / product.size.w);

      const scale = product.size.w / base_width;

      const square_top = Math.round(location.center.y - 0.5 * location.size.h);
      const square_left = Math.round(location.center.x - 0.5 * location.size.w);
      const square_width = Math.round(location.size.w);
      const square_height = Math.round(location.size.h);

      const top = square_top * scale + product.position.top;
      const left = square_left * scale + product.position.left;
      const width = square_width * scale;
      const height = square_height * scale;

      this.position.center = {
        x: top + 0.5 * width,
        y: left + 0.5 * height
      };
      this.position.position = {
        top,
        left
      };
      this.position.size = {
        w: width,
        h: height
      };

      const area = new fabric.Rect({
        left: left,
        top: top,
        width: width,
        height: height,
        fill: "transparent",
        stroke: "red",
        strokeDashArray: [10, 2],
        strokeWidth: 1
      });

      area.hoverCursor = "default";
      area.selectable = false;
      area.hasBorders = false;
      area.lockMovementY = true;
      area.lockMovementX = true;
      area.hasControls = false;

      self.canvas.add(area);
      resolve(this.position);
    });
  }

  setClipAreaLayout(isClipping: boolean): Promise<any> {
    return new Promise((resolve, reject) => {
      const self = this;
      const position = this.position;
      const container = this.container;
      let clipPath: any = {};

      if (isClipping) {
        clipPath = new fabric.Rect({
          left: position.position.left,
          top: position.position.top,
          width: position.size.w,
          height: position.size.h
        });
      } else {
        clipPath = new fabric.Rect({
          left: container.position.left,
          top: container.position.top,
          width: container.size.w,
          height: container.size.h
        });
      }
      self.canvas.dirty = true;
      self.canvas.clipPath = clipPath;
      self.canvas.renderAll();

      resolve();
    });
  }

  addToBoard(picture: IPicture, inStore: boolean = true) {
    const self = this;
    fabric.Image.fromURL(
      picture.src,
      function(oImg) {
        if (inStore) {
          const w0 = self.position.size.w;
          const h0 = self.position.size.h;
          const w = oImg.width;
          const h = oImg.height;

          const r0 = h0 / w0;
          const r = h / w;

          const image = { position: { top: 0, left: 0 }, size: { w: 0, h: 0 } };
          const imageW = self.position.size.w;
          const imageH = self.position.size.w * r;
          const imageDH = imageH - self.position.size.h;
          const scale = imageDH > 0 ? self.position.size.h / imageH : 1;
          const scaleWTo = imageW * scale;

          image.size.w = scaleWTo;
          image.size.h = scaleWTo * r;
          oImg.logoWidth = image.size.w;
          oImg.logoHeight = image.size.h;
          oImg.picture = picture;

          oImg.scaleToWidth(image.size.w);

          oImg.set({
            format: "png",
            top: self.position.position.top + 1 * 0.5 * (h0 - image.size.h),
            left: self.position.position.left + 1 * 0.5 * (w0 - image.size.w)
          });

          oImg.hasControls = true;
          oImg.logoId = picture.id + "::" + new Date().getTime();
          oImg.lockUniScaling = true;
          oImg.lockScalingX = false;
          oImg.lockScalingY = false;
          oImg.lockMovementX = false;
          oImg.lockMovementY = false;

          oImg.logoTop = self.position.position.top;
          oImg.logoLeft = self.position.position.left;

          const logo = {
            id: oImg.logoId,
            top: oImg.logoTop,
            left: oImg.logoLeft,
            width: oImg.logoWidth,
            height: oImg.logoHeight,
            picture: oImg.picture
          };
          self.addLogo(logo);
          self.canvas.add(oImg);
        } else {
          const logoFromStore = this.logosBS.getValue().filter(logo => logo.picture == picture)[0];
          oImg.logoWidth = logoFromStore.width;
          oImg.logoHeight = logoFromStore.height;
          oImg.picture = logoFromStore.picture;
          oImg.scaleToWidth(logoFromStore.width);

          oImg.set({
            format: "png",
            top: logoFromStore.top,
            left: logoFromStore.left
          });

          oImg.hasControls = true;
          oImg.logoId = logoFromStore.id;
          oImg.lockUniScaling = true;
          oImg.lockScalingX = false;
          oImg.lockScalingY = false;
          oImg.lockMovementX = false;
          oImg.lockMovementY = false;

          oImg.logoTop = logoFromStore.top;
          oImg.logoLeft = logoFromStore.left;
          self.canvas.add(oImg);
        }

        console.warn("fabric => oImg");
        console.warn(oImg);
      }.bind(this),
      {
        crossOrigin: "anonymous"
      }
    );
  }

  remoteSelectedFromBoard() {
    const self = this;
    self.removeLogo(self.canvas.getActiveObject());
    self.canvas.remove(self.canvas.getActiveObject());
  }

  zoomIn() {
    const self = this;
    var delta = 10;
    var zoom = self.canvas.getZoom();
    zoom = zoom + delta / 200;
    if (zoom > 20) zoom = 20;
    if (zoom < 0.01) zoom = 0.01;
    self.canvas.setZoom(zoom);
  }
  zoomOut() {
    const self = this;
    var delta = -10;
    var zoom = self.canvas.getZoom();
    zoom = zoom + delta / 200;
    if (zoom > 20) zoom = 20;
    if (zoom < 0.01) zoom = 0.01;
    self.canvas.setZoom(zoom);
  }

  addLogo(logo) {
    const logos = this.logosBS.getValue();
    logos.push(logo);
    this.logosBS.next(logos);
  }

  removeLogo(logo) {
    console.warn("removeLogo");
    console.warn(logo);
    const logos = this.logosBS.getValue();
    this.logosBS.next(logos.filter(kLogo => kLogo.id !== logo.logoId));
  }

  updateLogo(logoId, logoOptions) {
    const logos = this.logosBS.getValue();
    logos.map(logo => {
      if (logo.id === logoId) {
        const kLogo = logo;
        kLogo.top = logoOptions.top;
        kLogo.left = logoOptions.left;
        kLogo.width = logoOptions.width;
        kLogo.height = logoOptions.height;
        return kLogo;
      }
    });
    this.logosBS.next(logos);
  }

  syncLogos() {
    return this.logosObs;
  }

  setIsDrawingMode(isDrawing) {
    this.canvas.isDrawingMode = false;
  }

  getCanvas() {
    return this.canvas;
  }

  getClipArea() {
    const position0 = this.position;
    const position = this.position;
    const diffTop = 0;
    const diffLeft = 0;
    const diffScale = 1;

    position.size.w = position0.size.w * (1 / diffScale);
    position.size.h = position0.size.h * (1 / diffScale);
    position.position.top = position0.position.top - diffTop;
    position.position.left = position0.position.left - diffLeft;

    return position;
  }
}
