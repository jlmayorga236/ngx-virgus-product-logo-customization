import { TestBed } from '@angular/core/testing';

import { FabricHandlerService } from './fabric-handler.service';

describe('FabricHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FabricHandlerService = TestBed.get(FabricHandlerService);
    expect(service).toBeTruthy();
  });
});
