import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LayoutsService } from '../../common/layouts/layouts.service';

@Injectable()
export class MoveableHandlerService {


  private isClippingBS = new BehaviorSubject<boolean>(false);
  public $isClipping = this.isClippingBS.asObservable();

  constructor() { }

  public getClippingState(): boolean {
    return this.isClippingBS.getValue();
  }

  hideControllers(): void {
    const controllers = document.querySelectorAll('.moveable-control');
    const lines = document.querySelectorAll('.moveable-line');

    controllers.forEach((controller: any) => controller.style.display = 'none')
    lines.forEach((line: any) => line.style.display = 'none');

    this.isClippingBS.next(true);
  }

  showControllers(): void{
    const controllers = document.querySelectorAll('.moveable-control');
    const lines = document.querySelectorAll('.moveable-line');
    controllers.forEach((controller: any) => controller.style.display = 'block')
    lines.forEach((line: any) => line.style.display = 'block');
    this.isClippingBS.next(false);

  }

  syncClipping() {
    return this.$isClipping;
  }

}
