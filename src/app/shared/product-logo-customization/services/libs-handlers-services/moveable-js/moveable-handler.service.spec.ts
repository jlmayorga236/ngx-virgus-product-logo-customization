import { TestBed } from '@angular/core/testing';

import { MoveableHandlerService } from './moveable-handler.service';

describe('MoveableHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MoveableHandlerService = TestBed.get(MoveableHandlerService);
    expect(service).toBeTruthy();
  });
});
