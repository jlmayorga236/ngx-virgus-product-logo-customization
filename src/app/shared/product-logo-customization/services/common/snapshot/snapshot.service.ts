import { Injectable } from '@angular/core';
import html2canvas from "html2canvas";
import domtoimage from 'dom-to-image';
import { FabricHandlerService } from '../../libs-handlers-services/fabric-js/fabric-handler.service';

@Injectable()
export class SnapshotService {

  constructor(private fabricService: FabricHandlerService) { }

  public takeSnapshot(): Promise<any> {
    return new Promise((resolve, reject) => {

      const clipArea = this.fabricService.getClipArea();
      const zoom = this.fabricService.getZoom();
      const canvas = this.fabricService.getCanvas();
      const currentViewPort = canvas.viewportTransform;
      console.log({
        currentViewPort,
        level: zoom.level
      })
      this.fabricService.setIsDrawingMode(false);
      this.fabricService.resetZoom();

      setTimeout(() => {
        const image = canvas.toDataURL({
          format: 'png',
          multiplier: 1,
          left: clipArea.position.left,
          top: clipArea.position.top,
          width: clipArea.size.w,
          height: clipArea.size.h
        });

        setTimeout(() => {
          this.fabricService.resetZoom(currentViewPort, 1);
          resolve(image);
        },500)


      }, 500)

    });
  }

  private html2Canvas(): Promise<any> {
    return new Promise((resolve, reject) => {
      var node: any = document.querySelector("#cnv-board");
      var snap: any = document.querySelector("#img-snap");
      var canvas = node.getContext('2d');

      html2canvas(
        node,
        { allowTaint: true }
      ).then(canvas => {
          resolve(canvas);
      }).catch(error => reject(error));
    });
  }

  private dom2image(): Promise<any> {
    return new Promise((resolve, reject) => {
    var node = document.querySelector("#domToCanvasTarget-id");
    domtoimage.toPng(node)
        .then(function (dataUrl) {
            resolve(dataUrl)
      }).catch(error => reject(error));
      });
  }
}
