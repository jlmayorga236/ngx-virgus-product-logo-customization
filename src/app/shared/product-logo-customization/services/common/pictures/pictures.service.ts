import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IPosition } from '../positions/positions.service';
import { ILayout } from '../../../models/ILayout.model';

export interface IPicture {

  id: string;

  position: {
    id: string;
    name: string;
  };

  src: string;
  timestamp: string;

}

export interface ILogo extends IPicture {
  top: number;
  left: number;
  w: number;
  h: number;
  rotation: number;
  visible: boolean;
}

export function toIPicture(picture: any, positionId: string, positionName: string): IPicture {
  return{
    id: picture.id,
    position: {
      id: positionId,
      name: positionName,
    },
    src: picture.src,
    timestamp: picture.timestamp
  };
}


@Injectable()
export class PicturesService {

  private picturesBS = new BehaviorSubject<IPicture[]>([]);
  public picturesObs = this.picturesBS.asObservable();

  private logosBS = new BehaviorSubject<ILogo[]>([]);
  public logosObs = this.logosBS.asObservable();

  constructor() { }

  setList($pictures: IPicture[]): Promise<IPicture[]> {
    return new Promise((resolve, reject) => {
      this.picturesBS.next($pictures);
    });
  }

  getList(): any[] {
    return this.picturesBS.getValue();
  }

  getLogos(): any[] {
    return this.logosBS.getValue();
  }


  add($picture: IPicture, positionId: string): Promise<any>{
    return new Promise((resolve, reject) => {
      const pictures = this.getList();
      pictures.push(toIPicture($picture, positionId, positionId));
      this.picturesBS.next(pictures);
      resolve();
    });
  }

  remove($picture: IPicture): Promise<any> {
    return new Promise((resolve, reject) => {
      const picturesBeforeRemoved = this.getList();
      const picturesAfterRemoved = picturesBeforeRemoved.filter(picture => picture.id !== $picture.id);
      this.picturesBS.next(picturesAfterRemoved);
      resolve();
    });
  }


  addToBoard($logo: IPicture, position: IPosition, canvas: ILayout): Promise<any> {
    return new Promise((resolve, reject) => {
      const positionId = position.name;
      const positionName = position.name;
      const logos: ILogo[] = this.getLogos();
      const logo: any = toIPicture($logo, positionId, positionName);
      logo.top = canvas.position.top;
      logo.left = canvas.position.left;
      logo.w = canvas.size.w;
      logo.h = canvas.size.h;
      logo.rotation = 0;
      logo.visible = true;
      logos.push(logo);
      this.logosBS.next(logos);
      resolve(this.getLogos());
    });
  }

  removeFromBoard($logo: ILogo): Promise<any> {
    return new Promise((resolve, reject) => {
      const logosBeforeRemoved: ILogo[] = this.getLogos();
      const logosAfterRemoved = logosBeforeRemoved.filter(logo => logo.id !== $logo.id);
      this.logosBS.next(logosAfterRemoved);
      resolve(this.getLogos());
    });
  }

  updateLogo($logo: ILogo, $absoluteLayout: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const logos: ILogo[] = this.getLogos();
      logos.map(kLogo => {
        if (kLogo.id === $logo.id) {
          kLogo.w = $absoluteLayout.size.w;
          kLogo.h = $absoluteLayout.size.h;
          kLogo.top = $absoluteLayout.position.top;
          kLogo.left = $absoluteLayout.position.left;
          console.warn('kLogo')
          console.warn(kLogo)
        }
      });
      this.logosBS.next(logos);
      resolve();
    });
  }



  syncPictures(): Observable<IPicture[]> {
    return this.picturesObs;
  }
  syncLogos(): Observable<IPicture[]> {
    return this.logosObs;
  }






}
