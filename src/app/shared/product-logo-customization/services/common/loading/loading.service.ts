import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

const TRANSITION_TIME = 1000; //ms

@Injectable()
export class LoadingService {

  private previewBS = new BehaviorSubject<{ isShow: boolean, transition: string}>({ isShow: true, transition: 'none'});
  public dataObs = this.previewBS.asObservable();

  constructor() { }

  public hide(): void {


    this.previewBS.next({
      isShow : true,
      transition: 'toHide'
    });
    setTimeout(() => {
      this.previewBS.next({
        isShow : false,
        transition: 'toHide'
      });
      setTimeout(() => {
        this.previewBS.next({
          isShow : false,
          transition: 'none'
        });
      }, TRANSITION_TIME/2);
    }, TRANSITION_TIME/2);

  }

  public show(): void {
    this.previewBS.next({
      isShow : false,
      transition: 'toShow'
    });
    setTimeout(() => {
      this.previewBS.next({
        isShow : true,
        transition: 'toShow'
      });
      setTimeout(() => {
        this.previewBS.next({
          isShow : true,
          transition: 'none'
        });
      }, TRANSITION_TIME/2);
    }, TRANSITION_TIME/2);
  }
}
