import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, mergeMap } from "rxjs/operators";


import { ILayout } from '../../../models/ILayout.model';
import { toILayout } from '../../../models/ILayout.model';
import { LoadingService } from '../loading/loading.service';
import { MoveableHandlerService } from '../../libs-handlers-services/moveable-js/moveable-handler.service';


@Injectable()
export class LayoutsService {

  private zoomLayoutBS = new BehaviorSubject<{ sw, sh, dtop, dleft}>({ sw: 1, sh: 1, dtop: 0, dleft: 0});
  public $zoomLayout = this.zoomLayoutBS.asObservable();

  private productLayoutBS = new BehaviorSubject<ILayout>(toILayout({}));
  public $productLayout = this.productLayoutBS.asObservable();

  private previewLayoutBS = new BehaviorSubject<ILayout>(toILayout({}));
  public $previewLayout = this.previewLayoutBS.asObservable();

  private canvasLayoutBS = new BehaviorSubject<ILayout>(toILayout({}));
  public $canvasLayout = this.canvasLayoutBS.asObservable();

  private containerLayoutBS = new BehaviorSubject<ILayout>(toILayout({}));
  public $containerLayout = this.containerLayoutBS.asObservable();

  private productStyleBS = new BehaviorSubject<any>({});
  public $productStyle = this.productStyleBS.asObservable();

  private canvasStyleBS = new BehaviorSubject<any>({});
  public $canvastStyle = this.canvasStyleBS.asObservable();

  private wrapperStyleBS = new BehaviorSubject<any>({});
  public $wrapperStyle = this.wrapperStyleBS.asObservable();

  private wrapperContainerStyleBS = new BehaviorSubject<any>({});
  public $wrapperContainerStyle = this.wrapperContainerStyleBS.asObservable();

  constructor(
    private loaderService: LoadingService,
    private moveableService: MoveableHandlerService
  ) { }

  public initLayout(location: any): Promise <any> {
    return new Promise(async (resolve, reject) => {
      try {
        this.loaderService.show();
        const initContainer = await this.initContainer('preview-id');
        this.loaderService.hide();
        resolve({
          initContainer
        });

      } catch (error) {
        reject(error);
      }

    });
  }


  public updateLayout(): Promise <any> {
    return new Promise(async (resolve, reject) => {
      try {

        const container = JSON.parse(JSON.stringify(this.containerLayoutBS.getValue()) + '');
        const product = JSON.parse(JSON.stringify(this.productLayoutBS.getValue()) + '');
        const canvas = JSON.parse(JSON.stringify(this.canvasLayoutBS.getValue()) + '');
        this.containerLayoutBS.next(container);
        this.productLayoutBS.next(product);
        this.canvasLayoutBS.next(canvas);

        resolve();

      } catch (error) {
        reject(error);
      }

    });
  }

  private initContainer(tagId: string): Promise <any> {
    // preview-id
    return new Promise((resolve, reject) => {
      try {
        const preview: any = document.getElementById(tagId);
        const previewW = preview.offsetWidth;
        const previewH = preview.offsetHeight;
        const previewX = previewW / 2;
        const previewY = previewH / 2;
        const layout: ILayout = {
          center : {
            x: previewX,
            y: previewY
          },
          position : {
            top: 0,
            left: 0
          },
          size: {
            w: previewW,
            h: previewH
          }
        };
        this.containerLayoutBS.next(layout);
        resolve(layout);

      } catch (error) {
        reject(error);
      }
    });
  }

  private initProduct(tagId: string): Promise <any> {
    // product-picture-id
    return new Promise((resolve, reject) => {
      try {

        const preview: ILayout = this.containerLayoutBS.getValue();
        const previewW = preview.size.w;
        const previewH = preview.size.h;

        const img: any = document.getElementById(tagId);
        const imgW = img.width;
        const imgH = img.height;
        const imgTop = 0.5 * previewH - 0.5 * imgH;
        const imgLeft = 0.5 * previewW - 0.5 * imgW;

        const layout: ILayout = {
          center : {
            x: imgLeft + 0.5 * imgW,
            y: imgTop + 0.5 * imgH,
          },
          position : {
            top: imgTop,
            left: imgLeft
          },
          size: {
            w: imgW,
            h: imgH
          }
        };

        this.productLayoutBS.next(layout);
        resolve(layout);

      } catch (error) {
        reject(error);
      }
    });
  }

  private initCanvas(location: ILayout): Promise <any> {
    // product-container-id
    return new Promise((resolve, reject) => {
      try {

        const product: ILayout = this.productLayoutBS.getValue();

        const base_width = 100;
        const base_height = product.size.h * (base_width / product.size.w);

        const scale = product.size.w / base_width;

        const square_top = Math.round(location.center.y - 0.5 * location.size.h);
        const square_left = Math.round(location.center.x - 0.5 * location.size.w);
        const square_width = Math.round(location.size.w);
        const square_height = Math.round(location.size.h);

        const top = square_top * scale + product.position.top;
        const left = square_left * scale + product.position.left;
        const width = square_width * scale;
        const height = square_height * scale;

        const layout: ILayout = {
          center : {
            x: left + 0.5 * width,
            y: top + 0.5 * height
          },

          position : {
            top: top,
            left: left
          },

          size: {
            w: width,
            h: height
          }
        };

        this.canvasLayoutBS.next(layout);
        resolve(layout);

      } catch (error) {
        reject(error);
      }
    });
  }

  public getContainerStyle(): Observable<any> {
    return this.$productLayout.pipe(
      map(layout => {
        return {
          top: `0px`,
          left: `0px`,
        };
      })
    );
  }
  public getPreviewStyle(): Observable<any> {
    return this.$containerLayout.pipe(
      map(layout => {
        return {
          top: `${layout.position.top}px`,
          left: `${layout.position.left}px`,
          width: `${layout.size.w}px`,
          height: `${layout.size.h}px`,
        };
      })
    );
  }

  public getProductStyle(): Observable<any> {
    return this.$zoomLayout.pipe(
        mergeMap((zoom: any) => this.$productLayout.pipe(
            map((layout: ILayout) => {
                return {
                  top: `${Math.round(layout.position.top - 0.5 * (layout.size.h * zoom.sh - layout.size.h))}px`,
                  left: `${Math.round(layout.position.left - 0.5 * (layout.size.w * zoom.sw - layout.size.w))}px`,
                  width: `${Math.round(layout.size.w * zoom.sw)}px`,
                  height: `${Math.round(layout.size.h * zoom.sw)}px`,
                };
            }),
          )
        )
      );
  }

  public getCanvasStyle(): Observable<any> {
    return this.$canvasLayout.pipe(
      map(layout => {
        return {
          top: `${Math.round(layout.position.top)}px`,
          left: `${Math.round(layout.position.left)}px`,
          width: `${Math.round(layout.size.w)}px`,
          height: `${Math.round(layout.size.h)}px`
        };
      })
    );
  }

  public getClipCanvas(): Observable<any> {
    return this.$canvasLayout.pipe(
      map(layout => {

        if (this.moveableService.getClippingState()) {

          return {
            top: `${Math.round(layout.position.top)}px`,
            left: `${Math.round(layout.position.left)}px`,
            width: `${Math.round(layout.size.w)}px`,
            height: `${Math.round(layout.size.h)}px`
          };

        } else {
            return { };
        }

      })
    );
  }

  public getPreviewWrapperStyle(): Observable<any> {
    return this.$canvasLayout.pipe(
      map(layout => {

        if (this.moveableService.getClippingState()) {

          return {
            top: `${Math.round(layout.position.top)}px`,
            left: `${Math.round(layout.position.left)}px`,
            width: `${Math.round(layout.size.w)}px`,
            height: `${Math.round(layout.size.h)}px`,
            border: '1px solid red'
          };

        } else {
            return { };
        }

      })
    );
  }

  public getPreviewWrapperContainerStyle(): Observable<any> {
    return this.$canvasLayout.pipe(
      map(layout => {

        if (this.moveableService.getClippingState()) {

          return {
            transform: `translate(-${layout.position.left}px, -${layout.position.top}px)`
          };

        } else {
            return { };
        }

      })
    );
  }


  public syncLayoutContainer(): Observable<any> {
    return this.$productLayout;
  }
  public syncLayoutPreview(): Observable<any> {
    return this.$previewLayout;
  }
  public syncLayoutProduct(): Observable<any> {
    return this.$productLayout;
  }
  public syncLayoutCanvas(): Observable<any> {
    return this.$canvasLayout;
  }

  public syncStyleContainer(): Observable<any> {
    return this.getContainerStyle();
  }
  public syncStyleProduct(): Observable<any> {
    return this.getProductStyle();
  }
  public syncStyleCanvas(): Observable<any> {
    return this.getCanvasStyle();
  }
  public syncStylePreview(): Observable<any> {
    return this.getPreviewStyle();
  }
  public syncStylePreviewWrapper(): Observable<any> {
    return this.getPreviewWrapperStyle();
  }
  public syncStylePreviewWrapperContainer(): Observable<any> {
    return this.getPreviewWrapperContainerStyle();
  }

  public getLayoutCanvas(): any {
    return this.canvasLayoutBS.getValue();
  }
  public getLayoutProduct(): any {
    return this.productLayoutBS.getValue();
  }
  public getLayoutContainer(): any {
    return this.containerLayoutBS.getValue();
  }


  syncScale(): Observable<any> {
    return this.$zoomLayout;
  }

  setZoomIn(){
    const zoomLayout = this.getScale();
    zoomLayout.sw += 0.025;
    zoomLayout.sh += 0.025;
    zoomLayout.sw = Math.min(zoomLayout.sw, 2);
    zoomLayout.sh = Math.min(zoomLayout.sh, 2);
    this.zoomLayoutBS.next(zoomLayout);
  }
  setZoomOut(){
    const zoomLayout = this.getScale();
    zoomLayout.sw -= 0.025;
    zoomLayout.sh -= 0.025;
    zoomLayout.sw = Math.max(zoomLayout.sw, 0);
    zoomLayout.sh = Math.max(zoomLayout.sh, 0);
    this.zoomLayoutBS.next(zoomLayout);
  }

  getScale(): any {
    return this.zoomLayoutBS.getValue();
  }
}
