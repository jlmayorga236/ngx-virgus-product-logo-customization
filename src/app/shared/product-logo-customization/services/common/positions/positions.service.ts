import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface IPosition {

  name: string;

  center: {
    x: number;
    y: number;
  };

  size: {
    w: number;
    h: number;
  };

  colors: Array<{
    id: string;
    hex: string;
    price: number;
    imageSrc: string;
    description: string;
  }>;

  logos: Array<{
    id: string;
    name: string;
    timestamp: string;
    src: string;
  }>;


}

export function  toIPosition(position: any): IPosition {
  return{

    name: position.name,

    center: {
      x: position.center.x,
      y: position.center.y,
    },

    size: {
      w: position.size.w,
      h: position.size.h,
    },

    colors: position.colors,

    logos: position.logos,

  };
}


@Injectable()
export class PositionsService {

  private positionsBS = new BehaviorSubject<any>({});
  public positionsObs = this.positionsBS.asObservable();

  private activePositionBS = new BehaviorSubject<any>({});
  public activePositionObs = this.activePositionBS.asObservable();

  constructor() { }

  getList(): IPosition[] {
    return this.positionsBS.getValue();
  }

  setList(positions: IPosition[]): IPosition[] {
      this.positionsBS.next(positions);
      return positions;
  }

  getActive(): IPosition {
    return this.activePositionBS.getValue();
  }

  setActive(position: IPosition): IPosition {
    this.activePositionBS.next(position);
    return position;
  }

  syncList(): Observable<IPosition[]> {
    return this.positionsObs;
  }

  syncActive(): Observable<IPosition> {
    return this.activePositionObs;
  }

}
