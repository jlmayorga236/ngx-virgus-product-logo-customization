export let scaleToRatio = (r0, r) => {
  // r0 = h0/w0   r = h/w
  let out = '';
  if (r0 > 1) {
    if (r > 1) {
      // r0 > 1 && r > 1
      out = 'FIT-HEIGHT';
    } else if (r < 1) {
      // r0 > 1 && r < 1
      out = 'FIT-WIDTH';
    } else {
      // r0 > 1 && r == 1
      out = 'FIT-WIDTH';
    }
  } else if (r0 < 1) {
    if (r > 1) {
      // r0 < 1 && r > 1
      out = 'FIT-HEIGHT';
    } else if (r < 1) {
       // r0 < 1 && r < 1
       out = 'FIT-WIDTH';
    } else {
       // r0 < 1 && r == 1
       out = 'FIT-HEIGHT';
    }
  } else {
    if (r > 1) {
       // r0 == 1 && r > 1
       out = 'FIT-HEIGHT';
    } else if (r < 1) {
       // r0 == 1 && r < 1
       out = 'FIT-WIDTH';
    } else {
       // r0 == 1 && r == 1
       out = 'FIT-WIDTH';
    }
  }

  return out;
};
