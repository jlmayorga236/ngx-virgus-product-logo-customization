import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductLogoCustomizationModule } from './product-logo-customization/product-logo-customization.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProductLogoCustomizationModule
  ],
  exports: [ProductLogoCustomizationModule]
})
export class SharedModule { }
