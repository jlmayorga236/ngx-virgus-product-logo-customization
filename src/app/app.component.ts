import { Component } from '@angular/core';

import MockPositionsList from "./shared/product-logo-customization/mocks/positions-list.mock";
import MockPicturesList from "./shared/product-logo-customization/mocks/pictures-list.mock";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'ngx-vurbis-product-logo-customization';
  positions = MockPositionsList;

  onChange($event) {

  }

  onImageRemove($event) {

  }

  onImageAdd($event) {

  }

}
