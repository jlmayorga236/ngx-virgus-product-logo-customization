function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<router-outlet></router-outlet>\n\n\n<app-product-logo-customization\n  [isPreview]=\"false\"\n  [positions]=\"positions\"\n  (change)=\"onChange\"\n  (imageRemove)=\"onImageRemove\"\n  (imageAdd)=\"onImageAdd\"\n></app-product-logo-customization>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-picture-add/plc-picture-add.component.html":
  /*!***************************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-picture-add/plc-picture-add.component.html ***!
    \***************************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedProductLogoCustomizationComponentsPlcPictureAddPlcPictureAddComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<div id=\"add-picture-btn\" class=\"add-picture button\" (click)=\"file.click()\">\n  <span > + </span>\n</div>\n\n<input id=\"add-picture-input\" type=\"file\" accept=\"image/*\" #file style=\"display: none;\"  (change)=\"fileChangeEvent($event)\">\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-picture-carousel/plc-picture-carousel.component.html":
  /*!*************************************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-picture-carousel/plc-picture-carousel.component.html ***!
    \*************************************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedProductLogoCustomizationComponentsPlcPictureCarouselPlcPictureCarouselComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"picture-carousel\" id=\"cdkDragingPictureList\"  >\n<div class=\"card picture-add\">\n  <div class=\"card--wrapper\">\n    <app-plc-picture-add (addLogoPicture)=\"onAddLogoPicture($event)\" ></app-plc-picture-add>\n  </div>\n</div>\n  <ng-content></ng-content>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-picture-thumb/plc-picture-thumb.component.html":
  /*!*******************************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-picture-thumb/plc-picture-thumb.component.html ***!
    \*******************************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedProductLogoCustomizationComponentsPlcPictureThumbPlcPictureThumbComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"card picture-thumb\" [ngClass] = \"{ 'removing' : UX.isRemoving }\">\n  <div class=\"card--wrapper\">\n    <div class=\"picture-thumb\">\n      <img class=\"card--thumb\" [alt]=\"id\" [src]=\"src\" >\n      <span (click)=\"onClickRemove()\">x</span>\n    </div>\n  </div>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-position-item/plc-position-item.component.html":
  /*!*******************************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-position-item/plc-position-item.component.html ***!
    \*******************************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedProductLogoCustomizationComponentsPlcPositionItemPlcPositionItemComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"card position\"\n      [ngClass]=\"{ 'active' : isActive}\"\n      (click)=\"setActive($event)\">\n\n      <div class=\"card--wrapper\" [ngClass]=\"{ 'checked-card' : isChecked, 'no-checked-card': !isChecked }\"\n      >\n        <div class=\"card--col card-checkbox\">\n          <input type=\"checkbox\" (change)=\"setEnable($event)\">\n        </div>\n        <div class=\"card--col name-and-colors\">\n          <h2> {{ name }} </h2>\n          <select (change)=\"setColor($event)\">\n            <option *ngFor=\"let color of colors\" [value]=\"color.id\"> {{ color.description }}</option>\n          </select>\n        </div>\n      </div>\n\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-positions-list/plc-positions-list.component.html":
  /*!*********************************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-positions-list/plc-positions-list.component.html ***!
    \*********************************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedProductLogoCustomizationComponentsPlcPositionsListPlcPositionsListComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"position-list\" >\n  <h1 class=\"position-list--title\">List of Positions</h1>\n  <ul class=\"position-list--items\">\n    <li class=\"position-list--item\">\n      <ng-content></ng-content>\n    </li>\n    <li *ngIf=\"false\">\n      <hr>\n      <input type=\"checkbox\">\n      <label> Drop Zone </label>\n    </li>\n\n  </ul>\n\n  <div class=\"buttons--list\" >\n    <ng-content select=\"div.buttons\"></ng-content>\n  </div>\n</div>\n\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-preview-loader/plc-preview-loader.component.html":
  /*!*********************************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-preview-loader/plc-preview-loader.component.html ***!
    \*********************************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedProductLogoCustomizationComponentsPlcPreviewLoaderPlcPreviewLoaderComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"preview-loader\" *ngIf=\"loader.isShow\" >\n  <!-- [ngClass]=\"loader.transition\" -->\n    <img\n      width=\"100px\"\n      src=\n      \"\n      data:image/gif;base64,R0lGODlhyADIAPcAAAAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d319fYuLi5qamqioqLS0tLy8vMLCwsbGxsjIyMnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8vLy8vLy8zMzM3Nzc/Pz8/P0NDQ0dHR0dLS0tPT09TU1NXV1dbW1tfX19fY2NjY2dna2tra2trb29vb3Nzc3N3d3d3d3t7e3t7e39/f4N/g4ODg4eDg4eDh4eHh4uHh4uHh4uHh4uLi4+Li4+Li4+Pj5OPj5OTk5eXm5ubn5+bn5+fo6Ojo6ejp6unq6+rr7Ovr7Ovs7evs7ezs7ezt7uzt7u3u7+3u7+/w8e/w8fDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vHy8/Hy8/Hy8/Hy8yH/C05FVFNDQVBFMi4wAwEAAAAh+QQAAwAAACwAAAAAyADIAAAI/gD5CRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izTv02zdgwYL546cJVi5arZFrT8gtnbZguV5Diyp0bF63aqtyK7ZpFt29fu3efnsMGjK/fw3QBB1Yq7pkvxJD/Lk56LtmuyJgTTzZa7hitzKDlKt78U1yxz6FTjya9Exwxw6lVs+75bRjc2LhXz6aZjhju33V348yGCzhw3cJd9jZuHHnylcSZN38Oc7n06dRbRr+OPbtKY9yl/jv3HhJYePHkT667fJ75+PQby91qjx6+SG+w6SOmxevXsGTPYMPNN+WkY59I2eiHmC7EYEPOgSs9o+BcrviiDDcGQshSMhPGhUsyD2roEjYd+oKNiDB9o2AtxoCDIkzk6EeLM+u8CNM5qJ33Szg2wrRee7VM02NM5p0XTDlDwjTMebicmORLx5wHzDlPvpRgeMZU+VI6xXHHjJYv+XbdLNqA6dKV0tmSoZkrcXkdL2y6JKZ7cWp3HTB1suQmc7rkydKcwNFSo58pqSidi4SmtCRzTiZ6Eji3AaeMoykBilswlKIkTn6x4UJlpiYVwyioJpWT46WkmhQlcLUgmepI/jgaJ+SrI3EI3C+0kqRLoDzmGhI4xjnjq0jLsDrosB/xAlyWyH50jnGINtuRNcD5Iu1Hj/3W6LUbcQoaLtx2pM1x4XIkKm6uhFhuRuzFZu26Gnmb2XvwOhQOcGXWexG1v32qb0WLxpbLvxftiirBFUWa2qwIS2QobuI0PNE0v9Ei8UTg4QbnxREFnBquHENUZGzDhAxRtrHRa/JAyuL2zMoOGRzbtjAn1GVs3NTMUC2/faPzQqeG5urPCCkc2ppEG2Q0aEhr+OxvKAUN2tAiivPbLCjxjJvPL3rzWy0o3Zxazi+Oixu4J8mcGs0Q8htbnye1HNvLLzLz28YmoSzb/ouruovSyKmV/KLHoWF6EuGggYziL78Rg1LGseEtotypHYMSxbhZ/KLUmTFc0sOxqQthjD2ntHRmbMNHIroqqR2a4yJaChrcKCGeGe0Qug6a4Ci5HVvT6aUDnDUq3fsb2QdyA1yvKcmL2aQQKnM1S+2m9u6Beoe2C0vnxpbugeScjlkxLCn/m8qs2Yob8io5Hxna8IkdGtYtAb626sDh2dLquF3vXfahSd1JnvabaFEHWMDxF0sACBpmZQdyfnuJhL52rOSsQ2sug4nVgCMs6jjDOBGDSfVSQwvm7SYcnMvM9mKiPtwobjeMI1dMYgUcz5EGcxVTIJSM06rZlAOD/rix3ExMZRzDkSYYxqEF1WLSPW2Rhn+/IV9NNmUcT03mHPJLzSxCWBPZpUZ/gbFfbGBnE0gxx4FpgSC6DFgT26XmS2mxG3N4dxPQ4UsrZmMO13LiRS0CTyrpcB9oyJiTPRnHFlixhXRw8UeboMk4kpMK5YCTjZ/0cW9SaSFwCMkTQxoHjE8R49kamZNHGkcXFVTKOnT3m0oK5ZIkZONRwJHCMRLFk6NKChSZw8iimJI50DOK9MLjyqKokTnB0OFPzoFELCVFlMBpklCwkcVPLmWE0jmST8rRzPCsUJXzOU+QeDINIF7nFqlESjkE+Zsd4SQcMTzPLJaoFK/RZ0bp/nTJOpxRS+N4Iyq/5A6LZKkScBjDnMScygQVZCKWYIOB4aHbVDRJnw+JTiTkSEY1z4M+XXYoLhVKhjaUiZFzaCMZvhDfeQQYFTt+NBfBmAYXJyKOaQQjFx+lyx6xQo5+Kog//gGQgAiUjnSU4xvcwMYzkjGMX/DCpzK6KFbOgc2cWpU7uyApVtYBzat6NTbAyGda3PjVskaGjpvpm1nXChkh7mY7bI2rXHBRTOFYR65sJQYpWQNXvF6Vrva5q18/qlcN9XWw7QEsigSLWO4Utke1UWljM+OKYex0SK5h52T7MgtiEDRJpoHqZmlRjJnGqTOi9SstjkHPPFWGlYjVckUytJoocCxjknLlxTI+m6pzWMMXmtXPLHxhDdoiSxt6Ca5xZrGLYuRLYmxxi2RB4wpdDMMaJqwZV7wCFrGQxSyuoEUtcKELXvgCGMMwxjQum7T2uve98I2vfOdL3/ra9774za9+98vf/vr3vwAOsFYCAgAh+QQAAwAAACwAAAAAyADIAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NkZGRlZWVmZmZnZ2doaGhpaWlqampra2tsbGxtbW1ubm5vb29wcHBxcXFycnJzc3N0dHR1dXV2dnZ3d3d4eHh5eXl6enp7e3t8fHx9fX1+fn5/f3+AgICBgYGCgoKDg4OEhISFhYWKioqWlpaioqKwsLC6urrBwcHFxcXHx8fJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrLy8vMzMzMzMzNzc3Nzc3Ozs/Pz8/Q0NDR0dHR0dLS0tLS0tLS09PT09TU1NTU1dXV1tbW1tbX19jY2NnZ2dnZ2trb3Nzd3d7e39/f4ODg4eHh4uLi4+Pj5OTk5OXl5ufm5+jn6Ono6Ono6ero6erp6erp6urp6uvp6uvp6uvq6uvq6+zq6+zq6+zr7O3s7e7t7u/t7u/u7/Du7/Du7/Dv8PHv8PHw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLx8vPw8fLw8fLw8fLx8vPx8vPx8vPx8vPx8vPx8vMI/gDtCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izSj3nzZq0ZsmI/fLFS9etT5909fpVDJmyZtCsidMaNZ21ZsTOot3Lt69fvryKMZOGjR3douKiJeP1t7Hjx58OCxXXrBfky5jRApPsk7LlzKAhM+Os0920X6FTQ55G+mY6Z7pUy3ZcrjXNcMr0zt7NF5dtmdWG8R7e19jvl9p8EV++19lxlu6YMZ/+ydpzldZiU2d+7vrJc8e2/k/v5d1kNVzipycrP9KdsvTUo7EP6e0zfObZ5n+Mpvv+cnf6cXROMf5Rt1mAGomjXYHMjYYgRtmgx+B01Dx4kTUT9uXLMtBIQ8012ohzDoDupHOOON5kQ40zyfyiV20WUjRNhp/4wkw16Vh0jnUxTtQMg7cog2OPLjlT4DDTAEikS9Lcx0szMC7pEjXp3YIMj1K+dE16x3SXJUza9LdcL9h8GZM4EjJ3SzRKmukSOwsul4yXbr5EIHO4VFNnTNBMN4w5e8LUzXTQBApTOowRh0uZhto5ZpSNshTNcsPkGGlL5ogpGzJtXroSMsQp06mnKWE4XDKjknoSO4nuZkyq/qqa9MxwxcAaK0mZ8uaLYbeq9N5ut0Daq0nmDIflsCdJt1szyKZ0Dm/D2NosSMrKdgud05J0jqagFZqtST/O5ou033LkTpqqdVNuSdXstsy6JYE6G7bwfpTObg7WG1KTswGqb0jEzKbMvyGVs5uwBG/Up2zGJfyRMbOx5nBH7nB72S28TrwRNgJr3JGRsl3jMUfCqaYLuSNDdK9s+aZ80ZayHetyRQurZunMFi0j2y84Y1Ryasz2bJHFkMksNETFyobw0Q6ZqhrTFNUcWjBQ+yjbwFVH9Gtq3mb9kLypGe21Qj+H5s3YD6Gm2tJoH9RqaDe3rRC6oKEstz1EO+bb/t0L0Y2ZLnz3rdregSP0dma3FJ6QfaEpjhDjoDl+kHKqZSy5QMHI5u/lAkGsGqOc21NtaBKHPqtqz4QuEJWqvat6NrIVo7o9zw4+uz2zzSVlONbEjRPlqUmTZbi6HBNNfjeNDtp6UgLP1y/MUMP2SvymRniPtT+GizHPXGO5SgZrviTroPmijDTfm+Q3Zl3HqLNs6ZfkeWpU93iubMOwBLJqulvodGrOWcn/QhNAC20tNSJbCTtmwwu7/YYdeXMMxloCtrBZiHypkV1LMDg1C4VHNu1Tycpkow0EJU02/WNJBUODDAS9TzUHcsmMcqef7KlGPi8ZoWqYxx7lhYZe/iyZ3+fYY8PUtDAm7RKXA7XiQ9DoKSbugFxohHcdbgBriSWZ1Gx0ET/JuENtsgmaTO43GzHapnqyAeJL9kfC35xjfZlpmUyKmBpe+I4u7rhTv27ywk21Rmo7xMkJZ1MhycCONymsSbhmE6zDnCNOqjmGTtIBydT44o5UcUfmeBOOnaARf1hMinuGY0acuMN5suFhVfooGy72hGPDKeBUmpiaJ/YkGcQJ4VMW6SqgbIs4pWwKLVOjRp1wcDZyVIo7PjgcKgZlhbNBFVPOAYzlNEwoiFpOMTA5lHIcjoHc7Ik2mMOLsx1lGhEETQmNcjri3MKZQjmHEIeTuqO4o2zD/kFGF3VynukQI5Q5SYcUd8MLseHkHLicjh2XUo5K8sYYmzNlM9IZmluscyndoChobuEMgJ5EGg4dDuiYMsDl8AIaHhWJNVDJHFs6ZYbbwUUzwnmScAQsPfB8ChupYyVrpPQi7qDGTdMjy6jsdDu6YEYnS4KNZGi0Y1bRon94sQxr7NMi5WhGSKnDjJ8S5Zj3CUYzsFFMh7AjG9BAxla3g8OsXOOpagJGMpxBjWyIQ0TpINGJtHENakgDGswAI41cmhVtwJFGiAXNokijoMQ6NjW6SKQj8fnYyvqlGGWlCy8ty9k1lccah+XshHxhzvKYQ7CipdEyvHqVYaZWPLgwuGh5tFHN1/pHGTRljzRCa9vZBKO0S0rHAXvLG10U0k20JS6wOtooa2xSuZmRaW6/lI15QtcvvJAGay0kjoRe93mE7VU6olFb5T5pqevK6jcfm9SLEiwxxoArnpQxUo+5QxvPGIZ8U3MLY0DDvUJzBzakwYxirFc1/f2v4sRhDcAqAxnF+EUvtHMLXfDCF78gRjKaEQ1reCOztwuxiEdM4hKb+MQoTrGKV8ziFrv4xTCOsYxnTOOrBAQAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXl5eX19fYGBgYWFhgYGBycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrLy8vLzMzMzM3Nzc3Nzs7Oz9DQ0dHS0tLS0tPT09TU2NjZ3d3e4OHi5OXm5+jp6+zt7e7v7/Dx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8fLz8fLz8PHy8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHyCP4ApwkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs0qN5YrVqlSoTJEiJUrUJzto0YoyhSqVqlWtXGmFGquVqlOi0urdy7evnVKpVrGSO5doLFaqTPldzHixqFSsXhXu+WoVqsaYM/s1pYrwZJqVT2keTXrvqFWSP79cVaq069doU7VSrfKVqrywc7sutQoWbZKuUukeDpvU75CsLhNfXlrVcY+tWjOfrvlT6ucZXwmnzh1zKuwaV/6B6k6esWfwFFuRKs++73f0FGNtb08/7Wz4El2tr8/fFH6Jq5zFH3+s/PcQLPMNSN8osRjYkH4KDriKgwy1gluE9IHiG4UJtSIghml9IsoopZhySimkjDJebs5xiNAqGH7SlmCuNKhQXYihMspo17lIkCoDkhLYeRTBwkoqF7rno0EJljdKZyDZtd9eRLooX3ufyGZSK6l8iMqSBDVJHSmr2IhSZTveByaQ5KFS4EtqLskmd6jECSZMMHJXip13wskdKW/2KdMrKzKnipmCwhSLdMuRwmeiLomZWyqIQvoSK8yN8qilLLnyoW6obMipolPqRumoMs2pW4uowuTKcv4TthqTaMOxKqtLea56K0yEDvferpH6WimwKbUy3CnDEouSpKOB0qOyKb2q26bQlsSsZrZWi5K0xSWrLUnXZkbttyG9Yiq5KuXq2iiioltSLDvCNq670OX2Jb0nhdvYvPhuZC5s9/ZLkrql8StwRqWSFvDBIXHrWpUMd0TwaKdEPJJyrsVq8UewfDraJ+1uzBGmr/0qckeqkmbwyRIlrNkoLHsEC2wmx5yRsa+tbLNDE2vm7c4T6evXwkBfRGtpGhdtUSzyKo2Rw6T97LRDOJdWytQX9ewd1hZp3VjSXEeUsmaBhh2R0H1BbDZDGJP27NoNuZyZ1HAflGR1dUdU6P5oMOf90N2ZGee3Q4BjJvjgDMVLWt+IL6T4aKI0nrhroEi+kNyNfWK5Qpg3FvLmAzFKmtqbtz2aznmjzRfYoAs0dmbZtj6N14zVLHvVpB0uu0BQ87j7QDO/VvbunTMWe+uq7+Xf7wLRztjnoONO2vCtM10y8wIpRjndiL+eGfWgk+ya7aD/69on3A9u+misg+78YqSkn7f5rqFemCuk73R0aUTDd9kp4NvJ+xaTP9rg7jTQwwn9SkO+56wvSwWcyf4Khp/e6aVOPBGfayrmP8OxQn4riUXhyIYe6TFGFKp4m0wG6Jf4YQdepYGgTV7hidy0bzLey0z/YJK8vtgvKv4WHE0DXRJEzbiQNtqDjQphsj7SHC8rOczMEP00rc+YsDRLjEkP+SKKLE7lFSPElk5e4TGrJXArSXyNdXYSRR2CUCltxMwTaQKLx71mjk9hIfzOSBMNwgaPTNHjYn7YEtH9kSp+vN5PikiaKSIlkZTz4k0EuZhTOYWSfrnhTproGlRIMihXIs7yhNIr4pAigj6BBSdLAwpU4gSSagwgUNTDHELSZIvGM0ocRwPInsCieFazJRHTeKw35sQVeyPOoX7CijLmZhSf/ImHqDMKWc6kFRMkjjBf2R0M3kQ73bFmUTCZmVNs0yPaceZwNImUXb6mFOIsCZfUWaup4JJvKf5USSxYwx5HJiWU7HGTMSdipC61x5JUcaduZLQKV0KkMqukjj+bQs7iQCaaCrGLIdvTyzyC6BOmCMxgssgVr4CFnuVh51RgGSNSnMIUpRhFGBUUz6lYCEQ4bdY5lfKKbOb0p2kZhUOrolCg0seT6GFFMo0aoU+o9DOu2ChT61OKoc4lFqpA6VSps0wOucKnW52OKXaqFVbMNKyvqWaiEITW5Xyiq5AKTlsnhVEXpXOuQrSqj2yj1bZmqa6J2qdU24pAegWnrziFJ8MOE9EI8Qaw1XrFkc5anscqzRWWQaxuABOZtbXiK6fQbGNGgQpVtGKgQHPFZ1UBFlPcTUSiGCYLW0TqCj5i77a4za1ud8vb3vr2t8ANrnCHS9ziGve4yE2ucpUVEAAh+QQAAwAAACwAAAAAyADIAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NkZGRlZWVmZmZnZ2doaGhpaWlqampra2tsbGxtbW1ubm5vb29wcHBxcXFycnJzc3N0dHR1dXV2dnZ3d3d4eHh5eXl6enp7e3t8fHx9fX1+fn5/f3+AgICBgYGCgoKDg4OEhISFhYWGhoaPj4+goKCwsLC8vLzExMTIyMjJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrLy8vLy8vLy8vMzMzMzMzMzMzNzc3Nzc3Ozs7Pz8/Q0NDQ0NDR0dHR0dHS0tLS0tLT09PU1NTU1NXV1dbW19fY2NjY2dnZ2dra2tvb29zc3N3c3d3e3t/f3+Dg4OHh4eLi4uPj4+Tk5OXl5ubm5ufm5+jn6Ojo6erp6urq6+vq6+zq6+zr7O3r7O3s7e7s7e7t7u/t7u/u7/Du7/Dv8PHv8PHw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fII/gDTCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izTh2nrdozZcaC/eK1y1amTLt+DTumrFk0ati+aZUarhqzYLPO6t3Lt6/eWb+QRcs2bu5Rbc+M5fLLuLFjW8SWXTtn2Ge1Y2Yda97MeJaxaeUq2yw3zVhezqhT9w0GrZtomNmMqZ5Nm+8vaq9XTvtVu7fvXM0K5x45jllm38h7H9M2/GO5ZcmjJx+2rfnGZ6ela1cu3DpFaou3/ovvPYsZZe8Qs/Uaz/53NfQNx8luT7+3se7wDVY7Xr+/alvT5FdQOcf4Z2BtxIQjYDrZ7HLgg7PN4hp80EFoIWrDwDcObxd26Fgu+A33jYMelugXNuhhw5+JLDKDHjUeBnMMM9BQk8023YxD2TnfaHPNNNAsgwwx4bH3C3rNQNhLMtNwY1E41CTDy3azKGidMgbOckw153E0TjXLcOhbgNYx018uy2RjUjjLZKfaMd49Ux8x76l0TjQkosZLaM1F054tzMj1kjXDoFZdc9KwN0szNWEzpWPPWGcNe8mEOBM0bu5FjHXdZJqcMYLmNE6BfIHYXDmPSmeLNT+ptxeK/s0Rs90wlvIUTV6MNpekdtEUVZx11WiXy6EL/hSOp7X1YmWxP8kaHTFdMtvTNNIhIy1Q46xY2zLXAjUfctZ26xOMycEpbk/naDubMef6JCdyv0TbLk7lIJuaLcvOm5OZyKmpr07hJAfNvzslg5y5BOP0DXK58JnwTcggx+rDN5UDLsU4QfObwxjTlCdtE3dM0zW+sStyTd/S5uTJMwXcW7gsy/RubaHGDFOhtSVjs0zj+Fbzzi0lWlsxQMdUTG91Ft2SxbXlovRLwdYW6dMtYVlbvlSnFExtR2bNUm/OeL2SNr0xJ3ZKQs9my9kqWT2bzmyjJKZquMVt0jm9TWg3/knb9La3SSTTFszfJVFLmzKEkzSzar0mLlKFs4Xs+Eekzmb25B/hPNvPmG+Uqmocd75RkaqJ/hHpqZnuEeqoqd4R65y5zhHsm8k+em22a0S7ZrlntLtjofcu0eepESv8RM5GfnxFEdMmzfIU7Trb1NBH5Ofh1Us0KW0ZZg8R2bSt7f1DLtPG+fgJIY2+Q1vT5uL6DLmtWvfwK2Q4bfLWX1A3vUmuv0H20gy3/oeQ5KnGaQQ8iPRmY7wECiRqtKGeAwXCNNrwYoIFMaBqYIXBdKRtXR2koG9qRUDNTS+E6bhe+FDYs948L4RHq40t8kfAwNUmbCEkXmqqFMIPzgZm/hNMl2/8hcEFzqZhHSxfbUyGwcrVJmmVcQYJjdI330hINBqzBRSVIj/a7GKKU8HGXhDHlHKoKzXBoOFUwrGiXugtKSpUTlbOsZ6+zOKFStHhCa+iQb4YI3hEsaFvGkeVlDEmF0Q8iiGlRhUnagaHR8lWdN4HFUduJhhYGwoEkQO3ppxjkZzRIlIMFp0/MmVDyCFjUc7xMd/s4nJHGVF03GiUbGjnjkjJxhlps0WhOGM7xwCku7YDSaPEUFVkAso35pacTSWFjuIJxht3Io0AWlCYQwnH72izKDXORBvMTM4VmbINa9ZmF9fASThAmRz/KWV74zkGGFXynPbg0Ska/lNUMhq4km4ow5y9ISRUFjeeXyQzJdUw4XgEGpV80mcWy5hmSK6RjG0mR4JT8WF7eKGMasxzIue4DEAnmZX7GagXHcVmQ7gxjWTU8UAYtcomHzSLXhDjGMuARjWy0Y3QnGMc3dhGNqgBDWYco30X6uVVdMmipjbGFrCcSzhe6tSq8uJ8WjlHH6saI5VqhV9cLVGu0FONkYZVPLtIJHrCsdWztucY3rRONd1aH1F2axxtpSty7jMvauxSr6rphVrP9RyzArYvuTgowcZBysOiZlEsC4clHdsXee7sG22iLF8gilWWSUOPbu3FPbOGDXY61RgcjNs4pHFMFnkGNJgLXOkxDEulLeXOGstAKn12cQxp8LN3LFXGL2jbmWEwwxofXd42pNEMIR2jGMHoRVnOYotd8OIXwTCGMnRKGBR697vgDa94x0ve8pr3vOhNr3rXy972uve98I2v3QICACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+fn9/f4CAgIGBgYKCgoODg4SEhIWFhYaGhoeHh4iIiI2NjZWVlZ+fn6ysrLa2tr6+vsPDw8bGxsjIyMnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8zMzMzMzc3Nzc7Ozs/Pz9DQ0NDQ0dHR0dLS0tLT09PU1NXV1dbW1tfY2Nna2tvb3Nzc3N3d3t3e3t7f39/g4ODg4eDh4uHi4uLj4+Lj4+Pk5OPk5OTl5eTl5uXm5ubn5+bn6Ofo6efo6ejp6ujp6unq6+nq6+rr7Orr7Orr7Orr7Ovs7evs7evs7ezt7uzt7u3u7+7v8O/w8e/w8fDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vHy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8wj+APMJHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNOXedtGjNkxYYJA/aLl6izaIEZS7bMGbVt4rQ+fQcuWrJgaPPq3cv3rDFm2drJLdru2rJhfRMrTgws2TTBg3m2k1ZsseXLfYlBIxfZZrtoxDCLHq1XWLO4nV9qO0a6teuzwajFS53SXTRgr3O77sUMMm2R5JSZ1U3ctTLUvzuiQ1a8eW5j4JJrbKfMufXcydZJr/iu2fDr4En+82L2bjvEeNJ6hV+/W9ps8wvF4WVPv3Uw5PANQvtev79oaO/lJxA6lfln4GjEoCNgPtTwd+CDi/FCDXzvJAPhhaIlU15y64SG4YeWDaMdbeD8AuKJi/0SXWfZoOjiYtlE5gyGwByzjDTbgEPOOu0EKFA75HhzjTTMFAghM3Ix8+AxznizYUXiTKMMbgYqk5WS/Q3TzIoeidOMMP4tc9Uy9SFDjW8joQPNfOwhOVU8ZK7HyzIjquSNcG1KFU914fXSjDswvUONh+BBE1Uz65FXkzZUXjehUy2ClwyaNMUTjYO68bKNU+Bgqtsw3vC0DmvF9cLlUuuY6Bwv0QDVoG7+wCjY1DtgOnefUOsY81oxlCYVD3POLeNjUK+OhsyTTElj3TVIkdOoZcoMqxQ6nrbWS6hJtUOoYoY+Fc+2uQUjq1LvAJsYs1BF01wxyC5lIV+8dBMVOeu2u1Q87+b1C35NfVucMfYyhW9ewdT5lLLEAUzVwKIUA2hU8aia2zHSRpVMMhUzNeOnGUPV8VLtVIvZL70u+BOWr/FyqslArUNcjCwPxedrYsYsVDu6BROwzTshmhu/PPP0jnqvNRN0UNXkNszHR9tUq2tAN51TN9hJ/ZO5rRlsdU7u5Obm1jtNk/LDYOuE9WhGl61T169prXZN17yWzNs6zUzapnTfFI/+yIoBw3TeLGnz2jOA34QyaVEX/pKRo/2ieE3xvPb14zGJ85q8lMtEzW5/Z25SnKQh47lMjItG+OgvRe6aNqi/hE7brbtEtbWxuxR3a6LXzpK6rdWsu0qgj9bt7ynZLdqjxKNUOmZ4J3/Ss6Jx5vzzrpU8PUjQY0b29SNlf9n23IfkvWXgh/+RxKPtbP5G6Ium/voZtY/Z+/BfJP/39YPE5mjS598RqaTBnP84YjzMwGyAGzmcaKSBQI5AwzVpa2BGbkeauUkwI7MjTTAumJHXuYZ+HGSI6lqDrRBWZHmXYaAJKxI80VhphRTZXGt40TkYGoRerimhDR+yNwjuUCL+umqNMH4YEQWKxm1ETMg2XtOqJDakh63xmxMbUkDmTZEhSXNN7q6YEJzBjosJASBpfAdGg/DONeMqI0E8aBw1HkSMiHNjQSKlRTkSJGK5OaAdH/iaXpSvjF58zQvtmI8qiqZ5cgSHbn7xRzDCkTQUI6TldDM8ORpSNDpUIxtf4wskctGIpCFGDW3oDr5dZpBuPGNuoiXHeDxtlaNc4SSJw0o38pE4ZFyKO/qXFX8VR0NMcdYvPEkVHBaHGMQMijaGE4xGUkWVi1zZUG55FnbJ5VerauJQ2vFIUSAjlkt5B7h0Y4xk4iQbROMLKrHSjvulDF09eccl0VJJrIjDlBX+dOZMquHOvVRjMEu8Ti+iAUKWeGOci+HGYMQGnl8gTybUeU3inrmeX0RDnyYJDj4T44s0YoWa1+GFMniJktU0BxgYjQpIwXMMRI4EHMwY32uIUdCnrBQ8vUgGNTy6EZj2kzhb1MrG/AMMZVBjog6Jhzec0c3wrBMrCIPQX6RxDW+Qg1LxaMc6yAGObURjGceQKX2keZVupPNFaD0LL/QoF3TsL60o2tdv3NFUuD6IGNa75jztWp9abgedfH1QL+AJH24G1j/HyKt0rrHRw87woSwblWOvU06pMWqyxAEGYZsWj/1gdobRAGd+IvpZzCwjpUcTRxBL25dk8DRvGmWBLVpESlLKtaMZP00rMAj6u3hUAzGHPYZCr0cOZwD3RcaQhmKJt45ooNBAx6AGaqfnDm0UqT+8YJI3RMu9dyy1rvZJhjSQakNxZIMazVhGMoyRPV78AhjCGEYxkMEManjDnITMr373y9/++ve/AA6wgAdM4AIb+MAITrCCF8zgBAcEACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXJiYmMnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8vLy8vLzMvMzMzMzMzMzMzMzczNzc3Nzc3Nzs3Nzs7Ozs7Pz8/Pz8/P0M/Q0NDQ0NHR0dLS0tLS09PT09PT09TU1dXV1dbW1tfX2NjY2dna2tvc3N3d3t7f39/f4N/g4ODh4eHi4uLi4+Pj5OTk5eTl5uXm5+bn5+bn6Ofo6efo6ejp6ujp6ujp6unq6unq6+nq6+rr6+rr7Ovs7ezt7ezt7u3u7+7v7+7v8O7v8PDx8vDx8vDx8vDx8vDx8vDx8vDx8vHy8/Dx8vDx8vDx8vDx8vDx8vDx8vDx8vHy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8wj+AMcJHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNK1bbM2LBeu3LRkuVKVak4cVjJqpVrV69gxI4146YVqjZlv3KxQsu3r9+/fmPpErZMW12j2o7p2gu4sePHrHL9Ukb3ME9qv2o93sz58adcxipbpontF+POqFP7/YSrmOHRLrkZu6W6tu2+n3Q1g62SGq9Ut4MLl1VMNO+Q1HAJX748lS9sxz9i08W8OnPd0TVq4/XJuvflt6r+Za/IrVf37+iF9zI+vmGy0+nj21ZlrH1DbdTl6w9ei5r9hMWctd+Atn0SzH8FVaMZgQzaVgt0CBpzXoMUplbKMf/1UuGGtfHC3mjaLMjhiJ3FAiFs0cBH4oqOpbLbaMtMuOInrMQyFisyzoiMZcZwWMotvRwTTTWvHaTNNdQs8wsuwHE4TF3EUFhLL8hcc9E1xvQiIoO8ZCUMg7IIUyRH3CjDiyoM7vIhVBru54ov4pUUjS+yDNjlVNzwst8sO660jHL69TKVnvLNogxM13An3y9RfRkfLcvQpI158QnzFDLx1fKiTdPF9yRT0OTInCoY7jQNbegls9Q1TXrn4U/+xghoXSlWIsVNLN+5sulP2KBqnSxJEWrdq0QNI2pwdxaFqXWfFIPUNGha5yxR2Mi6nCv+JYXNLMxmGxQ3WwqXy5i25vLrmjsFY52gT/1iHaNARWOdL1INU90ntfZ0a3UHTgVMdbj8FCVzn1LV5nL18aRNq8EBg5WwwaVC7k0HB7eLVoAKd6hO2BybGi3obqViarmcmFN+wbUysVXReOxYLJHy1Axz0YzWI2qpFMyTr7f1O1rFjvGyMk7ULDdLyFdxA8tj4QUFcW20RrdMY7FsDJQ2y00b3dOlCIN0Tv/y197CfAlNFDcM1+ZtdsXEQcvaQt1828X/9WkUz7Xli2D+UVgHx+7eRtkb3NCA/4R3anQXTlTft9WsOFED20bL40VlXJvWlF8d8deZ37Ssbcl2/hPQqVktuk/holYK56fPxI3LjyXeOk/y3pbw7DwJbhvruMO0y23A9s4TrqALvxM3wRFjvE7XBBfn8jfNbBsp0Ockt2q3VI+TusVrb9PTqMHrPU3m2qb8+DRxa1vM6Mv0ym16t/+SK7fxLj9J9Nt2v0wjc/bJ/jHp32ZSAUCYpK0zBCygSw7IGVcocIG2YcUDWyLAzUyQJeqrDeEuGBLLpeZ5HDQJ+DqzqxCSxBe3sZsJSeKo2uhshSK5XmpCB8OQKOM2s6ghSaoRHB2OBHn+t4GbDzuSQdVgbogd+V33kOgR3alGFUz8iPRs47goboRxtaGXFTlCvNrkcIsbGWFnQAhGi3yuNn8ro0W0ATvHlGKDanSIB1NzxDhKxImqqYUdLdI85+2xInWa2x8pErYgDlIi2BDXISWCMtuUcJELOWNt9AhJh3AjWrepYyUR4q7gnAKOmxxHIoUju1AiRInBYZ8pD8JD4bgClJtEpSBXiZA+CueFtByIGFHziSrmciAdWw4rYAlJ0tXmFvYbJNmWQ8NfHqM6zcxl+ZaTxl8Gkzni++U48Og3bQ6EFtbBBTH/eA1rDcdktEyGd1ghRFOikFm4DCU3DiccWsQvlNr+qGCBzkdLaKRnFo+spAytk4t7yqQZtVCV4iL3nU+YbSbLWJArkmmZFqLHoe1MSUT94rPCvbNQofETOP9CCnTubZez0gUyKEoRaviii4CJJoJkqZ9P7OIYLG2IS2H6GF8CLk8U+kQteGEMk0KEG80QRi4wiZovUo6bDCqFZJZBjWsQThvViIYxeFFE23XumSziC41sFAscxQeKnWsGA8OqHy1m7ho8ZWtNjfpTlMrVO7lo3THMedf0qDJz2EhdX70Ti5weRxhtHOxt+Hm6aORPsd55I+64EQy+QrabvdvOZZnziWzi7hrT3GxqPtGLcT6uGV0VbWMcatrOLSO0qsWADS/oqj1s9GKtinVFMGiLPm4UI5CQzc1fL0iNYKR2RbIYRmvvpw1i5CKx8WHNMMjIxDLlhanykUUvhmvHu+RFn6mJTDCUsdwyYiMayigGMHqhC1zIon+pmIUuekGMwnjzvvjNr373y9/++ve/AA6wgAdM4AIb+MAITrCCF4yUgAAAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXl5eX19fYGBgYWFhYmJiY2NjZGRkZWVlZmZmZ2dnaGhoaWlpampqa2trbGxsbW1tbm5ub29vcHBwcXFxcnJyc3NzdHR0dXV1dnZ2d3d3eHh4eXl5enp6e3t7fHx8hoaGlpaWpaWlsrKyu7u7wcHBxcXFx8fHycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKy8vLy8vLzMzMzMzMzM3Nzc3Nzs7Ozs7Oz8/Pz9DQ0NHR0dLS0tLT09TU1NXV1tbX19jY2dna2trb2tvb3d7e39/g4OHi4uLj4+Tl5OXm5ebn5ebn5ufo5+jo5+jp6Onq6erq6err6uvr6uvs6+zt7O3u7O3u7e7u7e7v7e7v7e7v7e7v7e7v7u/w7/Dx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLzCP4A3QkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs06NtoxYsF65btmSFYsVJUqxZNm6tcuXMGPNsmmV+myYL1tn8+rdy1cvq1q7hCnbNvfoMmC3+ipezJiSLV/EohX2mY0Yr1iNM2vmK+vXssk3txXTtbm0ab2xeiEjDPplM1+uTsueHQvYtNYqswWrNbu3713KcJek1su3ceO1hLEW3nFa8ePQfbPqBY25Rmq+zEbf7vuXNusVt/4F0869/GxXwsBLVMbbvPvetZCpb5jt+fv7s3dVn49wGGb8AMrGSnr8EURNLgEmOJsu1BToDjL/KShhabHIp942v0yo4WnALIfbM3htKOJmt0iGmzEjpriZK5+BRoyKe6mFSy0RqsiKhXMFI6IsvQRDjDHLRPOdQdpE04wxwewii4jEzJWhhK4Eth9F2ygjzC7kBRgMVtvskiAruQTzjEdV+rJkgL54GJU2CAKoCzFqggRNMInh5wtV27T5Hi3BNKgSNLzgB8xUgb6HizEwRVOoewQ+paN7uSRDUzReulfMU8O4t0szOC1TZ3k4KoWMebW0qFMxZ27HiqlIOZPlcf6sBBMnTuK9apwrfh6VTarQ4TLlT9F82uusQVUa3ZZFAcPdoEa9GJ0rkh6lTI3GhRoUNLbOZouJSFEjrG+4DrVNiMfpMqRS2pA2rFDKQscLsUc9eRyyPz0TXS/wIiUMdKxw2xMu0DErlbPG7fITiscJPNW+xyHK0za0HLdLvk2165ss5+b0qG+3UOyUfb0pfFM2sflGi1xZ5XncbTnJS5u/WO1q3C85QXOctVktc1yuNYEsWy+4bTwbzTZRYxzGwuk5G8s0WTxbcMJh211N2pQ8m8itOS0b0zExPBvS1m3T3tUzQexbk+rp3JssHpuEsLYFGisbzizJfRqr4NnbG/4vMW3zm4Pu2F0aK22PlGlvzgCu92yNtqSubLkALpDgm+Hykt+90a2ezbOxwrNKh8tGi+QDKZ0ZK7wYU3hIj582DOkChc6YK6mvLlI2vREOuzvazN6L6jQV0xvQuwfOV2qr3aR1aZoX+HZq0eZk+maxFD9QLb5AvdM22WZ2p/VBNdMb3uDz5LVprpQflM+bGaz+T2ObRu/7249Pf0/RdG77/SypfVrk/NsJwUzzvQBqbDbzM6BNXFaaSykQJ5TLDPkeKBOAyQZlFKxJxGSTwZts0DSs6KBNPlia9ImQJryi3glpQi3NjG6FMmlhZl4IQ5hYrTQ0rKFLujc7HcJEhv6ZyZgPVULC0nxuiCiZnmbGhMSVRLAx2msiSnwxm9dJMSXL0wzRrngSoZUGgFw0ifBkY8IwloRzsjmiGUHCQ8Y4bI0jsSCH4EgSKsrmFnQcyflMo8Y8akQZvUGbHz3Su9nwbZAfkSP6EPmRLGrmjYzUiP9O475IaoR7vcGgJTHyxMY0bpMW2WNp8AhKjOTPfqW8yLdKo4tUXkSUpZmgKx2Cu95UcpYS6WRjEodLibxNNofsJUS2AUTNMEOYEXGkZmyxv16esjcJROZC2De4X0lTIWicTceu2ZBFkY2bC8nmbCAJzoN4k4wwKydBFtebbarzIHb0zbveaZBsFHMzxP6jJ0EG2Jst6lMgSpzjPwUiteP4U59etKUQ1bmNVc4GF5p8ZzRuaDJrqvOXtxIkPZUpm11EFJzbUORxKqRPalD0OLz46DWX0UYBfZKbGI1OLaLITX5uhxdMBCcso5MLmiKTo8exxTCa6cdzmod2wBPmNqhpHtQldSPbOEwubqnAhN6HFbsoBlEHsg1kAAMXWVIp/2waIFrowi3IeIaQ6tmMYgjjF7u4RQr14sAMKqOlCopFLXBBC7zuhaoPbMY9YaQZVixUgdMQKWFPU1cRWnWxmgnmCacF2dPoDobZaF1lM0POEwrDr5vNZw0ptdnMuGKr6ktG/ErLl+Z59qSspZKEaIeojXjG9iyn5SI0mFpZnyIxG8AYbIoKGMZtDGO1i61eHo0R0BTJkovUEIZiR4Q1OmZjGLoAbXliwQtiiDWPUQ1GdgOEC2A8F5TNuJJwC2uLXgyDl+/UBjSU4VZg9EIXYpFFyViRlrXkokfEUEY6B0rgAhv4wAhOsIIXzOAGO/jBEI6whCdM4Qpb+MIYHkpAAAAh+QQAAwAAACwAAAAAyADIAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2OHh4fJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKy8vLy8vLy8vMzMzNzc3Ozs7Pz8/Pz8/Q0NDR0dHS09PU1NXV1dbW1tfX19fY2NnZ2dra29vb3Nzd3d7e39/g4OHh4uPj5OTk5ebl5ufm5+jo6Ono6erp6uvq6+zs7e7t7u/u7/Dv8PHw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLx8vPx8vPw8fLx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLx8vPx8vPx8vPx8vPx8vPx8vPx8vPw8fLx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vMI/gCpCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izUgW269asWK9asVJlCg+eU6pYuYo1yxYuXb2EaY06bJctWKvM6t3Lt+9eU65s8So292iwXLFQ+V3MuLGqWLh+ES7Ms1guWKcaa97MWNSrXJMp0yy269UozqhT9x31Sldo0S15xTqturZts6xdw05J7Fbm28CBj6I1bDfJYLNoB19+W9QsucY99nLFvDrzWMCiawTGyrp35rKK/munWGzW9/PLS90aLxFXWfTwgavixb7hr7zx8wN/Bb2+wWK06CegcLj4V1Aw+A2oYG2uiOdfLsotKCFqpvRSXzGxTKhhbbS8Bhswim0oImqrZLfbLhGOqCJjpVgoWi4rxrjZKLpQdouIo7ASyy28+AIMMMIMQxgxwfzCSy640CJLWKJsWKBW5i1YSiu18NIfRb3QkqCCtGQFi4Kx5GJiR8ToIstvAsZiVTGvCNgKLh6GNN2Ar8TpVIDxlWLLlSchl+J5s0yFS3yn3ELMS72hiZ4tUekC3ylw0rRLd/A92ZQv6I1ii50x/fIlerk0BUwp57USDE/AqIIefUoNoyhz/qfU+FOU3o3CZ1GtfNegUL28Z90qnAJ1o3frETVMm951adQv3pniC1IQerdLUcWEyFwrDh4ljKrVlXKrT58yB0uwQw1DKXOskIvToNXFou5QxCDLXC1BCUMqc4FCVYws1o3ZU4bMMToVrcGx8hOmzClLFb/MybpTMVve5sq7ShUTLnCnUPwSu8GtcuhVxVC3nMA5DXMvcKZ8O1UxuQbnrU4E3+YvVtUuJ0tOwTBXbGHMLjfzTDHX1spuHN92s03D/JnaKdlSJu9tKrtky3LPGjeMr7bRO9rJtilsXNG1lfKxTGCrZsrY0Z1r284w1QxcqOwBE1zGMjkK3ND+1RKc/sMvtSyzgcRwrRosMQ0T3NEGTg1c0yuVndqpBlJjMnCWtuR3bYhHrvdteLdkOHA/15c0cFGX5Dhnr0ReEMNru3S5ai6qLlAvwLnSEjEoy17Qq6iJ0tIuwGmtu0Cb21a1SsXXFnrkct9Gckqvkzh8QRGjZrtKxSi9GdvTK17bKBpzhLBty6tO+22xnzRsbaZMX1AxwHFf0sWpEe4+QdFzli9K1qpW+f3JS03nTgK6+xGEF7c5RUpyZptRGJAgn7MN2khyPqE9kCCCQ80vUHI6zajpggJRW2rgZpIAokZ4F6TfCVGiQs6Q8IImXM0pWPGKWdxCTCgRWW3S90AYmWWG/q+QxQ15AYwJriR/mynf8IYBDMbRpHqcUSIIa8Kt2pRuijTpX2qciEWbYC014etiSzLIGTHyBDhm3AlwjJjGmXwRNVdsY0uqqBopypElUNwMD+8YEx2qZlp8pMnTRhhImrBONSgs5EvwVJvUKTImHWzMKh4ZkwqqxoGUfEkEawO5TLbkjZzhmydVIkLUeG2UKTlkagyGypV4rzZcbKVIEHgbQMryJLi7TSJvSRIkakYVvETJK1WzwWCWxJKqOaUxQZK925gijMukiC81w6pohiSSjcmcNT2yyUvGcZsV8WMywQkSHzYwluSsyOh0mU6PtJAzo0BnOyUCPOAoc54V/ikG71JTTHxiZH22AZY/MRK44PxvoBOJIWpehtCKCGM5rYBmQwmiytrscqIOaV5wqolRiFRUNaXoZEcdMrngoEKeIy0IQG/DCjam9CDFoCPnJDpRZEqMpg19p2o++NKFCEN7O8XpQLGJmla4tKcDESdwWIHSlBIDlLZRxTdHOj7mjOKFSC3IMJfjiqlOtBildJkos0oNYUAVOLA4akp9AdQE7hGptASUUMlpt/MUSq0YJeptRiELkfZ0q995xfF6+tHvsMKWKS3PgErxClzYsZ0r1Y8pYOHYjppTQqMAYixocQu4NJEgxBDGZ3mJIhk1Bqut7AUZTYsH+bUyVaztuMs9W7mv2O7FftHUxWpVxEprBiOsKkJFOhU6orny8RfA3VBTZZmLfWqon+kEUFsHhNh2BkOpEjpoO3+B3QFddJ7BkMV0z6NNhA6DFrs9z/VGWgzfDGiSSPWFlvJTCrIKRBi4cMV4a2NccBbjF7iIRR45aV+EFKMXt4CFFlPz1gIbRBhGysUtaAELV6wiM6MwRVpa8YrNPtbBIA6xiEdM4hKb+MQoTrGKV8ziFrv4xTCOsYxnTGN8BgQAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXl5eX19fYGBgYWFhYmJiY2NjZGRkZWVlZmZmZ2dnaGhoaWlpampqa2trbGxsbW1tbm5ub29vcHBwcXFxcnJyc3NzdHR0dXV1dnZ2d3d3eHh4eXl5g4ODkZGRn5+fq6urtra2vr6+w8PDxsbGyMjIycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKy8vLy8vLy8vLy8vLy8zMzMzMzMzNzc3Nzc3Nzs7Ozs7Pz8/Pz8/Q0NDQ0NDR0dHR0dHS0tLS0tLT09PT09PU1NTV1dXV1tbW19jY2NnZ2dra2tvb3Nzd3t7f4ODh4eHi4uLj4+Pk5OTl5eXm5ebn5ubn5ubn5ufn5+fo5+jo5+jp6Ojp6Onp6enq6urr6+vs6+zt6+zt7O3u7O3u7O3u7O3u7O3u7e3u7e7v7e7v7u/w7u/w7u/w7/Dx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8fLz8fLz8fLz8PHy8PHy8PHy8PHy8PHy8fLz8fLz8fLzCP4A6wkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs041hy1as2XJjhULtquWJFe1dv0aVuyYMmbTvGmVug7bsmJmJendy7evX72+jMFFN9coO27NjO36y7ix413Inpkr7JMdtGKuHGve3PiWsWfsKNu0jJmz6dN+XRWLFlq0S9KZUcuerXfWsWuuVZpTFpu2b9q4mK3LTdJcst6/k892hQwc8Y/ejCmfrrxYtuca0R2jzl25sG7YK/6yYzaru/nkyVqHf3gt1/n3v2s9W9/QXDH4+H/32kY/YTPk+QWImjLq9VcPOsMIqCBtvjhnYDV5LSjhaa5EQx87yEyooWzIFOiaN7xsKOJpu4CXmzayjKgiZ65U41o0AK4o41/zFdbMhq4IUwwyyjQDTTXcgDMcO+iY4w032lTDzDG/xKggM3MpM+EvylxnkTnVKDOMk/Apk5V0At6CDDTDedTNM2AGaIxV7BAjoHUmseMMMGpWtR1+riTjYEq7ldflVMvgd4twL40XoXnNRHXje8BYOBM7zRzKnTRPQXOeK8ZwgxM7vJnnCn9MXXMeLybq5E2C3dUil1LgcOmbl/4/QXNLd7cQhhQ7vdCKjVDouMndMEkl010xtg7FTHdQGjUNd7NAg1Q2kv6mKVHm+KncLpMlhY4w1OXiYWW/UAdMsUmxc990xwwl5XTFlMkUO3cqV+NP3FCXjFQZKidLtj6FqxysUq2bXLo+WaocMlYJq5w2Pa0za3LGfAsVO6jqJ3FNgSZXzMVQrbNYcs7sZI6rnA3DcVTgRItaLeTaFC9tu5wsVb3JIYwTOMm5Uupcxya3Z035+haya776dq9N5kBMHDoqm+ZKyzEJPBst7rqGTXLL1IROir7NS5zCtNUic0oZ0ybMekz/NnRM7DS9mSurhvfMb7qMbZLBtGXdH/6dvlEa07mz1VJ1eN38RkxM6Pymt4FBzwa1Ss74Nsvg6+EsNEwVy7a4gfW8jNrhLiVN29OcD1S4b4+f1PNsR5cuUJqyrb1S5qgx7LpA1fgG+krs+JbL7QQ9PJvdH12dN/ADSY3aNCyVLVvcwJ8+2+Yo0W7a2cgP5K9su6O0jm/UA786aq4Qz5Hxs9mefT3SyzYtSuNTaH54tNCWaEqNn9b9+rCftmZKwTje+gYSP9P0QiVuc8yuBigQmsnGFSn5Hm0ohzx2cE02/CqJNsLGQIJwK30oidxssNfBevTPNF4jifJMQ7ASrpAzyTJJ/kwTvvWJUDYtLAngUOOoEopqNv7FqB5tmFfCejgQNcA6Cd9ko74Otu80wEBJiGYDPQZaTjYHPEkCG5NBBiZuNrpACckaQ8HsSVA2t0CJb4o4EA6exFoUYmM9ercclGyRMXL84gNR4p7ZlBF5V1wZSnRBm9Qh74mm4QVKcjWbnzEQfaiJohJpszMGRoM2STTJB2UDKh/mYox+CeJJTsgZ2RURHdyYRjOUYQxh3IJLNjPJCzcDMDkixBzc8IoyxnKLWpLkhqj5ny1zkrvZ+GKYpqLNLJCZEzpSkZk4IeRsTAlNmZByMzmspkwKyJlaaLMm2/BNFb/5GlD+JYbkhEkAZyPJdMJklptxpDtXckkBzrMlZ/6UzS7u+RLrmeZ9/IScb2IZ0JTocTblKOjsjKZQgfqmiw0dyUFl48uIjsSfLJKnRUFST9rsb6MgYYfwZtNDkIaEm6a5xR9NmpF1XHB6LBUJPDnTxJhyJJCzqZVNP3JN04hypxw5Im3QCdSM7JCSRd1Iq5JTC40mdSLOo00uDPnUhzhMOb9YaVUb0lHDzW+rBlmnxr4KVtNRhxhaLStCouqbYKRVrQWhGHV8QVW4GsQcd+RMLippV4VAMmfO6mtD2PqbDgl2IXLlzi8geliCoKOP1JnF/Rp7EG/Urzu+6CRlCcINc6ImGW9VazHNMwtl1BWuc3tPnhK6WYEQljvEYN9Na1HKHVfcZrPAFNSYTlvUrgqIF8mQBmOfqg3PdscVvzgGMwCaVG5cVkTCrKo3RjqhiibVHBgNEFG3ygzjUqekZeUGLiZU07Kuo6fncSpYoeFd35A1puU46nnS2NppUBezrRUIp9rLmZ/m1xti5U7r8isQbWQXpgQuSDfQexpqJrgexuEvX1z04IOswxmbfF6FFVIOZnzsNLytMDeScd+/QHDDD+lGYkqsl32iWCLmgMYufdGbTL64IqiEBhFvzOMe+/jHQA6ykIdM5CIb+chITrKSl8zkJjv5yVAuSkAAACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+foyMjKSkpLe3t8LCwsfHx8nJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8vLy8vLy8zMzMzMzMzMzM3Nzc3Nzc3Nzc7Ozs7Pz8/Pz8/Q0NDQ0NDR0dHR0dLS0tPT09PT09PU1NTU1NTV1dbX19jZ2dra29vc3N3d3t7e39/f4ODh4eHi4uHi4uHi4+Li4+Lj5OPj5OPk5eTl5uXm5+bn5+fn6Ofo6ejo6ejp6ujp6unq6unq6+rr6+rr7Orr7Ovs7ezt7u3u7+7v8O7v8O7v8PDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vHy8/Hy8/Dx8vDx8vDx8vDx8vDx8vHy8wj+APEJHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNSVTdum7RmyooN84ULEqRZu3wNK5aMGTRs39JpjdruWzRlwmaZ3cu3r1++xJpZIzf3KLpszYj9Xcy4MSRjz8YV/vmtmS/HmDP73cVMm7rJNttxa7ZLs+nTfJVtawcaZjpnZVHLlo2rmeTWKrktm8279zBpn3GPVCcNWO/jvWcxkyvc47lmepFL7728uUZ1zaZrPz6rGXPrE9v+SYu9vfzsWdFYg4fIzbj597yDgVvf8Jwy+Ph7NwtO/6C06PkFiNou2PRXUDrJCKjgbMl8R183pS0o4Wm7dENfO85MqCFqzqgnHDqKbSiiZsScIxw45I2oYmOzbNPaNivytYswxowVI18FFmaNiL4sA80246DDH0HqoAPONthEw8xlG0ozFzQSzrLMNOB4ONE50yyTooDOYNUOMwr60ow3VmpUTjTJAJgfM2VGBWZ+xURTTkntZJNggMoMCdUza6KzUjrQuAefMm0yNQ1+y/j50jbF4MdMVNnAx4yiMnFjDHzRPMXNe8XcZtM4u5mXTVPfqCmdL9rw5I2g2nmzVDr+W3KXnk/tPGPqcbjMiVQ7jWpXjIM+ndPrdMDoORSf2j1jFJTaPWrUptPt4upR3MTK26hEoWOtbMQAWxQ6w0yHC6VAtRMicnkypc590hVT6E7RTNelU+2Eihw0QaFzq2zT0PUmcib+xO5xOUqVHbo/aSNdv1UdfBw3PakTYW/4WtXOwLwB8y5NGR7XTFbqDMsbwzmhg1wyG0eVzi+4GkuTw7P54nJV52yr2bw3pYOcp3PBeBy5Lx9H8mT/zuZsTTr3Zoxw6jB5nrcwwSwb0JN9c5yyNCXNW6bWdTwbLjOzhOxswqSslcS9VTNTOxPLxnNzO/JWtkw+z7ZMf+0I0xv+xDHZK1vAD/Z2N0zqCG6gQBijZnZJ0vQGeH/j9IatS+HaffhAd8o2eEvn9Kbr5dDOtrhIjc+mzOUDtRMMbwWvlLhp1qA+UNyat9TOvpjNEvZ66tjs2O4ihY7a5rLjU/RpLq7kNWrJF49P3ahhrdK5p+HiPEFtm5bMn7wR77zUmlmvEvSnxX69QODw9vlJ8c4GNert+M6Y2ikdT+L5BL2e2ccpiWwazvhrH2qIoRLepAp/6OPN6DbSudk87nzt4M37QEI+zcwCgQS5lGzmc5LSoWZpGBQI+DJzQJOM7TT8C6EAT+Okk4wQM+YLYaRkA0CS+M00FgohPqxWu5Nk7jT+b8Of1k4DQpNQzzRUgyDZUOK00yzwcLMJBkqylxnx6RAfuGsMMKYomy1eER9UxIwvuIgaYXwRH03UzBhPksbMePGKrNLMLlASx8ys8YptxMwcT1JHPZ7xayhZHWqsGMLCyWaPJtEbai54RZPJxown8Z9mgIe6yMlmeye5oWaCeD7hmcZ7I1meaSaHwWrMJoUlWaFpuBbCFzpGeiWZIWqOhsEfmmZoJOEhaopxxSw2pnklGeJpKGkgR04NJRGczbQQWMHMMBIlR9QMK/F3QtMU8ST2y8zpMCjJ/anElLTBoDBvqZJy8EYcCKQdapZ5ktvNpobF0+QkV6JB1NzxevL+WwwyxMYbdhaPHM5QpGkqphKFnTKE53iGQB2DzpUYkjZP7E85oFG5xeAioh3RX2ZIqcNzQGOhewElStR5GgKecSDpiAb1YujQ3nzjpAVBRzQalUSU2FJ7MD3IBE8CztlwMqcveWgPgUqTm5qGg0SViSy5hdGkhqQdedQMS53qElVSiJhUHU4+GQPLrLJElKj5qVdNYswoYnWsH8mmaVCJVpQ0sDfAbOtJwFq9B8qVJL07zi/OeteMHOo47uqrSfKGHJEK9iPdkM40DxsSeaIGl4z1iLYUG1mRLFU/lQ2JWmfZ1MwyRB19vCRfPdsQcWjHFy8l7UaYNZ0WqhYjvNrrjjHs+lqJpCOq54FGZ2s7kHJs1TTA8CdvIZI+8yiDMMOViEGNi9zkPqSnzHXuQ6pZnmJMY7S1deV0pKSN3WaWtfmJq3QN8lf8XHS8CyFpeWiJXoRcdjslbG9CtuHL83i3tuUAaWHl6xB1OHY2HOWvQrBR33kK2CH53e+BH4KhAjOmdQtuSDm6mRnsRhgbv93LNiMsEf+Wj8MVIYdG/WJhEPd2xGbZsIkrUo7/QnbFE2nxX0oMY4OkYxr1vGaNM3JjZLx4x0AOspCHTOQiG/nISE6ykpfM5CY7+clQjrKUp0zlKlv5yljOskcCAgAh+QQAAwAAACwAAAAAyADIAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NkZGRlZWVmZmZnZ2doaGhpaWlqampra2tsbGxtbW1ubm5vb29wcHBxcXFycnJzc3N0dHR1dXV2dnZ3d3d4eHh5eXl6enp7e3uAgICOjo6dnZ2qqqq1tbW9vb3Dw8PGxsbIyMjJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrLy8vLy8vMzMzMzMzNzc3Nzc7Ozs7Oz8/Pz8/Pz8/Pz9DPz9DP0NDQ0NDQ0NDQ0NHQ0dHR0dHR0tLS0tPT09TU1dXV1tbW19fX2NjY2NnZ2dra2tvb29zc3d3e3t/f3+Df3+Dg4OHg4eHh4eLh4uLi4uPi4+Pj4+Tj4+Tj5OTk5OXl5ebl5ufn5+jo6Ono6erp6erp6urq6+vr7Ozr7O3r7O3s7e3u7/Du7/Dv7/Dv8PHw8fLw8fLw8fLw8fLw8fLw8fLx8vPw8fLw8fLw8fLw8fLw8fLw8fLw8fLx8vPx8vMI/gDtCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izToVHDtw2atGcLUtWbBclSrd0CTOGbNkzadi8jWOnFeo7cdSaCTvLt6/fv353MaM27l3dovDETWNmFrDjx5AFfzN8mOc7b8xqQd7MGXIyaucq22SnTZnmzqhT/+01ja7ol+WaqZ5N268ycPBep4THzVjt38BzQQuteyQ7abmAK1euDF3xj+OYLZ++vJnr5xjTJaPOXXktaNex/kuEF627ee/ScouH6K3x+fe1d4Vb3/DcMfj4gVunn3Ba/v+/5eINfwWtgwyACNaWzDoE2gPOLQlGONst4NAHjzMSZjibNOKxs5eGIKKWDGWvmZNciChytos5r4VzWoqU1LILMcgQs8uLKNYiTmXWgJjLMtOMc8464RXETjrmgCONMhBmWKFW0EiYDDTgMGjROuBEs12C3GAFj3QA6vJMOOp1xI41vgGIzVXP/FfMNMSRlI40vfzHIVVR4rfMjiuFkyZ8z5T5lDTwfWelS+HcB9+dT1Hz3ndFviTOluet6dQ17zVzaE2TvvfkUuKc1xxP4LhHXS3lLLVOk9Td8s1P/vC02V0umxpFTHfKRMrTOHVyV4ygRMl2ajZFvSMsddAclQ13u8RZFDc4KjcfUedE+9sxug51Tq/L5ZLtTvAEQx0zwB7lzjLUKTOUf9NNA1Wey10TVDrW0kZsVOVN5yxPlAIn71SOLmfMT960axWmyw3IEzus/pbsVewCpwuJORGq3DJagQmcuzqxU29qvVBs1TuK/lZLrTXBW1st+17FDi/KOZPTOsu9Wpk5y7F4k6y/RaNbwL9hbFM6yvVSrlbwHAhcOjYdWxs5z63zcWcPz0QzcM+Itw1wtbhDU8S05SKybkrXRs1M8JxYm83i4fzbLkevBA5wxxDo9Gxsv6Qx/m3jEHh1berCxA5wgROYb23fngT2bKk2OPhv28D0IW1CN2jP3akx89LffFsu0DjAJU4SNr8J4/lAf87WZUvo1nbv6VvXVrlK8Jg8NoHvNKza7SWFWlvVp9ujsmqfpjR8akwHL5DvtDGaUuqp1a28QPBM7dlKtdfmvPKYd1ZL3CKBXlvf0wvEzW86o7QsbbeUP9A7kKvEs2rNuI96bVmnVLJqltp/fGcDQ0n2aNM4+8WONuD7yDl+k0DLEQ1xKGGeagJoP4HoDjXpK0nBaCOzCgqkX6kpHkkWh5p/eRBDtDFhSQ5HPA8KhISd2d5IuscZ8nnwgKrR3ElAiJrkeVCC/qlBBkp+AUEXPnA2pjuJqVLjQoHAjzbEQMkSO7OLJtpjgKoJhhRpo0Ur1qYXW5xNFa14Qc6AUYlhs6I9yriZMZpkipxpnxXh2MYwqqYWalRbanJhR9VYEYsg62NqROe5x81GiCeZnGoKWEF0yA4lyqiNCN0nvtkAjyT/48zZXDg32nDMJDDkzCXdh7DZ9K8kOEzN7Ow3vxBGMD5N3F9qbFgSzqkGZdP7DS5DAsjU5K18bkNgSohIm1EqL5WokR5KUDgbZbqvlagx5kjWRxveeU6PqYlcSsrxm9WVr5KMo91vdFg+iwlTJTz0XgPpAz3UUDAl5pzNJC23wN+xpJO0/smf8jJZQ5YYckLWXM87rPeY77HuN9o4ZtBcQk0kKq9ss3ndSv45G1oSqJ5FbEk6O1M/z9GQM85kSY9+Q0jRYJQ2m3SJLVXjswZFkqQxgehsdqkbcM6mozBp6GzIuR5ZqoZPMcGmaqC2HnYIFTVujEkoO4PI9cBDGgQFTEpj4jHgSFQ862jdIFPGNZrqRhyKhIw+Z3JS2iBjnaLJBhv74tWDAmeq/GEHPykxVpoEE32nO8dGS8oSZtaGF3x9DTi4dZaW4kRqyjlGQLEDD2qcphaBZclSUbMMtBaHHbKRoU3goYvlSJNA5lisTM63HMOqkSbtzKdlT2uSsv6mGatl7y1JSqkccslWJi9dTjIie1uQMIw6ushgb+XGnVpcdbgpgaZ+RIvcj7wjrEVjZHNNso6jYi2208VIOaKqGl3MM7sgwSd3VglekZCuOz4sL0lYqBzNqhcksZpOLrz23pPMtTPHre9I2DtB7OoXI5OFDFH/ixKgpQanBEYJbTlzsgT3aa1++aSDU5KOYqiIuRN27kfP8t0Mm0QbH0uGh1+SDp9SomUjVsk2GlbXFPsTTLfgrYtDUioVzvglGL6xjnfM4x77+MdADrKQh0zkIhv5yEhOspKXzOQmO/nJUI6ylKdM5Spb+cpYzrKWt8zlJgcEACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYKioqMnJycnJycnJycnJycnJycnKysnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8vLy8vLy8zMzMzMzMzMzMzMzM3Nzc3Nzc3Nzc3Nzc3Ozs7Ozs7Ozs7Ozs7Ozs/Pz8/Pz8/Q0NDQ0dHR0dHS0tLS0tPT09TV1dXV1dXW1tbX19bX19fX2NjY2dnZ2trb29zc3d3e3t7f39/g4ODg4eDh4eHh4uLi4+Lj4+Lj5OPk5OTk5eXm5ubn6Obn6Ofo6ejp6ujp6unq6+nq6+rq6+rr7Orr7Ovs7ezt7uzt7u3u7+3u7+3u7+3u7+7v8O/w8fDw8fDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8gj+AOMJHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNKFbfNGjRnyYztqkO2bCtbu4IJK4bsmbVv6LRCVbftWbGyePPq3Uv2V7Jp4ODJLaqOmrKxfBMr5ivsWTZ1g3uWkzZsseXLfItRgxy5JjhovzCLHp1XM+fOLq2FJs26dR1j3FCrVAdNluvbrXdNcyebJDhluIO7btWsXO+P4O4KX+462bnjGtElY079drPT0CW6e9aquvfWraD+ZZc4zfb386xtVRvfENxq9PBJByPHPmG0+PhZt5pWv2C5yvkFOJoxz/VHTXcCJohZK9iw9w4yCkYomjLvQIeOMBJmeNkw2EUGjnkahpiYLuKglo2IKCrWyjaRQSNhMc9Qo8035XQYjzrokAMON9M4I5aE0sjFjIC2NGNNiRbB8000xiQoHlbO5BcMNEh69BmG+D1Z1TPxBTNNgSSpY80x8T1TlYvntcLMNyyh0wyC3zkzlTTnDWNNTLTBWZ2cUFXz3S7d1LTdedE8xY13++V0znTescjUOXoKZ0xcO30DIHOtVImUO+8JxyBQ01Rni41ENcncpEKJ02lww1R4FJf+y7VyZ1FRMqfMUdswt4txR2FD3axDuQMibhwq9c2wt7VCqVCMBneMq0qdg1hwxgylzXLMPOXOpbhRE5Q6yLZ2K1TuKBccmD2Zitu4Ub1j7m3F/HSNcOxK9Y66twGr0zvhkpbtVfi2ZotgO6EJb1bvBBNckDqpEylpu5A6FTq24NYKbznVOhyvWoETnJk4oRNcbJGdiNuyNQ15m5aRNYPbvzWdg1swvb0zbWsSuwSra5p29g1uLMfkzsOiMXycwazJAm1Mobo2DHsKu3YNTTeTxjF0P7tGs0zXutZMf82yFmhMAY+Gcnbl3JZMTCK7FvR4GrOGsUtNJz13fQ5LDdP+u6MV2t9AO5OGzEvq3JYzdDK7drdK1rgG898CQdiaoyyR2Ro4kBOUq7gtuaN15gVVrF9LXbM2NegDIT0aySrFPdrSoHvcGsgqcSva4KgTJDpp1ar0jmvr5T6Q66KtJDtr6OZ+aGs9l+Qna78IXxDRlp2Oksus8Sm9QJaTpv1JUZOmr/SqY0ZMSr9fvv1Am7OWkjiuwS584sij1E1rW68vEPWLsXnSvKx53PrKdplsoIROrPGb/uJBvMvw5ySBE834tncf1tCuJMBhjTYWKJDGBRAlfMOM/xa4PN6hpGqYSd720gY9lOzudRy8UWt04cLWxDAenmPNLmrIGvlJr3D+OuQhaXwovBxCTIijWdz2jDiaHZ4EhZdRovSYKBonmgSKljkc6NJ3xJPoAmcxBCJpaHiSVWHmauuj32iidxICWoZ1+rsfa46BEpWRxnr6K91oBDiSCGLGaPqr22jeJhJBiuaC6yufA1FiMtLQkYNhEw3lSpI10sgihma8zAhLosbRaPFvVBSNCkfiGjhKT47uSwkxWgNI6RkSM/FKSQMtUy/pRRIziCwJAEljiwXyTzENep9rMLc9VJJmlCRxjQKF58fLtIIlIbxMLKWXScs8UiXNvMzZMtdJ0RCyJO27o/QQyBpTnoSLJhTeC0XzzJbc8oy5C+doapkSX7Xme5D+kxxrDNgSMZLmYqDrpmikiJLukeaBkJulNWHySszsgojHYWFrgucSgf4Rcu/EzCdLYtDRyIKgsjkea3oHE3kOsj9uvMwkX1JNbbLHpKLp5Uye1xrcQUdYt2nlS/h1GzzKJqOYASlLFLmgbQ4GpqL5Wk3a5hqSdgant2leTDLomglqpaOCC5nFiDmYCt6GPjghKmZsYdSqGLM1SsUJVLUG0aigo19m2wlNXXNNq7wDSyvrSUsxw0epUCM4SusJUkdj06pgzzUI5Yk+b/Osq4h1MfnrCbgk1damzFU0UtVJCXGDz6nocZFCsaNrdiHUp3Tjl3h5WrC+6BpZIHMqH7LATCtey5OzsjOzVSnHOvVCUaI8NjHmzAo6WkrPoQCVL/xETbn2QozK+iRhRYPOOzoasaVQ7DJphY4dW4FGpIiUL3XNDqzG1pTPloZg9ZmGt6DS0Dr8orQ35Ak5y0LW+CJlvq0Aq32PYjDy7vdVdVjpf42yyQEb+MAITrCCF8zgBjv4wRCOsIQnTOEKW/jCGM6whjfM4Q57+MMgDrGIR0ziEpv4xChOsYpXzOIWu/jFMI6xjGdM4xrb+MY4zrGOLRIQACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+fn9/f4SEhJGRkZ+fn6ysrLa2tr6+vsPDw8fHx8jIyMnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8zMzM3Nzc7Ozs/Q0NHR0tPT1NXV1tfX2Nrb297e3+Dh4uPj5Obn5+rr7O3u7/Dx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vHy8/Hy8/Hy8/Dx8vDx8vDx8vHy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Dx8vDx8vDx8vDx8vDx8vDx8vHy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Dx8vDx8vDx8vDx8vDx8vDx8gj+ALEJHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNSvWWrFi1ZsmC9csUKEyZVrFq5cgVL1qxat3BpjYrLFq1YrVCZ3cu3r9+9qFzJqiV3btFctmS9UvW3sePHq2DNsmXYJ65asPQ+3sy5sapYtnJVtnlrlqvOqFP/RQWL8OiXuWq1Uk27dl9YlF+nxCVrle3fwF3VEq17pC1YwJMnXyWrcPGOt04rnw4cVfPnGnEhp84duKpZxLH+T+Stubt526xqiZdYi/H5979d5V6/ENcr+PiBw3JO3yCt8vkFqJoq6vVHEC7SCaggbfsZiE0tAC4oYWerzIddLrJMqCFtsoSnG4IbhpiaK/xVdotvIqbIGSu3vFaLijBypoqFWc0S4iqvxOJWLbbccotoueDiY1ezhJWghgVmlaGErcTiWkW31BLLkQLOoqSCgj3JUWyxRAifLFctiZ91JYaUGIr4gTlVLrHk1wotHpoU2334WSmVmOe9QuNKt8ji3nm0REULfK3s6RKGXk6XJFO2vIcKnDfh0uZ5hhp1S6LJxVJmTbdsx50qLSqFC5rUFfpTe921EidRuczG3aP+QtnXHSyrCjUpdQ0SNSh3dhb1Iq9InchdpT3h8mdyM4rqqnKqbNqTp8m14ixRsk73ylC/KvdKrUexSd2ixR7727ZR4flbs0BB+xssVN2q30+NKhcLt0zloq5t4OLUqnK0WmWvcqvQO9OuwQnsVC5U1tarvqTWxsq0dIlLG7o52ehdqFrFC5yakWKKGsZzWfwbKhC7ZC5tgb52r2oc04RLcuS+NmpyIMskcm0kP6exbS3HlEvDqqX8nLu05TxTtrVdK56xwC0M07K1lTwX0rSpKtPOHBq4MmrEprQ1Z0bTd8u7ML38W8/rEa2a1CQRPDHbJjYN09eboU2f2qgp3ZL+2bbBPdrYv/n9EdWpsesgQXTWJvRKeHfWtXiEo2b4Srl4/BgrBl8INGqZgxR5Z04fjs3NquVr0slci14Q4LXZXVLCnanSuXhQp6Y3SrmcrbpBqHc2O0es02a66FinVnNJn3P2u+b4qtT4ZrfvPtDzj7kuEuycWS968o+5otJvw6sevGrLY8Q3bcdLn7tt6ZtpmyrSI4T9Zo93xL1j0ccvEPWOhf8R6ajRnuoACDqU9G4zodPf/RojwI7wrzGL059AiteZWKBkfo+p3+HGlzeUlKU27ZPe+VLTCpRIrDOCo8/6aMMKE9pGggdBjwtrA0ODyPAkJ9wMKmpYkBuaJIf+j9khDwWyQtW0EIe1gd8QsVHE1Bzxhy9cYhNR88SSAPExKVzPCKnoQfYtkYOdKeFJMOiYEO5ugX+ZXEno5hgNOshtqWkgRw74GP8N0DYJDAkBOZNH6T3wLxEcCRr9YsEhJk41btQIBTkjRh5arowoAaPveChJzmTRIlP8GA8H6ReVkBGCPKSjY7yXElE2ppAwPGQcVcJJvgQMhpnsjB0/Ukn6wbCVfDEjSGLJRxiacjUs+eRfSBm/XFwRfyz55V90uZ5F9nIluNyLHLGzR87McpfnKl9lcvHBqLWEjf2TnjM3o0aVwHFE0gMnKPcGHGZ+CDiX5Igw/4JKB1WTnDD+uedm3LnNYzomkCrZYuEOd87UxLMj6mxMIrPCNNuUsyXR3Asx16PMvyxUJD8DzjUZCpxGxkSfj6EYdv64TpkINDX11M04NyO7mpBUoc+Z2W/62JJacmYVB2VKQhuT05C81C+r4KdUIrqXh8rEpo7J1d8euc+c/BQTqNjoVGSqu5yctDGtEKpUEIasnpJEmR0qzr+SA9Ca4OKRyXoOSDdjNZ7c8xVeXUpBaXNRl6hyLzTVylo3k9KdnLUvLBJPRR0jUp+4bV4XeipMhXKfqC7trjyj1iu0upVuRkubSxTJf7hD2cyKZKfP9GxMBvuYmIkWJvtSjiswe1qQIHUzOG2SLU3m2hlUdFa2I4Gs43Br1s1tpqy8dclK/ZLX4CYzNcA17qFq1xipKjcll9qMc5+bEtpiYrrU9dpf0ppdnFDVLNLq7k521i/x7sRi0zQvTF6BXfW6973wja9850vf+tr3vvjNr373y9/++ve/AA6wgAdM4AIb+MAITrCCF8zgBjv4wRCOsIQnTOEKW/jCGDZQQAAAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcsLCwycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKy8vLy8vLy8zMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzc3Nzc3Nzc3Nzs7Ozs7Ozs7Ozs7Oz8/Pz8/Pz8/Q0NDQ0NDQ0dHR0tLS09PT1NTU1dXV1tfX19fY2NjZ2dna2trb29vc3Nzd3Nzd3d3e3t7f39/g3+Dh4ODh4eHi4uLj4+Pk4+Tl5OXl5eXm5ebn5ubn5ufn5ufo5+fo5+jp6urr6+vs7e3u7/Dx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8fLz8fLz8fLz8fLz8fLz8fLz8fLz8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PDx7/Dx7/Dx7+/w7u/w7u/w7u7v7e7v7e7u7e3u7e3u7e3u7O3u7O3t7O3t7Ozt7O3t7O3tCP4AwQkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs07tR4zXrVitVHWKQ5bspE6dSJ1SxSoWLmHH3Gl9qk8YLlipyurdy7dvnE6raBFbNreoPF+uxvpdzNivqlmDC/c8dktV48uY/bLaJU+yTXm4QGUeTXrvKl31PMM09qq069dkU/FSrVLZrlOwc7/udKsz7ZHyaE3STfz1JFjIfn8MPry489esiCnXqEz48+uwUxmbbnGXJezgYf7H8s394bG84dO7trSrfMNls9TLf31qu3uEv0TP319aFuH7AynjCn8ElpZKPwCCgwwpBTY42iS+3LdLcw5WeNkr//0moIUcYnaKPr8t2OGIjXVyjGrEUMihJaCcksoqqqQCimIjTmLfXL5UCIors/RyTIYIuYOMMLrEwoqKBM6mlS4FdvLKL8pghAwvsOjHHy1Z0cJfKrac+FE/tNAoHyxWLQPLfJO8cuNIxbyCJHiuAAlVLPKRggt5J9nGoHpkSnWLeqkIAxMwq6g3S1S9pKeKdDMZU2h4uDxFTHiL4kQMbuApudQxb+rWSYQ7+WLlc8Uspc9318USZU/wYWcJiP5ILYOpc6as6ZMxezqXipxDxfdcLLz6tIws18ly1KTOTdILUrg+B+pQ9YiZGynJJRXPo8RNUq1QrDjXyqpKLTNgcacEuxMuzsUClZbF3RLUMc4dGhW62SIo7Ky5uTsVu7qx8hMvxeViFbHELcuTMtK6huVVdOpmCZ442UJcK+ZCZSZx6upkT6ejpQLuVctgC5uXOLWWWyepzRWtbq/kBK9upUqGbG7b1tSKbvJ6xu9rGdeEjG6mVAwyvq7tY1PDIyv3MmzG0iSPbpFORzBsKcv0J2ykCK0VwrlFHdMyCY/2i3u/TDtTorCpAmC3sNnaksil1cydMbn1+VI9ubmSIP44NxsH8UpMwiZ3eXTD1t5LbLvWyt4C9e1aKi89DRujey/tmr0s7YK11spZ9trhLCVeGuiMA/za4ixJ7tokcjEukDKwTdJSjq+17PpASJdGeUq+ujb27QIVrjBLnpdWCefljdrxSsvAZjfw4ORO2scmWU5aMNAPFEzbKgXuGvW3w/65SiaXtkr2BDlOWtMomfJazujTa35Kzb/2O/rgWJ+Z7Cj1A9vfwKvf5VAiPNKcAn8EIVpmdkcStJVGbwgUyLhKYzCTyI80C4vg1UrjtZJILzOaQmDZhneSCZImZhHUH2Z6VhL0lAZzCNTH6VCSK9IgL0HiK43aTqI8zFgigv4EeQ0oUNLDy3QCiAMJW4mI6JohIhEcNczMEXnoGlI8ERxFbIwTTaJExhzwiV1czBZLEka/QO6JqCKNFU+SxtGY4oocY8waTZJFxvzwia+ZYwtfc0MA5ZA0bzyJ+kYDqxS+5nwnKR9pSIbA7bkGgibpHWnuh0DvkQZ+JLngaGyBREmOpoMkMd0DkWjC0TyrJMB4TSAjGMXMuE0kP3sN+ICXDNgUsiQCLM0rb0c71/RRI8UjDSihNzUDquSDmPEXAoM5mueZRHOyxN8fSUM6kxTwevgboWsYaZJpjoaFwFPkg365EWZmZozhiyNjEKmSnS0ye9osTQZTcs3RsO92pf4UW0vaOE7guUOdjJmlSfIJQuBZkjSoY4kodXi7ZbSyoC3B22t25TpHvgaAKIHbZVzBzQSJjjTKdMlC7TiLqrkuluN7iTIASopdkJM2BM0MRlNyJr+sAnv4U2FmQvqSeh5ncNBzoWtO+ZJcgaI3SIxnaUDx0pCgKxW+aCp3llHHy+hLJvXYJf42uDqBXvElOs3MPb8qE1npBoZkjYknR5nWRhEHqG1diT7K2BhwxpUlylAgaSZh0rsy76OuGaZfeUecTnh1sCRx52uIitiTrNU1O2wsSi5WHLhKFiTLECdsdHHZk8QDsNDprElEVByUiZYkwADoaBh4Wo8UkzhjbectR85zHVMcVrYUadV1LIFW3GIEGFVdnVZ9KxFHhUdQxMUIMczpnAomlyLBECp2rvpcikg3VdW9yEifAwupZlcgYANPbL8rEa4WR7DkjYg8VAvR9F7ktbChpHsrglLYdGK484XIIEmzir7mtyLCgM08/4sRvdoRpwTWyHYZk4pbJhgjyuCnXyZxC+8+GCESawwrjHbhjkjUL6CQb4c3UlO++GfEINHfK3qLYo6wbRKy8G+LO5LaWcx0xhy5MY53zOMe+/jHQA6ykIdM5CIb+chITrKSl8zkJjv5yVCOspSnTOUqW/nJAQEAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1deHh4ycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKy8vLy8vLy8vLy8vLy8vLy8vMzMzMzMzMzMzMzMzNzM3Nzc3Nzc3Nzc3Nzc3Nzc3Ozs7Oz8/Pz9DQ0NDQ0dLS0tPT09TU1dbW19fY2NjZ2dna2dra2trb29vc3Nzd3d7e3t/f4OHh4eHi4eLi4uLj4uPj4uPk4+Tk4+Tl5OTl5OXm5eXm5ubn5ufo5+fo6Ojp6erq6uvs6+vs6+zt7Ozt7O3t7O3u7e3u7e7v7e7v7e7v7e7v7u7v7u7v7u/w7u/w7+/w7/Dx8PDx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLzCP4A7wkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs1LlVk3ZsWHBgPnaVWvOHFW1dPXyBUwYsmXVtInT+pQdtmXGgOkyy7ev379+VQFDZg0dXaPblAEDzLixY7O6hDHbdrgnOmnBYD3ezBnwLmSUK9dsh7mz6dN+P4cW3bKdNWGqUMuePccXtHasU7ZTtpe2b9m1ks3NPRIdMs2/k8tWNWw1cY7iiMVWTn32r2rPNaIjVr27717asv5XbLcMuffzso3hFg+x2i708Gfnws6eobZf8fPPDja8/kFj+gUoGyz0+TdQN70IqCBqxKznnzTTLSghZ7yEx147wkyoYWeqMCPeNu9tKOJmwPQnmjURjqgiY7pwwxo0K8boGCwW0qXMhqr4Ikwy0mSzzTbioINbO+Fwk8010jSjDDG9SaiKNXQBuOAvyWBjjkXiTGNMiAo6k1WGAfqCTDYOboQONcOkGJ8yVq2zWH65IANOSe0840uAyVDVDn7wMZfNSt0c0yR6y0wF5nmwJLMOTK+pWd00USGDHizKlAmTOMHAd81TMJ5nzKI3WcNldar8yZQ15/1SI067OfobLP4uKrWNq7TVIs1P5hxKnS6gHoVOLt0BY1hQ0pinXDBJDdMdm0SBwyd1hRpVTXW5mGrUjdWtGpQ4xvomrFLYdEvbLuoQ9WZyw1h6FDeD+oasUM9Qd8xT6NypnIdAiUMds0+1k6ly4QClK234RoWhcsL8lI1y/Erlr3Kb8tROgr8lbFU7546rrk3MJPfLxlGt8yxtDd+Ejrin7TIsVuuMutycOUlJmyrdHDbrb8TktE1yzYg2TXLf4DQwau+KdsxvxtwEzm+6lMtaO/bSZqJMR/tmLWvg0MoZMjWto/VmXGeH7WyqrBzT2LLl0itx7fDiW9gxtdPuadDUtzBtsICs0v7PtPlioLK0FfhSxqhpm104vln8Ejq+JW3gPcX4tjZLzfg2NXvf+Nbz4LQV8/hAQ3cGzEvq+Bbr5zvTBjNL8e73OUGEmxYtS//KdvXnlbvOUumz7fI6QV6TrfdIqM5W8uuhc3b7SZLOVvPvAxUvW54rxc7ZL9AT1E5Zso2uUjtfNzZ79vdwN9vkJXGjOvkD8S1bxCjljlot7A+kr/EqmY+a4vVTzKBK/jvN5uoHOeuohDbOYd+0lpOS+6EGFgQciPpmczmR3I1oERQI+GaDDZS47zSey+A9onaa8ZEEbaY5HvuS9xi4lURmp4GUCFHYmZydpHanMRz7qDEb/pFkZP6muZIItTEb75mEhKYRoUAmiBq/nSSAHFLiPRyYMpS4jDMQVGI7ZkO/k3DvNLOQ4j3CB5gsmuSLptGFGFH2GFqghIx/6aIS4egXOZaENmLsHUpog776zYYXKJkbZ1YXwS3KphcogWJnElg/xsnGiSYBYmeWx75uGPCGs6mbCK+hO5Po7zTUy6D8TuM4kzRvf0o85WlUGJKOPVKJLHSMDE3CydkM73WS5IwOQ0LF05yufhuUjdlKQke+5EIYhGRf6lCjCpUgETCq+MUxqhEwJboSNYhMySf70gthNIORsJxNCFEyNlgEIxnX6KMY78HGxzxDJdkoBjR+uc6DWNJ59f7ECQ0584p84kSRnCmaP2dCRPwNlCaqPA38DgqT7ZFNnQxVCQ97GFGZPLMzs6xoS2p5Po1yjqIebckyZaPJkK4Eh6hxmklTckHZCHSlJpmYb6AEU5RcUza7uGVNOaKOdm6mYDslSdVmAwt2BLUkLZ3eUUmiDkGaZphL9QhKUTPOqH6kdTNLplU38o1iAsaFW92IOtzmm1pANKwU2VNy3olWjsSSM5Bsa0YSOhtKynUi+0TlXTECw5lVc69pBVxyogHY8by1Mz4s7EPEYT2cnlWxCMkGGn+zS8gmZBledQwrLXuQdRzWNB/jbEOq4dTeCVG0CelGY2kzC62idiDtQPBGZh+jisqKdh3KmCx1FvpagaDjGLPlTEl7245qBCO4nNmsYrdRDN16J5ScXcc0ilHa7gwQstdAxkXjk1HIAjQ+quggagUboFnYdq83zY8unvfaguqnRL3VYIDA2tvtKgcWvI1vX6vjC9f2tlPeKWV8CzJS5fjCrgMeI3VgwdYEI2S1piEGVB1MELqexhfnpfBEZXMdCjdkaURDsIcN4lzGDIO9I27IVP/ii2VMOMUKyescdIEMFMP4IdIziyqCwQx63hgijBOMMkT844eQqchITrKSl8zkJjv5yVCOspSnTOUqW/nKWM6ylrfM5S7nJiAAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXl5eX19fYGBgYWFhYmJiY2NjZGRkZWVlZmZmZ2dnaGhoaWlpampqa2trbGxsbW1tbm5ub29vcHBwcXFxcnJyc3NzdHR0dXV1dnZ2d3d3eHh4g4ODkZGRpKSks7Ozvb29xMTEyMjIycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLzMzMzMzMzM3Nzc3Nzc3Nzc3Nzs7Ozs7Ozs/Pz8/Pz8/Pz8/Pz9DQ0NDQ0NDR0NHR0NHR0dHR0dLS0dLS0tPT09PU1NTU1dXV1dXW1tbX19jY2NnZ2dra2tvb29zc3N3d3t7f3+Dg4ODh4eHi4uLj4+Pk4+Tk5OTl5eXm5ebm5ubn5ufn5ufo5+jo5+jp6Ojp6Onq6Onq6erq6err6err6uvr6uvs6+vs6+zt7O3u7e7v7u/w7/Dx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHyCP4A3wkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs05l962as2PEgu2qVQvVokWoyO4KRuyYs2rh2mmNuu7a11pn8+rdy1dvLbfX1s09mq4aM1x9EytejItZtXSDfbq7pszs4suYE6Nads1dZJvsqh3LTLp0YmTV2H1+6W7aMNOwY+8dNs3zapTsnFmWzVs2qmeqb488t6y38ePMzgn/yM3Y8efHj3lbrpHca+jYjQ8jR71iu2bZw/4fbya3O0Rqu8Wrjy2rmvmG336tn9/7V7j3CZ3R39/bGf6C6cjH34Cx/aLOf+9Ykx6BDGaGyjXvuVNcgxSatoxtwpGjS4UclqZLOcJp0+GIpKGyzWrXkKhiZtlEZk2FqACjDDTVbCPOOeqwY5s77KhzjjjcVPOMMsAsOKA1c03DoCzLWCPYRetYs4wsDE6TVTQD/jINiB+VM80uA0pz1TP71QINOiah8wyV9D1TlZLzKcMNS9wkQ5+VUmGzHirNHPiSOs0YCR2EUHWjnizRlCcTO9GwGd6cTokjqHHOYFiTO/qFh4o4TaXjKHTb8SSOgNjJAplS6yCGHSrUAIVedv64PInUdaD6CZQ6wWQ3TFJYYudfUZlCF81R3qza4lHXTBobOEWxg9dztaCZVDnPHleLokEhA50vsia1ji/QKTMUNdAZg61S7Tj3nHtAraNsaRdCJeFzqATn04SUUhVsb8z8FM5zy1iFb2/M9kQqb8hgpa1xv/RErnHEWEqVO7Tyxm5O7Hwa2y7nVtUOuL3J0jFN4PWGSrdYraMxbL/etM5x2nwmonG21rRvbNDcRmZvboKm3XIVxzbyS73KJgvKkancm5g0tbNyaUhS9yJvskj8EpyyEfMeMb21OlO1sXHZXTm94TJTiry1bN7NpmEjk52yXfuf07wlE1M7veH5n/40vQ2NUjVUW90d3bJd3JK6sTGN4DtFw5awSy/75jd17PRm70pYw5bz4gPtHJvXLHEtW82Lo8ObMS3hnTXnBQVd2uQizRyb3qy/w7dsJ64EDW+Xsx55bMOuhLhpdtdO0MKwoa6SO7yBbvw7mZsm+EjFyib28+LwVjBKt8OGyvMFPY2Z4icNDC/4BCkjW8ApuZ4Z7eB3b9quKb2r2PXg/xvb97jxhj5BzJNN70YCDtns4n8EURVs7nOSqcGmeAh8B/JMEzWTNK40avtfyWBDPpKw7X0RFEj0SJPBkZiPNLmLoOxM06+TwG2BIXxHAWMjrpMMrzTSiiDZYqM8k7gPM/6kQ586VncSMMVmgOD7nWl8gRJayGZ6xlMdbGqBEv/F8B2+uWJPsqhFnUjRNLLook6UWJoDihEnQyzQGXGSPR6u8SYzfOAbbZKN9c2xJvIrTTPuSJMPYiZ4fIzJDUlTwUC+RIGm2Z4hWxLAIy7yJfrz3iOvRsRJsuSFLLRkS+yXmA5q0iRtjA0DP4mSETqIlCoZZGYeh0qTkLE0nmxlSPJYmunIsiRGjA0Ub7kRctSNlyTZIGwKCUyPVI43sCumRTwnR2V+5JiyIaYzNXJB0yRzmhFhBycTU0NsbuSEpXGbNzVSvbiNcyO5nN05M0LL0qDimutUyA55s7l4UsQdiP6MDdLs+RD18YufFAGccZQD0IjMkzfsK+hDUmWckyn0IR87TiwfihBVmgYXu6ToQPxpnJhpdCHC5M0xProQcMYmhyQtiDtG85yJkrQdP0xeSg+yjnT2phZIJKk3wGYcW850ILvDTj1/uo6YxoZ+P31HNbZZon1S1Dri8SlJ3cFM7Hh0qtIQn3EI9dF2RIOpsDHcQ9nxDLByUKOhmaB6hspPL4luP/A75zqqMSUGHauYPPIRkISkjGGYlV6KvGU7RyQLTilzsBzCRRAFu6KzICOnskQsg1g1TskO6EPntOx+4pXZDj3InppVzzIg68zQZsc+Ba3mfGohVnuqVj3PyG+oN1+7Kmc41bXz+Q1pcSseRMFznbSVTTDiStLgluYX0VhsSo17GVycKakHYS5fatEk5UL3HUG9DCqG0YxrWPe6nUOLWthyjGZIAxvi+C1418ve9rr3vfCNr3znS9/62ve++M2vfvfL3/7697/mCQgAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXl5eX19fYGBgYWFhYmJiY2NjZGRkZWVlZmZmZ2dnaGhoaWlpampqa2trbGxsbW1tbm5ub29vcHBwcXFxcnJyc3NzdHR0dXV1dnZ2d3d3eHh4eXl5enp6e3t7fHx8fX19fn5+f39/hISEjo6OoKCgsLCwvLy8xMTEx8fHycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8vLy8vLy8vLy8vMzMzMzMzMzMzNzc3Nzc3Nzc3Nzc3Nzc3Ozs7Ozs7Ozs7Ozs/Pz8/Q0NDQ0NHR0dHR0tLS0tPT09PT09TU1NTU1dXV1tbW1tbX19fX19jY2dnZ2trb29zc3d3e3t/f4ODh4eLi4+Pk5OTl5eXm5ubn5ufo5+fo6Ojp6Onq6enq6err6err6uvs6uvs6+zt7O3u7u/w7u/w7/Dx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHyCP4A2wkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs0pNR66bNmvSmiU7JqwXrUmTehVL5iwaNWvZupnTGlWdN2vMZqHdy7ev377CnFXTBo4d3aLmskkT9rex48donWWbe7gnOWuMIWve7FeXNG6GK9c0Z60Y59Oo+dKCBlo0TG3IUsuePYnWtG+uVaq71ou279nBsuUuaW7a2d/IZeuypm74R27MkkunTataOucaxR2bzt13tebYK/5W606eti7h4SN2C1a+/exi3tI3TPfMvf3ZzMjJT9hN1/3/slWzn0HUAGhgasmgM2A75Jh24IOc0cLNftkcB+GFmlETXjrOYOjhZsZQ5po5vX1o4mO0xCdaOP6d6KJj21TWDYbFUJONNtx8I4451wmkTjrmkBMON9dIk4yFD6KX1TYPHlMNN+BRpA4410R3oDVZXQOgbVB+FI41yQA4zVVa3sdMjCWhYw179jkTmlTa2CeMNQqqFI40erXnzFRMlkdLNCq6xI41SHInTVTetAdNjzKxc02L3V3zlDiFJlcMODixMx55aC5lDqTS0aJkTuMoQx5uSrHD5nTSMMqTNv4lhiriUR1Op8uEQXHInTBvFpUNd8u4GtSv02lo1DjcjUpUOJkl101R6jT7my7hKKVOrcjpIuxP2P5WTJ1LFZjcM0MRixwzUTK1KXKd+pSOdIdGZU1ytGyrEzTJjTmVub7F21OiyPk7VZm/VcsTO9LORu5V0yB3TE/YILdnVqb+ps1O6VSK2jG9WqWOg+ala1PDvtECblboaHwalji9+9uzlcX528k0rTsby6LV55u+NblMGzPDqRPrbPa+ZHNq9ToXzm84y6ROnrMp69q85tVE9WwPh8fO0KlJ7RKoqY0jX5/vzTQjbcbKV/FsMMOEL21F50aObwu/pI5vae93NP5qIq8kM9ELtuOzbO2yZKVsPC9Ismx1s4SObzTvZ45vfaPEL2qNB64z4S5tnlrhC35DWzQuqQwZLR0HDnJqlZe09GyJBy4Qwam1nRLtqGEqO0GPzybpSsvM1svuBq2N2sQp3T1b3sS3E7FstKzETjbTrL6Z7c33LtusKrHzzTXPgP1X67LzMhvoLaGzTTVh+pVM8wZJszxO4WQTDZtNw9/O5ZxlrZNdkWve5GajP6LkAnAFDEq3TiO2BAJFXLVzIFCe1zUJ/oRsqBGQBXvyutRAY4M9GdxpgAZCnsxGGSU0oWzel0KdmM4xxmihTsSnGWHIMCdc28zwbngTGkJGF/48vAltglgT5aUGiEScifZQY8MkymRuK3SiTEQnG+RJ0SUYPA3prvgS3HGGeVxUydtS87swsmQ7srmYGVlCG4OtMSUDlA353hiSsx2RjirZ22ZIiMeToDE1GuxjSUR4Gq8J8jm0QdUhRwLB1KRukR7xIWSWAcmRAEw2ZazkRxaXGkVqsiPseKFjUPfJj2TxNFYs5UY8hxpsqLIjhDwN9155kUaiBoW01EgsOWPIXEpEj5yJmy8fssvNbHGYFgHmZnCFTIogizrNtEhsaJO/aD7kbwi0JkTScUDaBFKbD1lgaoQJToNQEHblfEgHaTPLdB4kHTnEnDsbYrzZtHOeBP7h5M3wqRBbyqYXj+RnO5R5GuwJVCBXo9tBCUQvcqZzjBZb6EDS0T7kZE6g6EgYbYQxx3JqQ5SnEYdE6cMdVy60G+abTjMWmg757aqj0SRUd2ghUn7Cqjy6m6c3/tgdZqbTHIcjjxrTuY1m2CeT1rwTSH8DxmGqaVX2aeoNvQFThbAjHNi434FM6sTe0SIZ0rgGN8JhDh5FCUji+AY3tJGNakwTQj5N4ilf9Jhc5FSKBKUrX3pxzyDWU69/cYZDZQhYFPWSiOssLF+MoZ838k+xUr2iPgurC4Oa0XqApUZViVjMEymjsYK0o16DEdc+etFEs0DqIVn5oeVsNoySPHeQMIb6ySViKBml1eRcATSnvlbSn/fpRTVqisy/tkcX1HBjM9lxH1o8IxvE1eZVtVENZ2h0Ns6FrkQHYo5utJUa0XBGMopRIlr0QhjHSEYzpAGXbpBjsNuNr3znS9/62ve++M2vfvfL3/76978ADrCAB0zgAu8nIAAh+QQAAwAAACwAAAAAyADIAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVlZWXJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKy8vKy8vKy8vLy8vLy8vLy8vLy8zLzMzMzMzMzc3Nzc3Nzc3Nzc7Nzs7Ozs7Ozs7Ozs7Ozs/Pz8/Pz8/Pz8/Pz9DPz9DPz9DQ0NDQ0NHR0dLR0dLS0tPS09PT09TU1NXV1dXV1tbW19fX19jX2NjY2NnZ2trb29zc3N3e3t/f4OHg4eHh4eLh4uLi4uPi4+Tj4+Tj5OXk5OXk5ebl5ubm5ufm5+jn6Ono6Ono6eno6ero6erp6uvq6+zr7Ozs7e3s7e7t7u/u7/Dv8PDv8PHw8fLx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLx8vPx8vPx8vPw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fII/gDDCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izSgWH7VmxYLpu0Wp1qpSnNWjRnrrFKxixZNGyaYUKLtowXKXS6t3Lt++aWr+KQds2tyi4Z8Ju+V3MeHGpX8y6Fe7JDVmuxpgz+71FDNtkm9yK4dJMuvTeVMUkf36ZrJbp17DR+pq2WmW3YXlj635NC9m32iSxATu7u/hrT8N+A/cI7bLx57CRK1+O8Zpr6NijJ6desRuw7OBj/o9Kxl3isVHh08O+5bk8w2mz1MuHLQyce4Tggs3f/xrVs/sFYRMffwSW1st07iVDXIEMZtbKNe5900uDFGq2CXnUbQNLhRxm9guCk12TW4ckLtZKe5NFsyCHnpSSCi234FLLLKigR6In0kyGDIWe6BKMYNjYpxA41ywjzC6pUNjMXMIU2MovyaBY0TfN/DLifshk5Qt/rAwj10fVDEMLf8NYBc6E8nkCDDUmVfPLiuEFI6RU36nXyjFzntRNMUmqF8xUTaaXCzQwQXNdeMVERYyg1dD0jGLhMfPUMeHVEg1O0UCa3aVMRQNeK5LuxMyVxnkCoVLb2AhdfT59Uyd0/qUQhhQ4hxr3oFDStIJdLXkSpd+qvf6UH3ZlGrUMdKw0etQycOrG6VDXNAubLqohdY2uxpXCzVDgYFvcL8Ea1c0uz+ky1KLGFdvUsMYZE5Q2zyUaVaDFScnTaMURQ9UwxtXyUzLGCWNVMcYt01M3qsb251X87oYKiDf98m24UYGjS747XVMcLhRL9c2AsXmybU5oijeyVtuQatrCN2VT3LNzSVPcqTa9Cpu+q9ELGy83bbPbLB1f9Y23sH1J06+x0byaxroJXJNz9HHXMGybQAwTNm+alorVk4HDim44g4YbaYSWx4xuqQT9EjjLjNnYLgDWatqSO0nDC2NKc1eN/m65+LRNMM3CDWA4d8d2Mk/f8KlX3uUxDdsxQj0zGs+DC3TxtERhY/Tg0OhWbeVEdRtblqAbZUxs5pZumLSZca16TxLD5szrRDkTm7q0A/UN643hkvtQsZvmidq/22Q7bIwXr1M3sUGu/E+alsby8zwhXdos1PvUTGzEZw+Tz7DJ6r1ORJPG5vg6FT43+jrpTJq87N9E6WvTx+8obL3Y3zJs/upf0zewgYX/bJIwzQhwgDQhxWtagUCaqAwzDGygTMrnIAnKZENas2BMQEaaVGgQJhTETCk++JJN9IeELQHga/qHQpW47DWpa2FKHFcaYMhQJdt7De5uWJKplSZs/jwsCbleg6EgliSEmCmbEUeiwtdoY4kkkdlxoEgSgr3Gd1QUCb5M47QsfqSJpiGdFz1yvNcoa4wdsVlpuofGiDwQM7doY0f2djM5csR6pTmjHS+yO9iMgo17XMj8XvOLQGYkeqUxmCEtgg3PLdIi6jNNDB8pERqaRpGUlEgkS+MJ12UyIZaU3ic1qZvzjdIhUgzgKR/iNd3Ab5UL8eFrPgdLhLwwNl2sZUIQaZrD6dIg6FLYLxNCR934cpgC4cYbNZNLZAoEHLzk5DGRKcvXANGZ4RhkbFrhSVgCrDg5wqZAjlUcG4ozHDnczShoOczTGUeMyASHGmMTR2x+A2rq/hTfMKuBxNfA7JfBTJcztxHN2HAMmczCDirYecpqbBE6yfskN7YEHrrBshuAC08Rc/fPlVRjOOm55uuesYYuTXMk4ECG3LJTP9pdDi26aAYgp8SMXvDuOb2YKXUayRdP8AIZ+tRIN5IxxP34gnrB88ssggGNkz5EGsJYqXyamTtucPIWwCjGM7DhVK5A4yu6uGl63EU990WnFbgQSyqWyaDZUe8boihRekZhyue5U67ZSYW9igeOPuEVOrxg6O/S+dfieGKj2ZNqYU1Ti81lr5iLvZ1OK7fJyJLmFhF93i0tSxpUhMp+ZuXsYpAz2dJ1Q6yiRUsvnOq9gKa2L77Yi6v9/PpavXgiGKxlHzhaU1u0oKIY3fRfNjLK2Vp8lofgYAY+S1SLYuS2hd1YRi/YKp/mPneJ2ChGLlBbnFoE4xmCDeQ1khEMXHAXM6nIhTCiUVo7biMaySBGMHhxi1PspUWnaAWMfASk4J7zvwAOsIAHTOACG/jACE6wghfM4AY7+MEQjrCEJ8yTgAAAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1dXl5eX19fYGBgYWFhYmJiY2NjZGRkZWVlZmZmZ2dnaGhoaWlpcHBwfHx8i4uLnZ2drKysubm5wcHBxcXFyMjIycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLzMzMzMzMzMzMzMzMzMzMzc3Nzc3Nzc3Nzc3Nzs7Ozs7Ozs7Ozs7Ozs7Oz8/Pz8/P0NDQ0dHR0dHR0dHR0dLS0tLS0tPT09TU1NTU1dXV1dbW1tfX2NjY2NnZ2dna2tra29vb29zc3N3d3d3d3d7e3d7e3t7f39/g3+Dg4ODh4ODh4OHh4eHi4eLi4uLj4uPk5OTl5ebn5ufo5+jp6Ojp6Onq6enq6err6uvr6uvs6uvs6+zt6+zt6+zt7Ozt7O3u7O3u7e3u7e7u7e7v7u7v7u/w7+/w7+/w7/Dx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHyCP4A1wkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs04th00aMmC7bs2CtUpUoECrZuXqBYzYsmjUummVGk6asFugzurdy7evXlCzfC2rVm7uUWvIdp3yy7ixY1G5hE1TZ9intF5mHWvezBgUL2jnKts8B21XXs6oU/e9pWybaJjVeKmeTZvvrGivV0KbVbu371PECuceSW5YZt/Ie/e6NvzjuWDJoyfHla35RmSnpWtXLtw6xWiLt/6L7w1qGGXvEKvJGs/+tzT0Dc3Jbk+/9y5z8BNKO16/v2pR0ORX0Dm9+GdgbbmAI+A61axy4IOzgfIefMJAaOFswnhXDm8XdogaLvjl1o2DHpa42SrViUYNfya26JeElUXj4S29CKNMNNVksw05lKnTzTXTQKOMML7oEp6FysxFDISy/AKNNhaFE80vsEBYTFbAGAhKL9Kcx1E50gTDYX/DXDVMf6cEU41J4QSTXXvBVGVMfblMmJI6y5DYni9TNdOeKMLI9dI0uNCXIVTPsAcKMTVRUyV7zjw1DXu/dFeTMm9qR01T22Sa3C6C5mROgeKBkmJS5zwqnSjT/KSeeP6nWGqULtvhImtPy3jq2yxeFrWkdssUVc580v1ylDTanYINUshs12pR2ajqmyzhKDUNi72dEppRvyCXS69IdbNedLwgJY2um/HplDq3SGdnUeC0q1qcUKlDa3Ki3DpUMakBQxWxvvWiVDVHNiZwVaQixxyquzi2C1YNIzcLU83oyitW6ozZW4BLbTOuXqKMo1U5etZ2CrhIZanXmnNtg61qxji1XyBJVlYNcqCI3FQ5V76WDHL0LmiUvLWBgo7QRnWDLmfJIG2Un9k6bVShveEm9VDb7Ho1USrXxvLWQIXjW7lgB1VhbyGW7dM5S2v2jNpAnU3bw3D71I1vKNeNU/7EtL2rd07I1qbu3zsVnBoohPMk92zPJo7TzbUd6jhOL2+Gy+Q5+VI05jgFTptrnNekTm9vh14T0ar5azpN0NFmy+o0eT4b7DPdXVu1tMfUdmPW5B4T1bP57ftKmtPWzPAv/Tobo8i35Izgzbc0KW2XR79SNrXBYv1K5pi8/Uref59S+OKfRH75JZ2P/ki7+3XK+iapD/9H3dP2/vwi2T6b9viHZE1t1evfR6Y3m4MJ0CPQiNwBPzIn2tRsgRwBWGqsBsGNSCs1X6sgRkZXm1Bp8CL/q80HNfI86o0wI8XD0AkxUjnNcGyFFIHc52BYEeXNjoYU+ZhqcoHDiWStNv5B6+FDFqca4QlRIepoH2PydkSEJKo2dGtiQ1CnmhdKUSE/rE3arpiQFM7mFlxciDZ807QwJiQXvsGdGQ1CDd9EcY0F0SHj4GiQBPZmFXQsSDmUyJgy5lEgfKvNtv4oO9oMLo/naCFnTpXHhNXGgHlURnI8CMc2IsdYfwyHIjmjRjiqQ460kRwd7+UbUAwSjoHszfHyKEEA/tGR5OlkGNWRStLRcUPS4SEcR7QqcsCxGptMjRENQ4xTBqVZ2unZcCoEIKF0Q2PJ0YV1CBiIW4CuJ874xHZgYczKlINFi2LiTK4BzeSA4pq5AR5fVrGpm4SjldFpXG5+5phe6Kslz/5pT6Sacw3OfOIXjGTJNoDBx9k8MDfoKNlmZmFFlEhDneM5aG7guRlQBAOdIpnGLww3nphZ54m+gQUwpLFFjajjMgX1jShFlFLNyGKk3YSINqDxC1DWBxnoKad2QCGLtQgjGdKoxjZCow5ybCMb1YjGkHpBxQcNszKtc1GLRLGwaUrVRbCgZG6+eVUT3SKmr4FoVyHEPPRko6Vjjc4qeicgcKAxrQfqhTg/ila4pkYUT4UPOUhp1+3woqRSi0Yw+8oZWWSwbOcQRl0JG4hTNLRu5ugWYw9X1smFA5aT9UsvADu5brgps3yxqFZX14wL9lUWq7QeNWrZVV60s3zmeGsGX00Eil2ARoAn7cViccalE04jGE0dzyp60YyAwnCmwJjFbv+Ci2FM455NzMYziBEMX/RCF7eQRVnOIopVwGIWt9gFMJQhDWtA94/oTa9618ve9rr3vfCNr3znS9/62ve++M2vfvfLX5UEBAAh+QQAAwAAACwAAAAAyADIAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dYWFhZWVlaWlrAwMDJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrLy8vLy8vLy8vLy8vLy8vMzMzMzMzMzMzNzc3Nzc7Ozs7Ozs7Ozs7Ozs/Ozs/Pz8/Pz8/Pz9DPz9DQ0NDQ0NHR0dHS0tLS09PT09TU1NXV1tbW19fX2NjX2NjY2dnZ2dnZ2tra29vb3Nzc3d3d3t7d3t/e39/e39/f4ODf4ODg4eHh4uLh4uPi4+Pj5OXk5ebm5+fm5+jn6Ono6erp6uvp6uvq6+zq6+zr7O3r7O3s7e7t7u/t7u/v8PHw8fLw8fLw8fLw8fLw8fLv8PHv8PHv8PHv8PHv8PHu7/Du7/Dv8PHv8PHv8PHv8PHw8fLw8fLw8fLw8fLw8fLx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vMI/gDPCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izTnVmzFetVqpQnQrFidKbs2g9rXJFC9evYci0Po12jJerUWjz6t3L9+wqW8OeyS36LFgtVH0TK07c6dUvwYN5PvOlarHly31T6VIW2eYzXqkwix6t99StuJ1fDmNFurXrs6N+SUudkhsvT69zu85kCzJtkcpkmdVN3DUs1L87MmtVvHnuVceSa3wmy7n13K+cSa8Y7dbw6+BJ/lOqFW07RGm+MoVfv9vXbPMLkeFlT7/1KOTwDer6Xr+/aF3v5ScQM5X5Z+BoqTAj4Dm/8Hfgg4tR8gt80bgC4YWiuVJecs6EhuGHlqGiHW3HcALiiYtxEl1nwqDo4mLCRGYMhp6wMosvwxyjjDPPBCjQM8oYI4xXBUJoS2SwGMgKLsZsWBEyvsDSyYGyRMbceqjcsqJHyNxyin+1DCaNh8618phJzOgyH3thyvUMbsRRMsuIKhkDi4PWHSkXM+q9lskt3MAUzS9kXpfLYMi8Rl5Nw8B53YRyEUOaK77VJA0veBJXzGDAYIaKMTw5w5p1mXAmFy4R8gJUg9aNEqhc/kn2dZ9QzqxiXSs+XiXNlXrNkuuqmbp2y2DRFPpGjEcp4yhxxAzGzXyZgJrUM8a+xsmrWjmTySgKLhUNr7q5Epky2DIVK3HDLEiUNK8Ul0ml6v7EbnGtxDvUvMQFY+9Q5/r56747SVPka8MCDNSbulECr8E7KRMsZm0y7NMwxHUrcU+26FblxT1Fs6ZrFnOs0zG66SnyTqi+Rkm5J980Zm6+tLxToq958q/MM/VLWro44+RMbvX2jFPGr9EpNE3c9NmaqkfbdMtrqzTtWW4sSw1Tu64BYzVNM7om7tYzfela1WCz1KlryJb9Ejev0aJ2TOCKpsrbMPHy2s10m6TM/mtb5r3SsqJB6vdKs7g2y+As+eJaKoiv1HVrTjZuUjSvmSr5SaW4tunlJ2FNmr6cm/R0a7qEbpLirZlsukjBGLc6ScW4FvXrwLlWCu0iPeOaJ7iHRHlrvPf+EdvACz/87sZ7RDxpwSe/0fKjjeI8R9CLJv300yGPfUZ7t3bK9hlR3FrQ4Ft0NmmwlH9RyqQVrD5FtLjG9PsTxY1Z2vRHpPRo0uYP0c8g819ExNcavAnwIOwbzdwO+BDPjSZiDFyINPYnGtBFcCEkc43lLpiQ0YnHgBw8R7UuM7sQIgSAqTNhQn7xGp6p0CADEw0lQHhBmrXmFS88SOHQlsOCcONh/oshmwp18Rry9fAc0TCRa2J2RIHkIjcLU+EPi9hEgdQiNy7sIQrtQ0MGjso1gushC/0UuRc6A4iLcV8OpWErKDYRda9J3xEzmBujqVBbJTvis3STCSEypYtMsR9pwviUXeHwKg50zfekQq2zwAKQRvlWcbLoFGYs65FSiUYbwyWVY1DwDbKAZFCiEcPd+BEpkuqL25zCjRG2pllQOV9fXlHGZAHuNRt7SgITkwo7FmUYaMRMJ05ZFGnoTDEqQgoRndM/pkhyNJSYn1CeIcjX4OIpjZSdL3kyjE/m5mtNsWTCLMiTaCSSOKmoJVI82RxXEDMmwFCiczgRxaMQI5ii/uGEe3BiDFfmhhJ9W8oYr+MJQsLkGcdsDiWZYrfwhIIX7ySJMu60Hmk+ZZnhoYQsNoiS1dDHolDB6HpYsdCQHKMWU6JP6awi0vVk4hXBCBlHTprS+qz0Krukjydg8Qv8TEQaxsDFF/1zTa3A8UF/8YUwjKEMeEnjGc5QxjGGwYtZsCIUGLqpVorhzRe5iBIlxQozsOrVsnbCp84aallBlIp6ykUa1VkriDBpnm7KFUKZwJ95qHlXA7XCrdIJBj776hoJGUxUhAXPKrYZr2GQNbG6CYVeGSaN/UC2sLwQpYCecc7LKqYWETUYMjbp2b64QqZHU8YrBltWjXJ0a8+4fYU8IesJXqizbNIABmII2wpYck4ZuNjti1bhC8A2zhm8KOWBzBTay3FjGLZQ7nUosSRjaHZ10QiqWnUzilf4Aq0MRMYwfnELWrhiFY9FCyU4EYpToEIVrajFL4zB2Cra9774za9+98vf/vr3vwAOsIAHTOACG/jACE7wEQMCACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fYKCgoyMjJubm6mpqbS0tL29vcTExMfHx8nJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8zMzMzMzMzMzM3Nzc7Ozs7Oz8/Pz8/Q0NDQ0NHR0dHS0tLS0tPT09TU1NXV1dXV1tbW1tfX2NjY2dnZ2dna2trb29vc3N3d3t7e397f4ODh4eHi4+Pk5OTl5uXm5+bn6Obn6Ofo6enq6+rr7Ozt7u3u7+3u7+/w8e/w8fDx8vDx8vDx8vDx8vHy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Dx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8u/w8e/w8e/w8e/w8e/w8e/w8e/w8e7v8O7v8O7v8O7v8O7v8O7v8O7v8O7v8O/w8Qj+ALUJHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNKvecs2bFhv3jlsjUrVqtJk2LZ0sXr1zBjyJQ5s6YV6j1mxXjJQsu3r9+/fmv5OuaMWl2j95T5igW4sePHaXsVY0b3ME9oxXBB3swZsKpeyipbpimtGOPOqFPzVbUr2b3RL60p06W6tu1Jqnw5g60SmjBYt4PbtpVMNO+Q0HYJX34bFjFpxz9G88W8enDd0TXeE6bKuvfbuqD+Zbc4rPv387aHGR//cBj697VjKWMf8d5Z+Pg75xJP3+Gx/ABupoox/TVkTS0BJuhYLtAVqJAzCkb4VyvzOZgQdRJmiBYw61kokDQahlhLgx4SREyIGsLyTIkEUXPad6rEUstYspiHIlqqLMPiQMoI14ouwyjzDDSvIXRPNNA4U8wuwGWIzI4C5aIakMtEc1E0yggjpYLCQPlMZ7YcU2RH1jADzIv4/dJhgb88Ngsx/JH0DDG25Ncli9Lc5xcuOq7kjHLwDbOjMXsyA1M03L1XDIvWzIJWLrvNtJ2N3h3D4jKQ4jQdek9CmRM0tH1nqKc5KaMnc61YSSpO0oRanS3+a64qEzKUCheMrDk9g6ZwyeDKap3MqRKnrzRR08ursRL7UjHVEaisTf8tp4qqz9LE7HK7VGuTe8v1qi1NwSwHy5jfwmSNq7cJWq5M9+yqmiokrvvSM7Wqdqe8MPUoHLX4usTtbbf2Gxstwg0rsJ/C/XLwS+EGF+/CKd3TpG2LQsxSMsHBkqzFI2l2W58cp8RMcL2EvNKWtj1sMkkQ3lbxyifxcpstMKPUsm381jySo7ZZqnNJ0dZW8s8kUVMvZ6oYRvRIDdcG8tIgQXPbvVCDBKxqNFcd0r+qbay1RTertuLXHllz9Gadkt0RhqoFrDZHQad2y9sdhZ2a13RDZM3+bQbnbRGCtY3qN0Zsp+bt4BcRWpuziFukr2rqNl6R3Z0pLHlFUtemy+UVgVhbLZxTtHdts4RO0cSolW66RDynpvrqEKHe2euwOyQ7Z7TXztDZbur+kG25+47Q6KoFL7xBnhd//EKUc4bL8gphLDT0CSluL/UINZ3ay9gTBKhqh3c/0KmoRSq+QJnXlnP30teGt+/aowb6+QNdnRow9AtEzW1pn9++2PnTxrFqo4r31W5/ttlc/h6nGmIEsE22eVr37sE7yJCre3FLDS8C2DrV9K97I7vNBbE3wNpki37Jq43PzieM4IwQeumrTS6QIsHsyOw24RMKNKSUw+M0rzP+GiPKPYDBl1a8cDT2Uw33fmKNY5DPctHJYGpUcUSdLANwfzEfbKBRQchQrSegcpPSRmONJKqmbzmhRgs3A0XLXOs2Q+sJMm7nmA9m5UsF64kzsNgZVYxNK9Jw1/14Eo0bxqeKUaGGGaeospqEMDgnxEoJb8O4nEhsOfi7ChGFE4sx6oSBU7PKGnn1E0MGJ3JQsQYEhfO8n0iji5xZIlOsUbjbCCsoyKgOKpdijUkGZ4VA8dhyeOFJpEgDZcJRoFCiQb6ZNXIozqBjbWDxzJ48cjmxQGNQjgHLzgiOKCeqjirs+BNjeeeLREHXchgkFGd0cDmwSkq7vDPOcsZPXOv+KwoevYMLLeJkGYIMjh+bAsrq9CKfMXGGOqtTw6Tk8jyqCAYiVaLQ95BTKVIUJzC0aTNkfmeXTnkjenJRHD959Dtqosoo39MKXyzDgBeBBjH4iB50RmWV+FHFL0LzEZnS9D0gncpK86OKXAhDGdU0kDOO0YuAogeYV8kogFrBi2I4AxrRqOI9oPGMLAkzQlDFyjJu1JcYzagWNSIrWi56lWdIU603akVDtRKNn8IVRbVAaF2sscm73iil7DGVXzWkih5mp1WDjdB+SsTNxOanFWylzzPe6Vjr+GKi9LGGMZpZ2dvE4pukusc9OztFYsDUQdHwJWkFNAzMesoZt1iNrYCE4VpZOcOUsl0NMJKqLWkM462DnYUxaqstayRjkXDNjT9XBg1jfJWstThGMYl2D2T0opsQ3cUxOAo1auCFF061ji2GsdzG3SUve2FOLHphjMJQTxrPYEYyjDEMX+zCFun1Cyxw4YthIMMZxA2ggAdM4AIb+MAITrCCF8zgBjv4wRCOsIQnTOEKlysgACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUnBwcMnJycnJycnJycnJycnKysnKysnKysnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8zMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzM3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzs7Ozs7Oz8/Pz8/Q0NDQ0NDQ0dHR0dHR0dHR0tHS0tHS0tLS0tLS09LT09PT1NPU1NTV1dXV1tbX19fY2NjZ2dna2tra29vc3N3d3t7f39/g4ODh4eHi4uLj4+Pk5eTl5eXl5uXm5uXm5+bn5+bn6Ofn6Ofo6ejp6enq6unq6+rr6+rr7Ovs7ezt7ezt7uzt7uzt7u3u7u3u7+/w8fDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vHy8wj+AP8JHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNONfetWjRmx4b1whXLFBo0sXD1Gpas2bRs4dRplTqumjNfZ/Pq3ctXryleyqZ5Yzf36Ddow/oqXswYja9m1swV9qnO2rJYjTNr5ovrGbjJOp9tHk06b6xm3AiDpvmttGvSsZ6lWy2THebXuDUn80YbprPcwDPzmqa6t8rWwZMvNtWMnPGU7FIpn67Y2brnJ5lR3743FTXsJbX+cR+flxc38CKjk1+fzDn6j9rXkzf1/X1H8fLXI5NrXyM7s/mRF0s3/Wm0TIDyPVNcgRRZg6B8wJTDYEXrPChfKt9MSFEypalVDC+3WaiYKedpGJGDfeHCjDTVaPONOdcZtI454WgTTTK4iJhXNSZCVGFeqShDjXsUseMNNcoAiKA0PT6kzDHSjOORkc7kGKAzCzbpEjnRJCafM1rSRM6B60UTJk3mkDneNGeiqQx517TJmpfblSinTNdYOZ0pGd5ZmzRKJpcKf37GZA6dyRWTZaEuQUMdNIzO5E2IwREYaUzqAKPcoJfGtA6HiS7a6Uq/JcfkqDBRk5wpkqH6Eor+wCXjaqrJaTPrS44Ch0uMt7LUTHCQ9soSO8cEN5uwK6mj52tgIrsScrkd62xK0gDX7LQpFRsttimRE2hpz3CbUjTASSsuSezskluw55YErWu3iNruR2/iZue8Io2T2zL4llSva6YQ2i9I+uJW38Ah/VtaMQjnm1urDX+krWsHR9yRqq8pY/FHPwIs78YWKUwabyBzhJ9r7JaskXSuDaMyR2qWJvDLFmHs2jY0Z2QObqfmfBEvrzXjM0aiuRbM0BfBSpopSFtEDm5ENh0RO7hlIzVFxLzG5tUS/Yoy1xLZTNq1YDu0zWv8lv0QOK8ho/ZDT7f8tkMdk6bL3A69Fgv+3g0tuxkufDOkboeBL+S3ZncXnhClm+2ieEIs2/04Qt9qxsvkB73mC+YFqfMaMZwTxLZrGocuUDWvhWv6P+S6trXp8ZUW5+qIjvaZ6XWTNvPk3OD2ceCtl+by6sgEvbp6rvFo+rukRY15qaWl4tM3zVyuJeObHbNTN82EaGuPvb+W8kzscNM9X7L2GHtpVpOvDTOR97W7ff/hxqtL7GizTPyLvc6g0qRJX0vYkY1lVI4xw5tQL3BjDZeo44Ca6VOBwvea35WkdqNxG4MmVhoBtmQaubmdfcSRG+U5MDceRA+oXnO/loiMNOKwjzdyo0GYZGNf72EH0HAzO5jYJoT+6BFbaXRhQZQU7TW9KCJW1oG90fgPJjvLTc96k6vXpKKFMPHaa0zhvMmQcF02iRtuhqHEqaxDF8CZH0zW5xrVgUaLr2EGTsSIm+8V5mRQy0nMXJMKiGWFHPwDl04KlhsyakUduQDOFXcCR7SVUSnsyBpwTJgTdTSRNHK8yh5dw4tHugR1wSGbVIKXG5L1hIO4MdNUQBic0vnEW8lx41OqmBtOAYWUuVEGFpHCjkZWTSjswODn1EgUdmwyjkQxRyBfo4suEkUdxlCOLnbpEzwqsoFHmdR0wnEUWraSmDzBJXCmWMxiUAcXpvzJOISJG0UlRR3LzA0zwEkTdkADgq/+wQU9gfINfFrxiTfxBhqpkwpnGsWayuFFOmkijhXuSYJMAeB0liGlmTSUPHZ0CivHc4x7seSi5KnYLOXDi2pQUyTqmAY7lTM+qByTOqZYhjY8WaRrFC8/oowKO9g4H2Vgg6YMYcdhjuHP6ajyKtV6kCl8ClSClO8ZxSjqdkRqFVCKSBfIcAY1uBEOGBlEHeHABjWeoYxhHO5BlMyKN6SKoFjwohi6YOuDsLEacVxSR3htDIl6kw5z5vWvmdmFQQsjTsAaFg3MaOpUvHGLwx7WFNh8jzoc6lgRCXZC1JBrZVcFDcVqxRyU3ex4ijHY/nRjh6LdTizSqiVqxDO1rmGexkl7tA7owfY1yahopMjB09s2RhmlvZM6oHFX355FGbq9FTuqgVrj5gW44tJGNI1bDGrs81Yp9WtljWFdi6mjGsjQLHVisQxrXHdeQo1GeANUDGhAtGnhmEYyijsaUzymGjEM3DrI4Y1rjJUZyBALLlhmirSs5RjMiEY1vOHH1Tn4wRCOsIQnTOEKW/jCGM6whjfM4Q57+MMgDjG3AgIAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXdHR0ycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8vLy8vLy8vLzMzMzMzMzMzMzMzMzc3Nzc3Nzc3Nzc3Nzc3Nzc7Ozs7Ozs7Ozs7Oz8/Pz8/Pz8/Q0NDQ0NDQ0dHR0dHR0tLS0tPT09PT1NTU1NTV1dXW1tbX19fX2NjZ2dna2trb2tvb29zc3d7e39/g4ODh4OHi4eLj4eLj4uLj4uPk4uPk4uPk4+Tl5OTl5eXm5ebn5ufo5+jo6Onp6Onq6err6uvs6+zt6+zt7O3u7e7v7e7v7e7v7e7v7e7v7u/w7u/w7u/w7u/w7u/w7/Dx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLzCP4A8QkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs1r9puxYsWG/dr1q06bWrmDEjilrBg3btnFas2ojS7eu3bu1iDXb5i7uVHB3Awuuu+sYNG99/TJtN7hxY1PFriVWjHSV48uCIUumbHQX5s+BNU/m/HMY6NN3TS1DR/onMtSw65ZKBrf1zmWxc9M9Bs52zme6g7dBxtp3zWzCg696Zpxmt+TCd21rHnMc9OTFalNn6e56clPStv63rOU9ObHi4lH+Kp/81fT0J42xh75sNPyQyeZD79X7vshm+kG3Cjf+hWRNgNCZgk2BH6HTzTfgjINOX+2I44021jyzDDLFCFMKgoJBw+BL7nCzzHog1rXMiDG1c80x5KVojH0stsQNMSkWQ2ONLImTjCkIJsPjTO04E+N8zAxJEzbB6CeikjN5Ix9710BJ0zeelaeNlTO5o0x5q4jD5UzbjHVdLzuOuRI6xXi3opoyWQMkdNnAKdM4WQq3inZ2uoQOisL9kmafKLXTZnJvEuqSO68l942iMH0pXDCQwtRocAtW2pI7U+pWy6CakuQOjsE1E+qmwijH56kooYNLcP5CsrqSN8L1J2tKwOmGzK0rHZrbqryShI6ZsSUarEnS6LZKO8eiBIxupjZr0jeegiotR7jllum1I7VjWWzGcFsSM7qhJy5I6HwLW3jnipQtbMO0KxI6c8IGrLwbXYqaM/iCtE1uxPT7kTtHnmaKtQJP9C5q3STcEbWxJekwR4CeFvDEG5EL28EYa/RvbAR2jFF3sfErMkapwqbMyRhpjJowLF80F2y1xGwROrkxazNF6p7mzc4UPQvbNEBP1OlpEhcNkcunxar0Q9OA+zREyMF28dQNcRMbpVg3BDFqu3TdkDix1Sz2QozBtsrZDMVmCtsLxfYK3AmRjJrZdBuEM/5suuR9kHWwhe13QYDB9otO6HxjLo9fn8a1n99sM00zyRQTzJHWWKk1vCh9M7kyxQBT8GBJD5ksbOGetDBoxVi5+melk3Q6ar1YeTRo7Jq0OWpvQ9lLbO+ZtDdsYipZ72nFn0TsadvWWDjvKjUJm7EsQhOb4CjpC9rhQ97+WeooARjb4gwuDxpzKc0MW53O5/ZzSmlPz6P4vCP8Ucpg81gxaPGuxLTPLHoeamJ3ko/Jb0T/A82WWHI80NCCRa+Kjf1AYprYLNA/z4nN1VaSK9jsqkDaw52fcmOKezWHbLkhX0pIdUD4hPAzG2TJgdymQt8ALja5awm9ckNA4+Qnhf4x8d5nTFFD0ggQNTFsCTZ0Qz3f4G99MiGYbt5nnCXmBhcTLIkzdCMo47TDfKdBn0y8pRsxtuZ1oFlWTdD4mT3ZJoO5aSJMbhgbYWQxKugYHWpM+JIXgqaHWalgbj5okyPC5oJxoV9uHoUTP7aRj1OBY27Ad5N0BUcXRYyKOHq2MUjGZIvB+YXOrOIq4URLJ+7IU27saJV2/C44WOyJAXVDjDsSxR2C1A0idyJE1BxjKu7wlW5a95NxNDA2x7DlT9rBwuAkzycdDM4wRrmUdkhPOCYLSjO5mEmhoOOVwumfUL4InV148iffiKBwXkHNoEgyOKYg2lGmcczcNMwoCf6k5TlxwijvnLIo7rhmclbRPJ+IA5zJgVlSxgHG4ByjnTqBBidh2U2gdKOengpeTryB0IE+Mynq844ylGmSdjiShFRkyjXmQ4tmQNRPy5hocnbJlGh6xxTJ+OhKxpGMD82nSlLJ53WKcU/4WWOb5flnVE6anF8UVCTbOAZGr6NUqUgKQasgxjMY2RF0YAMZepyPPK+iSBC9whjSsBVFxpENZXQURE+tStRSZBdT1CIYxTjGMp5xDW4ojiDtGMc3tOGMY/xiqgEyRcjigg3E0vWxsVlFSuPCDZlC9rKnqYVaFYMlzHoWiRW9Sj8/S9rBmCKbVbRsaTGLi8kaRxz7W84tZo3x0uYIVbYIiueIvBFb3OqHGPtsjjXC6lvo1IJ9Q3LHMhxbXIMxg6QnRGpzYSOMzXLJG9Kd7mWEodE+iQMZzNVuG4zBVU2hI6biDcxsggsndxgpvWQRhjSgq6ZuLOOtpe1FM9jLqnFAgxjhnU8tllHehLnDG9A4Bn7LUwvDFJhlJXKGMYhrsF0UYxnX4O/JxnGhaThjGcYgRi/IY4pX7OIXw8jrMpqBDW+EdnAwjrGMZ0zjGtv4xjjOsY53zOMe+/jHQA6ykIdMZIgEBAAh+QQAAwAAACwAAAAAyADIAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NkZGRlZWVmZmZnZ2doaGhpaWl1dXWFhYWZmZmqqqq3t7fAwMDFxcXHx8fJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKy8vLy8vLy8vLy8vLy8vLy8vMzMzMzMzMzMzNzc3Nzc3Nzc7Ozs7Ozs/Pz8/Pz9DQ0NDQ0NHR0dHR0dLS0tPS09PT09TT09TU1NTU1NXV1dbW1tfX19jY2dna2tvb29zc3N3e3t/f4ODg4eHh4uLi4+Pj5OXl5ebm5ufm5ufm5+fm5+jn5+jn6Ojn6Ono6Ono6Ono6erp6erp6uvq6+zr7O3r7O3s7e7s7e7s7e7t7u/t7u/t7u/u7/Du7/Dv8PHv8PHv8PHv8PHv8PHv8PHw8PHw8fLw8fLw8fLw8fLw8fLw8fLw8fLx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vMI/gDXCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izam1orNi0rWAd0gpENpYuYMa2hV1LsBXZt29d8RqGjS1YT3Dzvk2laxg5u1a36R381lOuYn8BR5VGuDFZUruWKX6qzLHlQKuCdZu8dNjly594VeOM9Nfn07iakS7K67RrWtJWC83lujavxLJ7yqpd+9Sw3D1P8eYNCxrwnOSGK/eF+/jMasqVxxrtfGaz6Mo/Fasukxj26Le5/r8E9j16q9jiWfoqH/0TsvQrdbHHTgx+ymfBdsn6NJ83MPssYZOML7fw159lvQD4kjTDyHcgYbooCBM5w8zyoF4RSggTNr6QcuFbvmgYEznBCPehMCKOGEwqHxqT4oi+GNjfJ8a9CNM0tjx4ylc2wkSMKgeqolaPL22Dy4G2EBlTMAf+p+RLzZjIXjRPvoTNWOyxslmVLZFDG3u7cOkSObvM95uYLbVWHil1oakemG621MuUca5Ezi3lyVLnSt24Ut6Ze6JUjZTKnTJkoCdFUx4viKZE3nfUNWrSkdiFKKlJ2xDK2yeHXjoSM9856SlJc0Z3SnOjgpQpdiimOpJn/uah6qpHFka33awhMRYdLriKVGZ0nfbKEXTRASpsRw4Ox+uxHumqXLDMZvTlcMZGm1Eyu1rbEYvKyaptResp5+K3GTnL2y/kagSLcsume9Gjm3rrLkTTREflvBZ5OFyr+FKUbG0Z9jsRk8PBIjBFig73ycETkaMvb20yDBGWvKkmMUT/ulbtSL6kMosuvQhTTDQRcxmufyipqdcnqtCiiy/CGCMNtAoKo1yYJ017miesWCwhqMqihOdw40qYMG9JnrTbcD4rWO9wep70inI8SkgscShpelrJAAo2XCsoyVjblhJ2o5wqKEX3YqEoPezawiImp3DWyqWITXYosaIc/tkKXl0b2ie5NRzN6R1dmysorTtcpAoCzVvUJtXKW9UKFsMuShTX1jSAsPIWcEkZn7YxfCfbhlLprlkqoc6po2TzcJ8DqDhvo4eE7XCxiCi2azWa9DRvpGj4O298kyT3cJTD591wp6gkOO0SqlzbLCqF/hnOsivHqKNsA2i2cqKeBE10yXNnuXLMqETO7qcFAyDqp30i70iS1wZ5enoHvRL8pzFe3fjgY8n5hoOu9JSKaSz53nAQlx6tfQZuLBnacO5VHWNkqyWvG872qvMrarnEa8rhmmzuBqyXSJA3GwTOAXmTC5hkcDgi5Aw22Hea+hSpUsfh3wOLF8H2+G8y/tug4fVkMkD9rUZ6vCnaS8gBJFutBoDKWcX8VEKws/FwLeSYWnT4FZNuuO1cnKnicEhxRZfo8DTPUMwMsVNAmvhtOEKyCznqtynCvcR6rmkhW874mRTSxHDKcV9YboedH86EdcNBj1aqIcTTYO8mjPzOKQw5lW08TzmksKNMTPMdVmjSKduY3RZ50o0mYkcWZXTKHMvjiinOpIi7cmVSyEGp72xOJ3RUTi5kaRRy4BFgQDEXdnCRyqN4iT2fiKFO4PWdWRSTKN3IEXu46BNySLM8sfhkUEI5Hz0KZRum7GT5iFKN/JVHFbzEiePK8wkbGmUYjeQNBYnCyfmEZyjY/qgle9pYFGseSBbK1EkxvvgdW6RzJ9swJ3tIQU2d+FJH2vzJNAj6nVj0LifL4NaM0siUaMRTe8/sEhLnIxmnEPJApPBFRE/SjV+Esz/udErnHhSagJ6kGrz4aLGmwswL4QIxK2mGPi8Uvqj09EKfiMxJiLG0D5HFj1KZqVMDkYpdEGOlEZFGMHBB0QepziqwnCpZXsELY9i0IdMQRi4c6FRBZqUZOkWqLHYRjGRIAxt824Y0mkEMYfhiF7mYhSri+qCYZiUaXRWrYg/0iWSwZRovXaxkF3rRsGzjmpPNbHReMc6wBIOwmg0tWXZx0KtEYxWiTW1jSHEr2XTjl6rNzewszgoYYoA2tl5NTz5xq1lZKDI9xogsbx+UCsPChxwxGi5SmfOiaeRSubuiLXyagVnosnCeSpIGbK2bl6RSUknY4AVeuEuYlGLVRt0YRubIqwphlPZJ2ACGn6D7Cl9wtFfS4IVwJesJXAhDuqOShjB0sd8DvWIXwojGe2eFDWL0AhevuC1hWKGLYDRjweTaxjOIAQxeAPYWsnCFcD6xCljgYhe+CMYw7Argi7n4xTCOsYxnTOMa2/jGOM6xjnfM4x77+MdADrKQh5yUgAAAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYZ2dnf39/mJiYq6ururq6xcXFyMjIycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLzMzMzMzMzMzMzMzMzc3Nzc3Nzc3Nzc3Nzs7Ozs7Oz8/Pz8/P0NDQ0dHR0tLS0tLS09PT09PT1NTU1NXV1dXV1dbW1tfX19jY2dnZ2dra2tvb3Nzd3d3e3t7f39/g3+Dh4OHh4eLi4eLj4uLj4uPk4+Pk4+Tl5OXm5OXm5ebn5+fo6Ojp6Onq6err6urr6uvs6uvs6+zt6+zt7O3u7O3u7e7v7u7v7u/w7u/w7u/w7/Dx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8fLz8fLz8fLz8PHy8PHy8PHy8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLzCP4ArQkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs2rdyrWr169gw4pNKquULmZjw4ZasybSKl9puxpjS3eNJ1jI4maVVbcuolTH9Fod1bcvolWBBUdFVrgxIlXGFD+d1bgyoldoJS8lVblzp16akzLuTFrVstBGaZFeXckWaqKnVsseFfn1z9GyWe+y7dNW7t+sMvPOGft3bk/ChuNkhsj4b0S6lNvE5dy5a+kzUVV3Hgt7TObbnf6z8v5SV/jqqISTT7nsFuHzv0udXs/SmKtN8GWXUk9fpa5P+ZGWSn8v7bJWgI29QuBLvpiCYGG3LPgSL/g9SBcvErrETCvNWRgJMBm6JAyAFm4yX4gsuWLhGgOi2NIukVgYoYssEePJg5EQQyNLy5Ty4Cf87WgSM8UF6IqQKzGTyoPJIanSKgiW4uRKqiAY3ZQoMeNjfpsEiaVIy5AInyxfomRMhedFkleZJgkTI3yrsHmSefnpKGdJS8LX4p0jIZMJfIjUxqdIu+TXyqAkQZnmiYh+tEwl8M3SqEiqndeJl5NmxEwn8F2ZqUeFnnfKpyA5eB6mpFrkC3wzupSYi/4HbjeqiGt8AougEoYaHqolxVKXJ6/gSt+m513H0pZ9eeKKnfRRFp4pLYHXmbIgkrdMh9utqZKuq3XSSrXSaReepymxUl0nrPyiHJ3bHbrSjdtFIt2b1Y2y0lzhoSJdlfGuxG51tEjHy3ngnvTKecK+xgy9zkmakrjVeeIdss6popKY4nl38HYSp3QeLt4NvCtKwpzXpHTMIIzSv8Yhsl6szoF2kq/bhbIev9WBfFIr4cVJ3sbVdXeSogCvd0t4451UpHO7kScyeii9Vx1c5OFbnc0nwVsds9ilvF0nKP25Ha+vhQf2SZBWJy992Bq3CUppO/c2fXEblwncX/cntv5zd5+0t9v9oWlcJSgJ/lvf6/39G+EnGZ4b4+uFt7ZJnKpN3zLhQV5S5c65vB4xZqOEsXHaYrfqdvYqHR7V3nFr3KwmEe1cq9g5W53PJgHtnILkyW6cuyYdvd2e2MFsHJknPe3cJ+u1bRy5JFldHaPDgR4e1yR5vR2G2LH8nEpa7+6duTWrlGd1UmIXvnEWp2S7c5MPJ71zyKMEzHms81bp9ipJe7t0S3MO9UxivMPNKzypUwn5+Mcb6oTnSCtx4HbaZxtTbSd/KMGN2sg2lvkZJxIcDAnFZmcb3TkHdivZX8Rs47jfBKwlmDsP9zSjvK29hDPhIQVqpFYd5r3kff5T08zpwlO/lmiwOijUywgFGBMeVkdmermfqGQivPB4IoRaCaBzoBctxTnnhWnpBXwqgcWSmNA5lRigV5jRQuDQBBkMqw7wwqIi+GAPJgsMj7rCUjL4UHAm1rOUGrPCjPXZ0CYQ6xlYfFcd4tGkj53yigTPc8eZMBJ+J8sKMZwHtZzAkUuls0ohA5TJm/AlP0DCCjMseB7c5YRY+dGXKBOZuUHWxHXh4R1Vzseqn2hxO0WESh7Po8OfECOOuZTKJauDiErqpIr5ceVSevQgXQKFl3oqY0+MYcjwpFIoy2hjvUJZFGF4UXIJ+8kQA5SJGRbFFpw8T9OKUscHwaIoyP5gZYDmSBRmjC4/oShlT3ZRtwB98yjGOOd5EOEKbboEGb+ETyTSSRRgIJNL7rwJM2ART0BBUSliXBFbTLFHm1BIpGzhYlK89yCS0gQYEQ2QziaDUrq4tEBOXFEwnTJMkX5CFuTMHi04V1N+QmWZFkKEKXDh0IkwQxeouOiKHCmVetaULogIhSt40dSFMAMXp+ioSFXR1aKc8qqFySoreGHLhjwVFWJFqTWv4hu0kiYToTgFK2SBC2CQcxnE8MUuYLGKUYgTrbTLyiTtmhtEbCIUnSgoYzuDiIxqRRdxnaxmM1dSrwCDqJsNbXhCQdGtLIOWok2tbCAYF3iq9rWdydnER+PyWdja1qZBTQszWJHZ29YUEWBEDTD+6Vu0mqK0krGFVIv7oE7MUzrIQC1zA1SJnWLnF/qcbivbKp1fxFS7q0mFQBcEU/AurhW5zVB5zVuZTtCirN45hiwKqN1RPFdOxpBFTm0biluRChm3QIVkNYuIUtAivZnqRSuIK1JEjGKrqXLrL2iRCk/09nGngAUGIxwRYvDCFq5IxSg+4YlMVKJDlciEJz5RilSwIha44IUwuMvhGtv4xjjOsY53zOMe+/jHQA6ykIdM5CIb+chITrKSl8xkjQQEACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY3BwcIaGhpmZmaqqqra2tr+/v8TExMfHx8nJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8zMzM3Nzc7Ozs7Ozs/Pz8/Pz8/P0M/P0M/Q0NDQ0NDQ0NDQ0NDR0dHR0dLS0tLT09PU1NTV1dXV1dXW1tbW19bX19fX2NjY2NjZ2dnZ2tra29vb3N3d3d3e3t7f39/f4ODg4eDh4eHi4uLj4+Pk5OTl5eXm5+fn6Ofo6Ojo6ejp6unq6urq6+vr7Ovs7ezt7uzt7u3t7u3u7u3u7+3u7+3u7+7u7+7v8O7v8O7v8O7v8O/w8e/w8e/w8fDx8vDx8vDx8vHy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Dx8vDx8vDx8vHy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8wj+ALsJHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNq3cq1q9evYMOKHUu2rNmzaNOqXcu2rduR2t5e7eVJmFyq0zjt2RNr2t2otfbu7QTsr9NkghOzWmZ46arEiTXZbXw0GOTLuCgXvdbp8mVWfjUH1eXZs6djon8601Tas6ZhqXvKat06WGydxmjTLnzbprZUumnz6l3TV3DdmYnHnNb5OO1acZW7xOU8eHLpK5VVP+4L+8pW24/+T/Z+Mpin8Lo1GSOPUltg9K07MWZ/shgo+KVRRadf8tps/JfZwh9Kv7AGYGLjDUgSMucdONgzCpo0jSsO7tVKhCZpQ0uFe/SCoUm3cIjMhyWR5mAqJJbES4W87TRNMnKZCKAnPVEnS2htweJgLjs9Y6Amu+yn1jWqHKhJMzrZAhkotrH1TIP4zZLTMqWxAuNaxziI2k3vlWbLNWvJCJ8rN2mnGyfdpaXNYwAqY9N/wZVCTFrLGAifgDRRud0rEJ71C4Ca4BhTLuhpgouQY7EJ3y40QRleJ32SlSV+NMokDICqoBULgE3CRCF+c56lJ3wXwvQMgGSmpSR+grJEKH7+66V1Kn6/wFQKfq+wBWd4sLxkJnzFsFUMgK2mtCJ8pbgFHHwtfodfmmz9CV+uLF2DnyZgtqWNXvBlq9Kld8oVInydptRleG6+NQx+16V0K3qZyqWNnduxspK18LXr1q7VaYJoScMC+xcw+G2JknHocWIYvujVmtKG6MnS2LLh0aJSkehBe9eq4aHYHqyNSYvevyL9Gh7JbZm8XaQlrYuex41xG57BJRGMnpSUsUIuSmJW5yFls8D3M4jwJfjXq+HpOxK/zgVLmcjbxYKSzuil25jL4cVrkijwFSuXys4le5Kj1YnG8HZim0T2cZVSpg18pKC0dnBxiwYfKHKj1zb+ZTJXh/fY6HWSWt/O7U3S3Gemhjhtho+0eGuapNbcdoIDjp5ob6P3t9rwSaOZNPBpXdK74cXaGGLoUWsSeOiV+1fA4eFpEsfbMUoZuOHxeFLPzsluWC/waUwS1NWl2ti51bk+EtbbNS4XxuGFatKo4TlDGX7zZYif8m5NOnJKXKPnu1zEOye6SUHD2xjEsauEMHreyjX5dg6jlBuzr4OcUuboSX0XdfBB2UgUtR1/3YVi24FZSgCIHqOtBXXiYwnuKiYX2m1HeimZxrXiNyTCOceALPlUxtxSPueobiXvC0/a1gK98AgvJaADFVsgiB6WrYRq/dMVfs63kmPBx4b+ZUEGgHQBE2cA6BZqESF6PAeTFoYne2UhBoD8B5MUhoeKZXHidmATk2t4sDo0E4vN4OMJAaoEaehZRVnyAiDbyWQa9HIhWdKHHybOxIIFhKJXmIeeWtiEhmk0o1Wu8bjg6FEmSkQPEb/CtO348SbeK5hXrFjDnNDxbkC0SiThg8ScNCOOWeNgVaZRyPTYESe845Ugm7ImBynNJoR0JVYuCZ9OeM0mYwTQK5+Cxwb+BIH4eaRUxnUgLPJEiBWixSqLgkb8cCKTOgFehWKxTKEQ80D1AwrrHLQKaBLlGjg8kBqH8owvBi6MRWkG6Q7ECSQRxTIc0sQLhXKM+R0Ig0P+Qd6BZiHKn/AClPhx42bW6SBQmO4nyyCgg05olGXY00HQ+YkvAEqpWw7FGBTFTyccaJNiaNFII2LKBDm0B1YclCbKeAVJEfQUSnLIpDR5hj451KymXHOlJT3pSo4hi4weaJ5LcQ9OIQNTlVzjF8DEqUCj0suVokIXV4JLMGLh0wqNb5hD9UwpcBFVjmhDGD3N6mVwdpVUitUTsOCFMaqJkGUEwxaqqOpKO5kVH4q1NKqwhTCOoQyLTmMZyRiGLl5hzrvuBahVcalhS6MJT5RiFajwhFwXKxiOYkUYhaWsZjWKzq0sAxWbDa2DSuFNrfhHtKgNT0TLUqDUurY1nLDcbFiU8dHXorab0cqsbe+qieG4ZRo62m1oQRFSuQijlMJlJy/YWhZt4GKyyY2dRd8yjVtAN7q6gUVp/zINW1wXu5BRhU5vI42Zgrc1sXAae56hC+QmtxO4OOWAiBHW8womFcBgrnKmwQuC7nYVu7BaigqiDF+44rsk1cQrfjHdAROEGLfw71A98QpdDKOfDmbINAT7ioeSscLDaHCGIzINYvwCF7JQxUMbmwpXyAIXuwDGMI6x3RHb+MY4zrGOd8zjHvv4x0AOspCHTOQiG/nISE6ykpfM5CY7eSQBAQAh+QQAAwAAACwAAAAAyADIAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NkZGRlZWVmZmZnZ2doaGhpaWlqampra2tsbGxtbW1ubm5vb29wcHBxcXFycnJzc3N0dHR6enqEhISSkpKfn5+urq65ubnBwcHFxcXIyMjJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrLy8vLy8vMzMzMzMzMzMzNzc3Nzc3Nzc3Ozs7Ozs7Ozs7Ozs7Ozs7Pz8/Pz8/Pz8/Pz9DQ0NDR0dHS0tLT09PT1NTV1dbW1tbW19fX2NjY2dnZ2dra29vc3d3e3t/e39/f4ODg4eHj4+Tk5ebm5+jq6+zs7e7u7/Dv8PHv8PHv8PHv8PHw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fII/gCvCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izat3KtavXr2DDih1LtqzZs2jTql3Ltq3bt3Djyp1Lt67du3jz6t3Lt6/fvxmfATbqzNGxwUOfOVp8GPFPaKsWM3bcM1oryZKNUdYpDRdmzJqhLlum1tfnz8ieKjaMNtjp086aQpZMzCyx16dXQVva+TOwaWOT4X4NK5pSYK99SQvLbDjuXsCPHhvOy7jX1c5fDzta2Dms3Vyf/kXOjptZUWmXs7cSrBVaevKvV1kXKgy+o1Xsr0abZX/4r6HN9LdYbFcVI+Bw5gFl2YGOJFiVNKYxeBp4PiEnYWpWTROhhJL58lNzHDpSzFXT/BKiZA7qNM14IaZIlYUhwhKdTsic6IiHWE3Di43J7BQNixLOMp9+sJy4ynI5GWMkhVlhx2FjN0VjI4FcBXjikDTdFiKUXRkYYm02QXMiL2FNw1+IWMY0XYhMXncihjNJA+SBPY5VI4etzBjTMiH2YtaOHLr40pkSthmWkweSKZOVEo54Vn0c5veSiRymKZaYHAoTk5QcwnmWkhwi6RKfErYi6lk/BgrThnSytSaD/sG8xKmElpaFqYR6qgQig2Cy5ZqEVK70K4OSqtUdry1Jw6GibhXJ4CotMXqgoGrdyWCwKIHKYK5rIdoflyj1ImGscTl74H8qTaNqXNoKCK1K3tpnKFvHHjjvSLsKuAtdc9rXjEpeHuioXMMKCG5JgE5Ll7UC4niSuhIW+1a9AqZ064HcunWxgLWCFC95zM7VL3wSi5Rvf73ORemB2I4kHIN10hWwgKSd9KqA1L718oGekjSMhP/WdbJ9KZPEan8lTywhMCgRKuC9GkuIC0rmCpjxW7MKOAvVEt6lLINbn1R1f1e79fWBYZs0tn1lt3W21lwzeKpcb/eXdklO9zd3/lx123c3SXnbtzdcfcP390iBwzc41kGi5BmDULe1sX0hk7SygC3D9XF2TJ+kpcJ1SYsySjf3F/NcpPKMUuoG28VwfzWbJLp95NIFKcsobe5cK3Y9fmDSIE1uX8drFQ4f8R9xmHlbujun0tH2nQ4X6/05fFK7/W031+2tpzQ0fLDwC7RKwpMsV/PDRR4Sh9K3Vbp97z4vofVuJSxg5yq9PzxcWQvY80kU60/Q3LKz37EEYgxCV7MkFL+VcO9pbgmgfbTHktnZJzS+4tAAV9K//hyJLeVT3EtgdKDYpWVm94MJ9ezWNrCEkDw5Q8kLs6MMtfysUjEhoYBMhZYZOkeB/i+RoH3+J5bL4U4mvmMQ8rYiRPDR5Hu0K8uC2EeTabyHQTHcSsHkVpPXuWuJVmkifAY2kw4KCIhdQc+J1MeSG67LKzo8UO1qYkYPslEqBSxUTrwooOKEx0ZktIkaQ8SLFj5limjaCRT7g78MietNPUmihIo2lQcyKE89EWMUq/K5EJlwJ1tcmiGP0sllKeiKHPpNVErJoTvSRJP2Uc5TUMihT/qElUFypU+mYUkJ+Wko0kjcs5YnlGjYL0S6KQr6YIeUZ6CyRUfhI4eCAUacKGNkEtIUUkIZolYQMyfQgF6IfDFKnujIRp8hRjlj0gxsBqmalVnbiWaxQZxEg5vI/tSlT5Z5oF4ADybKeKaN/lkUC6JzGPA0CTPkaaMsHmWF6LxPMfQJEmkoQ5g2smVTlBFR3PyCoCGBRjHcmdGq5LGjkoHFMUC6EWcYEaWLqaFVpAnTWRyDog+BBjOGQdKIalQqNIXpYoCBjGesEyE6JQZGhfrTqQRVqJKZxS+MwQxnPAMa0UhTNKDxjGYw4xjihOpnHCoVZ/RUrGiF6Xq+Ao2lpvWtNsIFTg8ZVrjaVUK/WBxXcHnXvtrnYGFphkD9StjTeDMt0YhjYRfrCGqyhRlnZaxYW1FPtUSjrpIVKzASKpZmMDSzKJ3FN9lyTdBCdRUy9Rr2THsiYnB2LdDAkidr7fOLuUpupLP14DBsS7hluHW2sEDGa+/iDMXOlheV3cxAosGMl0q2F8jgrV+Ya1y7+mIZw1XuNZ6BjF9E9kS+OIYzjqpdheh0GJJEJlGNWl6RROMZXw2GL3jBi1nAol9SDYYxkFFV6bb3vwAOsIAHTOACG/jACE6wghfM4AY7+MEQjrCEJ0zhClv4whh+SEAAACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHp6eoSEhJKSkp+fn6ysrLe3t7+/v8TExMfHx8nJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8zMzMzMzMzMzM3Nzc3Nzc3Nzs7Ozs7Oz8/Pz8/Pz9DQ0NHR0dHR0dHS0tLS0tLT09PT1NTU1dXV1dXW1tfX2NnZ2tvb3Nzd3d7e39/g4ODh4eLi4+Lj4+Pk5eTl5eTl5uXl5uXl5uXm5+Xm5+Xm5+bm5+bm5+bn6Ofn6Ofo6ejp6unq6+rr7Ovr7Ovs7ezs7ezt7u3t7u3u7+7u7+7v8O7v8O/w8e/w8fDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8vHy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8/Hy8wj+AO8JHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNq3cq1q9evYMOKHUu2rNmzaNOqXcu2rdu3cOPKnUu3rt2PynzdbWpumCNHxvYqBcfr719sgo9mm2X4Ly1xiYkqa9w4WGShxShTVnb5JzHNlFdx68zTL2jKucyRxtn3NOhiq22aE+b69LXYNE3X1hwLMm6YyHa7Bvb7JTThtZEVZ5ltFXLXq7YtT+kt1vPauFRPL0kO1/Xdw7b+l6T9vbZl8SKbla99DL1IcK7Wg3ZVjSw3X+GI/pKvGZc3ssk498tQy/BHWS/kjCVOMI0tE9Q38Rn41y8JioUNLZS5Ag5QDEroyHliGXMaiDxd46EjwmgH1je91OYgT+Z4JyEwKn7lTISuxVKhTsl4mMuOXrWGXHs6kWOdgbRsCNY2MiLnim84ZWagK6OBpYxz38GG0zdY8ofYVwvKt4qSNukmn3JfcVOLgeHZJI6EvdTIlThH8iddTSLyt8p/YTkjYZszmVPnemiKRZ6YUMY0GX96kSUOjusRKVOT63VjFjMG2iJnS9WwiRYwBlojE6hiklkWNgaS2BI4BmqJ1qH+65nKUnDyPamWNgYG9tJ+8hHDFqzfNdrSm/zxqRY3BsqaUoHyEdcWr+sl4xKw1znjFrPrObsSOV1+R8umaAnKH5AoqSefq23luV4zLH0m35duIdsrS4Wt5wq4alF6XS8rmcMfoG+5ux6+ImXDH7txoSpfNir1KB+5bYkbrUpm7ktXxc8BbFIu8861aHm8qHQwXbjKR/BH3thJl7/yaYOSwuW5YhfH69V3EqbrCTsXtci9aNIx8mkcF9Dr6WqSwN8ZPZe55fl6Es/CSVuXiesJg1K95U1jV8nlDXgShuu5XBer69mCEqTXJSoXy+XRcrZ8e8nn9kloPxf3enObVLf+cKvc3fbb5fVtF9veAv6dzHaRI7fh392lON6Mp21XypB/LV+VdBm8Hi5Xv2sX0995bVKHWdtFa3lCi4T0dZzVJWV56JJEdHlKywX1boWW9PF3Vtfli3xan2SNfJzX1e118JbEdXknq7UNf8qKRPh3DM+FbXkqYf2d1HKt/twuFMvXu1y/r+c0Sg6XF0vzZz2+Xusoab5e8m6B/h3mJ01/XextYYyc4CrZhXxyERdz7E04okuJ/5BzJ7fADHYsud53zteW15UHYSpxX8wgFi6wrUdtKIFWeeC3lmnwJ4EqOR3I3EK68nBvJd8wUPXUQjn5gDAl2uMdWyz4HW21RF3+65nhWQzIn9yxpIbrQWFZVFieG6pEhOW5zVkexR8fusR+3wHfWXj4HVHFRGLyIaFYYsif4skEiDGLnlduFzWakI0/4wuLCfnjCg5OS0LWCgudWmWTB66nN2Fp4XqMRRM2CieOXNndelQ1k+XJR1JbaY6ExHYTQwpHiloBx6DMpxMk1oqQViEHzejoxJl4rzw/woo5SGUgI94EHMfLFvuWwsXyZKcns0vVLJGSS/5gkict8hAin1LL8gyzk7Gs2i6HssDDqTEnTORPMJb5EyF5KI9BgSJ/eFHKoYhDm0EjCpdO5Ahb4M8oFyKnLezYE5ydyBX0GwoykrmwoxRTPrX+A0qYyAkYpJgDnNs8Z0+ScUD5/IKaORGHLfjpiFW8cCfayKGP2CkUbdCTP7uIZ03McUoJxeKZRHEnQ4MByplMw4P8XIUQlxLNE62CGN1UCTgsCbyo9DKlw6BkS7AhjItKyJVNQSNDHeELZyA0I+ZIhr6Gms+ndJSfsRgGNI46EW0Mw6cnAipUmsnQVQSjGRSlSDiWMYyFDhU0D6WKUM/aGFwMQxk6lQg4mlFWtrrGZ1iRoF1Bw4tgEOMYyrhGNrTxjQSJYxvWWMYxiCEMX+ACq3bFplauUdC9WnaorvjlVrZh1st69qy4sBRYxCHRz5q2WWFV5VNPy9ra8C8s2GDgTGtnW5tVYNAs+6Stbg3Ti5KaZRmQ3S1bHfoWcAhSuJf9xTfkUo2lIhezYoyLMjb5XJfCdC/mKEZwqyucYYBULjPlbqp8KxhuXFW8z1nFMARKGnIgo7Po7Q8yqGqXarAyvh/SrHvAkYxfbPeyq/hFMr6LHnIwo6e09eoyUuuegmQDGcJY02V5MYzgNdgi4HAGMXjxX+jkYhjJiOuFNwKOayh2GMDgRS5sQQssuYIWueDFL4QhjGEUA64jzrGOd8zjHvv4x0AOspCHTOQiG/nISE6ykpfM5CY7+clQjnJkAgIAIfkEAAMAAAAsAAAAAMgAyACHAAAAAQEBAgICAwMDBAQEBQUFBgYGBwcHCAgICQkJCgoKCwsLDAwMDQ0NDg4ODw8PEBAQEREREhISExMTFBQUFRUVFhYWFxcXGBgYGRkZGhoaGxsbHBwcHR0dHh4eHx8fICAgISEhIiIiIyMjJCQkJSUlJiYmJycnKCgoKSkpKioqKysrLCwsLS0tLi4uLy8vMDAwMTExMjIyMzMzNDQ0NTU1NjY2Nzc3ODg4OTk5Ojo6Ozs7PDw8PT09Pj4+Pz8/QEBAQUFBQkJCQ0NDRERERUVFRkZGR0dHSEhISUlJSkpKS0tLTExMTU1NTk5OT09PUFBQUVFRUlJSU1NTVFRUVVVVVlZWV1dXWFhYWVlZWlpaW1tbXFxcXV1da2trgYGBlpaWqKiotbW1vr6+w8PDx8fHyMjIycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLzMzMzMzMzMzMzc3Nzc3Nzs7Ozs7Ozs/Pz8/Pz8/Pz9DQ0NDQ0NHR0dHR0tPT09TU1NTV1dXV1tbW1tfX19fY2NjY2dnZ2trb2tvb29vc29zc3Nzd3d3e3t7f3t/f39/g3+Dg4ODh4eHi4eLi4uLj4uPj4+Pk4+Tl5OTl5OXm5ebn5ubn5+fo5+jp6Ojp6enq6err6urr6uvs6+vs6+vs7Ozt7e3u7e3u7e3u7e3u7u7v7u/w8PDx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PDx8PDx8PDx8PDx8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8PHy8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLz8fLzCP4A0wkcSLCgwYMIEypcyLChw4cQI0qcSLGixYsYM2rcyLGjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOnz59AgwodSrSo0aNIkypdyrSp06dQo0qdSrWq1atYs2rdyrWr169gw4odS7asWYfOznp15ipSL7Va2UaqU4cU3Kty6eotdpeqrUl6A6fqG3UYqMCIIzUj3BSaKsSQ67xivJQXpsiQJz2jfPQZKcyYbXEuOkwTaMyeRg+1Nfc05l+qfz4r5fq03dg8i5mufZov7py+WvMGPfj3zVzDa0dKa5wmreS8YTWf6Qo6b0vQpr+Ehsr68FzaXf6m8j4cVHiWscjzBgX7fEpc6l2Hau8e5a/4oOfXVzlMOH69luiyn0rJAPafYJsNiBI0nhyoVyfCKKgSKw7SlUp2EtpXYSTgZYhSM5Y4uIlvHp4kioOlJFiiSbM4WNyKJg3j4CownsTggbHUeBJ8/82io0nPhIifKz+a9Mp/pGBYpEjJ+OcdKCouGdIp+FmymJQiyYgffViC9Fl8rHQpkjH4daKkmB6NFx8xaILkDH4+hhfMlTLBEt8mZ+IGTS6HyTITNEKSx0tzzsQS6CYzIadeKMYZk4qTdQAjUyjxkTiaLydihkpMzcRnimrQ1LKJa8vB9Jx6xnDWzCqQYlYLTP6ZejcKZcGYYp15Lr2pHpdqQXMYeci4VIt6eDL2q3evthSrdckSdot6t62kq3eRRHnXM+pF0tIu6p0ympre8XoShYKOFox6rbBEqXeT5EnYZU+uBI16m6oGrnXWliSMem+p1ot6wah0qnf5EoYtec2eRKV3jOI2Cnk0ptQJeWHi1qJ3oqQ0b7m47UttSsiox1xsG3sX7EnAkJeacQ2Gi5Ki1n1qnK3edWiSLOSla5yd3sVp0irk3dIcL+QReRJtLhunpXX1mnQsdCf/lgx5s540sXfujnbwrSiNat0k2pGHSdfeWaJdoMmZfRK80I09ndfQqW0S28khOh3dw7ltEv7aw63cHN+86V0S4LXh2pyB0NltEuJ1a9eqa4qXhDdvYE9HXuQkPZ1c1pxtnThKD3uXTHNLQ2d4SQtbN0xz/3pXCkrkWjeocQNDpwpKOHs3mXGxQ5fjjuS9bhzS1tlcUuvWddIc3NBJelLI5BXM2NTALgjwb9ySx3lI61rns2qPeYd5SfcmlzFu3UPX9EnDRh+b59AlbNK55PU7GtHkrZ4S/Mm9yFnq1tmeSDSXN9U843HyYcmRyBMhzuDPO0ZTCf28czvO0Mw7s1sJ45ITCQGS5YDqkR7q1CMaxtQOOlVjCY8YxhhoMA86JWRJp1BFGOR5Z2TqUo/M7rIs6JzPJf4r9I6lzEIm9chvJSAkzw7PckHv0Ek88UnVWUpnnR++xGNKVAsBk7OLmbwQOgErywO9gwkP4i4+ZiILNCaXnN/JZIbqcWNY0pMtEbIEaNmK2leoaJ2I0YR66rFiV24UHz3OBI/q+R5Xeuc6nADyel2ZoHqGSBNEksdKXEkG4fqnk0eSRxRmdMozWpatJ96kOkPKCjRCFx8/7eQZX7RODKvCyLLZcSb3+Q8urIJK/HTRJwBUj4CmYslFAaUZG+TYU6BRPhoCpX3/meVSoBFM9dBCKL6aUVOc0cNAEgUZCEThLX9SjFiWzZRAGSN+NqE/o/AinNARV1B6eSBFBqWY+P7xY1Gg0c34hGJ0QTFMheqwiXH2pBlszNYwewINfOJnEoY0ChYrJIp25sQyA61DJBq4lOxl1BTolAkw0lchXzzlhA6KxCoMepJfkLRCu4TKAjNaB0ikwqIs8cUWHSTHpzRzoKDARSg70oxXbNJFVIFGE2mq0VLogqUWgYYu+plRVAwVKdVkal1ygUONIMMWpYCngyJoFYdqtQ6gaEUYK9IMX6TCnFq1Z1WgeVbIQGIUqaCFL4yxvWQUoxezOEUoxMpUoXFFnXUFTSQsgYlPhCIUmCBsYumSQa4EI5mTzex/LMFRrxgjoZoNrflCypVmsFK0qB0OWceCC8mmVrOTsNqfWZCx09ei9hMAhQs06Glb1EYiFlfd40t7m1hRRJQwugAtcSuEiV/qqS3L1aoqgquWZhAvugc6xXGNQ4ysYnc4kVAFabWzKtd+NxKt6KqCnjEL5X4XrUItEp/ei5hJqEKKWGrGLGqbWlHogroeaoYs+FtXS6CCq21KiDN2oQpSnjUSopgFJROskGf0ohWiOCp0IhEKVdwCpxSeCDSIAdhUjCIUntiEJSzhJEuEohSpiMUtfpHbENv4xjjOsY53zOMe+/jHQA6ykIdM5CIb+chITrKSl8zkJlMmIAAh+QQAAwAAACwAAAAAyADIAIcAAAABAQECAgIDAwMEBAQFBQUGBgYHBwcICAgJCQkKCgoLCwsMDAwNDQ0ODg4PDw8QEBARERESEhITExMUFBQVFRUWFhYXFxcYGBgZGRkaGhobGxscHBwdHR0eHh4fHx8gICAhISEiIiIjIyMkJCQlJSUmJiYnJycoKCgpKSkqKiorKyssLCwtLS0uLi4vLy8wMDAxMTEyMjIzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk6Ojo7Ozs8PDw9PT0+Pj4/Pz9AQEBBQUFCQkJDQ0NERERFRUVGRkZHR0dISEhJSUlKSkpLS0tMTExNTU1OTk5PT09QUFBRUVFSUlJTU1NUVFRVVVVWVlZXV1dYWFhZWVlaWlpbW1tcXFxdXV1eXl5fX19gYGBhYWFiYmJjY2NkZGRlZWVmZmZnZ2doaGhpaWlqampra2tsbGxtbW1ubm5vb29wcHBxcXFycnJzc3N0dHR1dXV2dnZ3d3d4eHh+fn6IiIiUlJShoaGvr6+5ubnBwcHFxcXIyMjJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrLy8vLy8vLy8vLy8vMzMzMzMzMzMzMzM3Nzc3Nzc3Ozs7Oz8/Pz8/Q0NDQ0dHR0tLS0tLS09PT09TU1NTV1dXW1tbX19fY2NjZ2dnZ2tra2tra29vc3N3d3t7e3t/f3+Dg4OHh4eLi4+Pj5OTj5OXk5OXk5OXk5ebl5ebm5ufn5+jn6Ono6erp6erq6uvr6+zs7O3s7e7s7e7t7e7t7u/t7u/u7/Dw8PHw8PHw8fLw8fLw8fLw8fLw8fLw8fLw8fLw8fLx8vPx8vPx8vPw8fLw8fLw8fLx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vPx8vMI/gDhCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izat3KlaC5bV3DIvTmLNgrSeXEii03jdgsSXDhZlO7Fd20YXHzwm1G9yo6bMXO6tVLrC9VdM5yDV4s6ZbhqOWWvWXMeNzjpuOOCabM2NplpeWOcR4tadnno+ieTSZNedjpotd2sSY967XQbsBmz/Zm+2cy3bqn9eb5jRdw3cmG65y2+TjpX8pvojPm/Di66DS9ya4OvBt2mdia/nNnDe07TGnjnR8z79JZeue92LP8/V43MGba5KssVv/5Mm3X6ZeSOcH0R9kty4AjIEvo5GagXq8Yk9+CLfH3YFyzMGMOhS4tcyFcszQTIIcsRfOhJMlsSGJL2HwIjHcrttSNeOndIk2MLpVzy4PCpIVjS9QZ6MyPLl1j4C1gEcnSOKul16OSLQnT3zEjQpkSNP0pY+WS/fG15UoWplfelyppU5+XZKKEjnHpFZOmSlimB0yVb5JUTpPO7eJjnSYpk94r3/B5kjfvjSloSWFWV9ihJYGTXi57MiqSn+MlKalI5tCom2mXjuQed7eo2ClI6CjG3Y2jhkTNeNClGhJe/tzB6KpH5Yyn5awfxVmdZbh6BKtzyfXaUa3c8SYsRyYqemxHvx434bIZEZsntBu1WB2a1F7kYXWBZouRlM4B4y1G6GhK2jPjXkRodQqS+Y00yVh6kq7AOQalOdkwM8xqQ6YkmnPG/OgNNMawOdh6KfVSnaECinPNMgWSJm5KxQoITjGm6vZKSuNUV5uA6zrHq0nbVOeagOVWN9dJqwJLocLOoXvSp8f1K2CQx3HaZ3XUUNhMdQib1Kxu3FBoTXVunuTgcSPrx011wqAE83EchgxcqyYZrDGHHU970nbAfbwgOtXZa9KOx9lCormciV0S2sDlQiKerKl9EtsHru2c/tklcUdidbugxJ2oAlbHC0oZA9cuyoajpLVuxgoo7dUoLQ3cs/p9Ux3WJRFTHaoCZmMySv8exwyF9Op2q0nbHpe0gK0Dhy1Jqc8W34I4A9fzSaJXR6d5v1RX9EldOxc5eyk7F2lJeFN2DcjVbZxS8M6tzl7trB3ub3W3y5eobsioRPNxy2NHN2vRqGStc7ubpzm3Kk0OXLDm/eyc2yj5Uvbvwz0+28kqiR1wVoadbnBndrzjTtCiI0DdEHBA3JkF4XqDDrgB5xUTRAm42IedoyHNJfZzzqKUoz+eucRqx+lWb3pXnQwmjDvhG87QdPO6lozvOOLojQFPBZPiVa83/jPUTflWEkTWvKJpjzETd0b4EvTA8DRrGg82ZJKp8cjKMNibzS74Nx9WcZErdxoPw3qYnvQZpoG6kWBNSnc/JHalZOM5XU1QeBwAhqUciVPeTb5XM7V4bjz0q8nT0oM5rSSLO69YnE2KOJtbDLEq32jeaHR2kx2OJxhfhIz/6uVCmtBnPAHDSoPeY0ad6Og9gaQKH4/TvZ20LD2UlAoajyOvnUQsPXKUSghBCRRH1SeWTZnlBXMIlGf0pxiZLAobxwM6oDASOL5wo1Gm0x8mAuWU9cnF8YxSjlumRxeP7IkS6/OKZhJlG3m04lF2+R5ihJMn0JDkbGxWlFEe6XlB/jHHKrkzp6SMw4L1KQYxe3KNdI5HT0vZhjw1toxkwuQbG+yPLRSZlPU96BZjnAk6lrHQ2bziiks55IV48UCYmMMZAO1PSZlizBNJIhggXUlkOgqcUkJFmPUJhjQc6hF0XIMYNJVdVXJ3ogitFCTeSEZKL5TLqaBjmS7NxTJUyBFzUOMYBv1QRqdyQ5fCJRfEeAaALDKObCyDel7VS/uwMo20UoYXyXAGNKaRjW14QxzXQUc5xuGNbmhjGswohi+CSs5CYiUb53OrYg00i5hmxRtLXaxkQbVNrozDm5PN7HGAIc2uPIOwmpUsMOnSDbCF9rSDmcVR+/JU1Lo2Lr+gy+hprpHY17rVesMJjW0n64taRocbaN3tiWZhU/lMI6vCHc8x3omdjYI2uXrxxfBiNA7JQLc6wMCnktABDeReVy/DMCyUqDG17w7GGFR9kzeW4d3X9uIZzCUTN5SaXKmmN1XaOEZt0/oKYkCjsr1CxzacIYznejQYzfBtugZc4P7kQhjKgEY2eJqucXTjGtBYRjGC0Qte5OIWgrnFLnohDGIcYxnOuAaA08XiFrv4xTCOsYxnTOMa2/jGOM6xjnfM4x77+MdADrKQfRIQACH5BAADAAAALAAAAADIAMgAhwAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fYKCgpCQkJ6enqurq7W1tb29vcPDw8bGxsjIyMnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycnJycrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysvLy8vLy8vLy8vMzMzMzM3Nzc3Nzs7Ozs7Pz8/Pz8/Q0NDQ0NDQ0dDQ0dHR0dHR0tHS0tLT09PT09TU1NTV1dbW1tfX19jY2dnZ2tra29vb3Nzc3d3d3t3e3t7e397e397e39/f4ODh4eLi4+Pk5OTl5eXm5ubn5+bn6Ofo6Ofo6Ofo6efo6ejp6ejp6ujp6ujp6unq6unq6+rq6+rr7Orr7Ovs7Ovs7e/w8fDx8vDx8vDx8vDx8vDx8vDx8vDx8u/w8e/w8e/w8fDx8vDx8vDx8vDx8vDx8vDx8vDx8vDx8gj+AN0JHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNOZeftWrNjxYL52qWrlqVnWtO6Y5fNGTJdluLKnRsXrdqq67At40W3b19od6O2y6bMrN/DdAEHZsru2jHEkP8uTtrOGrHImBNPNsrOmeHMoC0p3hwUnbLQqOOOJt2Tm7HUsFezzmluGOzb0mbrbMfstu/cum9e++w7NfDgM70BK178OHKYzZgzn/YcZrrl0otTr94SG/HssLf+c0/Z7jT46eNTmut1Xrr49CS1tc9uDX7JbPPp2x+JLT/mWsEo88w124yDDjvttLPfSNb4d1guymDDzoIsQeMgXcBYcw6FLj1zYVy6PJMOhy81eKEy3JAIE37+1cLMhCq+xI1/uUCjYIwvjfNdds3ciKNL6+Qy3zDm/AgTO7u0V8s1RsZk23nDwNikSxae18yUMHVzXi3aYPkSO3Blp8uIXrqEDHi/SFnmSteAZ4yPa6rEzo6wKQNnnCktwyOeLYGTnTJ8toRdccgEylKbzBVzp6ElsYMLc74syihJ0RVXi5qTNirdNpmm5AxzdnVqEjvMESMqSlXelgump4bUzqP+vmHTqkkm3lbMrCaFeRs6uJLE4m3O9EpSMr7pIqmwG7VTXH3IhoQobLkc22xGj+E2LUik3laLtNdaVGtqoXbLUTG+sSruRcraem5H8t3G7LoaeXibufBS9BpsydSrUbqwMakvRlretuG/F02jLcEY6Vknwhc9mdq7DE9EJ2YDRyxRtrBZTJGfsPmi8UT94fuxRKmiduXIEFX6MMoQKZwapyw7RCxs4sTs0L2pkWnzQg6jRu/OBPkyL9AL6Yoat0T7RvRCE0dm5MEnGR0a0uPxi1ouKAkN28/wYYyaxycNito6OHodGjAokQtbxSSOc5sxKJ0JGzg4cpxavifNnFr+lzG2mxqgJ7mMGsQcGgwbMyjJm1qwMaqMmmwjPYtaoTFWm5qsJwWcGtgq8jU3SmZPraLVPqfUNGRFkigO1Cj1HNp7C34b2q153oY3h3qjtoxKhqeGi4qnI0Z4SXanxrZ9bt9Ws0q+DZ+e7KDVwpLlk3NIfWiUq1Ry6fuFDppzKM3o7oK9p9YNS6SfvaDWsFEdktxr22eOb7ez6Rvj6fV2G+boF8c1ad4DjftEAj9wpedTtoNJyGCzLe4EMDP8a0n6QgM+3WzvaDHJHWpw8b+7zIl+MvkVbHaHHPPcJhszkRpq6KYbzcFGFzSBnvp0wz7YOE+CwYMM7BZTvtQ0kCb+CNSWzhZzDlDZ5IGZidRk2uG5ct1EcCOcjAlvQ0KbFJE5EdSK5HaVkykycIhYuWJxqniTDxaHFx10CpKYc6mdSEM6wRigUtrxC/fwpB1JYk72qnI92PBCji7x2xit4sXb8K0nOCvOyaSiP+YcAyjpAA8ZnQJF34CRJ2/MDjIA6ZN29PE2O+zJJ2GjKKa0w3W+eeRQ2CGk7AAjjT1hRw2LowtY3mQb59HF+Y7CDRX6ZpdFCWJ2IAcUxWUnXEUJRnuKYcuZsOMyUFLKOnIYmlxkcSfDUVIzdyK+9hDjeDepTX6AqRRBngeZNGmH485zSKYssD26CKVL2jGNVs7nmkv+2eJ5chENTn6kHdCgJmzwyZRMtugZ29xIZwQanqoI0z/IkJBJ2oGNAvoHnVGRYX6KoSGQnMMaavuQJW4YlXeKFEISvQg79GJPkVqinVfpBkPBUwtiKMMZBDIQgm6UIHagYxzcuIYzlEGMmWanFixMyzha6tKmticX41hMOpro1KoyB42bYYdFrcpV0CAjoVLJZlfHCpklIWc9ZE0rXXoBTtaUR61qXYY/teIduHK1Fii0DzsqaVcHKQOsi1FOXy8EjKSSCBtMHSxzdEFQCrXjoYq9zTPmWh12NMOoka1FMwCbnoVGNjO1cAZn9wPQxH42Lvyk7OisoczTxiUYJJ1sFDuiMUu1AiMaoy0TOp5BVa7yQkT6Wuky8ijSXSwjpRFjyzKCgdnD1JQZ2cjttdgxjmxMYxlgEQtZDFMLXezCF8EoxjGYMY1sjEO1S0uvetfL3va6973wja9850vf+tr3vvjNr373y9/+LiYgADsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=\n\n      \"/>\n  </div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-preview/plc-preview.component.html":
  /*!*******************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-preview/plc-preview.component.html ***!
    \*******************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedProductLogoCustomizationComponentsPlcPreviewPlcPreviewComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"preview\">\n  <ng-content></ng-content>\n  <div class=\"buttons buttons--zoom\">\n    <button class=\"zoom-in\" (click)=\"doZoomIn()\"> <span> + </span> </button>\n    <button class=\"zoom-out\" (click)=\"doZoomOut()\"> <span> - </span> </button>\n  </div>\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/product-logo-customization.component.html":
  /*!***********************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/product-logo-customization.component.html ***!
    \***********************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedProductLogoCustomizationProductLogoCustomizationComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"view product-logo-customization\" id=\"view-id\">\n\n\n\n  <!-- -------------------------------------- ---\n  ---- --  SIDEBAR :: POSITIONS            -- ---\n  ---- -------------------------------------- -->\n  <div class=\"view-column positions\" *ngIf=\"!isPreview\">\n    <app-plc-positions-list [hide]=\"isPreview\">\n      <app-plc-position-item *ngFor=\"let position of $POSITIONS | async;\"\n\n        [isActive] = \"position.name === ($POSITION | async).name\"\n        [name] = \"position.name\"\n        [colors] = \"position.colors\"\n\n        (active)=\"onPositionActive(position)\"\n        (enable)=\"onPositionEnable(position)\"\n        (colorChange)=\"onPositionColorChange(position, $event)\"\n\n      ></app-plc-position-item>\n      <br>\n      <div class=\"buttons\" style=\"width: 100%;\">\n        <div class=\"buttons-wrapper\">\n          <button (click) = \"previewClip()\"> PREVIEW CLIP </button>\n          <button (click) = \"saveClip()\"> SAVE CLIP IMAGE4 </button>\n        </div>\n      </div>\n    </app-plc-positions-list>\n  </div>\n  <!-- -------------------------------------- -->\n\n\n\n\n\n  <!-- -------------------------------------- ---\n  ---- --  CONTENT :: PREVIEW AND PICTURES -- ---\n  ---- -------------------------------------- -->\n  <div class=\"view-column preview-and-pictures\">\n\n\n      <!-- -------------------------------- ---\n      ---- -- CONTENT :: ROW :: PREVIEW  -- ---\n      ---- -------------------------------- -->\n      <div class=\"view-row preview\" id=\"preview-id\" style=\"overflow: hidden;\" dndDropzone (dndDrop)=\"onDrop($event)\" >\n        <app-plc-preview style=\"height: 100%; width: 100%;; overflow: hidden;\">\n            <canvas id=\"cnv-board\"></canvas>\n        </app-plc-preview>\n      </div>\n      <!-- -------------------------------- -->\n\n\n      <!-- -------------------------------- ---\n      ---- -- CONTENT :: ROW :: PICTURES -- ---\n      ---- -------------------------------- -->\n      <div class=\"view-row pictures\" *ngIf=\"!isPreview\" style=\"overflow: hidden;\">\n        <app-plc-picture-carousel\n          style=\"width: var(--plc-view-pictures-width); overflow: hidden;\"\n          [hide]=\"isPreview\"\n          [pictures] = \"picturesList\"\n          (addPicture)=\"addPicture($event)\">\n\n            <app-plc-picture-thumb\n              [id]=\"picture.id\"\n              [src]=\"picture.src\"\n              [dndDraggable]=\"picture\"\n              (remove)=\"removePicture(picture)\"\n              *ngFor=\"let picture of $PICTURES | async ; let k = index; trackBy: trackByIds \"\n            ></app-plc-picture-thumb>\n\n            </app-plc-picture-carousel>\n      </div>\n      <!-- -------------------------------- -->\n\n  </div>\n  <!-- -------------------------------------- -->\n\n\n</div>\n\n";
    /***/
  },

  /***/
  "./node_modules/tslib/tslib.es6.js":
  /*!*****************************************!*\
    !*** ./node_modules/tslib/tslib.es6.js ***!
    \*****************************************/

  /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */

  /***/
  function node_modulesTslibTslibEs6Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__extends", function () {
      return __extends;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__assign", function () {
      return _assign;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__rest", function () {
      return __rest;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__decorate", function () {
      return __decorate;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__param", function () {
      return __param;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__metadata", function () {
      return __metadata;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
      return __awaiter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__generator", function () {
      return __generator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
      return __exportStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__values", function () {
      return __values;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__read", function () {
      return __read;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spread", function () {
      return __spread;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
      return __spreadArrays;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__await", function () {
      return __await;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
      return __asyncGenerator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
      return __asyncDelegator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
      return __asyncValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
      return __makeTemplateObject;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importStar", function () {
      return __importStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
      return __importDefault;
    });
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0
    
    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.
    
    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    /* global Reflect, Promise */


    var _extendStatics = function extendStatics(d, b) {
      _extendStatics = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (d, b) {
        d.__proto__ = b;
      } || function (d, b) {
        for (var p in b) {
          if (b.hasOwnProperty(p)) d[p] = b[p];
        }
      };

      return _extendStatics(d, b);
    };

    function __extends(d, b) {
      _extendStatics(d, b);

      function __() {
        this.constructor = d;
      }

      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var _assign = function __assign() {
      _assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) {
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
        }

        return t;
      };

      return _assign.apply(this, arguments);
    };

    function __rest(s, e) {
      var t = {};

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
      }

      if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
      }
      return t;
    }

    function __decorate(decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    }

    function __metadata(metadataKey, metadataValue) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    }

    function __generator(thisArg, body) {
      var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
          f,
          y,
          t,
          g;
      return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
      }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
        return this;
      }), g;

      function verb(n) {
        return function (v) {
          return step([n, v]);
        };
      }

      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");

        while (_) {
          try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];

            switch (op[0]) {
              case 0:
              case 1:
                t = op;
                break;

              case 4:
                _.label++;
                return {
                  value: op[1],
                  done: false
                };

              case 5:
                _.label++;
                y = op[1];
                op = [0];
                continue;

              case 7:
                op = _.ops.pop();

                _.trys.pop();

                continue;

              default:
                if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                  _ = 0;
                  continue;
                }

                if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                  _.label = op[1];
                  break;
                }

                if (op[0] === 6 && _.label < t[1]) {
                  _.label = t[1];
                  t = op;
                  break;
                }

                if (t && _.label < t[2]) {
                  _.label = t[2];

                  _.ops.push(op);

                  break;
                }

                if (t[2]) _.ops.pop();

                _.trys.pop();

                continue;
            }

            op = body.call(thisArg, _);
          } catch (e) {
            op = [6, e];
            y = 0;
          } finally {
            f = t = 0;
          }
        }

        if (op[0] & 5) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    }

    function __exportStar(m, exports) {
      for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
      }
    }

    function __values(o) {
      var m = typeof Symbol === "function" && o[Symbol.iterator],
          i = 0;
      if (m) return m.call(o);
      return {
        next: function next() {
          if (o && i >= o.length) o = void 0;
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
    }

    function __read(o, n) {
      var m = typeof Symbol === "function" && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o),
          r,
          ar = [],
          e;

      try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
          ar.push(r.value);
        }
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          if (r && !r.done && (m = i["return"])) m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }

      return ar;
    }

    function __spread() {
      for (var ar = [], i = 0; i < arguments.length; i++) {
        ar = ar.concat(__read(arguments[i]));
      }

      return ar;
    }

    function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
        s += arguments[i].length;
      }

      for (var r = Array(s), k = 0, i = 0; i < il; i++) {
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
          r[k] = a[j];
        }
      }

      return r;
    }

    ;

    function __await(v) {
      return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var g = generator.apply(thisArg, _arguments || []),
          i,
          q = [];
      return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i;

      function verb(n) {
        if (g[n]) i[n] = function (v) {
          return new Promise(function (a, b) {
            q.push([n, v, a, b]) > 1 || resume(n, v);
          });
        };
      }

      function resume(n, v) {
        try {
          step(g[n](v));
        } catch (e) {
          settle(q[0][3], e);
        }
      }

      function step(r) {
        r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
      }

      function fulfill(value) {
        resume("next", value);
      }

      function reject(value) {
        resume("throw", value);
      }

      function settle(f, v) {
        if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
      }
    }

    function __asyncDelegator(o) {
      var i, p;
      return i = {}, verb("next"), verb("throw", function (e) {
        throw e;
      }), verb("return"), i[Symbol.iterator] = function () {
        return this;
      }, i;

      function verb(n, f) {
        i[n] = o[n] ? function (v) {
          return (p = !p) ? {
            value: __await(o[n](v)),
            done: n === "return"
          } : f ? f(v) : v;
        } : f;
      }
    }

    function __asyncValues(o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    }

    function __makeTemplateObject(cooked, raw) {
      if (Object.defineProperty) {
        Object.defineProperty(cooked, "raw", {
          value: raw
        });
      } else {
        cooked.raw = raw;
      }

      return cooked;
    }

    ;

    function __importStar(mod) {
      if (mod && mod.__esModule) return mod;
      var result = {};
      if (mod != null) for (var k in mod) {
        if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
      }
      result.default = mod;
      return result;
    }

    function __importDefault(mod) {
      return mod && mod.__esModule ? mod : {
        default: mod
      };
    }
    /***/

  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var routes = [];

    var AppRoutingModule = function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    };

    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppRoutingModule);
    /***/
  },

  /***/
  "./src/app/app.component.css":
  /*!***********************************!*\
    !*** ./src/app/app.component.css ***!
    \***********************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _shared_product_logo_customization_mocks_positions_list_mock__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./shared/product-logo-customization/mocks/positions-list.mock */
    "./src/app/shared/product-logo-customization/mocks/positions-list.mock.ts");

    var AppComponent =
    /*#__PURE__*/
    function () {
      function AppComponent() {
        _classCallCheck(this, AppComponent);

        this.title = 'ngx-vurbis-product-logo-customization';
        this.positions = _shared_product_logo_customization_mocks_positions_list_mock__WEBPACK_IMPORTED_MODULE_2__["default"];
      }

      _createClass(AppComponent, [{
        key: "onChange",
        value: function onChange($event) {}
      }, {
        key: "onImageRemove",
        value: function onImageRemove($event) {}
      }, {
        key: "onImageAdd",
        value: function onImageAdd($event) {}
      }]);

      return AppComponent;
    }();

    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-root',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./app.component.css */
      "./src/app/app.component.css")).default]
    })], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./shared/shared.module */
    "./src/app/shared/shared.module.ts");

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]],
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"]],
      providers: [],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-picture-add/plc-picture-add.component.css":
  /*!************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-picture-add/plc-picture-add.component.css ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPictureAddPlcPictureAddComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "div.add-picture.button{\r\n  display: block;\r\n  width: 100%;\r\n  height: 100%;\r\n  display: flex;\r\n  flex-direction: column;\r\n  flex-wrap: nowrap;\r\n  justify-content: center;\r\n  align-content: center;\r\n  align-items: center;\r\n\r\n  cursor: pointer;\r\n}\r\n\r\n\r\n\r\n\r\ndiv.add-picture.button span{\r\n  font-size: 50px;\r\n  color: #7b7b7b;\r\ntransition: all 200ms ease-in-out;\r\n}\r\n\r\n\r\n\r\n\r\ndiv.add-picture.button:hover span{\r\n  font-size: 55px;\r\n  color: #9b9b9b;\r\n\r\n}\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uL2NvbXBvbmVudHMvcGxjLXBpY3R1cmUtYWRkL3BsYy1waWN0dXJlLWFkZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxZQUFZO0VBSVosYUFBYTtFQUdiLHNCQUFzQjtFQUd0QixpQkFBaUI7RUFHakIsdUJBQXVCO0VBR3ZCLHFCQUFxQjtFQUdyQixtQkFBbUI7O0VBRW5CLGVBQWU7QUFDakI7Ozs7O0FBS0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztBQU1oQixpQ0FBaUM7QUFDakM7Ozs7O0FBR0E7RUFDRSxlQUFlO0VBQ2YsY0FBYzs7QUFFaEIiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvcHJvZHVjdC1sb2dvLWN1c3RvbWl6YXRpb24vY29tcG9uZW50cy9wbGMtcGljdHVyZS1hZGQvcGxjLXBpY3R1cmUtYWRkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJkaXYuYWRkLXBpY3R1cmUuYnV0dG9ue1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuXHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAtbXMtZmxleC13cmFwOiBub3dyYXA7XHJcbiAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgLXdlYmtpdC1hbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgLW1zLWZsZXgtbGluZS1wYWNrOiBjZW50ZXI7XHJcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcblxyXG5cclxuZGl2LmFkZC1waWN0dXJlLmJ1dHRvbiBzcGFue1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxuICBjb2xvcjogIzdiN2I3YjtcclxuXHJcbi13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0O1xyXG4tbW96LXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuLW1zLXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuLW8tdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0O1xyXG50cmFuc2l0aW9uOiBhbGwgMjAwbXMgZWFzZS1pbi1vdXQ7XHJcbn1cclxuXHJcblxyXG5kaXYuYWRkLXBpY3R1cmUuYnV0dG9uOmhvdmVyIHNwYW57XHJcbiAgZm9udC1zaXplOiA1NXB4O1xyXG4gIGNvbG9yOiAjOWI5YjliO1xyXG5cclxufVxyXG5cclxuXHJcblxyXG4iXX0= */";
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-picture-add/plc-picture-add.component.ts":
  /*!***********************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-picture-add/plc-picture-add.component.ts ***!
    \***********************************************************************************************************/

  /*! exports provided: PlcPictureAddComponent */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPictureAddPlcPictureAddComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlcPictureAddComponent", function () {
      return PlcPictureAddComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var PlcPictureAddComponent =
    /*#__PURE__*/
    function () {
      function PlcPictureAddComponent() {
        _classCallCheck(this, PlcPictureAddComponent);

        this.addLogoPicture = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }

      _createClass(PlcPictureAddComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "fileChangeEvent",
        value: function fileChangeEvent($event) {
          var _this = this;

          var selectedFile = $event.target.files[0];
          var reader = new FileReader();

          reader.onload = function (event) {
            _this.addLogoPicture.emit({
              name: selectedFile.name,
              base64: event.target.result
            });
          };

          reader.readAsDataURL(selectedFile);
        }
      }]);

      return PlcPictureAddComponent;
    }();

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PlcPictureAddComponent.prototype, "addLogoPicture", void 0);
    PlcPictureAddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-plc-picture-add",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./plc-picture-add.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-picture-add/plc-picture-add.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./plc-picture-add.component.css */
      "./src/app/shared/product-logo-customization/components/plc-picture-add/plc-picture-add.component.css")).default]
    })], PlcPictureAddComponent);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-picture-carousel/plc-picture-carousel.component.css":
  /*!**********************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-picture-carousel/plc-picture-carousel.component.css ***!
    \**********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPictureCarouselPlcPictureCarouselComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\r\n/* ------------------------------------------------\r\n ---- PICTURE :: Position                      ----\r\n ------------------------------------------------*/\r\n\r\n div.picture-carousel{\r\n  width: 100%;\r\n  height: 185px;\r\n\r\n  overflow-y: hidden;\r\n  overflow-x: scroll;\r\n  display: flex;\r\n  flex-direction: row;\r\n  flex-wrap: nowrap;\r\n  justify-content: flex-start;\r\n  align-content: stretch;\r\n  align-items: center;\r\n }\r\n\r\n div.card.picture-add{\r\n  display: block;\r\n  width: 150px;\r\n  min-width: 150px;\r\n  height: 150px;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n}\r\n\r\n div.card.picture-thumb{\r\n  display: block;\r\n  width: 150px;\r\n  min-width: 150px;\r\n  height: 150px;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n\r\n  opacity: 1;\r\n}\r\n\r\n div.card.picture-thumb.removing{\r\n  display: block;\r\n  width: 0px;\r\n  min-width: 0px;\r\n  height: 0px;\r\n  min-width: 0px;\r\n\r\n  padding: 0px;\r\n\r\n  opacity: 0;\r\n  transition: all 350ms ease-in-out;\r\n}\r\n\r\n div.card.picture-add div.card--wrapper{\r\n  display: block;\r\n  position: relative;\r\n  width: 100%;\r\n  height: 100%;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n\r\n  border: 1px solid gray;\r\n}\r\n\r\n div.card.picture-thumb div.card--wrapper{\r\n  display: block;\r\n  position: relative;\r\n  width: 100%;\r\n  height: 100%;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n\r\n  border: 1px solid gray;\r\n}\r\n\r\n /* ----------------------------------------- */\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uL2NvbXBvbmVudHMvcGxjLXBpY3R1cmUtY2Fyb3VzZWwvcGxjLXBpY3R1cmUtY2Fyb3VzZWwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7O2tEQUVrRDs7Q0FFakQ7RUFDQyxXQUFXO0VBQ1gsYUFBYTs7RUFFYixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBSWxCLGFBQWE7RUFHYixtQkFBbUI7RUFHbkIsaUJBQWlCO0VBR2pCLDJCQUEyQjtFQUczQixzQkFBc0I7RUFHdEIsbUJBQW1CO0NBQ3BCOztDQUVBO0VBQ0MsY0FBYztFQUNkLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsYUFBYTs7RUFFYixzQkFBc0I7RUFDdEIsWUFBWTtBQUNkOztDQUVDO0VBQ0MsY0FBYztFQUNkLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsYUFBYTs7RUFFYixzQkFBc0I7RUFDdEIsWUFBWTs7RUFFWixVQUFVO0FBQ1o7O0NBRUE7RUFDRSxjQUFjO0VBQ2QsVUFBVTtFQUNWLGNBQWM7RUFDZCxXQUFXO0VBQ1gsY0FBYzs7RUFFZCxZQUFZOztFQUVaLFVBQVU7RUFNVixpQ0FBaUM7QUFDbkM7O0NBRUE7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxZQUFZOztFQUVaLHNCQUFzQjtFQUN0QixZQUFZOztFQUVaLHNCQUFzQjtBQUN4Qjs7Q0FDQTtFQUNFLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7O0VBRVosc0JBQXNCO0VBQ3RCLFlBQVk7O0VBRVosc0JBQXNCO0FBQ3hCOztDQUVBLDhDQUE4QyIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9wcm9kdWN0LWxvZ28tY3VzdG9taXphdGlvbi9jb21wb25lbnRzL3BsYy1waWN0dXJlLWNhcm91c2VsL3BsYy1waWN0dXJlLWNhcm91c2VsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAtLS0tIFBJQ1RVUkUgOjogUG9zaXRpb24gICAgICAgICAgICAgICAgICAgICAgLS0tLVxyXG4gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuXHJcbiBkaXYucGljdHVyZS1jYXJvdXNlbHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDE4NXB4O1xyXG5cclxuICBvdmVyZmxvdy15OiBoaWRkZW47XHJcbiAgb3ZlcmZsb3cteDogc2Nyb2xsO1xyXG5cclxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAtd2Via2l0LWZsZXgtd3JhcDogbm93cmFwO1xyXG4gIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICBmbGV4LXdyYXA6IG5vd3JhcDtcclxuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAtbXMtZmxleC1wYWNrOiBzdGFydDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgLXdlYmtpdC1hbGlnbi1jb250ZW50OiBzdHJldGNoO1xyXG4gIC1tcy1mbGV4LWxpbmUtcGFjazogc3RyZXRjaDtcclxuICBhbGlnbi1jb250ZW50OiBzdHJldGNoO1xyXG4gIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiB9XHJcblxyXG4gZGl2LmNhcmQucGljdHVyZS1hZGR7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDE1MHB4O1xyXG4gIG1pbi13aWR0aDogMTUwcHg7XHJcbiAgaGVpZ2h0OiAxNTBweDtcclxuXHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBwYWRkaW5nOiAxZW07XHJcbn1cclxuXHJcbiBkaXYuY2FyZC5waWN0dXJlLXRodW1ie1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxNTBweDtcclxuICBtaW4td2lkdGg6IDE1MHB4O1xyXG4gIGhlaWdodDogMTUwcHg7XHJcblxyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcGFkZGluZzogMWVtO1xyXG5cclxuICBvcGFjaXR5OiAxO1xyXG59XHJcblxyXG5kaXYuY2FyZC5waWN0dXJlLXRodW1iLnJlbW92aW5ne1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAwcHg7XHJcbiAgbWluLXdpZHRoOiAwcHg7XHJcbiAgaGVpZ2h0OiAwcHg7XHJcbiAgbWluLXdpZHRoOiAwcHg7XHJcblxyXG4gIHBhZGRpbmc6IDBweDtcclxuXHJcbiAgb3BhY2l0eTogMDtcclxuXHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMzUwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgLW1vei10cmFuc2l0aW9uOiBhbGwgMzUwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgLW1zLXRyYW5zaXRpb246IGFsbCAzNTBtcyBlYXNlLWluLW91dDtcclxuICAtby10cmFuc2l0aW9uOiBhbGwgMzUwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDM1MG1zIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG5kaXYuY2FyZC5waWN0dXJlLWFkZCBkaXYuY2FyZC0td3JhcHBlcntcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG5cclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIHBhZGRpbmc6IDFlbTtcclxuXHJcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JheTtcclxufVxyXG5kaXYuY2FyZC5waWN0dXJlLXRodW1iIGRpdi5jYXJkLS13cmFwcGVye1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcGFkZGluZzogMWVtO1xyXG5cclxuICBib3JkZXI6IDFweCBzb2xpZCBncmF5O1xyXG59XHJcblxyXG4vKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAqL1xyXG4iXX0= */";
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-picture-carousel/plc-picture-carousel.component.ts":
  /*!*********************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-picture-carousel/plc-picture-carousel.component.ts ***!
    \*********************************************************************************************************************/

  /*! exports provided: PlcPictureCarouselComponent */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPictureCarouselPlcPictureCarouselComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlcPictureCarouselComponent", function () {
      return PlcPictureCarouselComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _helpers_md5__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../helpers/md5 */
    "./src/app/shared/product-logo-customization/helpers/md5.ts");

    var PlcPictureCarouselComponent =
    /*#__PURE__*/
    function () {
      function PlcPictureCarouselComponent() {
        _classCallCheck(this, PlcPictureCarouselComponent);

        this.addPicture = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.removePicture = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.errorUploadingPicture = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }

      _createClass(PlcPictureCarouselComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "setDragingStart",
        value: function setDragingStart($event) {}
      }, {
        key: "setDragingStop",
        value: function setDragingStop($event) {
          console.log('event');
          console.log($event);
        }
      }, {
        key: "onAddLogoPicture",
        value: function onAddLogoPicture($event) {
          var name = $event.name;
          var base64 = $event.base64;
          var logo = {
            id: Object(_helpers_md5__WEBPACK_IMPORTED_MODULE_2__["md5"])(name + new Date().toISOString() + Math.random() + 'UPLOAD_IMAGE::'),
            name: name,
            timestamp: new Date().toISOString(),
            src: base64
          };
          this.addPicture.emit(logo);
        }
      }]);

      return PlcPictureCarouselComponent;
    }();

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('pictures')], PlcPictureCarouselComponent.prototype, "pictures", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('hide')], PlcPictureCarouselComponent.prototype, "hide", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PlcPictureCarouselComponent.prototype, "addPicture", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PlcPictureCarouselComponent.prototype, "removePicture", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PlcPictureCarouselComponent.prototype, "errorUploadingPicture", void 0);
    PlcPictureCarouselComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-plc-picture-carousel',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./plc-picture-carousel.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-picture-carousel/plc-picture-carousel.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./plc-picture-carousel.component.css */
      "./src/app/shared/product-logo-customization/components/plc-picture-carousel/plc-picture-carousel.component.css")).default]
    })], PlcPictureCarouselComponent);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-picture-thumb/plc-picture-thumb.component.css":
  /*!****************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-picture-thumb/plc-picture-thumb.component.css ***!
    \****************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPictureThumbPlcPictureThumbComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\r\n div.card.picture-add{\r\n  display: block;\r\n  width: 150px;\r\n  min-width: 150px;\r\n  height: 150px;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n}\r\n\r\n div.card.picture-thumb{\r\n  display: block;\r\n  width: 150px;\r\n  min-width: 150px;\r\n  height: 150px;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n\r\n  opacity: 1;\r\n}\r\n\r\n div.card.picture-thumb.removing{\r\n  display: block;\r\n  width: 0px;\r\n  min-width: 0px;\r\n  height: 0px;\r\n  min-width: 0px;\r\n\r\n  padding: 0px;\r\n\r\n  opacity: 0;\r\n  transition: all 350ms ease-in-out;\r\n}\r\n\r\n div.card.picture-add div.card--wrapper{\r\n  display: block;\r\n  position: relative;\r\n  width: 100%;\r\n  height: 100%;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n\r\n  border: 1px solid gray;\r\n}\r\n\r\n div.card.picture-thumb div.card--wrapper{\r\n  display: block;\r\n  position: relative;\r\n  width: 100%;\r\n  height: 100%;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n\r\n  border: 1px solid gray;\r\n}\r\n\r\n div.picture-thumb{\r\n  display: block;\r\n  width: 150px;\r\n  min-width: 150px;\r\n  height: 150px;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n\r\n  opacity: 1;\r\n}\r\n\r\n img.card--thumb{\r\n  display: block;\r\n  width: 100%;\r\n  height: 100%;\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n  -o-object-fit: contain;\r\n     object-fit: contain;\r\n  -o-object-position: center;\r\n     object-position: center;\r\n  position: absolute;\r\n  top: 0px;\r\n  left: 0px;\r\n}\r\n\r\n span{\r\n  display: block;\r\n  position: absolute;\r\n  top: 0px;\r\n  right: 7px;\r\n  font-weight: 900;\r\n  font-family: monospace;\r\n  cursor: pointer;\r\n}\r\n\r\n span:hover{\r\n\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uL2NvbXBvbmVudHMvcGxjLXBpY3R1cmUtdGh1bWIvcGxjLXBpY3R1cmUtdGh1bWIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0NBQ0M7RUFDQyxjQUFjO0VBQ2QsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixhQUFhOztFQUViLHNCQUFzQjtFQUN0QixZQUFZO0FBQ2Q7O0NBRUM7RUFDQyxjQUFjO0VBQ2QsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixhQUFhOztFQUViLHNCQUFzQjtFQUN0QixZQUFZOztFQUVaLFVBQVU7QUFDWjs7Q0FFQTtFQUNFLGNBQWM7RUFDZCxVQUFVO0VBQ1YsY0FBYztFQUNkLFdBQVc7RUFDWCxjQUFjOztFQUVkLFlBQVk7O0VBRVosVUFBVTtFQU1WLGlDQUFpQztBQUNuQzs7Q0FFQTtFQUNFLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7O0VBRVosc0JBQXNCO0VBQ3RCLFlBQVk7O0VBRVosc0JBQXNCO0FBQ3hCOztDQUNBO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTs7RUFFWixzQkFBc0I7RUFDdEIsWUFBWTs7RUFFWixzQkFBc0I7QUFDeEI7O0NBR0E7RUFDRSxjQUFjO0VBQ2QsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixhQUFhOztFQUViLHNCQUFzQjtFQUN0QixZQUFZOztFQUVaLFVBQVU7QUFDWjs7Q0FFQTtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsWUFBWTtFQUNaLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1osc0JBQW1CO0tBQW5CLG1CQUFtQjtFQUNuQiwwQkFBdUI7S0FBdkIsdUJBQXVCO0VBQ3ZCLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsU0FBUztBQUNYOztDQUNBO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixzQkFBc0I7RUFDdEIsZUFBZTtBQUNqQjs7Q0FDQTs7QUFFQSIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9wcm9kdWN0LWxvZ28tY3VzdG9taXphdGlvbi9jb21wb25lbnRzL3BsYy1waWN0dXJlLXRodW1iL3BsYy1waWN0dXJlLXRodW1iLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuIGRpdi5jYXJkLnBpY3R1cmUtYWRke1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxNTBweDtcclxuICBtaW4td2lkdGg6IDE1MHB4O1xyXG4gIGhlaWdodDogMTUwcHg7XHJcblxyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcGFkZGluZzogMWVtO1xyXG59XHJcblxyXG4gZGl2LmNhcmQucGljdHVyZS10aHVtYntcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTUwcHg7XHJcbiAgbWluLXdpZHRoOiAxNTBweDtcclxuICBoZWlnaHQ6IDE1MHB4O1xyXG5cclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIHBhZGRpbmc6IDFlbTtcclxuXHJcbiAgb3BhY2l0eTogMTtcclxufVxyXG5cclxuZGl2LmNhcmQucGljdHVyZS10aHVtYi5yZW1vdmluZ3tcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMHB4O1xyXG4gIG1pbi13aWR0aDogMHB4O1xyXG4gIGhlaWdodDogMHB4O1xyXG4gIG1pbi13aWR0aDogMHB4O1xyXG5cclxuICBwYWRkaW5nOiAwcHg7XHJcblxyXG4gIG9wYWNpdHk6IDA7XHJcblxyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDM1MG1zIGVhc2UtaW4tb3V0O1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDM1MG1zIGVhc2UtaW4tb3V0O1xyXG4gIC1tcy10cmFuc2l0aW9uOiBhbGwgMzUwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDM1MG1zIGVhc2UtaW4tb3V0O1xyXG4gIHRyYW5zaXRpb246IGFsbCAzNTBtcyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuZGl2LmNhcmQucGljdHVyZS1hZGQgZGl2LmNhcmQtLXdyYXBwZXJ7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuXHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBwYWRkaW5nOiAxZW07XHJcblxyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGdyYXk7XHJcbn1cclxuZGl2LmNhcmQucGljdHVyZS10aHVtYiBkaXYuY2FyZC0td3JhcHBlcntcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG5cclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIHBhZGRpbmc6IDFlbTtcclxuXHJcbiAgYm9yZGVyOiAxcHggc29saWQgZ3JheTtcclxufVxyXG5cclxuXHJcbmRpdi5waWN0dXJlLXRodW1ie1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxNTBweDtcclxuICBtaW4td2lkdGg6IDE1MHB4O1xyXG4gIGhlaWdodDogMTUwcHg7XHJcblxyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcGFkZGluZzogMWVtO1xyXG5cclxuICBvcGFjaXR5OiAxO1xyXG59XHJcblxyXG5pbWcuY2FyZC0tdGh1bWJ7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcGFkZGluZzogMWVtO1xyXG4gIG9iamVjdC1maXQ6IGNvbnRhaW47XHJcbiAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMHB4O1xyXG4gIGxlZnQ6IDBweDtcclxufVxyXG5zcGFue1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDBweDtcclxuICByaWdodDogN3B4O1xyXG4gIGZvbnQtd2VpZ2h0OiA5MDA7XHJcbiAgZm9udC1mYW1pbHk6IG1vbm9zcGFjZTtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuc3Bhbjpob3ZlcntcclxuXHJcbn1cclxuXHJcbiJdfQ== */";
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-picture-thumb/plc-picture-thumb.component.ts":
  /*!***************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-picture-thumb/plc-picture-thumb.component.ts ***!
    \***************************************************************************************************************/

  /*! exports provided: PlcPictureThumbComponent */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPictureThumbPlcPictureThumbComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlcPictureThumbComponent", function () {
      return PlcPictureThumbComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var PlcPictureThumbComponent =
    /*#__PURE__*/
    function () {
      function PlcPictureThumbComponent() {
        _classCallCheck(this, PlcPictureThumbComponent);

        this.remove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.UX = {
          isRemoving: false
        };
      }

      _createClass(PlcPictureThumbComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "onClickRemove",
        value: function onClickRemove() {
          var _this2 = this;

          this.UX.isRemoving = true;
          setTimeout(function () {
            _this2.UX.isRemoving = false;

            _this2.remove.emit();
          }, 600);
        }
      }]);

      return PlcPictureThumbComponent;
    }();

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("id")], PlcPictureThumbComponent.prototype, "id", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("src")], PlcPictureThumbComponent.prototype, "src", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PlcPictureThumbComponent.prototype, "remove", void 0);
    PlcPictureThumbComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-plc-picture-thumb',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./plc-picture-thumb.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-picture-thumb/plc-picture-thumb.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./plc-picture-thumb.component.css */
      "./src/app/shared/product-logo-customization/components/plc-picture-thumb/plc-picture-thumb.component.css")).default]
    })], PlcPictureThumbComponent);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-position-item/plc-position-item.component.css":
  /*!****************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-position-item/plc-position-item.component.css ***!
    \****************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPositionItemPlcPositionItemComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/* ---------------------------------------------\r\n ---- Card :: Position                      ----\r\n -----------------------------------------------*/\r\ndiv.card.position{\r\n  display: block;\r\n  width: 100%;\r\n\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n\r\n  font-family: 'Open Sans', sans-serif;\r\n\r\n}\r\ndiv.card.position div.card--wrapper{\r\n\r\n width: 100%;\r\n display: flex;\r\n flex-direction: row;\r\n flex-wrap: nowrap;\r\n justify-content: flex-start;\r\n align-content: stretch;\r\n align-items: flex-start;\r\n}\r\n.no-checked-card{\r\n  border: 1px solid rgb(226, 226, 226);\r\n  border-radius: 1px;\r\n}\r\n.checked-card{\r\n  border: 1px solid green;\r\n  border-radius: 1px;\r\n}\r\ndiv.card.position.active div.card--wrapper{\r\n  background-color: rgb(206, 206, 206);\r\n}\r\ndiv.card.position:hover div.card--wrapper{\r\n background-color: rgb(226, 226, 226);\r\n cursor: pointer;\r\n transition: all 200ms ease-in-out;\r\n\r\n}\r\ndiv.card--col{\r\n display: flex;\r\n flex-direction: column;\r\n flex-wrap: nowrap;\r\n justify-content: flex-start;\r\n align-content: stretch;\r\n align-items: center;\r\n}\r\ndiv.card--col.card-checkbox{\r\n\r\n  width: 60px;\r\n  height: 60px;\r\n  display: flex;\r\n  flex-direction: column;\r\n  flex-wrap: nowrap;\r\n  justify-content: center;\r\n  align-content: stretch;\r\n  align-items: center;\r\n}\r\ndiv.card--col.card-checkbox input{\r\n   display: block;\r\n   margin: 0px;\r\n   padding: 0px;\r\n }\r\ndiv.card--col.name-and-colors{\r\n\r\n   width: calc( 100% - 60px);\r\n   height: 60px;\r\n\r\n   box-sizing: border-box;\r\n   padding: 10px;\r\n   padding-left: 0px;\r\n   display: flex;\r\n   flex-direction: column;\r\n   flex-wrap: nowrap;\r\n   justify-content: center;\r\n   align-content: stretch;\r\n   align-items: flex-start;\r\n}\r\ndiv.card--col.name-and-colors h2{\r\n  font-size: 16px;\r\n  box-sizing: border-box;\r\n  padding-bottom: 5px;\r\n  margin: 0;\r\n}\r\ndiv.card--col.name-and-colors select{\r\n  font-size: 10px;\r\n  box-sizing: border-box;\r\n  padding-bottom: 0px;\r\n}\r\n/* ----------------------------------------- */\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uL2NvbXBvbmVudHMvcGxjLXBvc2l0aW9uLWl0ZW0vcGxjLXBvc2l0aW9uLWl0ZW0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7aURBRWlEO0FBQ2pEO0VBQ0UsY0FBYztFQUNkLFdBQVc7O0VBRVgsc0JBQXNCO0VBQ3RCLFlBQVk7O0VBRVosb0NBQW9DOztBQUV0QztBQUVBOztDQUVDLFdBQVc7Q0FJWCxhQUFhO0NBR2IsbUJBQW1CO0NBR25CLGlCQUFpQjtDQUdqQiwyQkFBMkI7Q0FHM0Isc0JBQXNCO0NBR3RCLHVCQUF1QjtBQUN4QjtBQUVBO0VBQ0Usb0NBQW9DO0VBQ3BDLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0Usb0NBQW9DO0FBQ3RDO0FBQ0E7Q0FDQyxvQ0FBb0M7Q0FDcEMsZUFBZTtDQU1mLGlDQUFpQzs7QUFFbEM7QUFDQTtDQUdDLGFBQWE7Q0FHYixzQkFBc0I7Q0FHdEIsaUJBQWlCO0NBR2pCLDJCQUEyQjtDQUczQixzQkFBc0I7Q0FHdEIsbUJBQW1CO0FBQ3BCO0FBQ0E7O0VBRUUsV0FBVztFQUNYLFlBQVk7RUFJWixhQUFhO0VBR2Isc0JBQXNCO0VBR3RCLGlCQUFpQjtFQUdqQix1QkFBdUI7RUFHdkIsc0JBQXNCO0VBR3RCLG1CQUFtQjtBQUNyQjtBQUNBO0dBQ0csY0FBYztHQUNkLFdBQVc7R0FDWCxZQUFZO0NBQ2Q7QUFDRDs7R0FFRyx5QkFBeUI7R0FDekIsWUFBWTs7R0FFWixzQkFBc0I7R0FDdEIsYUFBYTtHQUNiLGlCQUFpQjtHQUlqQixhQUFhO0dBR2Isc0JBQXNCO0dBR3RCLGlCQUFpQjtHQUdqQix1QkFBdUI7R0FHdkIsc0JBQXNCO0dBR3RCLHVCQUF1QjtBQUMxQjtBQUNBO0VBQ0UsZUFBZTtFQUNmLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsU0FBUztBQUNYO0FBQ0E7RUFDRSxlQUFlO0VBQ2Ysc0JBQXNCO0VBQ3RCLG1CQUFtQjtBQUNyQjtBQUNDLDhDQUE4QyIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9wcm9kdWN0LWxvZ28tY3VzdG9taXphdGlvbi9jb21wb25lbnRzL3BsYy1wb3NpdGlvbi1pdGVtL3BsYy1wb3NpdGlvbi1pdGVtLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuIC0tLS0gQ2FyZCA6OiBQb3NpdGlvbiAgICAgICAgICAgICAgICAgICAgICAtLS0tXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmRpdi5jYXJkLnBvc2l0aW9ue1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG5cclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIHBhZGRpbmc6IDFlbTtcclxuXHJcbiAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG5cclxufVxyXG5cclxuZGl2LmNhcmQucG9zaXRpb24gZGl2LmNhcmQtLXdyYXBwZXJ7XHJcblxyXG4gd2lkdGg6IDEwMCU7XHJcblxyXG4gZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiBkaXNwbGF5OiBmbGV4O1xyXG4gLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAtbXMtZmxleC1wYWNrOiBzdGFydDtcclxuIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuIC13ZWJraXQtYWxpZ24tY29udGVudDogc3RyZXRjaDtcclxuIC1tcy1mbGV4LWxpbmUtcGFjazogc3RyZXRjaDtcclxuIGFsaWduLWNvbnRlbnQ6IHN0cmV0Y2g7XHJcbiAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xyXG4gYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbn1cclxuXHJcbi5uby1jaGVja2VkLWNhcmR7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiKDIyNiwgMjI2LCAyMjYpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDFweDtcclxufVxyXG4uY2hlY2tlZC1jYXJke1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGdyZWVuO1xyXG4gIGJvcmRlci1yYWRpdXM6IDFweDtcclxufVxyXG5cclxuZGl2LmNhcmQucG9zaXRpb24uYWN0aXZlIGRpdi5jYXJkLS13cmFwcGVye1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyMDYsIDIwNiwgMjA2KTtcclxufVxyXG5kaXYuY2FyZC5wb3NpdGlvbjpob3ZlciBkaXYuY2FyZC0td3JhcHBlcntcclxuIGJhY2tncm91bmQtY29sb3I6IHJnYigyMjYsIDIyNiwgMjI2KTtcclxuIGN1cnNvcjogcG9pbnRlcjtcclxuXHJcbiAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuIC1tb3otdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0O1xyXG4gLW1zLXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuIC1vLXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuIHRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuXHJcbn1cclxuZGl2LmNhcmQtLWNvbHtcclxuIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gZGlzcGxheTogZmxleDtcclxuIC13ZWJraXQtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuIC13ZWJraXQtZmxleC13cmFwOiBub3dyYXA7XHJcbiAtbXMtZmxleC13cmFwOiBub3dyYXA7XHJcbiBmbGV4LXdyYXA6IG5vd3JhcDtcclxuIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gLW1zLWZsZXgtcGFjazogc3RhcnQ7XHJcbiBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAtd2Via2l0LWFsaWduLWNvbnRlbnQ6IHN0cmV0Y2g7XHJcbiAtbXMtZmxleC1saW5lLXBhY2s6IHN0cmV0Y2g7XHJcbiBhbGlnbi1jb250ZW50OiBzdHJldGNoO1xyXG4gLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xyXG4gLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuZGl2LmNhcmQtLWNvbC5jYXJkLWNoZWNrYm94e1xyXG5cclxuICB3aWR0aDogNjBweDtcclxuICBoZWlnaHQ6IDYwcHg7XHJcblxyXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC13ZWJraXQtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIC13ZWJraXQtZmxleC13cmFwOiBub3dyYXA7XHJcbiAgLW1zLWZsZXgtd3JhcDogbm93cmFwO1xyXG4gIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIC13ZWJraXQtYWxpZ24tY29udGVudDogc3RyZXRjaDtcclxuICAtbXMtZmxleC1saW5lLXBhY2s6IHN0cmV0Y2g7XHJcbiAgYWxpZ24tY29udGVudDogc3RyZXRjaDtcclxuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbmRpdi5jYXJkLS1jb2wuY2FyZC1jaGVja2JveCBpbnB1dHtcclxuICAgZGlzcGxheTogYmxvY2s7XHJcbiAgIG1hcmdpbjogMHB4O1xyXG4gICBwYWRkaW5nOiAwcHg7XHJcbiB9XHJcbmRpdi5jYXJkLS1jb2wubmFtZS1hbmQtY29sb3Jze1xyXG5cclxuICAgd2lkdGg6IGNhbGMoIDEwMCUgLSA2MHB4KTtcclxuICAgaGVpZ2h0OiA2MHB4O1xyXG5cclxuICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgcGFkZGluZzogMTBweDtcclxuICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcblxyXG4gICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICBkaXNwbGF5OiBmbGV4O1xyXG4gICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAtd2Via2l0LWZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAtbXMtZmxleC13cmFwOiBub3dyYXA7XHJcbiAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAtd2Via2l0LWp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XHJcbiAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAtd2Via2l0LWFsaWduLWNvbnRlbnQ6IHN0cmV0Y2g7XHJcbiAgIC1tcy1mbGV4LWxpbmUtcGFjazogc3RyZXRjaDtcclxuICAgYWxpZ24tY29udGVudDogc3RyZXRjaDtcclxuICAgLXdlYmtpdC1hbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xyXG4gICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxufVxyXG5kaXYuY2FyZC0tY29sLm5hbWUtYW5kLWNvbG9ycyBoMntcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG5kaXYuY2FyZC0tY29sLm5hbWUtYW5kLWNvbG9ycyBzZWxlY3R7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcGFkZGluZy1ib3R0b206IDBweDtcclxufVxyXG4gLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gKi9cclxuIl19 */";
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-position-item/plc-position-item.component.ts":
  /*!***************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-position-item/plc-position-item.component.ts ***!
    \***************************************************************************************************************/

  /*! exports provided: PlcPositionItemComponent */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPositionItemPlcPositionItemComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlcPositionItemComponent", function () {
      return PlcPositionItemComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var PlcPositionItemComponent =
    /*#__PURE__*/
    function () {
      function PlcPositionItemComponent() {
        _classCallCheck(this, PlcPositionItemComponent);

        this.active = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.enable = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.colorChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.isChecked = false;
        this.color = {};
      }

      _createClass(PlcPositionItemComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var colorId = this.colors[0].id;
          this.color = this.colors.filter(function (color) {
            return color.id === colorId;
          })[0];
        }
      }, {
        key: "ngOnChanges",
        value: function ngOnChanges(changes) {
          var colorId = this.colors[0].id;
          this.color = this.colors.filter(function (color) {
            return color.id === colorId;
          })[0];
        }
      }, {
        key: "setActive",
        value: function setActive($event) {
          if ($event.target.localName !== 'input' && $event.target.localName !== 'select') {
            this.active.emit();
            this.colorChange.emit(this.color);
          }
        }
      }, {
        key: "setEnable",
        value: function setEnable($event) {
          this.isChecked = !this.isChecked;
          this.enable.emit();
        }
      }, {
        key: "setColor",
        value: function setColor($event) {
          var colorId = $event.target.value;
          this.color = this.colors.filter(function (color) {
            return color.id === colorId;
          })[0];
          this.colorChange.emit(this.color);
        }
      }]);

      return PlcPositionItemComponent;
    }();

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('name')], PlcPositionItemComponent.prototype, "name", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('isActive')], PlcPositionItemComponent.prototype, "isActive", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('colors')], PlcPositionItemComponent.prototype, "colors", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PlcPositionItemComponent.prototype, "active", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PlcPositionItemComponent.prototype, "enable", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PlcPositionItemComponent.prototype, "colorChange", void 0);
    PlcPositionItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-plc-position-item',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./plc-position-item.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-position-item/plc-position-item.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./plc-position-item.component.css */
      "./src/app/shared/product-logo-customization/components/plc-position-item/plc-position-item.component.css")).default]
    })], PlcPositionItemComponent);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-positions-list/plc-positions-list.component.css":
  /*!******************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-positions-list/plc-positions-list.component.css ***!
    \******************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPositionsListPlcPositionsListComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/* ---------------------------------------------\r\n ---- Column :: Positions List                  ----\r\n -----------------------------------------------*/\r\ndiv.position-list{\r\n\r\n  display: inline-block;\r\n  vertical-align: top;\r\n\r\n  width: var(--plc-view-sidebar-width);\r\n  height: var(--plc-view-sidebar-height);\r\n\r\n  overflow-y: scroll;\r\n  overflow-x: hidden;\r\n\r\n  padding: 0px;\r\n  margin: 0px;\r\n\r\n  background-color: #f7f7f7;\r\n  border-right: 1px solid #b7b7b7;\r\n    display: flex;\r\n    flex-direction: column;\r\n    flex-wrap: nowrap;\r\n    justify-content: flex-start;\r\n    align-content: center;\r\n    align-items: flex-start;\r\n\r\n}\r\nh1.position-list--title{\r\n  font-weight: 700;\r\n  font-size: 36px;\r\n  font-family: 'Open Sans';\r\n\r\n  color: #56778e;\r\n  line-height: 36px;\r\n\r\n  padding: 0.5em;\r\n  margin: 0px;\r\n}\r\nul.position-list--items{\r\n  display: block;\r\n  list-style-type: none;\r\n  margin: 0px;\r\n  padding: 0px;\r\n  width: 100%;\r\n\r\n}\r\nli.position-list--item{\r\n  display: block;\r\n  list-style-type: none;\r\n  margin: 0px;\r\n  padding: 0px;\r\n  width: 100%;\r\n}\r\ndiv.buttons--list{\r\n  order: 0;\r\n  flex: 1 1 auto;\r\n  align-self: auto;\r\n    display: flex;\r\n    flex-direction: column;\r\n    flex-wrap: nowrap;\r\n    justify-content: flex-end;\r\n    align-content: center;\r\n    align-items: center;\r\n\r\n    width: 100%;\r\n\r\n}\r\n/* ----------------------------------------- */\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uL2NvbXBvbmVudHMvcGxjLXBvc2l0aW9ucy1saXN0L3BsYy1wb3NpdGlvbnMtbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztpREFFaUQ7QUFDakQ7O0VBRUUscUJBQXFCO0VBQ3JCLG1CQUFtQjs7RUFFbkIsb0NBQW9DO0VBQ3BDLHNDQUFzQzs7RUFFdEMsa0JBQWtCO0VBQ2xCLGtCQUFrQjs7RUFFbEIsWUFBWTtFQUNaLFdBQVc7O0VBRVgseUJBQXlCO0VBQ3pCLCtCQUErQjtJQUk3QixhQUFhO0lBR2Isc0JBQXNCO0lBR3RCLGlCQUFpQjtJQUdqQiwyQkFBMkI7SUFHM0IscUJBQXFCO0lBR3JCLHVCQUF1Qjs7QUFFM0I7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysd0JBQXdCOztFQUV4QixjQUFjO0VBQ2QsaUJBQWlCOztFQUVqQixjQUFjO0VBQ2QsV0FBVztBQUNiO0FBQ0E7RUFDRSxjQUFjO0VBQ2QscUJBQXFCO0VBQ3JCLFdBQVc7RUFDWCxZQUFZO0VBQ1osV0FBVzs7QUFFYjtBQUNBO0VBQ0UsY0FBYztFQUNkLHFCQUFxQjtFQUNyQixXQUFXO0VBQ1gsWUFBWTtFQUNaLFdBQVc7QUFDYjtBQUNBO0VBR0UsUUFBUTtFQUdSLGNBQWM7RUFHZCxnQkFBZ0I7SUFLZCxhQUFhO0lBR2Isc0JBQXNCO0lBR3RCLGlCQUFpQjtJQUdqQix5QkFBeUI7SUFHekIscUJBQXFCO0lBR3JCLG1CQUFtQjs7SUFFbkIsV0FBVzs7QUFFZjtBQUNFLDhDQUE4QyIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9wcm9kdWN0LWxvZ28tY3VzdG9taXphdGlvbi9jb21wb25lbnRzL3BsYy1wb3NpdGlvbnMtbGlzdC9wbGMtcG9zaXRpb25zLWxpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gLS0tLSBDb2x1bW4gOjogUG9zaXRpb25zIExpc3QgICAgICAgICAgICAgICAgICAtLS0tXHJcbiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXHJcbmRpdi5wb3NpdGlvbi1saXN0e1xyXG5cclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuXHJcbiAgd2lkdGg6IHZhcigtLXBsYy12aWV3LXNpZGViYXItd2lkdGgpO1xyXG4gIGhlaWdodDogdmFyKC0tcGxjLXZpZXctc2lkZWJhci1oZWlnaHQpO1xyXG5cclxuICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xyXG5cclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgbWFyZ2luOiAwcHg7XHJcblxyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XHJcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2I3YjdiNztcclxuXHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIC13ZWJraXQtYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gICAgLW1zLWZsZXgtbGluZS1wYWNrOiBjZW50ZXI7XHJcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgLW1zLWZsZXgtYWxpZ246IHN0YXJ0O1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcblxyXG59XHJcbmgxLnBvc2l0aW9uLWxpc3QtLXRpdGxle1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgZm9udC1zaXplOiAzNnB4O1xyXG4gIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJztcclxuXHJcbiAgY29sb3I6ICM1Njc3OGU7XHJcbiAgbGluZS1oZWlnaHQ6IDM2cHg7XHJcblxyXG4gIHBhZGRpbmc6IDAuNWVtO1xyXG4gIG1hcmdpbjogMHB4O1xyXG59XHJcbnVsLnBvc2l0aW9uLWxpc3QtLWl0ZW1ze1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxuICBtYXJnaW46IDBweDtcclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcblxyXG59XHJcbmxpLnBvc2l0aW9uLWxpc3QtLWl0ZW17XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIHBhZGRpbmc6IDBweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5kaXYuYnV0dG9ucy0tbGlzdHtcclxuICAtd2Via2l0LW9yZGVyOiAwO1xyXG4gIC1tcy1mbGV4LW9yZGVyOiAwO1xyXG4gIG9yZGVyOiAwO1xyXG4gIC13ZWJraXQtZmxleDogMSAxIGF1dG87XHJcbiAgLW1zLWZsZXg6IDEgMSBhdXRvO1xyXG4gIGZsZXg6IDEgMSBhdXRvO1xyXG4gIC13ZWJraXQtYWxpZ24tc2VsZjogYXV0bztcclxuICAtbXMtZmxleC1pdGVtLWFsaWduOiBhdXRvO1xyXG4gIGFsaWduLXNlbGY6IGF1dG87XHJcblxyXG5cclxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICAgIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAtd2Via2l0LWZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgLW1zLWZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgICAtd2Via2l0LWp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICAtbXMtZmxleC1wYWNrOiBlbmQ7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAtbXMtZmxleC1saW5lLXBhY2s6IGNlbnRlcjtcclxuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICAgIHdpZHRoOiAxMDAlO1xyXG5cclxufVxyXG4gIC8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tICovXHJcblxyXG4iXX0= */";
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-positions-list/plc-positions-list.component.ts":
  /*!*****************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-positions-list/plc-positions-list.component.ts ***!
    \*****************************************************************************************************************/

  /*! exports provided: PlcPositionsListComponent */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPositionsListPlcPositionsListComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlcPositionsListComponent", function () {
      return PlcPositionsListComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var PlcPositionsListComponent =
    /*#__PURE__*/
    function () {
      function PlcPositionsListComponent() {
        _classCallCheck(this, PlcPositionsListComponent);
      }

      _createClass(PlcPositionsListComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return PlcPositionsListComponent;
    }();

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('hide')], PlcPositionsListComponent.prototype, "hide", void 0);
    PlcPositionsListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-plc-positions-list',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./plc-positions-list.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-positions-list/plc-positions-list.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./plc-positions-list.component.css */
      "./src/app/shared/product-logo-customization/components/plc-positions-list/plc-positions-list.component.css")).default]
    })], PlcPositionsListComponent);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-preview-loader/plc-preview-loader.component.css":
  /*!******************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-preview-loader/plc-preview-loader.component.css ***!
    \******************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPreviewLoaderPlcPreviewLoaderComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "div.preview-loader{\r\n  z-index: 100000000;\r\n  display: block;\r\n  position: absolute;\r\n  top: 0px;\r\n  left: 0px;\r\n  width: 100%;\r\n  height: 100%;\r\n  display: flex;\r\n  flex-direction: column;\r\n  flex-wrap: nowrap;\r\n  justify-content: center;\r\n  align-content: center;\r\n  align-items: center;\r\n\r\n  background-color: #f1f2f3;\r\n}\r\n\r\ndiv.preview-loader.toHide{\r\n  opacity: 0;\r\ntransition: all 1000ms ease-in-out;\r\n}\r\n\r\ndiv.preview-loader.toShow{\r\n  opacity: 1;\r\ntransition: all 1000ms ease-in-out;\r\n}\r\n\r\ndiv.preview-loader.toHide img{\r\n  transform: scale(0.5);\r\n  transition: all 1000ms ease-in-out;\r\n}\r\n\r\ndiv.preview-loader.toShow img{\r\n  transform: scale(1.5);\r\n  transition: all 1000ms ease-in-out;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uL2NvbXBvbmVudHMvcGxjLXByZXZpZXctbG9hZGVyL3BsYy1wcmV2aWV3LWxvYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFNBQVM7RUFDVCxXQUFXO0VBQ1gsWUFBWTtFQUlaLGFBQWE7RUFHYixzQkFBc0I7RUFHdEIsaUJBQWlCO0VBR2pCLHVCQUF1QjtFQUd2QixxQkFBcUI7RUFHckIsbUJBQW1COztFQUVuQix5QkFBeUI7QUFDM0I7O0FBRUE7RUFDRSxVQUFVO0FBS1osa0NBQWtDO0FBQ2xDOztBQUVBO0VBQ0UsVUFBVTtBQUtaLGtDQUFrQztBQUNsQzs7QUFFQTtFQUNFLHFCQUFxQjtFQUtyQixrQ0FBa0M7QUFDcEM7O0FBQ0E7RUFDRSxxQkFBcUI7RUFLckIsa0NBQWtDO0FBQ3BDIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uL2NvbXBvbmVudHMvcGxjLXByZXZpZXctbG9hZGVyL3BsYy1wcmV2aWV3LWxvYWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZGl2LnByZXZpZXctbG9hZGVye1xyXG4gIHotaW5kZXg6IDEwMDAwMDAwMDtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAwcHg7XHJcbiAgbGVmdDogMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuXHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAtbXMtZmxleC13cmFwOiBub3dyYXA7XHJcbiAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgLXdlYmtpdC1hbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgLW1zLWZsZXgtbGluZS1wYWNrOiBjZW50ZXI7XHJcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmMWYyZjM7XHJcbn1cclxuXHJcbmRpdi5wcmV2aWV3LWxvYWRlci50b0hpZGV7XHJcbiAgb3BhY2l0eTogMDtcclxuLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG4tbW96LXRyYW5zaXRpb246IGFsbCAxMDAwbXMgZWFzZS1pbi1vdXQ7XHJcbi1tcy10cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG4tby10cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG50cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG5kaXYucHJldmlldy1sb2FkZXIudG9TaG93e1xyXG4gIG9wYWNpdHk6IDE7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG4tbW96LXRyYW5zaXRpb246IGFsbCAxMDAwbXMgZWFzZS1pbi1vdXQ7XHJcbi1tcy10cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG4tby10cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG50cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG59XHJcblxyXG5kaXYucHJldmlldy1sb2FkZXIudG9IaWRlIGltZ3tcclxuICB0cmFuc2Zvcm06IHNjYWxlKDAuNSk7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG4gIC1tb3otdHJhbnNpdGlvbjogYWxsIDEwMDBtcyBlYXNlLWluLW91dDtcclxuICAtbXMtdHJhbnNpdGlvbjogYWxsIDEwMDBtcyBlYXNlLWluLW91dDtcclxuICAtby10cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG4gIHRyYW5zaXRpb246IGFsbCAxMDAwbXMgZWFzZS1pbi1vdXQ7XHJcbn1cclxuZGl2LnByZXZpZXctbG9hZGVyLnRvU2hvdyBpbWd7XHJcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLjUpO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDEwMDBtcyBlYXNlLWluLW91dDtcclxuICAtbW96LXRyYW5zaXRpb246IGFsbCAxMDAwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgLW1zLXRyYW5zaXRpb246IGFsbCAxMDAwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgLW8tdHJhbnNpdGlvbjogYWxsIDEwMDBtcyBlYXNlLWluLW91dDtcclxuICB0cmFuc2l0aW9uOiBhbGwgMTAwMG1zIGVhc2UtaW4tb3V0O1xyXG59XHJcbiJdfQ== */";
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-preview-loader/plc-preview-loader.component.ts":
  /*!*****************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-preview-loader/plc-preview-loader.component.ts ***!
    \*****************************************************************************************************************/

  /*! exports provided: PlcPreviewLoaderComponent */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPreviewLoaderPlcPreviewLoaderComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlcPreviewLoaderComponent", function () {
      return PlcPreviewLoaderComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_common_loading_loading_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/common/loading/loading.service */
    "./src/app/shared/product-logo-customization/services/common/loading/loading.service.ts");

    var PlcPreviewLoaderComponent =
    /*#__PURE__*/
    function () {
      function PlcPreviewLoaderComponent(loaderService) {
        _classCallCheck(this, PlcPreviewLoaderComponent);

        this.loaderService = loaderService;
      }

      _createClass(PlcPreviewLoaderComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this3 = this;

          this.loader = {
            isShow: true,
            transition: 'none'
          };
          this.subs = this.loaderService.dataObs.subscribe(function (loader) {
            return _this3.loader = loader;
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subs.unsubscribe();
        }
      }]);

      return PlcPreviewLoaderComponent;
    }();

    PlcPreviewLoaderComponent.ctorParameters = function () {
      return [{
        type: _services_common_loading_loading_service__WEBPACK_IMPORTED_MODULE_2__["LoadingService"]
      }];
    };

    PlcPreviewLoaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-plc-preview-loader',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./plc-preview-loader.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-preview-loader/plc-preview-loader.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./plc-preview-loader.component.css */
      "./src/app/shared/product-logo-customization/components/plc-preview-loader/plc-preview-loader.component.css")).default]
    })], PlcPreviewLoaderComponent);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-preview/plc-preview.component.css":
  /*!****************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-preview/plc-preview.component.css ***!
    \****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPreviewPlcPreviewComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "div.preview{\r\n  display: block;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\ndiv.preview .buttons{\r\n  position: absolute;\r\n  top: 0px;\r\n  right: 0px;\r\n  display: block;\r\n  width: -webkit-fit-content;\r\n  width: -moz-fit-content;\r\n  width: fit-content;\r\n  padding: 1em 1em;\r\n  box-sizing: border-box;\r\n  display: flex;\r\n  flex-direction: row;\r\n  flex-wrap: nowrap;\r\n  justify-content: flex-end;\r\n  align-content: flex-start;\r\n  align-items: center;\r\n}\r\ndiv.preview .buttons button{\r\n  background: none;\r\n  outline: none;\r\n  border: 1px solid gray;\r\n  border-radius: 1px;\r\n  width: black;\r\n  width: 2.5em;\r\n  height: 2.5em;\r\n  display: inline-block;\r\n  position: relative;\r\n  padding: 0.25em;\r\n  margin-right: 1em;\r\n  display: flex;\r\n  flex-direction: row;\r\n  flex-wrap: nowrap;\r\n  justify-content: center;\r\n  align-content: center;\r\n  align-items: center;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uL2NvbXBvbmVudHMvcGxjLXByZXZpZXcvcGxjLXByZXZpZXcuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7RUFDZCxXQUFXO0VBQ1gsWUFBWTtBQUNkO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFVBQVU7RUFDVixjQUFjO0VBQ2QsMEJBQWtCO0VBQWxCLHVCQUFrQjtFQUFsQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLHNCQUFzQjtFQUl0QixhQUFhO0VBR2IsbUJBQW1CO0VBR25CLGlCQUFpQjtFQUdqQix5QkFBeUI7RUFHekIseUJBQXlCO0VBR3pCLG1CQUFtQjtBQUNyQjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixZQUFZO0VBQ1osYUFBYTtFQUNiLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGlCQUFpQjtFQUlqQixhQUFhO0VBR2IsbUJBQW1CO0VBR25CLGlCQUFpQjtFQUdqQix1QkFBdUI7RUFHdkIscUJBQXFCO0VBR3JCLG1CQUFtQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9wcm9kdWN0LWxvZ28tY3VzdG9taXphdGlvbi9jb21wb25lbnRzL3BsYy1wcmV2aWV3L3BsYy1wcmV2aWV3LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJkaXYucHJldmlld3tcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuZGl2LnByZXZpZXcgLmJ1dHRvbnN7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMHB4O1xyXG4gIHJpZ2h0OiAwcHg7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xyXG4gIHBhZGRpbmc6IDFlbSAxZW07XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuXHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIC1tcy1mbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAtbXMtZmxleC13cmFwOiBub3dyYXA7XHJcbiAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gIC1tcy1mbGV4LXBhY2s6IGVuZDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gIC13ZWJraXQtYWxpZ24tY29udGVudDogZmxleC1zdGFydDtcclxuICAtbXMtZmxleC1saW5lLXBhY2s6IHN0YXJ0O1xyXG4gIGFsaWduLWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuZGl2LnByZXZpZXcgLmJ1dHRvbnMgYnV0dG9ue1xyXG4gIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICBib3JkZXI6IDFweCBzb2xpZCBncmF5O1xyXG4gIGJvcmRlci1yYWRpdXM6IDFweDtcclxuICB3aWR0aDogYmxhY2s7XHJcbiAgd2lkdGg6IDIuNWVtO1xyXG4gIGhlaWdodDogMi41ZW07XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBwYWRkaW5nOiAwLjI1ZW07XHJcbiAgbWFyZ2luLXJpZ2h0OiAxZW07XHJcblxyXG4gIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIC13ZWJraXQtZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAtbXMtZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIC13ZWJraXQtZmxleC13cmFwOiBub3dyYXA7XHJcbiAgLW1zLWZsZXgtd3JhcDogbm93cmFwO1xyXG4gIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gIC13ZWJraXQtanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgLW1zLWZsZXgtcGFjazogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIC13ZWJraXQtYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gIC1tcy1mbGV4LWxpbmUtcGFjazogY2VudGVyO1xyXG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbiJdfQ== */";
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/components/plc-preview/plc-preview.component.ts":
  /*!***************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/components/plc-preview/plc-preview.component.ts ***!
    \***************************************************************************************************/

  /*! exports provided: PlcPreviewComponent */

  /***/
  function srcAppSharedProductLogoCustomizationComponentsPlcPreviewPlcPreviewComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlcPreviewComponent", function () {
      return PlcPreviewComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_libs_handlers_services_fabric_js_fabric_handler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/libs-handlers-services/fabric-js/fabric-handler.service */
    "./src/app/shared/product-logo-customization/services/libs-handlers-services/fabric-js/fabric-handler.service.ts");

    var PlcPreviewComponent =
    /*#__PURE__*/
    function () {
      function PlcPreviewComponent(fabricService) {
        _classCallCheck(this, PlcPreviewComponent);

        this.fabricService = fabricService;
      }

      _createClass(PlcPreviewComponent, [{
        key: "onKeyPress",
        value: function onKeyPress(event) {
          if (event.key === 'Delete') {
            this.fabricService.remoteSelectedFromBoard();
          }
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "doZoomIn",
        value: function doZoomIn() {
          this.fabricService.zoomIn();
        }
      }, {
        key: "doZoomOut",
        value: function doZoomOut() {
          this.fabricService.zoomOut();
        }
      }]);

      return PlcPreviewComponent;
    }();

    PlcPreviewComponent.ctorParameters = function () {
      return [{
        type: _services_libs_handlers_services_fabric_js_fabric_handler_service__WEBPACK_IMPORTED_MODULE_2__["FabricHandlerService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("window:keydown", ["$event"])], PlcPreviewComponent.prototype, "onKeyPress", null);
    PlcPreviewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-plc-preview',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./plc-preview.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/components/plc-preview/plc-preview.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./plc-preview.component.css */
      "./src/app/shared/product-logo-customization/components/plc-preview/plc-preview.component.css")).default]
    })], PlcPreviewComponent);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/helpers/md5.ts":
  /*!******************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/helpers/md5.ts ***!
    \******************************************************************/

  /*! exports provided: md5 */

  /***/
  function srcAppSharedProductLogoCustomizationHelpersMd5Ts(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "md5", function () {
      return md5;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js"); // Taken from https://github.com/killmenot/webtoolkit.md5


    var md5 = function md5(string) {
      function RotateLeft(lValue, iShiftBits) {
        return lValue << iShiftBits | lValue >>> 32 - iShiftBits;
      }

      function AddUnsigned(lX, lY) {
        var lX4, lY4, lX8, lY8, lResult;
        lX8 = lX & 0x80000000;
        lY8 = lY & 0x80000000;
        lX4 = lX & 0x40000000;
        lY4 = lY & 0x40000000;
        lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);

        if (lX4 & lY4) {
          return lResult ^ 0x80000000 ^ lX8 ^ lY8;
        }

        if (lX4 | lY4) {
          if (lResult & 0x40000000) {
            return lResult ^ 0xC0000000 ^ lX8 ^ lY8;
          } else {
            return lResult ^ 0x40000000 ^ lX8 ^ lY8;
          }
        } else {
          return lResult ^ lX8 ^ lY8;
        }
      }

      function F(x, y, z) {
        return x & y | ~x & z;
      }

      function G(x, y, z) {
        return x & z | y & ~z;
      }

      function H(x, y, z) {
        return x ^ y ^ z;
      }

      function I(x, y, z) {
        return y ^ (x | ~z);
      }

      function FF(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
      }

      ;

      function GG(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
      }

      ;

      function HH(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
      }

      ;

      function II(a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
      }

      ;

      function ConvertToWordArray(string) {
        var lWordCount;
        var lMessageLength = string.length;
        var lNumberOfWords_temp1 = lMessageLength + 8;
        var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - lNumberOfWords_temp1 % 64) / 64;
        var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
        var lWordArray = Array(lNumberOfWords - 1);
        var lBytePosition = 0;
        var lByteCount = 0;

        while (lByteCount < lMessageLength) {
          lWordCount = (lByteCount - lByteCount % 4) / 4;
          lBytePosition = lByteCount % 4 * 8;
          lWordArray[lWordCount] = lWordArray[lWordCount] | string.charCodeAt(lByteCount) << lBytePosition;
          lByteCount++;
        }

        lWordCount = (lByteCount - lByteCount % 4) / 4;
        lBytePosition = lByteCount % 4 * 8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | 0x80 << lBytePosition;
        lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
        lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
        return lWordArray;
      }

      ;

      function WordToHex(lValue) {
        var WordToHexValue = "",
            WordToHexValue_temp = "",
            lByte,
            lCount;

        for (lCount = 0; lCount <= 3; lCount++) {
          lByte = lValue >>> lCount * 8 & 255;
          WordToHexValue_temp = "0" + lByte.toString(16);
          WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
        }

        return WordToHexValue;
      }

      ;

      function Utf8Encode(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {
          var c = string.charCodeAt(n);

          if (c < 128) {
            utftext += String.fromCharCode(c);
          } else if (c > 127 && c < 2048) {
            utftext += String.fromCharCode(c >> 6 | 192);
            utftext += String.fromCharCode(c & 63 | 128);
          } else {
            utftext += String.fromCharCode(c >> 12 | 224);
            utftext += String.fromCharCode(c >> 6 & 63 | 128);
            utftext += String.fromCharCode(c & 63 | 128);
          }
        }

        return utftext;
      }

      ;
      var x = Array();
      var k, AA, BB, CC, DD, a, b, c, d;
      var S11 = 7,
          S12 = 12,
          S13 = 17,
          S14 = 22;
      var S21 = 5,
          S22 = 9,
          S23 = 14,
          S24 = 20;
      var S31 = 4,
          S32 = 11,
          S33 = 16,
          S34 = 23;
      var S41 = 6,
          S42 = 10,
          S43 = 15,
          S44 = 21;
      string = Utf8Encode(string);
      x = ConvertToWordArray(string);
      a = 0x67452301;
      b = 0xEFCDAB89;
      c = 0x98BADCFE;
      d = 0x10325476;

      for (k = 0; k < x.length; k += 16) {
        AA = a;
        BB = b;
        CC = c;
        DD = d;
        a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
        d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
        c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
        b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
        a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
        d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
        c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
        b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
        a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
        d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
        c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
        b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
        a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
        d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
        c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
        b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
        a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
        d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
        c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
        b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
        a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
        d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
        c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
        b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
        a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
        d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
        c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
        b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
        a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
        d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
        c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
        b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
        a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
        d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
        c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
        b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
        a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
        d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
        c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
        b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
        a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
        d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
        c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
        b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
        a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
        d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
        c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
        b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
        a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
        d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
        c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
        b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
        a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
        d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
        c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
        b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
        a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
        d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
        c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
        b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
        a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
        d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
        c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
        b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
        a = AddUnsigned(a, AA);
        b = AddUnsigned(b, BB);
        c = AddUnsigned(c, CC);
        d = AddUnsigned(d, DD);
      }

      var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);
      return temp.toLowerCase();
    };
    /***/

  },

  /***/
  "./src/app/shared/product-logo-customization/mocks/positions-list.mock.ts":
  /*!********************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/mocks/positions-list.mock.ts ***!
    \********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedProductLogoCustomizationMocksPositionsListMockTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony default export */


    __webpack_exports__["default"] = [{
      name: 'T-Shirt Front Logo',
      center: {
        x: 50,
        y: 25
      },
      size: {
        w: 15,
        h: 15
      },
      colors: [{
        id: 'TSHIRTF33',
        hex: '#6f79a0',
        price: 12.34,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-gray.jpg',
        description: 'COLOR 1'
      }, {
        id: 'TSHIRTF4F',
        hex: '#7f7f7f',
        price: 10.23,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-gray.jpg',
        description: 'COLOR 2'
      }, {
        id: 'TSHIRTF22',
        hex: '#689144',
        price: 15.99,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-gray.jpg',
        description: 'COLOR 3'
      }, {
        id: 'TSHIRTFR4D',
        hex: '#b46767',
        price: 13.33,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-gray.jpg',
        description: 'COLOR 4'
      }],
      logos: [{
        id: '96YC5Z9G44VL4KS2P2SVD43YB9P4LPMUX',
        name: 'Logo Coca Cola',
        timestamp: '2019-12-30T04:30:32.799Z',
        src: 'https://blipoint.com/blog/wp-content/uploads/2018/04/logo-coca-cola-actual.png'
      }, {
        id: '96YC5Z9G44VL4s3EE2P2SVD43YB9PMUP4',
        name: 'Logo PayPal ',
        timestamp: '2019-12-30T04:30:33.799Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/a/a4/Paypal_2014_logo.png'
      }, {
        id: 'SVD43YB9G44VL49PMUP4EE2P2KKLI',
        name: 'Logo Apple',
        timestamp: '2019-11-30T04:30:32.799Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Apple_logo_black.svg/1200px-Apple_logo_black.svg.png'
      }]
    }, {
      name: 'T-Shirt Back Logo',
      center: {
        x: 50,
        y: 20
      },
      size: {
        w: 25,
        h: 15
      },
      colors: [{
        id: 'TSHIRTB33',
        hex: '#6f79a0',
        price: 12.34,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-back-gray.jpg',
        description: 'COLOR 1'
      }, {
        id: 'TSHIRTB4F',
        hex: '#7f7f7f',
        price: 10.23,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-back-gray.jpg',
        description: 'COLOR 2'
      }, {
        id: 'TSHIRTB22',
        hex: '#689144',
        price: 15.99,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-back-gray.jpg',
        description: 'COLOR 3'
      }, {
        id: 'TSHIRTB4D',
        hex: '#b46767',
        price: 13.33,
        imageSrc: '../assets/images/t-shirt-colors/t-shirt-back-gray.jpg',
        description: 'COLOR 4'
      }],
      logos: [{
        id: '96YSsg4VLG4KS2P2SFCXX43YLKJMUX',
        name: 'Logo Microsoft',
        timestamp: '2019-11-20T04:30:32.799Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Microsoft_logo_%282012%29.svg/1280px-Microsoft_logo_%282012%29.svg.png'
      }, {
        id: 'X43G44YLKJMUXZ9G44VL4TTGHVD43YB9UP3',
        name: 'Logo IBM ',
        timestamp: '2019-11-22T04:32:23.569Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/IBM_logo.svg/1200px-IBM_logo.svg.png'
      }, {
        id: 'P2LSVD4MUXZ9GCXX43Y44VKMUP4EE2IK',
        name: 'Logo Intel',
        timestamp: '2019-09-30T04:30:32.799Z',
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Intel-logo.svg/1280px-Intel-logo.svg.png'
      }]
    }];
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/models/ILayout.model.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/models/ILayout.model.ts ***!
    \***************************************************************************/

  /*! exports provided: toILayout */

  /***/
  function srcAppSharedProductLogoCustomizationModelsILayoutModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "toILayout", function () {
      return toILayout;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    function toILayout(layout) {
      return {
        center: {
          x: layout.center ? layout.center.x : 0,
          y: layout.center ? layout.center.y : 0
        },
        position: {
          top: layout.position ? layout.position.top : 0,
          left: layout.position ? layout.position.left : 0
        },
        size: {
          w: layout.size ? layout.size.w : 0,
          h: layout.size ? layout.size.h : 0
        }
      };
    }
    /***/

  },

  /***/
  "./src/app/shared/product-logo-customization/product-logo-customization.component.css":
  /*!********************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/product-logo-customization.component.css ***!
    \********************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedProductLogoCustomizationProductLogoCustomizationComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\r\n @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap');\n/* ---------------------------------------------\r\n ---- Product Logo Customization Components ----\r\n -----------------------------------------------*/\ndiv.view.product-logo-customization{\r\n\r\n    display: block;\r\n    width: 100%;\r\n\r\n    font-family: 'Open Sans', sans-serif;\r\n\r\n    /* !IMPORTANT FIXED VALUE */\r\n    --plc-view-width: 100vw;\r\n    --plc-view-height: 800px;\r\n\r\n    --plc-view-sidebar-width: 350px;\r\n    --plc-view-sidebar-height: var(--plc-view-height);\r\n\r\n    --plc-view-content-and-pictures-width: calc( var(--plc-view-width) - var(--plc-view-sidebar-width) );\r\n    --plc-view-content-and-pictures-height: var(--plc-view-height);\r\n\r\n    --plc-view-pictures-width: var(--plc-view-content-and-pictures-width);\r\n    --plc-view-pictures-height: 185px;\r\n\r\n    --plc--view-content-preview-width: var(--plc-view-content-and-pictures-width);\r\n    --plc--view-content-preview-height: calc( var(--plc-view-content-and-pictures-height) - var(--plc-view-pictures-height));\r\n    display: flex;\r\n    flex-direction: row;\r\n    flex-wrap: nowrap;\r\n    justify-content: space-between;\r\n    align-content: stretch;\r\n    align-items: flex-start;\r\n\r\n    width: var(--plc-view-width);\r\n    height: var(--plc-view-height);\r\n\r\n    padding: 0px;\r\n    margin: 0px;\r\n\r\n    border: 1px solid #b7b7b7;\r\n    border-left: 0px;\r\n    border-right: 0px;\r\n\r\n }\n/* ----------------------------------------- */\n/* ---------------------------------------------\r\n ---- Column :: Preview And Pictures        ----\r\n -----------------------------------------------*/\ndiv.view-column.preview-and-pictures{\r\n\r\n    display: inline-block;\r\n    vertical-align: top;\r\n    position: relative;\r\n\r\n    width: var(--plc-view-content-and-pictures-width);\r\n    height: var(--plc-view-content-and-pictures-height);\r\n\r\n    padding: 0px;\r\n    margin: 0px;\r\n\r\n    background-color: #f9f9f9;\r\n    display: flex;\r\n    flex-direction: column;\r\n    flex-wrap: nowrap;\r\n    justify-content: space-between;\r\n    align-content: space-between;\r\n    align-items: center;\r\n\r\n  }\ndiv.view-row.preview, div.view-row.preview section{\r\n    display: block;\r\n    width: 100%;\r\n    height: calc( var(--plc-view-content-and-pictures-height) - 185px);\r\n    overflow: hidden;\r\n    display: flex;\r\n    flex-direction: column;\r\n    flex-wrap: nowrap;\r\n    justify-content: center;\r\n    align-content: center;\r\n    align-items: center;\r\n\r\n  }\ndiv.view-row.preview{\r\n    position: relative;\r\n    top: 0px;\r\n    left: 0px;\r\n  }\ndiv.view-row.preview section{\r\n    position: absolute;\r\n    top:0px;\r\n    left: 0px;\r\n  }\ndiv.view-row.preview section#IDPreviewZone{\r\n    z-index: 0;\r\n    opacity: 1;\r\n    filter: blur(0px);\r\n    -webkit-filter: blur(0px);\r\n\r\n  }\ndiv.view-row.preview section#IDPreviewLogos{\r\n    z-index: 0;\r\n    opacity: 1;\r\n    filter: blur(0px);\r\n    -webkit-filter: blur(0px);\r\n  }\ndiv.view-row.preview section#IDPreviewZone.draging{\r\n    z-index: 100;\r\n    opacity: 0.5;\r\n    filter: blur(0px);\r\n    -webkit-filter: blur(0px);\r\n    transition: all 200ms ease-in-out;\r\n\r\n  }\ndiv.view-row.preview section#IDPreviewLogos.draging{\r\n    z-index: 0;\r\n    opacity: 1;\r\n    filter: blur(8px);\r\n    -webkit-filter: blur(8px);\r\n    transition: all 200ms ease-in-out;\r\n  }\ndiv.view-row.pictures{\r\n\r\n    width: 100%;\r\n    height: 185px;\r\n\r\n    overflow-y: hidden;\r\n    overflow-x: scroll;\r\n    display: flex;\r\n    flex-direction: row;\r\n    flex-wrap: nowrap;\r\n    justify-content: flex-start;\r\n    align-content: stretch;\r\n    align-items: center;\r\n  }\n/* ----------------------------------------- */\n/* ----------------------------------------------\r\n ---- DragAndDrop :: Preview                 ----\r\n -----------------------------------------------*/\ndiv.preview--wrapper{\r\n  display: block;\r\n  position: absolute;\r\n\r\n  width: var(--plc-view-content-and-pictures-width);\r\n  height: calc( var(--plc-view-content-and-pictures-height) - 185px);\r\n\r\n  top: 0px;\r\n  left: 0px;\r\n\r\n  overflow: hidden;\r\n\r\n}\ndiv.preview--wrapper--container{\r\n  display: block;\r\n  position: absolute;\r\n  width: var(--plc-view-content-and-pictures-width);\r\n  height: calc( var(--plc-view-content-and-pictures-height) - 185px);\r\n  top: 0px;\r\n  left: 0px;\r\n  overflow: hidden;\r\n}\ndiv.preview--product, div.preview--drag-drop-logos{\r\n  display: block;\r\n  position: absolute;\r\n\r\n  width: var(--plc-view-content-and-pictures-width);\r\n  height: calc( var(--plc-view-content-and-pictures-height) - 185px);\r\n\r\n  top: 0px;\r\n  left: 0px;\r\n\r\n  overflow: hidden;\r\n}\ndiv.preview--product{\r\n\r\n  background-color: white;\r\n  position: absolute;\r\n\r\n  width: auto;\r\n  height: auto;\r\n\r\n}\ndiv.preview--product img{\r\n  display: block;\r\n  position: initial;\r\n  width: 500px;\r\n}\ndiv.preview--drop-can{\r\n  position: absolute;\r\n  bottom: 35px;\r\n  right: 40px;\r\n  width: 90px;\r\n  padding: 2em;\r\n  height: -webkit-fit-content;\r\n  height: -moz-fit-content;\r\n  height: fit-content;\r\n  cursor: pointer;\r\n  box-sizing: border-box;\r\n  pointer-events: none;\r\n\r\n}\ndiv.preview--drop-zone{\r\n      display: block;\r\n      width: 100%;\r\n      height: 100%;\r\n    display: flex;\r\n    flex-direction: row;\r\n    flex-wrap: nowrap;\r\n    justify-content: center;\r\n    align-content: center;\r\n    align-items: center;\r\n\r\n}\ndiv.preview--zoom{\r\n  position: absolute;\r\n  bottom:0px;\r\n  width: 100%;\r\n  height: 35px;\r\n\r\n  box-sizing: border-box;\r\n  padding-right: 20px;\r\n    display: flex;\r\n    flex-direction: row;\r\n    flex-wrap: nowrap;\r\n    justify-content: flex-end;\r\n    align-content: center;\r\n    align-items: center;\r\n\r\n}\ndiv.preview--drop-can img.close {\r\n  display: block;\r\n}\ndiv.preview--drop-can img.open {\r\n  display: none;\r\n}\ndiv.preview--drop-can:hover img.close {\r\n  display: none;\r\n}\ndiv.preview--drop-can:hover img.open {\r\n  display: block;\r\n}\ndiv.preview--drag-drop-logos{\r\n  /*\r\n  display: block;\r\n  width: 0px;\r\n  height: 0px;\r\n  position: absolute;\r\n  top: 0px;\r\n  left:0px;\r\n  */\r\n}\ndiv.preview--drag-drop-logos img.canvas--logo{\r\n  display: block;\r\n  position: absolute;\r\n  z-index: 1;\r\n  top: -100vh;\r\n  left: -100vw;\r\n}\ndiv.preview--drag-drop-logos div.canvas--square{\r\n  display: block;\r\n  position: absolute;\r\n  z-index: 10000;\r\n  border:1px dashed red;\r\n  min-width: 10px;\r\n  min-height: 10px;\r\n  top: 50%;\r\n  left: 50%;\r\n  pointer-events: none\r\n}\ndiv.buttons-wrapper{\r\n  display: block;\r\n  width: 100%;\r\n  display: flex;\r\n  flex-direction: row;\r\n  flex-wrap: nowrap;\r\n  justify-content: space-between;\r\n  align-content: center;\r\n  align-items: center;\r\n\r\n  box-sizing: border-box;\r\n    padding: 1em;\r\n\r\n}\ndiv.preview--trash-can{\r\n  display:block;\r\n  cursor: pointer;\r\n  position: absolute;\r\n  right: 50px;\r\n  bottom: 50px;\r\n  width: 100px;\r\n  opacity: 0.5;\r\n  box-sizing: border-box;\r\n  padding: 1em;\r\n  pointer-events: none\r\n}\ndiv.preview--trash-can img{\r\n  display:block; width: 100%;\r\n  pointer-events: none\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uL3Byb2R1Y3QtbG9nby1jdXN0b21pemF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtDQUdDLHlGQUF5RjtBQUgxRjs7aURBRWlEO0FBR2hEOztJQUVHLGNBQWM7SUFDZCxXQUFXOztJQUVYLG9DQUFvQzs7SUFFcEMsMkJBQTJCO0lBQzNCLHVCQUF1QjtJQUN2Qix3QkFBd0I7O0lBRXhCLCtCQUErQjtJQUMvQixpREFBaUQ7O0lBRWpELG9HQUFvRztJQUNwRyw4REFBOEQ7O0lBRTlELHFFQUFxRTtJQUNyRSxpQ0FBaUM7O0lBRWpDLDZFQUE2RTtJQUM3RSx3SEFBd0g7SUFJeEgsYUFBYTtJQUdiLG1CQUFtQjtJQUduQixpQkFBaUI7SUFHakIsOEJBQThCO0lBRzlCLHNCQUFzQjtJQUd0Qix1QkFBdUI7O0lBRXZCLDRCQUE0QjtJQUM1Qiw4QkFBOEI7O0lBRTlCLFlBQVk7SUFDWixXQUFXOztJQUVYLHlCQUF5QjtJQUN6QixnQkFBZ0I7SUFDaEIsaUJBQWlCOztDQUVwQjtBQUNBLDhDQUE4QztBQUsvQzs7aURBRWlEO0FBQy9DOztJQUVFLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsa0JBQWtCOztJQUVsQixpREFBaUQ7SUFDakQsbURBQW1EOztJQUVuRCxZQUFZO0lBQ1osV0FBVzs7SUFFWCx5QkFBeUI7SUFJekIsYUFBYTtJQUdiLHNCQUFzQjtJQUd0QixpQkFBaUI7SUFHakIsOEJBQThCO0lBRzlCLDRCQUE0QjtJQUc1QixtQkFBbUI7O0VBRXJCO0FBRUE7SUFDRSxjQUFjO0lBQ2QsV0FBVztJQUNYLGtFQUFrRTtJQUNsRSxnQkFBZ0I7SUFJaEIsYUFBYTtJQUdiLHNCQUFzQjtJQUd0QixpQkFBaUI7SUFHakIsdUJBQXVCO0lBR3ZCLHFCQUFxQjtJQUdyQixtQkFBbUI7O0VBRXJCO0FBQ0E7SUFDRSxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7RUFDWDtBQUNBO0lBQ0Usa0JBQWtCO0lBQ2xCLE9BQU87SUFDUCxTQUFTO0VBQ1g7QUFDQTtJQUNFLFVBQVU7SUFDVixVQUFVO0lBQ1YsaUJBQWlCO0lBQ2pCLHlCQUF5Qjs7RUFFM0I7QUFDQTtJQUNFLFVBQVU7SUFDVixVQUFVO0lBQ1YsaUJBQWlCO0lBQ2pCLHlCQUF5QjtFQUMzQjtBQUNBO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixpQkFBaUI7SUFDakIseUJBQXlCO0lBTXpCLGlDQUFpQzs7RUFFbkM7QUFDQTtJQUNFLFVBQVU7SUFDVixVQUFVO0lBQ1YsaUJBQWlCO0lBQ2pCLHlCQUF5QjtJQU16QixpQ0FBaUM7RUFDbkM7QUFFQTs7SUFFRSxXQUFXO0lBQ1gsYUFBYTs7SUFFYixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBSWxCLGFBQWE7SUFHYixtQkFBbUI7SUFHbkIsaUJBQWlCO0lBR2pCLDJCQUEyQjtJQUczQixzQkFBc0I7SUFHdEIsbUJBQW1CO0VBQ3JCO0FBRUYsOENBQThDO0FBTzlDOztpREFFaUQ7QUFDakQ7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCOztFQUVsQixpREFBaUQ7RUFDakQsa0VBQWtFOztFQUVsRSxRQUFRO0VBQ1IsU0FBUzs7RUFFVCxnQkFBZ0I7O0FBRWxCO0FBQ0E7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGlEQUFpRDtFQUNqRCxrRUFBa0U7RUFDbEUsUUFBUTtFQUNSLFNBQVM7RUFDVCxnQkFBZ0I7QUFDbEI7QUFFQTtFQUNFLGNBQWM7RUFDZCxrQkFBa0I7O0VBRWxCLGlEQUFpRDtFQUNqRCxrRUFBa0U7O0VBRWxFLFFBQVE7RUFDUixTQUFTOztFQUVULGdCQUFnQjtBQUNsQjtBQUNBOztFQUVFLHVCQUF1QjtFQUN2QixrQkFBa0I7O0VBRWxCLFdBQVc7RUFDWCxZQUFZOztBQUVkO0FBQ0E7RUFDRSxjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLFlBQVk7QUFDZDtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixXQUFXO0VBQ1gsV0FBVztFQUNYLFlBQVk7RUFDWiwyQkFBbUI7RUFBbkIsd0JBQW1CO0VBQW5CLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysc0JBQXNCO0VBQ3RCLG9CQUFvQjs7QUFFdEI7QUFDQTtNQUNNLGNBQWM7TUFDZCxXQUFXO01BQ1gsWUFBWTtJQUlkLGFBQWE7SUFHYixtQkFBbUI7SUFHbkIsaUJBQWlCO0lBR2pCLHVCQUF1QjtJQUd2QixxQkFBcUI7SUFHckIsbUJBQW1COztBQUV2QjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixXQUFXO0VBQ1gsWUFBWTs7RUFFWixzQkFBc0I7RUFDdEIsbUJBQW1CO0lBSWpCLGFBQWE7SUFHYixtQkFBbUI7SUFHbkIsaUJBQWlCO0lBR2pCLHlCQUF5QjtJQUd6QixxQkFBcUI7SUFHckIsbUJBQW1COztBQUV2QjtBQUNBO0VBQ0UsY0FBYztBQUNoQjtBQUNBO0VBQ0UsYUFBYTtBQUNmO0FBQ0E7RUFDRSxhQUFhO0FBQ2Y7QUFDQTtFQUNFLGNBQWM7QUFDaEI7QUFFQTtFQUNFOzs7Ozs7O0dBT0M7QUFDSDtBQUNBO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsV0FBVztFQUNYLFlBQVk7QUFDZDtBQUNBO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QscUJBQXFCO0VBQ3JCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsUUFBUTtFQUNSLFNBQVM7RUFDVDtBQUNGO0FBRUE7RUFDRSxjQUFjO0VBQ2QsV0FBVztFQUlYLGFBQWE7RUFHYixtQkFBbUI7RUFHbkIsaUJBQWlCO0VBR2pCLDhCQUE4QjtFQUc5QixxQkFBcUI7RUFHckIsbUJBQW1COztFQUVuQixzQkFBc0I7SUFDcEIsWUFBWTs7QUFFaEI7QUFJQTtFQUNFLGFBQWE7RUFDYixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxZQUFZO0VBQ1osWUFBWTtFQUNaLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsWUFBWTtFQUNaO0FBQ0Y7QUFDQTtFQUNFLGFBQWEsRUFBRSxXQUFXO0VBQzFCO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvcHJvZHVjdC1sb2dvLWN1c3RvbWl6YXRpb24vcHJvZHVjdC1sb2dvLWN1c3RvbWl6YXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gLS0tLSBQcm9kdWN0IExvZ28gQ3VzdG9taXphdGlvbiBDb21wb25lbnRzIC0tLS1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuIEBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9T3BlbitTYW5zOjMwMCw0MDAsNzAwJmRpc3BsYXk9c3dhcCcpO1xyXG5cclxuIGRpdi52aWV3LnByb2R1Y3QtbG9nby1jdXN0b21pemF0aW9ue1xyXG5cclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcblxyXG4gICAgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG5cclxuICAgIC8qICFJTVBPUlRBTlQgRklYRUQgVkFMVUUgKi9cclxuICAgIC0tcGxjLXZpZXctd2lkdGg6IDEwMHZ3O1xyXG4gICAgLS1wbGMtdmlldy1oZWlnaHQ6IDgwMHB4O1xyXG5cclxuICAgIC0tcGxjLXZpZXctc2lkZWJhci13aWR0aDogMzUwcHg7XHJcbiAgICAtLXBsYy12aWV3LXNpZGViYXItaGVpZ2h0OiB2YXIoLS1wbGMtdmlldy1oZWlnaHQpO1xyXG5cclxuICAgIC0tcGxjLXZpZXctY29udGVudC1hbmQtcGljdHVyZXMtd2lkdGg6IGNhbGMoIHZhcigtLXBsYy12aWV3LXdpZHRoKSAtIHZhcigtLXBsYy12aWV3LXNpZGViYXItd2lkdGgpICk7XHJcbiAgICAtLXBsYy12aWV3LWNvbnRlbnQtYW5kLXBpY3R1cmVzLWhlaWdodDogdmFyKC0tcGxjLXZpZXctaGVpZ2h0KTtcclxuXHJcbiAgICAtLXBsYy12aWV3LXBpY3R1cmVzLXdpZHRoOiB2YXIoLS1wbGMtdmlldy1jb250ZW50LWFuZC1waWN0dXJlcy13aWR0aCk7XHJcbiAgICAtLXBsYy12aWV3LXBpY3R1cmVzLWhlaWdodDogMTg1cHg7XHJcblxyXG4gICAgLS1wbGMtLXZpZXctY29udGVudC1wcmV2aWV3LXdpZHRoOiB2YXIoLS1wbGMtdmlldy1jb250ZW50LWFuZC1waWN0dXJlcy13aWR0aCk7XHJcbiAgICAtLXBsYy0tdmlldy1jb250ZW50LXByZXZpZXctaGVpZ2h0OiBjYWxjKCB2YXIoLS1wbGMtdmlldy1jb250ZW50LWFuZC1waWN0dXJlcy1oZWlnaHQpIC0gdmFyKC0tcGxjLXZpZXctcGljdHVyZXMtaGVpZ2h0KSk7XHJcblxyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1jb250ZW50OiBzdHJldGNoO1xyXG4gICAgLW1zLWZsZXgtbGluZS1wYWNrOiBzdHJldGNoO1xyXG4gICAgYWxpZ24tY29udGVudDogc3RyZXRjaDtcclxuICAgIC13ZWJraXQtYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAtbXMtZmxleC1hbGlnbjogc3RhcnQ7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuXHJcbiAgICB3aWR0aDogdmFyKC0tcGxjLXZpZXctd2lkdGgpO1xyXG4gICAgaGVpZ2h0OiB2YXIoLS1wbGMtdmlldy1oZWlnaHQpO1xyXG5cclxuICAgIHBhZGRpbmc6IDBweDtcclxuICAgIG1hcmdpbjogMHB4O1xyXG5cclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNiN2I3Yjc7XHJcbiAgICBib3JkZXItbGVmdDogMHB4O1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAwcHg7XHJcblxyXG4gfVxyXG4gLyogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0gKi9cclxuXHJcblxyXG5cclxuXHJcbi8qIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gLS0tLSBDb2x1bW4gOjogUHJldmlldyBBbmQgUGljdHVyZXMgICAgICAgIC0tLS1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuICBkaXYudmlldy1jb2x1bW4ucHJldmlldy1hbmQtcGljdHVyZXN7XHJcblxyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgICB3aWR0aDogdmFyKC0tcGxjLXZpZXctY29udGVudC1hbmQtcGljdHVyZXMtd2lkdGgpO1xyXG4gICAgaGVpZ2h0OiB2YXIoLS1wbGMtdmlldy1jb250ZW50LWFuZC1waWN0dXJlcy1oZWlnaHQpO1xyXG5cclxuICAgIHBhZGRpbmc6IDBweDtcclxuICAgIG1hcmdpbjogMHB4O1xyXG5cclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWY5Zjk7XHJcblxyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgLW1zLWZsZXgtbGluZS1wYWNrOiBqdXN0aWZ5O1xyXG4gICAgYWxpZ24tY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICB9XHJcblxyXG4gIGRpdi52aWV3LXJvdy5wcmV2aWV3LCBkaXYudmlldy1yb3cucHJldmlldyBzZWN0aW9ue1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogY2FsYyggdmFyKC0tcGxjLXZpZXctY29udGVudC1hbmQtcGljdHVyZXMtaGVpZ2h0KSAtIDE4NXB4KTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcblxyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAtbXMtZmxleC1saW5lLXBhY2s6IGNlbnRlcjtcclxuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxuICB9XHJcbiAgZGl2LnZpZXctcm93LnByZXZpZXd7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDBweDtcclxuICAgIGxlZnQ6IDBweDtcclxuICB9XHJcbiAgZGl2LnZpZXctcm93LnByZXZpZXcgc2VjdGlvbntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDowcHg7XHJcbiAgICBsZWZ0OiAwcHg7XHJcbiAgfVxyXG4gIGRpdi52aWV3LXJvdy5wcmV2aWV3IHNlY3Rpb24jSURQcmV2aWV3Wm9uZXtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgZmlsdGVyOiBibHVyKDBweCk7XHJcbiAgICAtd2Via2l0LWZpbHRlcjogYmx1cigwcHgpO1xyXG5cclxuICB9XHJcbiAgZGl2LnZpZXctcm93LnByZXZpZXcgc2VjdGlvbiNJRFByZXZpZXdMb2dvc3tcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgZmlsdGVyOiBibHVyKDBweCk7XHJcbiAgICAtd2Via2l0LWZpbHRlcjogYmx1cigwcHgpO1xyXG4gIH1cclxuICBkaXYudmlldy1yb3cucHJldmlldyBzZWN0aW9uI0lEUHJldmlld1pvbmUuZHJhZ2luZ3tcclxuICAgIHotaW5kZXg6IDEwMDtcclxuICAgIG9wYWNpdHk6IDAuNTtcclxuICAgIGZpbHRlcjogYmx1cigwcHgpO1xyXG4gICAgLXdlYmtpdC1maWx0ZXI6IGJsdXIoMHB4KTtcclxuXHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuICAgIC1tb3otdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1zLXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuICAgIC1vLXRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAyMDBtcyBlYXNlLWluLW91dDtcclxuXHJcbiAgfVxyXG4gIGRpdi52aWV3LXJvdy5wcmV2aWV3IHNlY3Rpb24jSURQcmV2aWV3TG9nb3MuZHJhZ2luZ3tcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBvcGFjaXR5OiAxO1xyXG4gICAgZmlsdGVyOiBibHVyKDhweCk7XHJcbiAgICAtd2Via2l0LWZpbHRlcjogYmx1cig4cHgpO1xyXG5cclxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0O1xyXG4gICAgLW1vei10cmFuc2l0aW9uOiBhbGwgMjAwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAtbXMtdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0O1xyXG4gICAgLW8tdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0O1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIDIwMG1zIGVhc2UtaW4tb3V0O1xyXG4gIH1cclxuXHJcbiAgZGl2LnZpZXctcm93LnBpY3R1cmVze1xyXG5cclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxODVweDtcclxuXHJcbiAgICBvdmVyZmxvdy15OiBoaWRkZW47XHJcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XHJcblxyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICAtbXMtZmxleC1wYWNrOiBzdGFydDtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIC13ZWJraXQtYWxpZ24tY29udGVudDogc3RyZXRjaDtcclxuICAgIC1tcy1mbGV4LWxpbmUtcGFjazogc3RyZXRjaDtcclxuICAgIGFsaWduLWNvbnRlbnQ6IHN0cmV0Y2g7XHJcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB9XHJcblxyXG4vKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSAqL1xyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG4vKiAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAtLS0tIERyYWdBbmREcm9wIDo6IFByZXZpZXcgICAgICAgICAgICAgICAgIC0tLS1cclxuIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cclxuZGl2LnByZXZpZXctLXdyYXBwZXJ7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG5cclxuICB3aWR0aDogdmFyKC0tcGxjLXZpZXctY29udGVudC1hbmQtcGljdHVyZXMtd2lkdGgpO1xyXG4gIGhlaWdodDogY2FsYyggdmFyKC0tcGxjLXZpZXctY29udGVudC1hbmQtcGljdHVyZXMtaGVpZ2h0KSAtIDE4NXB4KTtcclxuXHJcbiAgdG9wOiAwcHg7XHJcbiAgbGVmdDogMHB4O1xyXG5cclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG5cclxufVxyXG5kaXYucHJldmlldy0td3JhcHBlci0tY29udGFpbmVye1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB3aWR0aDogdmFyKC0tcGxjLXZpZXctY29udGVudC1hbmQtcGljdHVyZXMtd2lkdGgpO1xyXG4gIGhlaWdodDogY2FsYyggdmFyKC0tcGxjLXZpZXctY29udGVudC1hbmQtcGljdHVyZXMtaGVpZ2h0KSAtIDE4NXB4KTtcclxuICB0b3A6IDBweDtcclxuICBsZWZ0OiAwcHg7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuZGl2LnByZXZpZXctLXByb2R1Y3QsIGRpdi5wcmV2aWV3LS1kcmFnLWRyb3AtbG9nb3N7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG5cclxuICB3aWR0aDogdmFyKC0tcGxjLXZpZXctY29udGVudC1hbmQtcGljdHVyZXMtd2lkdGgpO1xyXG4gIGhlaWdodDogY2FsYyggdmFyKC0tcGxjLXZpZXctY29udGVudC1hbmQtcGljdHVyZXMtaGVpZ2h0KSAtIDE4NXB4KTtcclxuXHJcbiAgdG9wOiAwcHg7XHJcbiAgbGVmdDogMHB4O1xyXG5cclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbmRpdi5wcmV2aWV3LS1wcm9kdWN0e1xyXG5cclxuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcblxyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIGhlaWdodDogYXV0bztcclxuXHJcbn1cclxuZGl2LnByZXZpZXctLXByb2R1Y3QgaW1ne1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiBpbml0aWFsO1xyXG4gIHdpZHRoOiA1MDBweDtcclxufVxyXG5kaXYucHJldmlldy0tZHJvcC1jYW57XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogMzVweDtcclxuICByaWdodDogNDBweDtcclxuICB3aWR0aDogOTBweDtcclxuICBwYWRkaW5nOiAyZW07XHJcbiAgaGVpZ2h0OiBmaXQtY29udGVudDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuXHJcbn1cclxuZGl2LnByZXZpZXctLWRyb3Atem9uZXtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAtbXMtZmxleC1saW5lLXBhY2s6IGNlbnRlcjtcclxuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC13ZWJraXQtYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cclxufVxyXG5kaXYucHJldmlldy0tem9vbXtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYm90dG9tOjBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDM1cHg7XHJcblxyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcGFkZGluZy1yaWdodDogMjBweDtcclxuXHJcbiAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1mbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgLW1zLWZsZXgtcGFjazogZW5kO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIC13ZWJraXQtYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gICAgLW1zLWZsZXgtbGluZS1wYWNrOiBjZW50ZXI7XHJcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAtd2Via2l0LWFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbn1cclxuZGl2LnByZXZpZXctLWRyb3AtY2FuIGltZy5jbG9zZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuZGl2LnByZXZpZXctLWRyb3AtY2FuIGltZy5vcGVuIHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcbmRpdi5wcmV2aWV3LS1kcm9wLWNhbjpob3ZlciBpbWcuY2xvc2Uge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuZGl2LnByZXZpZXctLWRyb3AtY2FuOmhvdmVyIGltZy5vcGVuIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuZGl2LnByZXZpZXctLWRyYWctZHJvcC1sb2dvc3tcclxuICAvKlxyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAwcHg7XHJcbiAgaGVpZ2h0OiAwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMHB4O1xyXG4gIGxlZnQ6MHB4O1xyXG4gICovXHJcbn1cclxuZGl2LnByZXZpZXctLWRyYWctZHJvcC1sb2dvcyBpbWcuY2FudmFzLS1sb2dve1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB6LWluZGV4OiAxO1xyXG4gIHRvcDogLTEwMHZoO1xyXG4gIGxlZnQ6IC0xMDB2dztcclxufVxyXG5kaXYucHJldmlldy0tZHJhZy1kcm9wLWxvZ29zIGRpdi5jYW52YXMtLXNxdWFyZXtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgei1pbmRleDogMTAwMDA7XHJcbiAgYm9yZGVyOjFweCBkYXNoZWQgcmVkO1xyXG4gIG1pbi13aWR0aDogMTBweDtcclxuICBtaW4taGVpZ2h0OiAxMHB4O1xyXG4gIHRvcDogNTAlO1xyXG4gIGxlZnQ6IDUwJTtcclxuICBwb2ludGVyLWV2ZW50czogbm9uZVxyXG59XHJcblxyXG5kaXYuYnV0dG9ucy13cmFwcGVye1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG5cclxuICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgLW1zLWZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAtd2Via2l0LWZsZXgtd3JhcDogbm93cmFwO1xyXG4gIC1tcy1mbGV4LXdyYXA6IG5vd3JhcDtcclxuICBmbGV4LXdyYXA6IG5vd3JhcDtcclxuICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAtbXMtZmxleC1wYWNrOiBqdXN0aWZ5O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAtd2Via2l0LWFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICAtbXMtZmxleC1saW5lLXBhY2s6IGNlbnRlcjtcclxuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIHBhZGRpbmc6IDFlbTtcclxuXHJcbn1cclxuXHJcblxyXG5cclxuZGl2LnByZXZpZXctLXRyYXNoLWNhbntcclxuICBkaXNwbGF5OmJsb2NrO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDUwcHg7XHJcbiAgYm90dG9tOiA1MHB4O1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBvcGFjaXR5OiAwLjU7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBwYWRkaW5nOiAxZW07XHJcbiAgcG9pbnRlci1ldmVudHM6IG5vbmVcclxufVxyXG5kaXYucHJldmlldy0tdHJhc2gtY2FuIGltZ3tcclxuICBkaXNwbGF5OmJsb2NrOyB3aWR0aDogMTAwJTtcclxuICBwb2ludGVyLWV2ZW50czogbm9uZVxyXG59XHJcbiJdfQ== */";
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/product-logo-customization.component.ts":
  /*!*******************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/product-logo-customization.component.ts ***!
    \*******************************************************************************************/

  /*! exports provided: ProductLogoCustomizationComponent */

  /***/
  function srcAppSharedProductLogoCustomizationProductLogoCustomizationComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductLogoCustomizationComponent", function () {
      return ProductLogoCustomizationComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_common_loading_loading_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./services/common/loading/loading.service */
    "./src/app/shared/product-logo-customization/services/common/loading/loading.service.ts");
    /* harmony import */


    var _services_common_snapshot_snapshot_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./services/common/snapshot/snapshot.service */
    "./src/app/shared/product-logo-customization/services/common/snapshot/snapshot.service.ts");
    /* harmony import */


    var _services_common_layouts_layouts_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./services/common/layouts/layouts.service */
    "./src/app/shared/product-logo-customization/services/common/layouts/layouts.service.ts");
    /* harmony import */


    var _services_libs_handlers_services_moveable_js_moveable_handler_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./services/libs-handlers-services/moveable-js/moveable-handler.service */
    "./src/app/shared/product-logo-customization/services/libs-handlers-services/moveable-js/moveable-handler.service.ts");
    /* harmony import */


    var _services_common_pictures_pictures_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./services/common/pictures/pictures.service */
    "./src/app/shared/product-logo-customization/services/common/pictures/pictures.service.ts");
    /* harmony import */


    var _services_common_positions_positions_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./services/common/positions/positions.service */
    "./src/app/shared/product-logo-customization/services/common/positions/positions.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _services_libs_handlers_services_fabric_js_fabric_handler_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./services/libs-handlers-services/fabric-js/fabric-handler.service */
    "./src/app/shared/product-logo-customization/services/libs-handlers-services/fabric-js/fabric-handler.service.ts");

    var ProductLogoCustomizationComponent =
    /*#__PURE__*/
    function () {
      function ProductLogoCustomizationComponent(loaderService, snaphotService, moveableService, fabricService, layoutService, picturesService, positionsService) {
        _classCallCheck(this, ProductLogoCustomizationComponent);

        this.loaderService = loaderService;
        this.snaphotService = snaphotService;
        this.moveableService = moveableService;
        this.fabricService = fabricService;
        this.layoutService = layoutService;
        this.picturesService = picturesService;
        this.positionsService = positionsService;
        this.picturesList = [];
        this.positionsList = [];
        this.positionActive = null;
        this.colorActive = null;
        this.PICTURES = {
          toAdd: [],
          toRemove: []
        };
        this.UX = {
          isResizingTimmer: null
        };
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.imageRemove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.imageAdd = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }

      _createClass(ProductLogoCustomizationComponent, [{
        key: "onResize",
        value: function onResize(event) {
          var _this4 = this;

          clearTimeout(this.UX.isResizingTimmer);
          this.UX.isResizingTimmer = setTimeout(function () {
            var w = event.target.innerWidth;
            var h = event.target.innerHeight;
            var view = document.getElementById("view-id");
            view.style.setProperty("--plc-view-width", w + "px");
            view.style.setProperty("--plc-view-height", h + "px");

            _this4.layoutService.initLayout(_this4.positionActive).then(function (initsLayouts) {
              var container = _this4.layoutService.getLayoutContainer();

              var position = _this4.positionsService.getActive();

              var product = position.colors[0].imageSrc;

              var logos = _this4.picturesService.getLogos().filter(function (logo) {
                return logo.position.name === position.name;
              });

              console.log({
                container: container,
                position: position,
                product: product,
                logos: logos
              });
            });
          }, 750);
        } // ------------------------------------------------------ //
        // -- Angular Hooks Methods ----------------------------- //
        // ------------------------------------------------------ //

      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this5 = this,
              _ref;

          this.$POSITION = this.positionsService.syncActive();
          this.$POSITIONS = this.positionsService.syncList();
          this.$IS_CLIPPING = this.moveableService.syncClipping();
          this.$PICTURES = this.picturesService.syncPictures().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["mergeMap"])(function (pictures) {
            return _this5.$POSITION.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["map"])(function (position) {
              return pictures.filter(function (picture) {
                return picture.position.name === position.name;
              });
            }));
          }));
          this.$POSITION.subscribe(function ($position) {
            _this5.layoutService.initLayout(_this5.positionActive).then(function (initsLayouts) {
              var container = _this5.layoutService.getLayoutContainer();

              var position = _this5.positionsService.getActive();

              var product = position.colors[0].imageSrc;

              var logos = _this5.picturesService.getLogos().filter(function (logo) {
                return logo.position.name === position.name;
              });

              _this5.fabricService.init(container, position, product, logos).then(function (response) {}).catch(function (error) {
                return console.error(error);
              });
            });
          });
          this.positionsList = this.positions;

          var initPictures = (_ref = []).concat.apply(_ref, _toConsumableArray(this.positionsList.map(function (position) {
            return position.logos.map(function (logo) {
              return Object(_services_common_pictures_pictures_service__WEBPACK_IMPORTED_MODULE_6__["toIPicture"])(logo, position.name, position.name);
            });
          })));

          this.picturesService.setList(initPictures);
          this.positionsService.setList(this.positions);
          this.positionsService.setActive(this.positions[0]); //this.onPositionActive(this.positions[0]);
          //this.onPositionColorChange(this.positions[0], this.positions[0].colors[0]);
        } // ------------------------------------------------------ //
        // ------------------------------------------------------ //
        // -- Eevents Handlers Methods  ------------------------- //
        // ------------------------------------------------------ //

      }, {
        key: "onPositionEnable",
        value: function onPositionEnable(position) {//this.positionsService.setEnablePosition(position);
        }
      }, {
        key: "onPositionActive",
        value: function onPositionActive(positionActive) {
          this.positionsService.setActive(positionActive);
        }
      }, {
        key: "onPositionColorChange",
        value: function onPositionColorChange(position, color) {} //this.positionsService.setColorActive(position, color);
        // ------------------------------------------------------ //
        // ------------------------------------------------------ //
        // -- UI/UX Methods ------------------------------------- //
        // ------------------------------------------------------ //

      }, {
        key: "trackByIds",
        value: function trackByIds(index, item) {
          return item.id;
        }
      }, {
        key: "initLayout",
        value: function initLayout() {
          this.layoutService.initLayout(this.positionActive).then(function (response) {
            console.log(" initLayout :: " + JSON.stringify(response));
          }).catch(function (error) {
            console.error(error);
          });
        } // ------------------------------------------------------ //
        // ------------------------------------------------------ //
        // -- Add/Remove Pictures-------------------------------- //
        // ------------------------------------------------------ //

      }, {
        key: "addPicture",
        value: function addPicture($picture) {
          var currentLocation = this.positionsService.getActive();
          this.picturesService.add($picture, currentLocation.name).then(function (response) {
            return console.log('PictureSaved');
          }).catch(function (error) {
            return console.error(error);
          });
          this.PICTURES.toAdd.push($picture);
          this.PICTURES.toAdd = Array.from(new Set(this.PICTURES.toAdd));
        }
      }, {
        key: "removePicture",
        value: function removePicture($picture) {
          this.picturesService.remove($picture).then(function (response) {
            return console.log('PictureRemoved');
          }).catch(function (error) {
            return console.error(error);
          });
          this.PICTURES.toRemove.push($picture);
          this.PICTURES.toRemove = Array.from(new Set(this.PICTURES.toRemove));
        } // ------------------------------------------------------ //
        // ------------------------------------------------------ //
        // -- Drag,Drop,Resize,Rotate Logos on Preview ---------- //
        // ------------------------------------------------------ //

      }, {
        key: "onDrop",
        value: function onDrop($event) {
          var picture = $event.data;
          this.fabricService.addToBoard(picture);
        }
      }, {
        key: "onDropRemove",
        value: function onDropRemove($event) {
          var logo = $event.data;
          this.onDeleteLogo(logo);
        }
      }, {
        key: "onDeleteLogo",
        value: function onDeleteLogo(logo) {
          this.picturesService.removeFromBoard(logo).then(function (response) {
            return console.log('LOGO REMOVED FROM BOARD');
          }).catch(function (error) {
            return console.error(error);
          });
        }
      }, {
        key: "onChangeLogo",
        value: function onChangeLogo($event, $logo) {
          var stateRelative = $event;
          var layoutProduct = this.layoutService.getLayoutProduct();
          var stateTop = stateRelative.position.top - 0.5 * stateRelative.w0 * (stateRelative.size.h - 1) - layoutProduct.position.top;
          var stateLeft = stateRelative.position.left - 0.5 * stateRelative.w0 * (stateRelative.size.w - 1) - layoutProduct.position.left;
          var stateAbsolute = {
            position: {
              top: stateTop,
              left: stateLeft
            },
            size: {
              w: stateRelative.w0 * stateRelative.size.w,
              h: stateRelative.w0 * stateRelative.size.h
            },
            rotation: stateRelative.rotation
          };
          var event = {
            positionId: this.positionActive.name,
            imagesToUpload: this.PICTURES.toAdd,
            imagesIdsToDelete: this.PICTURES.toRemove,
            imageId: this.positionActive.logos[0],
            offsetX: stateAbsolute.position.left,
            offsetY: stateAbsolute.position.top,
            scaleX: stateAbsolute.size.w,
            scaleY: stateAbsolute.size.h,
            rotation: stateAbsolute.rotation
          };
          this.change.emit(event);
        } // ------------------------------------------------------ //
        // ------------------------------------------------------ //
        // -- Canvas Methods ------------------------------------- //
        // ------------------------------------------------------ //

      }, {
        key: "previewClip",
        value: function previewClip() {
          var _this6 = this;

          this.fabricService.setClipAreaLayout(true).then(function (response) {
            return console.log("");
          });
          setTimeout(function () {
            _this6.fabricService.setClipAreaLayout(false).then(function (response) {
              return console.log("");
            });
          }, 3500);
        }
      }, {
        key: "saveClip",
        value: function saveClip() {
          this.snaphotService.takeSnapshot().then(function (response) {
            //
            var img = new Image();
            img.setAttribute('crossOrigin', 'anonymous');
            img.src = response;
            document.body.appendChild(img);
          }).catch(function (error) {
            console.error("");
            console.error("ProductLogoCustomization @saveClip() ");
            console.error(error);
            console.error("");
          });
        }
      }]);

      return ProductLogoCustomizationComponent;
    }();

    ProductLogoCustomizationComponent.ctorParameters = function () {
      return [{
        type: _services_common_loading_loading_service__WEBPACK_IMPORTED_MODULE_2__["LoadingService"]
      }, {
        type: _services_common_snapshot_snapshot_service__WEBPACK_IMPORTED_MODULE_3__["SnapshotService"]
      }, {
        type: _services_libs_handlers_services_moveable_js_moveable_handler_service__WEBPACK_IMPORTED_MODULE_5__["MoveableHandlerService"]
      }, {
        type: _services_libs_handlers_services_fabric_js_fabric_handler_service__WEBPACK_IMPORTED_MODULE_9__["FabricHandlerService"]
      }, {
        type: _services_common_layouts_layouts_service__WEBPACK_IMPORTED_MODULE_4__["LayoutsService"]
      }, {
        type: _services_common_pictures_pictures_service__WEBPACK_IMPORTED_MODULE_6__["PicturesService"]
      }, {
        type: _services_common_positions_positions_service__WEBPACK_IMPORTED_MODULE_7__["PositionsService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("isPreview")], ProductLogoCustomizationComponent.prototype, "isPreview", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("positions")], ProductLogoCustomizationComponent.prototype, "positions", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], ProductLogoCustomizationComponent.prototype, "change", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], ProductLogoCustomizationComponent.prototype, "imageRemove", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], ProductLogoCustomizationComponent.prototype, "imageAdd", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("window:resize", ["$event"])], ProductLogoCustomizationComponent.prototype, "onResize", null);
    ProductLogoCustomizationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: "app-product-logo-customization",
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./product-logo-customization.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-logo-customization/product-logo-customization.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./product-logo-customization.component.css */
      "./src/app/shared/product-logo-customization/product-logo-customization.component.css")).default]
    })], ProductLogoCustomizationComponent);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/product-logo-customization.module.ts":
  /*!****************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/product-logo-customization.module.ts ***!
    \****************************************************************************************/

  /*! exports provided: ProductLogoCustomizationModule */

  /***/
  function srcAppSharedProductLogoCustomizationProductLogoCustomizationModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductLogoCustomizationModule", function () {
      return ProductLogoCustomizationModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var ngx_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ngx-drag-drop */
    "./node_modules/ngx-drag-drop/fesm2015/ngx-drag-drop.js");
    /* harmony import */


    var ngx_moveable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ngx-moveable */
    "./node_modules/ngx-moveable/fesm2015/ngx-moveable.js");
    /* harmony import */


    var ng_click_outside__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ng-click-outside */
    "./node_modules/ng-click-outside/lib/index.js");
    /* harmony import */


    var ng_click_outside__WEBPACK_IMPORTED_MODULE_5___default =
    /*#__PURE__*/
    __webpack_require__.n(ng_click_outside__WEBPACK_IMPORTED_MODULE_5__);
    /* harmony import */


    var _product_logo_customization_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./product-logo-customization.component */
    "./src/app/shared/product-logo-customization/product-logo-customization.component.ts");
    /* harmony import */


    var _components_plc_preview_plc_preview_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./components/plc-preview/plc-preview.component */
    "./src/app/shared/product-logo-customization/components/plc-preview/plc-preview.component.ts");
    /* harmony import */


    var _components_plc_picture_carousel_plc_picture_carousel_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./components/plc-picture-carousel/plc-picture-carousel.component */
    "./src/app/shared/product-logo-customization/components/plc-picture-carousel/plc-picture-carousel.component.ts");
    /* harmony import */


    var _components_plc_picture_add_plc_picture_add_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./components/plc-picture-add/plc-picture-add.component */
    "./src/app/shared/product-logo-customization/components/plc-picture-add/plc-picture-add.component.ts");
    /* harmony import */


    var _components_plc_picture_thumb_plc_picture_thumb_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./components/plc-picture-thumb/plc-picture-thumb.component */
    "./src/app/shared/product-logo-customization/components/plc-picture-thumb/plc-picture-thumb.component.ts");
    /* harmony import */


    var _components_plc_positions_list_plc_positions_list_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./components/plc-positions-list/plc-positions-list.component */
    "./src/app/shared/product-logo-customization/components/plc-positions-list/plc-positions-list.component.ts");
    /* harmony import */


    var _components_plc_position_item_plc_position_item_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./components/plc-position-item/plc-position-item.component */
    "./src/app/shared/product-logo-customization/components/plc-position-item/plc-position-item.component.ts");
    /* harmony import */


    var _services_settings_settings_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./services/settings/settings.service */
    "./src/app/shared/product-logo-customization/services/settings/settings.service.ts");
    /* harmony import */


    var _services_common_pictures_pictures_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./services/common/pictures/pictures.service */
    "./src/app/shared/product-logo-customization/services/common/pictures/pictures.service.ts");
    /* harmony import */


    var _services_common_positions_positions_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./services/common/positions/positions.service */
    "./src/app/shared/product-logo-customization/services/common/positions/positions.service.ts");
    /* harmony import */


    var _services_common_layouts_layouts_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./services/common/layouts/layouts.service */
    "./src/app/shared/product-logo-customization/services/common/layouts/layouts.service.ts");
    /* harmony import */


    var _services_common_loading_loading_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ./services/common/loading/loading.service */
    "./src/app/shared/product-logo-customization/services/common/loading/loading.service.ts");
    /* harmony import */


    var _services_common_snapshot_snapshot_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ./services/common/snapshot/snapshot.service */
    "./src/app/shared/product-logo-customization/services/common/snapshot/snapshot.service.ts");
    /* harmony import */


    var _services_libs_handlers_services_fabric_js_fabric_handler_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ./services/libs-handlers-services/fabric-js/fabric-handler.service */
    "./src/app/shared/product-logo-customization/services/libs-handlers-services/fabric-js/fabric-handler.service.ts");
    /* harmony import */


    var _services_libs_handlers_services_moveable_js_moveable_handler_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ./services/libs-handlers-services/moveable-js/moveable-handler.service */
    "./src/app/shared/product-logo-customization/services/libs-handlers-services/moveable-js/moveable-handler.service.ts");
    /* harmony import */


    var _components_plc_preview_loader_plc_preview_loader_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! ./components/plc-preview-loader/plc-preview-loader.component */
    "./src/app/shared/product-logo-customization/components/plc-preview-loader/plc-preview-loader.component.ts");

    var COMPONENTS = [_product_logo_customization_component__WEBPACK_IMPORTED_MODULE_6__["ProductLogoCustomizationComponent"], _components_plc_preview_plc_preview_component__WEBPACK_IMPORTED_MODULE_7__["PlcPreviewComponent"], _components_plc_picture_carousel_plc_picture_carousel_component__WEBPACK_IMPORTED_MODULE_8__["PlcPictureCarouselComponent"], _components_plc_picture_add_plc_picture_add_component__WEBPACK_IMPORTED_MODULE_9__["PlcPictureAddComponent"], _components_plc_picture_thumb_plc_picture_thumb_component__WEBPACK_IMPORTED_MODULE_10__["PlcPictureThumbComponent"], _components_plc_positions_list_plc_positions_list_component__WEBPACK_IMPORTED_MODULE_11__["PlcPositionsListComponent"], _components_plc_position_item_plc_position_item_component__WEBPACK_IMPORTED_MODULE_12__["PlcPositionItemComponent"]];
    var SERVICES = [_services_settings_settings_service__WEBPACK_IMPORTED_MODULE_13__["SettingsService"], _services_common_positions_positions_service__WEBPACK_IMPORTED_MODULE_15__["PositionsService"], _services_common_layouts_layouts_service__WEBPACK_IMPORTED_MODULE_16__["LayoutsService"], _services_libs_handlers_services_fabric_js_fabric_handler_service__WEBPACK_IMPORTED_MODULE_19__["FabricHandlerService"], _services_libs_handlers_services_moveable_js_moveable_handler_service__WEBPACK_IMPORTED_MODULE_20__["MoveableHandlerService"], _services_common_loading_loading_service__WEBPACK_IMPORTED_MODULE_17__["LoadingService"], _services_common_snapshot_snapshot_service__WEBPACK_IMPORTED_MODULE_18__["SnapshotService"], _services_common_pictures_pictures_service__WEBPACK_IMPORTED_MODULE_14__["PicturesService"]];

    var ProductLogoCustomizationModule = function ProductLogoCustomizationModule() {
      _classCallCheck(this, ProductLogoCustomizationModule);
    };

    ProductLogoCustomizationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], ngx_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DndModule"], ngx_moveable__WEBPACK_IMPORTED_MODULE_4__["NgxMoveableModule"], ng_click_outside__WEBPACK_IMPORTED_MODULE_5__["ClickOutsideModule"]],
      providers: [].concat(SERVICES),
      declarations: [].concat(COMPONENTS, [_components_plc_preview_loader_plc_preview_loader_component__WEBPACK_IMPORTED_MODULE_21__["PlcPreviewLoaderComponent"]]),
      exports: [].concat(COMPONENTS)
    })], ProductLogoCustomizationModule);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/services/common/layouts/layouts.service.ts":
  /*!**********************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/services/common/layouts/layouts.service.ts ***!
    \**********************************************************************************************/

  /*! exports provided: LayoutsService */

  /***/
  function srcAppSharedProductLogoCustomizationServicesCommonLayoutsLayoutsServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LayoutsService", function () {
      return LayoutsService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _models_ILayout_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../models/ILayout.model */
    "./src/app/shared/product-logo-customization/models/ILayout.model.ts");
    /* harmony import */


    var _loading_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../loading/loading.service */
    "./src/app/shared/product-logo-customization/services/common/loading/loading.service.ts");
    /* harmony import */


    var _libs_handlers_services_moveable_js_moveable_handler_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../libs-handlers-services/moveable-js/moveable-handler.service */
    "./src/app/shared/product-logo-customization/services/libs-handlers-services/moveable-js/moveable-handler.service.ts");

    var LayoutsService =
    /*#__PURE__*/
    function () {
      function LayoutsService(loaderService, moveableService) {
        _classCallCheck(this, LayoutsService);

        this.loaderService = loaderService;
        this.moveableService = moveableService;
        this.zoomLayoutBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({
          sw: 1,
          sh: 1,
          dtop: 0,
          dleft: 0
        });
        this.$zoomLayout = this.zoomLayoutBS.asObservable();
        this.productLayoutBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](Object(_models_ILayout_model__WEBPACK_IMPORTED_MODULE_4__["toILayout"])({}));
        this.$productLayout = this.productLayoutBS.asObservable();
        this.previewLayoutBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](Object(_models_ILayout_model__WEBPACK_IMPORTED_MODULE_4__["toILayout"])({}));
        this.$previewLayout = this.previewLayoutBS.asObservable();
        this.canvasLayoutBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](Object(_models_ILayout_model__WEBPACK_IMPORTED_MODULE_4__["toILayout"])({}));
        this.$canvasLayout = this.canvasLayoutBS.asObservable();
        this.containerLayoutBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](Object(_models_ILayout_model__WEBPACK_IMPORTED_MODULE_4__["toILayout"])({}));
        this.$containerLayout = this.containerLayoutBS.asObservable();
        this.productStyleBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.$productStyle = this.productStyleBS.asObservable();
        this.canvasStyleBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.$canvastStyle = this.canvasStyleBS.asObservable();
        this.wrapperStyleBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.$wrapperStyle = this.wrapperStyleBS.asObservable();
        this.wrapperContainerStyleBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.$wrapperContainerStyle = this.wrapperContainerStyleBS.asObservable();
      }

      _createClass(LayoutsService, [{
        key: "initLayout",
        value: function initLayout(location) {
          var _this7 = this;

          return new Promise(function (resolve, reject) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this7, void 0, void 0,
            /*#__PURE__*/
            regeneratorRuntime.mark(function _callee() {
              var initContainer;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.prev = 0;
                      this.loaderService.show();
                      _context.next = 4;
                      return this.initContainer('preview-id');

                    case 4:
                      initContainer = _context.sent;
                      this.loaderService.hide();
                      resolve({
                        initContainer: initContainer
                      });
                      _context.next = 12;
                      break;

                    case 9:
                      _context.prev = 9;
                      _context.t0 = _context["catch"](0);
                      reject(_context.t0);

                    case 12:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this, [[0, 9]]);
            }));
          });
        }
      }, {
        key: "updateLayout",
        value: function updateLayout() {
          var _this8 = this;

          return new Promise(function (resolve, reject) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this8, void 0, void 0,
            /*#__PURE__*/
            regeneratorRuntime.mark(function _callee2() {
              var container, product, canvas;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      try {
                        container = JSON.parse(JSON.stringify(this.containerLayoutBS.getValue()) + '');
                        product = JSON.parse(JSON.stringify(this.productLayoutBS.getValue()) + '');
                        canvas = JSON.parse(JSON.stringify(this.canvasLayoutBS.getValue()) + '');
                        this.containerLayoutBS.next(container);
                        this.productLayoutBS.next(product);
                        this.canvasLayoutBS.next(canvas);
                        resolve();
                      } catch (error) {
                        reject(error);
                      }

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          });
        }
      }, {
        key: "initContainer",
        value: function initContainer(tagId) {
          var _this9 = this;

          // preview-id
          return new Promise(function (resolve, reject) {
            try {
              var preview = document.getElementById(tagId);
              var previewW = preview.offsetWidth;
              var previewH = preview.offsetHeight;
              var previewX = previewW / 2;
              var previewY = previewH / 2;
              var layout = {
                center: {
                  x: previewX,
                  y: previewY
                },
                position: {
                  top: 0,
                  left: 0
                },
                size: {
                  w: previewW,
                  h: previewH
                }
              };

              _this9.containerLayoutBS.next(layout);

              resolve(layout);
            } catch (error) {
              reject(error);
            }
          });
        }
      }, {
        key: "initProduct",
        value: function initProduct(tagId) {
          var _this10 = this;

          // product-picture-id
          return new Promise(function (resolve, reject) {
            try {
              var preview = _this10.containerLayoutBS.getValue();

              var previewW = preview.size.w;
              var previewH = preview.size.h;
              var img = document.getElementById(tagId);
              var imgW = img.width;
              var imgH = img.height;
              var imgTop = 0.5 * previewH - 0.5 * imgH;
              var imgLeft = 0.5 * previewW - 0.5 * imgW;
              var layout = {
                center: {
                  x: imgLeft + 0.5 * imgW,
                  y: imgTop + 0.5 * imgH
                },
                position: {
                  top: imgTop,
                  left: imgLeft
                },
                size: {
                  w: imgW,
                  h: imgH
                }
              };

              _this10.productLayoutBS.next(layout);

              resolve(layout);
            } catch (error) {
              reject(error);
            }
          });
        }
      }, {
        key: "initCanvas",
        value: function initCanvas(location) {
          var _this11 = this;

          // product-container-id
          return new Promise(function (resolve, reject) {
            try {
              var product = _this11.productLayoutBS.getValue();

              var base_width = 100;
              var base_height = product.size.h * (base_width / product.size.w);
              var scale = product.size.w / base_width;
              var square_top = Math.round(location.center.y - 0.5 * location.size.h);
              var square_left = Math.round(location.center.x - 0.5 * location.size.w);
              var square_width = Math.round(location.size.w);
              var square_height = Math.round(location.size.h);
              var top = square_top * scale + product.position.top;
              var left = square_left * scale + product.position.left;
              var width = square_width * scale;
              var height = square_height * scale;
              var layout = {
                center: {
                  x: left + 0.5 * width,
                  y: top + 0.5 * height
                },
                position: {
                  top: top,
                  left: left
                },
                size: {
                  w: width,
                  h: height
                }
              };

              _this11.canvasLayoutBS.next(layout);

              resolve(layout);
            } catch (error) {
              reject(error);
            }
          });
        }
      }, {
        key: "getContainerStyle",
        value: function getContainerStyle() {
          return this.$productLayout.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (layout) {
            return {
              top: "0px",
              left: "0px"
            };
          }));
        }
      }, {
        key: "getPreviewStyle",
        value: function getPreviewStyle() {
          return this.$containerLayout.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (layout) {
            return {
              top: "".concat(layout.position.top, "px"),
              left: "".concat(layout.position.left, "px"),
              width: "".concat(layout.size.w, "px"),
              height: "".concat(layout.size.h, "px")
            };
          }));
        }
      }, {
        key: "getProductStyle",
        value: function getProductStyle() {
          var _this12 = this;

          return this.$zoomLayout.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function (zoom) {
            return _this12.$productLayout.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (layout) {
              return {
                top: "".concat(Math.round(layout.position.top - 0.5 * (layout.size.h * zoom.sh - layout.size.h)), "px"),
                left: "".concat(Math.round(layout.position.left - 0.5 * (layout.size.w * zoom.sw - layout.size.w)), "px"),
                width: "".concat(Math.round(layout.size.w * zoom.sw), "px"),
                height: "".concat(Math.round(layout.size.h * zoom.sw), "px")
              };
            }));
          }));
        }
      }, {
        key: "getCanvasStyle",
        value: function getCanvasStyle() {
          return this.$canvasLayout.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (layout) {
            return {
              top: "".concat(Math.round(layout.position.top), "px"),
              left: "".concat(Math.round(layout.position.left), "px"),
              width: "".concat(Math.round(layout.size.w), "px"),
              height: "".concat(Math.round(layout.size.h), "px")
            };
          }));
        }
      }, {
        key: "getClipCanvas",
        value: function getClipCanvas() {
          var _this13 = this;

          return this.$canvasLayout.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (layout) {
            if (_this13.moveableService.getClippingState()) {
              return {
                top: "".concat(Math.round(layout.position.top), "px"),
                left: "".concat(Math.round(layout.position.left), "px"),
                width: "".concat(Math.round(layout.size.w), "px"),
                height: "".concat(Math.round(layout.size.h), "px")
              };
            } else {
              return {};
            }
          }));
        }
      }, {
        key: "getPreviewWrapperStyle",
        value: function getPreviewWrapperStyle() {
          var _this14 = this;

          return this.$canvasLayout.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (layout) {
            if (_this14.moveableService.getClippingState()) {
              return {
                top: "".concat(Math.round(layout.position.top), "px"),
                left: "".concat(Math.round(layout.position.left), "px"),
                width: "".concat(Math.round(layout.size.w), "px"),
                height: "".concat(Math.round(layout.size.h), "px"),
                border: '1px solid red'
              };
            } else {
              return {};
            }
          }));
        }
      }, {
        key: "getPreviewWrapperContainerStyle",
        value: function getPreviewWrapperContainerStyle() {
          var _this15 = this;

          return this.$canvasLayout.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (layout) {
            if (_this15.moveableService.getClippingState()) {
              return {
                transform: "translate(-".concat(layout.position.left, "px, -").concat(layout.position.top, "px)")
              };
            } else {
              return {};
            }
          }));
        }
      }, {
        key: "syncLayoutContainer",
        value: function syncLayoutContainer() {
          return this.$productLayout;
        }
      }, {
        key: "syncLayoutPreview",
        value: function syncLayoutPreview() {
          return this.$previewLayout;
        }
      }, {
        key: "syncLayoutProduct",
        value: function syncLayoutProduct() {
          return this.$productLayout;
        }
      }, {
        key: "syncLayoutCanvas",
        value: function syncLayoutCanvas() {
          return this.$canvasLayout;
        }
      }, {
        key: "syncStyleContainer",
        value: function syncStyleContainer() {
          return this.getContainerStyle();
        }
      }, {
        key: "syncStyleProduct",
        value: function syncStyleProduct() {
          return this.getProductStyle();
        }
      }, {
        key: "syncStyleCanvas",
        value: function syncStyleCanvas() {
          return this.getCanvasStyle();
        }
      }, {
        key: "syncStylePreview",
        value: function syncStylePreview() {
          return this.getPreviewStyle();
        }
      }, {
        key: "syncStylePreviewWrapper",
        value: function syncStylePreviewWrapper() {
          return this.getPreviewWrapperStyle();
        }
      }, {
        key: "syncStylePreviewWrapperContainer",
        value: function syncStylePreviewWrapperContainer() {
          return this.getPreviewWrapperContainerStyle();
        }
      }, {
        key: "getLayoutCanvas",
        value: function getLayoutCanvas() {
          return this.canvasLayoutBS.getValue();
        }
      }, {
        key: "getLayoutProduct",
        value: function getLayoutProduct() {
          return this.productLayoutBS.getValue();
        }
      }, {
        key: "getLayoutContainer",
        value: function getLayoutContainer() {
          return this.containerLayoutBS.getValue();
        }
      }, {
        key: "syncScale",
        value: function syncScale() {
          return this.$zoomLayout;
        }
      }, {
        key: "setZoomIn",
        value: function setZoomIn() {
          var zoomLayout = this.getScale();
          zoomLayout.sw += 0.025;
          zoomLayout.sh += 0.025;
          zoomLayout.sw = Math.min(zoomLayout.sw, 2);
          zoomLayout.sh = Math.min(zoomLayout.sh, 2);
          this.zoomLayoutBS.next(zoomLayout);
        }
      }, {
        key: "setZoomOut",
        value: function setZoomOut() {
          var zoomLayout = this.getScale();
          zoomLayout.sw -= 0.025;
          zoomLayout.sh -= 0.025;
          zoomLayout.sw = Math.max(zoomLayout.sw, 0);
          zoomLayout.sh = Math.max(zoomLayout.sh, 0);
          this.zoomLayoutBS.next(zoomLayout);
        }
      }, {
        key: "getScale",
        value: function getScale() {
          return this.zoomLayoutBS.getValue();
        }
      }]);

      return LayoutsService;
    }();

    LayoutsService.ctorParameters = function () {
      return [{
        type: _loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]
      }, {
        type: _libs_handlers_services_moveable_js_moveable_handler_service__WEBPACK_IMPORTED_MODULE_6__["MoveableHandlerService"]
      }];
    };

    LayoutsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], LayoutsService);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/services/common/loading/loading.service.ts":
  /*!**********************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/services/common/loading/loading.service.ts ***!
    \**********************************************************************************************/

  /*! exports provided: LoadingService */

  /***/
  function srcAppSharedProductLogoCustomizationServicesCommonLoadingLoadingServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoadingService", function () {
      return LoadingService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var TRANSITION_TIME = 1000; //ms

    var LoadingService =
    /*#__PURE__*/
    function () {
      function LoadingService() {
        _classCallCheck(this, LoadingService);

        this.previewBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({
          isShow: true,
          transition: 'none'
        });
        this.dataObs = this.previewBS.asObservable();
      }

      _createClass(LoadingService, [{
        key: "hide",
        value: function hide() {
          var _this16 = this;

          this.previewBS.next({
            isShow: true,
            transition: 'toHide'
          });
          setTimeout(function () {
            _this16.previewBS.next({
              isShow: false,
              transition: 'toHide'
            });

            setTimeout(function () {
              _this16.previewBS.next({
                isShow: false,
                transition: 'none'
              });
            }, TRANSITION_TIME / 2);
          }, TRANSITION_TIME / 2);
        }
      }, {
        key: "show",
        value: function show() {
          var _this17 = this;

          this.previewBS.next({
            isShow: false,
            transition: 'toShow'
          });
          setTimeout(function () {
            _this17.previewBS.next({
              isShow: true,
              transition: 'toShow'
            });

            setTimeout(function () {
              _this17.previewBS.next({
                isShow: true,
                transition: 'none'
              });
            }, TRANSITION_TIME / 2);
          }, TRANSITION_TIME / 2);
        }
      }]);

      return LoadingService;
    }();

    LoadingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], LoadingService);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/services/common/pictures/pictures.service.ts":
  /*!************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/services/common/pictures/pictures.service.ts ***!
    \************************************************************************************************/

  /*! exports provided: toIPicture, PicturesService */

  /***/
  function srcAppSharedProductLogoCustomizationServicesCommonPicturesPicturesServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "toIPicture", function () {
      return toIPicture;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PicturesService", function () {
      return PicturesService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    function toIPicture(picture, positionId, positionName) {
      return {
        id: picture.id,
        position: {
          id: positionId,
          name: positionName
        },
        src: picture.src,
        timestamp: picture.timestamp
      };
    }

    var PicturesService =
    /*#__PURE__*/
    function () {
      function PicturesService() {
        _classCallCheck(this, PicturesService);

        this.picturesBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.picturesObs = this.picturesBS.asObservable();
        this.logosBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.logosObs = this.logosBS.asObservable();
      }

      _createClass(PicturesService, [{
        key: "setList",
        value: function setList($pictures) {
          var _this18 = this;

          return new Promise(function (resolve, reject) {
            _this18.picturesBS.next($pictures);
          });
        }
      }, {
        key: "getList",
        value: function getList() {
          return this.picturesBS.getValue();
        }
      }, {
        key: "getLogos",
        value: function getLogos() {
          return this.logosBS.getValue();
        }
      }, {
        key: "add",
        value: function add($picture, positionId) {
          var _this19 = this;

          return new Promise(function (resolve, reject) {
            var pictures = _this19.getList();

            pictures.push(toIPicture($picture, positionId, positionId));

            _this19.picturesBS.next(pictures);

            resolve();
          });
        }
      }, {
        key: "remove",
        value: function remove($picture) {
          var _this20 = this;

          return new Promise(function (resolve, reject) {
            var picturesBeforeRemoved = _this20.getList();

            var picturesAfterRemoved = picturesBeforeRemoved.filter(function (picture) {
              return picture.id !== $picture.id;
            });

            _this20.picturesBS.next(picturesAfterRemoved);

            resolve();
          });
        }
      }, {
        key: "addToBoard",
        value: function addToBoard($logo, position, canvas) {
          var _this21 = this;

          return new Promise(function (resolve, reject) {
            var positionId = position.name;
            var positionName = position.name;

            var logos = _this21.getLogos();

            var logo = toIPicture($logo, positionId, positionName);
            logo.top = canvas.position.top;
            logo.left = canvas.position.left;
            logo.w = canvas.size.w;
            logo.h = canvas.size.h;
            logo.rotation = 0;
            logo.visible = true;
            logos.push(logo);

            _this21.logosBS.next(logos);

            resolve(_this21.getLogos());
          });
        }
      }, {
        key: "removeFromBoard",
        value: function removeFromBoard($logo) {
          var _this22 = this;

          return new Promise(function (resolve, reject) {
            var logosBeforeRemoved = _this22.getLogos();

            var logosAfterRemoved = logosBeforeRemoved.filter(function (logo) {
              return logo.id !== $logo.id;
            });

            _this22.logosBS.next(logosAfterRemoved);

            resolve(_this22.getLogos());
          });
        }
      }, {
        key: "updateLogo",
        value: function updateLogo($logo, $absoluteLayout) {
          var _this23 = this;

          return new Promise(function (resolve, reject) {
            var logos = _this23.getLogos();

            logos.map(function (kLogo) {
              if (kLogo.id === $logo.id) {
                kLogo.w = $absoluteLayout.size.w;
                kLogo.h = $absoluteLayout.size.h;
                kLogo.top = $absoluteLayout.position.top;
                kLogo.left = $absoluteLayout.position.left;
                console.warn('kLogo');
                console.warn(kLogo);
              }
            });

            _this23.logosBS.next(logos);

            resolve();
          });
        }
      }, {
        key: "syncPictures",
        value: function syncPictures() {
          return this.picturesObs;
        }
      }, {
        key: "syncLogos",
        value: function syncLogos() {
          return this.logosObs;
        }
      }]);

      return PicturesService;
    }();

    PicturesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], PicturesService);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/services/common/positions/positions.service.ts":
  /*!**************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/services/common/positions/positions.service.ts ***!
    \**************************************************************************************************/

  /*! exports provided: toIPosition, PositionsService */

  /***/
  function srcAppSharedProductLogoCustomizationServicesCommonPositionsPositionsServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "toIPosition", function () {
      return toIPosition;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PositionsService", function () {
      return PositionsService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    function toIPosition(position) {
      return {
        name: position.name,
        center: {
          x: position.center.x,
          y: position.center.y
        },
        size: {
          w: position.size.w,
          h: position.size.h
        },
        colors: position.colors,
        logos: position.logos
      };
    }

    var PositionsService =
    /*#__PURE__*/
    function () {
      function PositionsService() {
        _classCallCheck(this, PositionsService);

        this.positionsBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.positionsObs = this.positionsBS.asObservable();
        this.activePositionBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.activePositionObs = this.activePositionBS.asObservable();
      }

      _createClass(PositionsService, [{
        key: "getList",
        value: function getList() {
          return this.positionsBS.getValue();
        }
      }, {
        key: "setList",
        value: function setList(positions) {
          this.positionsBS.next(positions);
          return positions;
        }
      }, {
        key: "getActive",
        value: function getActive() {
          return this.activePositionBS.getValue();
        }
      }, {
        key: "setActive",
        value: function setActive(position) {
          this.activePositionBS.next(position);
          return position;
        }
      }, {
        key: "syncList",
        value: function syncList() {
          return this.positionsObs;
        }
      }, {
        key: "syncActive",
        value: function syncActive() {
          return this.activePositionObs;
        }
      }]);

      return PositionsService;
    }();

    PositionsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], PositionsService);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/services/common/snapshot/snapshot.service.ts":
  /*!************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/services/common/snapshot/snapshot.service.ts ***!
    \************************************************************************************************/

  /*! exports provided: SnapshotService */

  /***/
  function srcAppSharedProductLogoCustomizationServicesCommonSnapshotSnapshotServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SnapshotService", function () {
      return SnapshotService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var html2canvas__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! html2canvas */
    "./node_modules/html2canvas/dist/html2canvas.js");
    /* harmony import */


    var html2canvas__WEBPACK_IMPORTED_MODULE_2___default =
    /*#__PURE__*/
    __webpack_require__.n(html2canvas__WEBPACK_IMPORTED_MODULE_2__);
    /* harmony import */


    var dom_to_image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! dom-to-image */
    "./node_modules/dom-to-image/src/dom-to-image.js");
    /* harmony import */


    var dom_to_image__WEBPACK_IMPORTED_MODULE_3___default =
    /*#__PURE__*/
    __webpack_require__.n(dom_to_image__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */


    var _libs_handlers_services_fabric_js_fabric_handler_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../libs-handlers-services/fabric-js/fabric-handler.service */
    "./src/app/shared/product-logo-customization/services/libs-handlers-services/fabric-js/fabric-handler.service.ts");

    var SnapshotService =
    /*#__PURE__*/
    function () {
      function SnapshotService(fabricService) {
        _classCallCheck(this, SnapshotService);

        this.fabricService = fabricService;
      }

      _createClass(SnapshotService, [{
        key: "takeSnapshot",
        value: function takeSnapshot() {
          var _this24 = this;

          return new Promise(function (resolve, reject) {
            var clipArea = _this24.fabricService.getClipArea();

            var zoom = _this24.fabricService.getZoom();

            var canvas = _this24.fabricService.getCanvas();

            var currentViewPort = canvas.viewportTransform;
            console.log({
              currentViewPort: currentViewPort,
              level: zoom.level
            });

            _this24.fabricService.setIsDrawingMode(false);

            _this24.fabricService.resetZoom();

            setTimeout(function () {
              var image = canvas.toDataURL({
                format: 'png',
                multiplier: 1,
                left: clipArea.position.left,
                top: clipArea.position.top,
                width: clipArea.size.w,
                height: clipArea.size.h
              });
              setTimeout(function () {
                _this24.fabricService.resetZoom(currentViewPort, 1);

                resolve(image);
              }, 500);
            }, 500);
          });
        }
      }, {
        key: "html2Canvas",
        value: function html2Canvas() {
          return new Promise(function (resolve, reject) {
            var node = document.querySelector("#cnv-board");
            var snap = document.querySelector("#img-snap");
            var canvas = node.getContext('2d');
            html2canvas__WEBPACK_IMPORTED_MODULE_2___default()(node, {
              allowTaint: true
            }).then(function (canvas) {
              resolve(canvas);
            }).catch(function (error) {
              return reject(error);
            });
          });
        }
      }, {
        key: "dom2image",
        value: function dom2image() {
          return new Promise(function (resolve, reject) {
            var node = document.querySelector("#domToCanvasTarget-id");
            dom_to_image__WEBPACK_IMPORTED_MODULE_3___default.a.toPng(node).then(function (dataUrl) {
              resolve(dataUrl);
            }).catch(function (error) {
              return reject(error);
            });
          });
        }
      }]);

      return SnapshotService;
    }();

    SnapshotService.ctorParameters = function () {
      return [{
        type: _libs_handlers_services_fabric_js_fabric_handler_service__WEBPACK_IMPORTED_MODULE_4__["FabricHandlerService"]
      }];
    };

    SnapshotService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], SnapshotService);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/services/libs-handlers-services/fabric-js/fabric-handler.service.ts":
  /*!***********************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/services/libs-handlers-services/fabric-js/fabric-handler.service.ts ***!
    \***********************************************************************************************************************/

  /*! exports provided: FabricHandlerService */

  /***/
  function srcAppSharedProductLogoCustomizationServicesLibsHandlersServicesFabricJsFabricHandlerServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FabricHandlerService", function () {
      return FabricHandlerService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var fabric__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! fabric */
    "./node_modules/fabric/dist/fabric.js");
    /* harmony import */


    var fabric__WEBPACK_IMPORTED_MODULE_2___default =
    /*#__PURE__*/
    __webpack_require__.n(fabric__WEBPACK_IMPORTED_MODULE_2__);
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var FabricHandlerService =
    /*#__PURE__*/
    function () {
      function FabricHandlerService() {
        _classCallCheck(this, FabricHandlerService);

        this.zoom = {
          level: 1,
          x: 0,
          y: 0
        };
        this.container = {};
        this.position = {};
        this.product = {};
        this.logosBS = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]([]);
        this.logosObs = this.logosBS.asObservable();
      }

      _createClass(FabricHandlerService, [{
        key: "init",
        value: function init(containerLayout, position, productSrc, logos) {
          var _this25 = this;

          return new Promise(function (resolve, reject) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this25, void 0, void 0,
            /*#__PURE__*/
            regeneratorRuntime.mark(function _callee3() {
              var _this26 = this;

              var isCanvasLayoutReady, isFabricJSReady, isSetProductReady, isSetAreaReady, isSetZoomHandlerReady, isClipAreaReady, initLogos;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.prev = 0;
                      _context3.next = 3;
                      return this.setCanvasLayout(containerLayout);

                    case 3:
                      isCanvasLayoutReady = _context3.sent;
                      _context3.next = 6;
                      return this.setFabricCanvas();

                    case 6:
                      isFabricJSReady = _context3.sent;
                      _context3.next = 9;
                      return this.setProductLayout(containerLayout, productSrc);

                    case 9:
                      isSetProductReady = _context3.sent;
                      _context3.next = 12;
                      return this.setAreaLayout(position);

                    case 12:
                      isSetAreaReady = _context3.sent;
                      _context3.next = 15;
                      return this.setZoomHandler();

                    case 15:
                      isSetZoomHandlerReady = _context3.sent;
                      _context3.next = 18;
                      return this.setClipAreaLayout(false);

                    case 18:
                      isClipAreaReady = _context3.sent;
                      initLogos = this.logosBS.getValue().filter(function (logo) {
                        return logo.picture.position.name === position.name;
                      });
                      initLogos.forEach(function (logo) {
                        _this26.addToBoard(logo.picture, false);
                      });
                      resolve();
                      _context3.next = 27;
                      break;

                    case 24:
                      _context3.prev = 24;
                      _context3.t0 = _context3["catch"](0);
                      console.error({
                        name: "ERROR INIT FABRIC",
                        error: _context3.t0
                      });

                    case 27:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this, [[0, 24]]);
            }));
          });
        }
      }, {
        key: "setFabricCanvas",
        value: function setFabricCanvas() {
          var _this27 = this;

          return new Promise(function (resolve, reject) {
            var self = _this27;

            if (self.canvas) {
              self.canvas.clear();

              _this27.resetZoom();
            } else {
              self.canvas = new fabric__WEBPACK_IMPORTED_MODULE_2__["fabric"].Canvas("cnv-board");
              self.canvas.on("object:moved", function ($event) {
                var target = $event.target;
                var logoId = target.logoId;
                var logoTop = target.top;
                var logoLeft = target.left;
                var logoWith = target.width * target.scaleX;
                var logoHeight = target.height * target.scaleY;
                self.updateLogo(logoId, {
                  top: logoTop,
                  left: logoLeft,
                  width: logoWith,
                  height: logoHeight
                });
              });
              self.canvas.on("object:scaled", function ($event) {
                var target = $event.target;
                var logoId = target.logoId;
                var logoTop = target.top;
                var logoLeft = target.left;
                var logoWith = target.width * target.scaleX;
                var logoHeight = target.height * target.scaleY;
                self.updateLogo(logoId, {
                  top: logoTop,
                  left: logoLeft,
                  width: logoWith,
                  height: logoHeight
                });
              });
              self.canvas.on("object:rotated", function ($event) {
                var target = $event.target;
                var logoId = target.logoId;
                var logoTop = target.top;
                var logoLeft = target.left;
                var logoWith = target.width * target.scaleX;
                var logoHeight = target.height * target.scaleY;
                self.updateLogo(logoId, {
                  top: logoTop,
                  left: logoLeft,
                  width: logoWith,
                  height: logoHeight
                });
              });
            } //this.logosBS.next([])


            resolve();
          });
        }
      }, {
        key: "setCanvasLayout",
        value: function setCanvasLayout($containerLayout) {
          var _this28 = this;

          return new Promise(function (resolve, reject) {
            var ctx = document.getElementById("cnv-board").getContext("2d");
            var w = $containerLayout.size.w;
            var h = $containerLayout.size.h;
            ctx.canvas.width = w;
            ctx.canvas.height = h;
            _this28.container.position = {
              top: 0,
              left: 0
            };
            _this28.container.size = {
              w: w,
              h: h
            };
            _this28.zoom.x = 0.5 * _this28.container.size.w;
            _this28.zoom.y = 0.5 * _this28.container.size.h;
            resolve();
          });
        }
      }, {
        key: "setZoomHandler",
        value: function setZoomHandler() {
          var _this29 = this;

          return new Promise(function (resolve, reject) {
            var self = _this29; //self.canvas.selection = false;
            //self.canvas.perPixelTargetFind = true;

            self.canvas.on("mouse:drag", function (opt) {
              console.warn("touch:drag");
              console.warn(opt);
            });
            self.canvas.on("mouse:wheel", function (opt) {
              var delta = -opt.e.deltaY / 10;
              var zoom = self.canvas.getZoom();
              zoom = zoom + delta / 100;
              if (zoom > 10) zoom = 10;
              if (zoom < 0.25) zoom = 0.25; //self.canvas.setZoom(zoom);

              self.zoom.x = opt.pointer.x;
              self.zoom.y = opt.pointer.y;
              self.zoom.level = zoom;
              self.canvas.zoomToPoint(new fabric__WEBPACK_IMPORTED_MODULE_2__["fabric"].Point(opt.pointer.x, opt.pointer.y), zoom);
              opt.e.preventDefault();
              opt.e.stopPropagation();
            });
            self.canvas.on("mouse:down", function (opt) {
              var evt = opt.e;
              var isThereAnActiveObject = self.canvas.getActiveObject();

              if (evt.altKey === true) {
                this.isDragging = true;
                this.selection = false;
                this.lastPosX = evt.clientX;
                this.lastPosY = evt.clientY;
              }

              if (this.isAllowCanvasDragging) {
                this.isDragging = true;
                this.selection = false;
                this.lastPosX = evt.clientX;
                this.lastPosY = evt.clientY;
              }
            });
            self.canvas.on("mouse:move", function (opt) {
              if (this.isDragging) {
                var e = opt.e;
                this.viewportTransform[4] += e.clientX - this.lastPosX;
                this.viewportTransform[5] += e.clientY - this.lastPosY;
                this.requestRenderAll();
                this.lastPosX = e.clientX;
                this.lastPosY = e.clientY;
              }

              if (this.isZooming) {
                var delta = Math.sign(opt.pointer.y - this.lastPosY) * Math.sqrt(Math.pow(opt.pointer.x - this.lastPosX, 2) + Math.pow(opt.pointer.y - this.lastPosY, 2)) / 500;
                console.log(delta);
                var zoom = self.canvas.getZoom();
                zoom = zoom + delta / 100;
                if (zoom > 10) zoom = 5;
                if (zoom < 0.25) zoom = 0.5; //self.canvas.setZoom(zoom);

                self.zoom.x = opt.pointer.x;
                self.zoom.y = opt.pointer.y;
                self.zoom.level = zoom;
                self.canvas.zoomToPoint(new fabric__WEBPACK_IMPORTED_MODULE_2__["fabric"].Point(opt.pointer.x, opt.pointer.y), zoom);
                opt.e.preventDefault();
                opt.e.stopPropagation();
              }

              if (!(opt.target && opt.target.logoId)) {
                this.isAllowCanvasDragging = true;
              } else {
                this.isAllowCanvasDragging = false;
              }
            });
            self.canvas.on("mouse:up", function (opt) {
              this.isDragging = false;
              this.isZooming = false;
              this.selection = false;
              this.getObjects().map(function (o) {
                //  console.log(o)
                return o.set("active", true);
              });
            });
            resolve();
          });
        }
      }, {
        key: "getZoom",
        value: function getZoom() {
          var self = this;
          return self.zoom;
        }
      }, {
        key: "setZoom",
        value: function setZoom(zoom) {
          var self = this;
          return self.canvas.setZoom(zoom);
        }
      }, {
        key: "resetZoom",
        value: function resetZoom(viewport, level) {
          var self = this;
          var x = 0 + 0.5 * self.container.size.w;
          var y = 0 + 0.5 * self.container.size.h;
          this.canvas.viewportTransform[4] = 0;
          this.canvas.viewportTransform[5] = 0;
          this.canvas.setViewportTransform(viewport || [1, 0, 0, 1, 0, 0]);
          this.canvas.requestRenderAll();
          this.canvas.setZoom(level || 1); //self.canvas.zoomToPoint(new fabric.Point(x, y), 1);
        }
      }, {
        key: "setProductLayout",
        value: function setProductLayout($containerLayout, $productPicture) {
          var _this30 = this;

          return new Promise(function (resolve, reject) {
            var self = _this30;
            var productPictureSrc = $productPicture;
            fabric__WEBPACK_IMPORTED_MODULE_2__["fabric"].Image.fromURL(productPictureSrc, function (oImg) {
              var productLayout = {
                w: 0,
                h: 0,
                top: 0,
                left: 0
              };
              var w0 = oImg.width;
              var h0 = oImg.height;

              if (h0 > w0) {
                productLayout.h = 0.75 * $containerLayout.size.h;
                productLayout.w = w0 / h0 * productLayout.h;
                productLayout.left = 0.5 * $containerLayout.size.w - 0.5 * productLayout.w;
                productLayout.top = 0.5 * $containerLayout.size.h - 0.5 * productLayout.h;
                oImg.scaleToHeight(productLayout.h);
              }

              if (h0 == w0) {
                productLayout.h = 0.75 * $containerLayout.size.h;
                productLayout.w = w0 / h0 * productLayout.h;
                productLayout.left = 0.5 * $containerLayout.size.w - 0.5 * productLayout.w;
                productLayout.top = 0.5 * $containerLayout.size.h - 0.5 * productLayout.h;
                oImg.scaleToHeight(productLayout.h);
              }

              if (h0 < w0) {
                productLayout.w = 0.75 * $containerLayout.size.w;
                productLayout.h = h0 / w0 * productLayout.w;
                productLayout.left = 0.5 * $containerLayout.size.w - 0.5 * productLayout.w;
                productLayout.top = 0.5 * $containerLayout.size.h - 0.5 * productLayout.h;
                oImg.scaleToWidth(productLayout.w);
              }

              oImg.set({
                top: productLayout.top,
                left: productLayout.left
              });
              self.product.position = {
                top: oImg.top,
                left: oImg.left
              };
              self.product.center = {
                x: oImg.left + 0.5 * oImg.width,
                y: oImg.top + 0.5 * oImg.height
              };
              self.product.size = {
                w: productLayout.w,
                h: productLayout.h
              };
              oImg.hoverCursor = "default";
              oImg.selectable = false, oImg.hasBorders = false;
              oImg.lockMovementY = true;
              oImg.lockMovementX = true;
              oImg.hasControls = false;
              self.canvas.add(oImg);
              resolve();
            });
          });
        }
      }, {
        key: "setAreaLayout",
        value: function setAreaLayout($positionLayout) {
          var _this31 = this;

          return new Promise(function (resolve, reject) {
            var self = _this31;
            var product = _this31.product;
            var location = {
              position: {
                top: $positionLayout.center.y - 0.5 * $positionLayout.size.h,
                left: $positionLayout.center.x - 0.5 * $positionLayout.size.w
              },
              center: {
                x: $positionLayout.center.x,
                y: $positionLayout.center.y
              },
              size: {
                w: $positionLayout.size.w,
                h: $positionLayout.size.h
              }
            };
            var base_width = 100;
            var base_height = product.size.h * (base_width / product.size.w);
            var scale = product.size.w / base_width;
            var square_top = Math.round(location.center.y - 0.5 * location.size.h);
            var square_left = Math.round(location.center.x - 0.5 * location.size.w);
            var square_width = Math.round(location.size.w);
            var square_height = Math.round(location.size.h);
            var top = square_top * scale + product.position.top;
            var left = square_left * scale + product.position.left;
            var width = square_width * scale;
            var height = square_height * scale;
            _this31.position.center = {
              x: top + 0.5 * width,
              y: left + 0.5 * height
            };
            _this31.position.position = {
              top: top,
              left: left
            };
            _this31.position.size = {
              w: width,
              h: height
            };
            var area = new fabric__WEBPACK_IMPORTED_MODULE_2__["fabric"].Rect({
              left: left,
              top: top,
              width: width,
              height: height,
              fill: "transparent",
              stroke: "red",
              strokeDashArray: [10, 2],
              strokeWidth: 1
            });
            area.hoverCursor = "default";
            area.selectable = false;
            area.hasBorders = false;
            area.lockMovementY = true;
            area.lockMovementX = true;
            area.hasControls = false;
            self.canvas.add(area);
            resolve(_this31.position);
          });
        }
      }, {
        key: "setClipAreaLayout",
        value: function setClipAreaLayout(isClipping) {
          var _this32 = this;

          return new Promise(function (resolve, reject) {
            var self = _this32;
            var position = _this32.position;
            var container = _this32.container;
            var clipPath = {};

            if (isClipping) {
              clipPath = new fabric__WEBPACK_IMPORTED_MODULE_2__["fabric"].Rect({
                left: position.position.left,
                top: position.position.top,
                width: position.size.w,
                height: position.size.h
              });
            } else {
              clipPath = new fabric__WEBPACK_IMPORTED_MODULE_2__["fabric"].Rect({
                left: container.position.left,
                top: container.position.top,
                width: container.size.w,
                height: container.size.h
              });
            }

            self.canvas.dirty = true;
            self.canvas.clipPath = clipPath;
            self.canvas.renderAll();
            resolve();
          });
        }
      }, {
        key: "addToBoard",
        value: function addToBoard(picture) {
          var inStore = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
          var self = this;
          fabric__WEBPACK_IMPORTED_MODULE_2__["fabric"].Image.fromURL(picture.src, function (oImg) {
            if (inStore) {
              var w0 = self.position.size.w;
              var h0 = self.position.size.h;
              var w = oImg.width;
              var h = oImg.height;
              var r0 = h0 / w0;
              var r = h / w;
              var image = {
                position: {
                  top: 0,
                  left: 0
                },
                size: {
                  w: 0,
                  h: 0
                }
              };
              var imageW = self.position.size.w;
              var imageH = self.position.size.w * r;
              var imageDH = imageH - self.position.size.h;
              var scale = imageDH > 0 ? self.position.size.h / imageH : 1;
              var scaleWTo = imageW * scale;
              image.size.w = scaleWTo;
              image.size.h = scaleWTo * r;
              oImg.logoWidth = image.size.w;
              oImg.logoHeight = image.size.h;
              oImg.picture = picture;
              oImg.scaleToWidth(image.size.w);
              oImg.set({
                format: "png",
                top: self.position.position.top + 1 * 0.5 * (h0 - image.size.h),
                left: self.position.position.left + 1 * 0.5 * (w0 - image.size.w)
              });
              oImg.hasControls = true;
              oImg.logoId = picture.id + "::" + new Date().getTime();
              oImg.lockUniScaling = true;
              oImg.lockScalingX = false;
              oImg.lockScalingY = false;
              oImg.lockMovementX = false;
              oImg.lockMovementY = false;
              oImg.logoTop = self.position.position.top;
              oImg.logoLeft = self.position.position.left;
              var logo = {
                id: oImg.logoId,
                top: oImg.logoTop,
                left: oImg.logoLeft,
                width: oImg.logoWidth,
                height: oImg.logoHeight,
                picture: oImg.picture
              };
              self.addLogo(logo);
              self.canvas.add(oImg);
            } else {
              var logoFromStore = this.logosBS.getValue().filter(function (logo) {
                return logo.picture == picture;
              })[0];
              oImg.logoWidth = logoFromStore.width;
              oImg.logoHeight = logoFromStore.height;
              oImg.picture = logoFromStore.picture;
              oImg.scaleToWidth(logoFromStore.width);
              oImg.set({
                format: "png",
                top: logoFromStore.top,
                left: logoFromStore.left
              });
              oImg.hasControls = true;
              oImg.logoId = logoFromStore.id;
              oImg.lockUniScaling = true;
              oImg.lockScalingX = false;
              oImg.lockScalingY = false;
              oImg.lockMovementX = false;
              oImg.lockMovementY = false;
              oImg.logoTop = logoFromStore.top;
              oImg.logoLeft = logoFromStore.left;
              self.canvas.add(oImg);
            }

            console.warn("fabric => oImg");
            console.warn(oImg);
          }.bind(this), {
            crossOrigin: "anonymous"
          });
        }
      }, {
        key: "remoteSelectedFromBoard",
        value: function remoteSelectedFromBoard() {
          var self = this;
          self.removeLogo(self.canvas.getActiveObject());
          self.canvas.remove(self.canvas.getActiveObject());
        }
      }, {
        key: "zoomIn",
        value: function zoomIn() {
          var self = this;
          var delta = 10;
          var zoom = self.canvas.getZoom();
          zoom = zoom + delta / 200;
          if (zoom > 20) zoom = 20;
          if (zoom < 0.01) zoom = 0.01;
          self.canvas.setZoom(zoom);
        }
      }, {
        key: "zoomOut",
        value: function zoomOut() {
          var self = this;
          var delta = -10;
          var zoom = self.canvas.getZoom();
          zoom = zoom + delta / 200;
          if (zoom > 20) zoom = 20;
          if (zoom < 0.01) zoom = 0.01;
          self.canvas.setZoom(zoom);
        }
      }, {
        key: "addLogo",
        value: function addLogo(logo) {
          var logos = this.logosBS.getValue();
          logos.push(logo);
          this.logosBS.next(logos);
        }
      }, {
        key: "removeLogo",
        value: function removeLogo(logo) {
          console.warn("removeLogo");
          console.warn(logo);
          var logos = this.logosBS.getValue();
          this.logosBS.next(logos.filter(function (kLogo) {
            return kLogo.id !== logo.logoId;
          }));
        }
      }, {
        key: "updateLogo",
        value: function updateLogo(logoId, logoOptions) {
          var logos = this.logosBS.getValue();
          logos.map(function (logo) {
            if (logo.id === logoId) {
              var kLogo = logo;
              kLogo.top = logoOptions.top;
              kLogo.left = logoOptions.left;
              kLogo.width = logoOptions.width;
              kLogo.height = logoOptions.height;
              return kLogo;
            }
          });
          this.logosBS.next(logos);
        }
      }, {
        key: "syncLogos",
        value: function syncLogos() {
          return this.logosObs;
        }
      }, {
        key: "setIsDrawingMode",
        value: function setIsDrawingMode(isDrawing) {
          this.canvas.isDrawingMode = false;
        }
      }, {
        key: "getCanvas",
        value: function getCanvas() {
          return this.canvas;
        }
      }, {
        key: "getClipArea",
        value: function getClipArea() {
          var position0 = this.position;
          var position = this.position;
          var diffTop = 0;
          var diffLeft = 0;
          var diffScale = 1;
          position.size.w = position0.size.w * (1 / diffScale);
          position.size.h = position0.size.h * (1 / diffScale);
          position.position.top = position0.position.top - diffTop;
          position.position.left = position0.position.left - diffLeft;
          return position;
        }
      }]);

      return FabricHandlerService;
    }();

    FabricHandlerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], FabricHandlerService);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/services/libs-handlers-services/moveable-js/moveable-handler.service.ts":
  /*!***************************************************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/services/libs-handlers-services/moveable-js/moveable-handler.service.ts ***!
    \***************************************************************************************************************************/

  /*! exports provided: MoveableHandlerService */

  /***/
  function srcAppSharedProductLogoCustomizationServicesLibsHandlersServicesMoveableJsMoveableHandlerServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MoveableHandlerService", function () {
      return MoveableHandlerService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var MoveableHandlerService =
    /*#__PURE__*/
    function () {
      function MoveableHandlerService() {
        _classCallCheck(this, MoveableHandlerService);

        this.isClippingBS = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](false);
        this.$isClipping = this.isClippingBS.asObservable();
      }

      _createClass(MoveableHandlerService, [{
        key: "getClippingState",
        value: function getClippingState() {
          return this.isClippingBS.getValue();
        }
      }, {
        key: "hideControllers",
        value: function hideControllers() {
          var controllers = document.querySelectorAll('.moveable-control');
          var lines = document.querySelectorAll('.moveable-line');
          controllers.forEach(function (controller) {
            return controller.style.display = 'none';
          });
          lines.forEach(function (line) {
            return line.style.display = 'none';
          });
          this.isClippingBS.next(true);
        }
      }, {
        key: "showControllers",
        value: function showControllers() {
          var controllers = document.querySelectorAll('.moveable-control');
          var lines = document.querySelectorAll('.moveable-line');
          controllers.forEach(function (controller) {
            return controller.style.display = 'block';
          });
          lines.forEach(function (line) {
            return line.style.display = 'block';
          });
          this.isClippingBS.next(false);
        }
      }, {
        key: "syncClipping",
        value: function syncClipping() {
          return this.$isClipping;
        }
      }]);

      return MoveableHandlerService;
    }();

    MoveableHandlerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], MoveableHandlerService);
    /***/
  },

  /***/
  "./src/app/shared/product-logo-customization/services/settings/settings.service.ts":
  /*!*****************************************************************************************!*\
    !*** ./src/app/shared/product-logo-customization/services/settings/settings.service.ts ***!
    \*****************************************************************************************/

  /*! exports provided: SettingsService */

  /***/
  function srcAppSharedProductLogoCustomizationServicesSettingsSettingsServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SettingsService", function () {
      return SettingsService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var SettingsService = function SettingsService() {
      _classCallCheck(this, SettingsService);
    };

    SettingsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], SettingsService);
    /***/
  },

  /***/
  "./src/app/shared/shared.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/shared/shared.module.ts ***!
    \*****************************************/

  /*! exports provided: SharedModule */

  /***/
  function srcAppSharedSharedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
      return SharedModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _product_logo_customization_product_logo_customization_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./product-logo-customization/product-logo-customization.module */
    "./src/app/shared/product-logo-customization/product-logo-customization.module.ts");

    var SharedModule = function SharedModule() {
      _classCallCheck(this, SharedModule);
    };

    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _product_logo_customization_product_logo_customization_module__WEBPACK_IMPORTED_MODULE_3__["ProductLogoCustomizationModule"]],
      exports: [_product_logo_customization_product_logo_customization_module__WEBPACK_IMPORTED_MODULE_3__["ProductLogoCustomizationModule"]]
    })], SharedModule);
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js"); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"]).catch(function (err) {
      return console.error(err);
    });
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! C:\Users\walla\Documents\GitHub\Vurbis\ngx-virgus-product-logo-customization\src\main.ts */
    "./src/main.ts");
    /***/
  },

  /***/
  1:
  /*!***********************!*\
    !*** jsdom (ignored) ***!
    \***********************/

  /*! no static exports found */

  /***/
  function _(module, exports) {
    /* (ignored) */

    /***/
  },

  /***/
  2:
  /*!********************************************************!*\
    !*** jsdom/lib/jsdom/living/generated/utils (ignored) ***!
    \********************************************************/

  /*! no static exports found */

  /***/
  function _(module, exports) {
    /* (ignored) */

    /***/
  },

  /***/
  3:
  /*!***************************************!*\
    !*** jsdom/lib/jsdom/utils (ignored) ***!
    \***************************************/

  /*! no static exports found */

  /***/
  function _(module, exports) {
    /* (ignored) */

    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map